<?php
/* ############################################################ *\
 ----------------------------------------------------------------
@package	Jcow Social Networking Script.
@copyright	Copyright (C) 2009 - 2010 jcow.net.  All Rights Reserved.
@license	see http://jcow.net/license
 ----------------------------------------------------------------
\* ############################################################ */

function customprofile_profile_page($profile) {
	$custom = unserialize($profile['page']['custom_css']);
	if ($custom['musicplayer']) {
		$res = sql_query("select s.id from ".tb()."liked as l left join ".tb()."streams as s on s.id=l.stream_id where l.uid='{$profile['id']}' and s.app='music' order by l.id DESC limit 10");
		while ($row = sql_fetch_array($res)) {
			$sids[] = $row['id'];
		}
		if (is_array($sids)) {
			$mids = implode(',',$sids);
			$res = sql_query("select id,title,var1,var2 from ".tb()."stories where stream_id in ($mids)");
			$mi = 1;
			while ($row = sql_fetch_array($res)) {
				$music_ids[] = $row['id'];
				$titles[] = h(addslashes($row['title']));
				if (!$row['var2']) $row['var2'] = $profile['username'];
				$artists[] = h(addslashes($row['var2']));
				$soundfiles[] = $row['var1'];
				$mplist .= $mi.'. '.url('music/viewstory/'.$row['id'],h($row['title'])).'<br />';
				$mi++;
			}
			$umids = implode('_',$music_ids);
			$encoded_uhome = urlencode(uhome().'/');
			if (is_array($soundfiles)) {
				$soundfiles = implode(',',$soundfiles);
				$artists = implode(',',$artists);
				$titles = implode(',',$titles);
				section(array('title'=>t('Liked music'),'content'=>
					'<p id="audioplayer_1">Mp3 Player</p>
				<script type="text/javascript" src="'.uhome().'/js/player/audio-player.js"></script>  
		   
			<script type="text/javascript"> 
	$(document).ready(function(){
		AudioPlayer.setup("'.uhome().'/js/player/player.swf", {  
			width: 500  
		});  
		AudioPlayer.embed("audioplayer_1", {  
		soundFile: "'.$soundfiles.'",  
		titles: "'.$titles.'",  
		artists: "'.$artists.'",  
		autostart: "yes",
		loop: "yes"
		}); 
			});
			</script> <br />'.$mplist
				));
			}
		}
	}
}