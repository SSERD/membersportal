<?php
global $client;
if (!preg_match("/^[0-9a-z]+$/i",$username)) die('wrong username');
$res = sql_query("select * from ".tb()."accounts where username='$username'");
$user = sql_fetch_array($res);
if (!$user['id']) die('wrong uid');

set_title(h($user['username']));
if (!$user['avatar']) {
	$user['avatar'] = 'undefined.jpg';
}
$user['avatar'] = 's_'.$user['avatar'];
c('<table border="0"><tr><td>
<a href="'.url('mobile/u/'.$user['username']).'"><img  src="'.uhome().'/'.uploads.'/avatars/'.$user['avatar'].'" class="avatar" /></a>
</td>
<td>
'.h($user['username']).'<br />
'.t('Gender').': '.gender($user['gender']).'<br />
'.t('Age').': '.get_age($user['birthyear'],$user['hide_age']).'
</td>
</tr>
</table>');
$res = sql_query("select * from ".tb()."pages where uid='{$user['id']}' and type='u'");
$page = sql_fetch_array($res);
if ($client['id']) {
	$res = sql_query("select * from ".tb()."friends where uid='{$client['id']}' and fid='{$user['id']}' limit 1");
	$row = sql_fetch_array($res);
	if ($row['uid']) {
		$user['is_friend'] = 1;
	}
}

if (!$user['is_friend']) {
	c(url('mobile/friends/add/'.$user['id'],t('Add as friend')).' | ');
}
c(url('mobile/message/compose/u'.$user['id'],t('Message')));

section_close(h($user['username']));




if ($user['id'] != $client['id']) {
	if ($user['profile_permission'] == 2 && !$user['is_friend']) {
			c(t('Sorry, only friends can view this profile'));
			section_close();
			stop_here();
	}
	elseif ($user['profile_permission'] == 1 && !$user['is_friend']) {
		$user['no_comment'] = 1;
	}
	if ($client['id']) {
		$res = sql_query("select * from `".tb()."blacks` where bid='{$client['id']}' and uid='{$user['id']}' ");
		if (sql_counts($res)) {
			$user['no_comment'] = 1;
		}
	}
}

if (!$user['no_comment']) {
	if ($_POST['act'] == 'post') {
			$url_search = array(            
			"/\[url]www.([^'\"]*)\[\/url]/iU",
			"/\[url]([^'\"]*)\[\/url]/iU",
			"/\[url=www.([^'\"\s]*)](.*)\[\/url]/iU",
			"/\[url=([^'\"\s]*)](.*)\[\/url]/iU",
		);
		$url_replace = array(
				"<a href=\"http://www.\\1\" target=\"_blank\" rel=\"nofollow\">www.\\1</a>",
				"<a href=\"\\1\" target=\"_blank\" rel=\"nofollow\">\\1</a>",
				"<a href=\"http://www.\\1\" target=\"_blank\" rel=\"nofollow\">\\2</a>",
				"<a href=\"\\1\" target=\"_blank\" rel=\"nofollow\">\\2</a>"
				);
		$stream_id = stream_publish(preg_replace($url_search,$url_replace, h($_POST['message']) ),'','',$client['id'],$_POST['page_id']);
		sys_notice(t('Posted'));
	}
	c('<form action="'.url('mobile/u/'.$user['username']).'" method="post">
	'.t('Write something..').'<br />
	<textarea name="message" maxlegth="140" style="width:200px;height:35px"></textarea>
	<input type="hidden" name="act" value="post" />
	<input type="hidden" name="page_id" value="'.$page['id'].'" />
	<input type="submit" value="'.t('Share').'" />
	</form>');
}

$mpage = $_POST['mpage'];
if (!$mpage) $mpage = 1;
$offset = 15*($mpage-1);
$res = sql_query("select s.*,u.username,u.avatar,p.uid as wall_uid from ".tb()."streams as s left join ".tb()."accounts as u on u.id=s.uid left join ".tb()."pages as p on p.id=s.wall_id where s.wall_id='{$page['id']}' and s.hide!=1 order by id desc limit $offset,15");

$acts = '';
$i=0;
while($row = sql_fetch_array($res)) {
	$i++;
	c(stream_display($row,'mobile',0,$page['id']));
}
if ($i == 15) {
	$next = $mpage+1;
	c('
	<form action="'.url('mobile/u/'.$user['username']).'" method="post" />
	<input type="hidden" name="filter" value="'.$filter.'" />
	<input type="hidden" name="mpage" value="'.$next.'" />
	<input type="submit" value="More.." />
	</form>
	');
}
section_close(t('Wall'));