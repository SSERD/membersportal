<?php
/* ############################################################ *\
 ----------------------------------------------------------------
Jcow Software (http://www.jcow.net)
IS NOT FREE SOFTWARE
http://www.jcow.net/commercial_license
Copyright (C) 2009 - 2010 jcow.net.  All Rights Reserved.
 ----------------------------------------------------------------
\* ############################################################ */
global $client,$offset,$num_per_page, $page;
if (!$act) {
	$uid = $client['id'];
	//ass(friends_box());
	$current_sub_menu['href'] = 'friends';

	$res = sql_query("SELECT u.* FROM `".tb()."friends` as f left join `".tb()."accounts` as u on u.id=f.fid where f.uid={$uid}  ORDER BY u.lastlogin DESC LIMIT $offset,$num_per_page");
	c('<ul class="common">');
	while ($row = sql_fetch_array($res)) {
		c('<li><table border="0"><tr><td>');
		c(mobile_avatar($row));
		c('</td><td>'.url('mobile/u/'.$row['username'], h($row['username'])).'<br />'.
			url('mobile/friends/delete/'.$row['id'],t('Remove')));
		c('</td></tr></table></li>');
	}
	c('</ul>');

	// pager
	$res = sql_query("select count(*) as total from `".tb()."friends` where uid='{$uid}'");
	$row = sql_fetch_array($res);
	$total = $row['total'];
	$pb       = new PageBar($total, $num_per_page, $page);
	$pb->paras = url('mobile/friends');
	$pagebar  = $pb->whole_num_bar();
	c($pagebar);

	section_close(t('My friends'));
}

elseif ($act == 'requests') {
	$res = sql_query("select count(*) as num from ".tb()."friend_reqs where uid='{$client['id']}' ");
	$row = sql_fetch_array($res);
	c(t('You have {1} pending requests','<strong>'.$row['num'].'</strong>'));
	section_close(t('To others'));


	$res = sql_query("select u.*,r.created as timeline,r.msg from `".tb()."friend_reqs` as r 
								left join `".tb()."accounts` as u on u.id=r.uid
								where r.fid={$client['id']} limit 50");
	if (sql_counts($res)) {
		c( '<ul class="common">');
		while($row = sql_fetch_array($res)) {
			if (strlen($row['username'])) {
				c('
				<table border="0"><tr><td>'.mobile_avatar($row).'</td><td>
			'.url('mobile/u/'.$row['username'],$row['username']).'<br />
			'.htmlspecialchars($row['msg']).'<br />
			'.url('mobile/friends/approve/'.$row['id'],t('Approve')).'<br />
			'.url('mobile/friends/deletes/'.$row['id'],t('Reject')).'</td></tr>
			</table>
			</li>');
			}
		}
		c('</ul>');
	}
	else {
		c('Currently no friend requests');
	}
	section_close(t('To you'));
}
elseif ($act == 'delete') {
		sql_query("delete from `".tb()."friends` where uid={$client['id']} and fid='$id' ");
		sql_query("delete from `".tb()."friends` where uid='$id' and fid={$client['id']} ");
		redirect('mobile/friends');
}
elseif ($act == 'approve') {
	if (!$user = valid_user($id)) die('wrong uid');
	// 
	$res = sql_query("select * from `".tb()."friend_reqs` where uid='$id' and fid={$client['id']} ");
	if (sql_counts($res)) {
		$res = sql_query("select * from `".tb()."friends` where uid='$id' and fid={$client['id']} ");
		// 
		if (!sql_counts($res)) {
			sql_query("insert into `".tb()."friends` (uid,fid,created) values ($id,{$client['id']},".time().")");
			sql_query("insert into `".tb()."friends` (uid,fid,created) values ({$client['id']},$id,".time().")");
		}
		// 
		sql_query("delete from `".tb()."friend_reqs` where uid=$id and fid={$client['id']} ");
		sql_query("delete from `".tb()."friend_reqs` where uid={$client['id']} and fid=$id ");
		// 
		stream_publish(
			t(
				'became a friend of {1}',
				url('u/'.$user['username'],h($user['username']),'','',1)
			)
		);
		mail_notice('dismail_friend_request_c',$user['username'],t('{1} confirmed your friend request',$client['username']),t('{1} confirmed your friend request',$client['fullname']) );
	}
	redirect('mobile/friends', 1);
}

elseif ($act == 'deletes') {
	$res = sql_query("select * from `".tb()."friend_reqs` where uid='$id' and fid={$client['id']} ");
	if (sql_counts($res)) {
		// ɾ������
		sql_query("delete from `".tb()."friend_reqs` where uid=$id and fid={$client['id']} ");
		sql_query("delete from `".tb()."friend_reqs` where uid={$client['id']} and fid=$id ");
	}
	// �ɹ�
	redirect('mobile/friends', 1);
}

elseif ($act == 'add') {
	if ($id) {
		$res = sql_query("select id,username from `".tb()."accounts` where id='$id' ");
		$user = sql_fetch_array($res);
	}
	$res = sql_query("select * from `".tb()."friends` where uid={$client['id']} and fid={$user['id']} ");
	if (sql_counts($res)) {
		c('This user is already in your friend listings');
	}
	else {
		c('<p>'.t('Adding {1} {2} as friend',$user['username']).'</p>');
		$res = sql_query("select * from `".tb()."blacks` where bid={$client['id']} and uid={$user['id']} ");
		if (sql_counts($res)) {
			c(t('This user has blocked you'));
		}
		else {
			c('
					<form method="post" name="form1" action="'.url('mobile/friends/addpost').'"  enctype="multipart/form-data">
					<p>
					'.label(t('Request message')).'
					<input type="text" name="msg" size="25" /><br />
					<span>'.t('Say something to help your request be accepted').'</span>
					</p>
					<p>
					<input type="hidden" name="uid" value="'.$id.'" />
					<input class="button" type="submit" value="'.t('Send request').'" />
					</p>
					</form>
					');
		}
	}
	section_close();
}

elseif ($act == 'addpost') {
	if(!$user = valid_user($_POST['uid'])) {
		die('wrong uid');
	}
	if ($user['id'] == $client['id']) {
		c('you cannot add yourself');
		section_close();
	}
	else {
		$res = sql_query("select * from `".tb()."friends` where uid={$client['id']} and fid={$user['id']} ");
		if (sql_counts($res)) {
			die('This user is already in your friend listings');
		}
		// do add
		$res = sql_query("select * from `".tb()."friend_reqs` where uid={$client['id']} and fid={$user['id']} ");
		if (!sql_counts($res)) {
			sql_query("insert into `".tb()."friend_reqs` (uid,fid,created,msg) values ({$client['id']},{$user['id']},".time().",'{$_POST['msg']}')");
		}
		else {
			sql_query("update `".tb()."friend_reqs` set created=".time().",msg='{$_POST['msg']}' where uid={$client['id']} and fid={$user['id']} ");
		}
		mail_notice('friend_request',$user['username'],t('{1} wants to be friends with you',$client['username']),t('{1} wants to be friends with you',$client['fullname']) );
		redirect(url('mobile/friends'),1);
	}
}
	