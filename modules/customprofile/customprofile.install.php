<?php
/* ############################################################ *\
 ----------------------------------------------------------------
@package	Jcow Social Networking Script.
@copyright	Copyright (C) 2009 - 2010 jcow.net.  All Rights Reserved.
@license	see http://jcow.net/license
 ----------------------------------------------------------------
\* ############################################################ */

function customprofile_menu() {
	$items = array();
	$items['customprofile'] = array(
		'name'=>'Custom theme',
		'type'=>'tab',
		'parent'=>'account',
		'protected'=>1
	);
	return $items;
}


?>