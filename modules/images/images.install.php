<?php
/* ############################################################ *\
 ----------------------------------------------------------------
@package	Jcow Social Networking Script.
@copyright	Copyright (C) 2009 - 2010 jcow.net.  All Rights Reserved.
@license	see http://jcow.net/license
 ----------------------------------------------------------------
\* ############################################################ */

function images_menu() {
	$items = array();
	$items['images'] = array(
		'name'=>'Images',
		'tab_name'=>'Hot',
		'type'=>'community'
	);
	$items['images/newest'] = array(
		'name'=>'New',
		'type'=>'tab',
		'parent'=>'images'
	);
	$items['images/following'] = array(
		'name'=>'In my network',
		'type'=>'tab',
		'parent'=>'images'
	);
	$items['images/mine'] = array(
		'name'=>'My images',
		'type'=>'personal',
		'parent'=>'jcow_base'
	);
	
	return $items;
}

?>