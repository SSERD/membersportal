<?php
/* ############################################################ *\
 ----------------------------------------------------------------
Jcow Software (http://www.jcow.net)
IS NOT FREE SOFTWARE
http://www.jcow.net/commercial_license
Copyright (C) 2009 - 2010 jcow.net.  All Rights Reserved.
 ----------------------------------------------------------------
\* ############################################################ */

function jcow_page_feed($target_id, $args = array(), $uid = 0, $hide = 0,$cwall_id='',$data=array()) {
	global $client, $parr;
	if (!$client['id'] && !$uid) return false;
	if (!$uid) $uid = $client['id'];
	$thumbs = explode(',',$args['picture']);
	if (count($thumbs) > 3) {
		$thumbs = array($thumbs[0],$thumbs[1],$thumbs[2]);
	}
	$attachment = array(
				'cwall_id' => $cwall_id,
				'uri' => $args['link'],
				'name' => substr($args['name'],0,100),
				'thumb' => $thumbs
				);
	if (!preg_match("/^[0-9a-z_]+$/i",$args['app'])) $args['app'] = $parr[0];
	$app = array('name'=>$args['app'],'id'=>$args['aid']);
	$stream_id = stream_publish($args['message'],$attachment,$app, $uid,$target_id,$hide,$data);
	return $stream_id;
}




class jcow_pages {
	var $profile = array();
	var $extra = array();
	var $type = '';
	function u() {
		global $client, $nav, $ss;
		/*
		if (!$client['id'] && get_gvar('profile_access') == 'member' && !$ss['is_bot']) {
			redirect('member/login/1');
		}
		*/
	}

	
	function status($url = 0, $id = 0) {
		global $client, $apps, $uhome,$ubase, $current_sub_menu, $offset, $num_per_page, $page;
		$profile = $this->settabmenu($url, 1,'u');
		$current_sub_menu['href'] = 'u/'.$url.'/friends';
		

		$res = sql_query("select * from ".tb()."streams where id='$id' and uid='{$profile['id']}'");
		$row = sql_fetch_array($res);
		if ($row['id']) {
			$row['attachment'] = unserialize($row['attachment']);
			$row['username'] = $profile['username'];
			$row['avatar'] = $profile['avatar'];
			section(
			array(
			'content'=>stream_display($row,'simple'))
				);
		}

		section(
				array('title'=>t('Comments'),
				'content'=>
				comment_form($row['id']).
				comment_get($row['id'],100)
				
				)
		);
	}

	
	
	function index($url = 0) {
		global $client, $content, $nav, $apps, $uhome, $blocks,$sections, $ubase, $offset, $num_per_page, $page, $tab_menu,$current_sub_menu, $config, $menuon;
		enable_jcow_inline_ad();
		if (!strlen($url) || $url == 'index') {
			die('wrong url:'.$url);
		}
		$owner = $this->settabmenu($url,0,$this->type);
		$page = $owner['page'];

		enreport();
		if ($owner['id'] == $client['id']) {
			$menuon = 'myprofile';
		}
		$current_sub_menu['href'] = 'u/'.$url;
		// update views
		$key = 'vp'.$owner['page']['id'];
		if (!$_COOKIE[$key]) {
			sql_query("update `".tb()."pages` SET views=views+1 WHERE id='{$owner['page']['id']}' ");
			setcookie($key,1, time()+3600*12,"/");
		}
		//visitors
		/*
		if ($client['id'] && $client['id'] != $owner['id'] && 
			($page['type'] == 'u' || $page['type'] == 'page')
			) {
			$res = sql_query("select * from ".tb()."page_visitors where pid='{$page['id']}' and uid='{$client['id']}'");
			if (sql_counts($res)) {
				sql_query("update ".tb()."page_visitors set updated='".time()."' where pid='{$page['id']}' and uid='{$client['id']}'");
			}
			else {
				sql_query("insert into ".tb()."page_visitors(pid,uid,updated) values('{$page['id']}','{$client['id']}','".time()."')");
			}
		}
		*/
		if (!$page['no_comment']) {
			c(stream_form($owner['id'],$owner['page']));
			section_close();
		}
		stream_get($owner['page']['id'],10,0,$page['id'],'',1);

		
		//section(array('content'=>$output) );
		
		$hooks = check_hooks('profile_page');
		if ($hooks) {
			foreach ($hooks as $hook) {
				$hook_func = $hook.'_profile_page';
				$hook_func($owner);
			}
		}

		
	
	}



	function settabmenu($url, $hide_as=0, $page_type='') {
		global $nav, $apps, $uhome, $client, $styles, $custom_css,$profile_css, $optional_apps, $config, $tab_menu,$parr,$defined_tab_parent,$parr;
		$nav = array();
		$config['is_profile_page'] = 1;
		if (!preg_match("/[0-9a-z]+/i",$url)) {
			die('wrong url');
		}
		if ($page_type) {
			$res = sql_query("select * from `".tb()."pages` where uri='$url' and type='{$page_type}'");
		}
		else {
			$res = sql_query("select * from `".tb()."pages` where id='$url'");
		}
		$page = sql_fetch_array($res);
		if (!$page['id']) die('wrong uri');

		$res = sql_query("SELECT * from `".tb()."accounts` where id='{$page['uid']}' ");
		$owner = sql_fetch_array($res);
		if (!$owner['id']) {
			sys_break('can not find the owner');
		}
		if ($client['id']) {
			$res = sql_query("select * from ".tb()."friends where uid='{$client['id']}' and fid='{$owner['id']}' limit 1");
			$row = sql_fetch_array($res);
			if ($row['uid']) {
				$owner['is_friend'] = 1;
			}
		}
		/*
		app_header('<style>
			#jcow_side {
	float:left;
}
#jcow_content {
	overflow: hidden;
}
.content_general {
	float: right;	
}
</style>');
*/
		if ($page['type'] == 'u') {
			$tab_menu = u::tab_menu($owner,$page);
		}
		elseif ($page['type'] == 'page') {
			$tab_menu = page::tab_menu($owner,$page);
		}
		elseif ($page['type'] == 'group') {
			$tab_menu = group::tab_menu($owner,$page);
		}
		if ($page['type'] == 'u') {
			$hooks = check_hooks('u_menu');
			if ($hooks) {
				foreach ($hooks as $hook) {
					$hook_func = $hook.'_u_menu';
					$hook_func($tab_menu,$page);
				}
			}
		}
		elseif ($page['type'] == 'page') {
			$hooks = check_hooks('page_menu');
			if ($hooks) {
				foreach ($hooks as $hook) {
					$hook_func = $hook.'_page_menu';
					$hook_func($tab_menu,$page);
				}
			}
		}
		elseif ($page['type'] == 'group') {
			$hooks = check_hooks('group_menu');
			if ($hooks) {
				foreach ($hooks as $hook) {
					$hook_func = $hook.'_group_menu';
					$hook_func($tab_menu,$page);
				}
			}
		}
		if (!$hide_as) {
			$this->show_sidebar($page,$owner);
		}
		$profile_css = unserialize($page['custom_css']);
		if ($owner['id'] == $client['id']) {
			$output = '
			<div id="jcow_cover_edit"><a href="'.url('customprofile/'.$page['id']).'" class="btn btn-primary"><i class="fa fa-photo"></i> '.t('Change Cover').'</a></div>';
		}
		$page_menu = page_tab_menu();
		$tab_menu = array();
		if ($page['type'] == 'u') {
			if (!$profile_css['cover_image']) {
				$cover_bg_class = 'gbox_'.strlen($owner['fullname'])%7;
			}
			if ($client['id']) {
				if ($page['uid'] != $client['id']) {
					$res = sql_query("select * from ".tb()."followers where uid='{$client['id']}' and fid='{$page['uid']}' limit 1");
					if (!sql_counts($res)) {
						$follow_btn = '<span><a class="dofollow btn btn-default" href="#" page_id="'.$page['id'].'"><i class="fa fa-rss"></i>  '.t('Follow').'</a></span>';
					}
					else {
						$follow_btn = '<span><a class="dofollow btn btn-default" href="#" page_id="'.$page['id'].'"> '.t('Un-follow').'</a></span>';
					}
				}
				
			}
			else {
				$follow_btn = '<a class=" btn btn-default" href="'.url('member/login/1').'" page_id="'.$page['id'].'"><i class="fa fa-rss"></i>  '.t('Follow').'</a>';
			}
			set_title($owner['username']."'s profile");
			if ($owner['disabled'] == 3) {
				c('This user('.$owner['username'].') has been marked as Spammer');
				if (allow_access(3)) {
					c('<p>'.url('admin/useredit/'.$owner['id'],'Manage').'</p>');
				}
				stop_here();
			}
			$defined_tab_parent = $owner['fullname'];
			if ($owner['lastlogin'] > (time()-300))
				$owner['user_online'] = '<img src="'.uhome().'/files/icons/online.gif" />';
			else
				$owner['user_online'] = '<img src="'.uhome().'/files/icons/offline.gif" />';

			$add_friend = add_friend_btn($owner['id'],'nm');
			if ($client['id'] && $client['id'] != $owner['id'] && jedition != 'CE') {
				$message_btn = '<a href="javascript:void();" class="chatbox_btn btn btn-default" uid="'.$owner['id'].'" ctitle="'.str_replace('"','&quot;',avatar($owner,'small',array('nolink'=>1)).' '.h($owner['fullname'])).'"><i class="fa fa-send"></i> '.t('Message').'</a>';
			}
		$page_buttons = $follow_link.'
			<li>'.$ublock.'</li>';
			if ($client['id']) {
				$res = sql_query("select * from `".tb()."blacks` where uid={$client['id']} and bid='{$owner['id']}' ");
				if ($client['id'] != $owner['id']) {
					if (sql_counts($res)) {
						$block_link .= '<a href="'.url('blacklist/remove/'.$owner['id']).'" class="jcow_btn">'.t('Unblock').'</a>';
					}
					else {
						$block_link .= '<a href="'.url('blacklist/add/'.$owner['id']).'" class="jcow_btn"><i class="fa fa-minus-circle"></i> '.t('Block').'</a>';
					}
					$page_buttons .= '<li>'.$block_link.'</li>';
				}
			}
			if (allow_access(3)) {
				$page_buttons .= '<li>'.url('admin/useredit/'.$owner['id'],'Manage this User').'</li>';
				$page_buttons .= '<li><a href="#" class="deleteuser" uid="'.$owner['id'].'">Delete this User</a></li>';
			}
			$page_buttons .= '
			<li>'.url('report',t('Report'),'',array('uid'=>$owner['id'])).'</li>
			';

			$logo_link = url('u/'.$owner['username']);
			if (is_mobile()) {
				$output = '<div>'.avatar($owner,'large',array('nolink'=>1)).'</div>'.$add_friend.' '.$follow_btn.' '.$message_btn.' '.$block_link;
				if ($client['id'] == $owner['id']) {
					$output .= '<a href="'.url('account/avatar').'"><i class="fa fa-camera fa-3x"></i></a>';
				}

			}
			else {
				$output .= '
				<div id="jcow_cover_avatar_box">
					<div id="jcow_cover_avatar"><a href="'.$logo_link.'">'.avatar($owner,'large',array('nolink'=>1)).'</a>
					</div>
				</div>
				<div class="jcow_cover_content '.$cover_bg_class.'">
					<div id="jcow_cover_name">'.h($owner['fullname']).' '.$owner['user_online'].'
					'.url('u/'.$owner['username'],'@'.$owner['username']).'
					</div>
					<div id="jcow_cover_follow">
					'.$add_friend.' '.$follow_btn.' '.$message_btn.'
						<div class="btn-group">
						  <button id="dLabel" class="btn btn-default" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    <i class="fa fa-ellipsis-h"></i>
						  </button>
						  <ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
						    '.$page_buttons.'
						  </ul>
						</div>
					</div>
				</div>
				<div id="jcow_page_menu">'.$page_menu.'</div>';
			}
			app_header('<div id="jcow_cover">'.$output.'</div>');
			//section(array('content'=>$output,'custom_class'=>'profile_head') );
			$output = '';

			// friends
			$res = sql_query("select u.* from ".tb()."friends as f left join ".tb()."accounts as u on u.id=f.fid where f.uid='{$owner['id']}' order by rand() limit 10");
			if (sql_counts($res)) {
				$i=0;
				while($member = sql_fetch_array($res)) {
					if ($member['id'] && $member['id'] != $owner['id'] && $i<5) {
						$friendlists .= avatar($member);
						$i++;
					}
				}
				ass(array('title'=>t('Friends'),'content'=>$friendlists));
			}
			if ($parr[0] == 'u') {
				ass($this->details($owner));
			}
			// liked pages
			$res = sql_query("select p.* from ".tb()."page_users as pu left join ".tb()."pages as p on p.id=pu.pid where pu.uid='{$owner['id']}' and p.type='page' order by rand() limit 4");
			if (sql_counts($res)) {
				while($fanpage = sql_fetch_array($res)) {
					if ($fanpage['logo']) {
						$fanpage['logo'] = uhome().'/'.uploads.'/avatars/s_'.$fanpage['logo'];
					}
					if ($fanpage['id']) {
						$fanpages .= listing_box(
						array('name'=>h($fanpage['name']),'img'=>$fanpage['logo'],'link'=>url('page/'.$fanpage['uri'])

						));
					}
				}
				ass(array('title'=>t('Liked pages'),'content'=>$fanpages));
			}

			// groups
			$res = sql_query("select p.* from ".tb()."page_users as pu left join ".tb()."pages as p on p.id=pu.pid where pu.uid='{$owner['id']}' and p.type='group' order by rand() limit 4");
			if (sql_counts($res)) {
				while($ugroup = sql_fetch_array($res)) {
					if ($ugroup['logo']) {
						$ugroup['logo'] = uhome().'/'.uploads.'/avatars/s_'.$ugroup['logo'];
					}
					if ($ugroup['id']) {
						$ugroups .= listing_box(
						array('name'=>h($ugroup['name']),'img'=>$ugroup['logo'],'link'=>url('group/'.$ugroup['uri'])

						));
					}
				}
				ass(array('title'=>t('Groups'),'content'=>$ugroups));
			}
			
			if ($parr[0] == 'u') {
				if($parr[1] == 'index') {
					$current_path = $parr[0].'/'.$parr[2];
				}
				else {
					$current_path = $parr[0].'/'.$parr[2].'/'.$parr[1];
				}
			}
			else {
				$current_path = $parr[0].'/'.$parr[1].'/'.$parr[2];
			}
			if ($page['description'] == 'private') {
				if ($owner['id'] != $client['id'] && !$owner['is_friend']) {
					c(t('This profile page is friends only'));
					stop_here();
				}
			}
		}
		elseif ($page['type'] == 'page') {
			if (!$profile_css['cover_image']) {
				$cover_bg_class = 'gbox_'.strlen($page['name'])%7;
			}
			if ($client['id']) {
				$res = sql_query("select * from ".tb()."page_users where uid='{$client['id']}' and pid='{$page['id']}'");
				if (sql_counts($res)) {
					$like_link = '<span><a class="btn btn-success  dofollow" href="#" page_id="'.$page['id'].'"><i class="fa fa-thumbs-up"></i> '.t('Liked').'</a></span>';
				}
				else {
					$like_link = '<span><a class="btn btn-default  dofollow" href="#" page_id="'.$page['id'].'"><i class="fa fa-thumbs-up"></i>  '.t('Like').'</a></span>';
				}
			}
			else {
				$like_link = '<span><a class="btn btn-success" href="'.url('member/login/1').'" page_id="'.$page['id'].'"><i class="fa fa-plus"></i> '.t('Like').' <span class="badge">'.$page['users'].'</span></a></span>';
			}
			$defined_tab_parent = $page['name'];
			if ($page['uid'] == $client['id']) {
				$page_buttons .= '
				<li>'.url('pages/manage/'.$page['id'],t('Edit page')).'</li>';
				$page_buttons .= '<li>'.url('pages/logo/'.$page['id'],t('Edit page Logo')).'</li>';
			}
			if (allow_access(3)) {
				$page_buttons .= '<li>'.url('admin/useredit/'.$owner['id'],'Modify page owner').'</li>';
				$page_buttons .= '<li><a href="#" class="deleteuser" uid="'.$owner['id'].'">Delete page owner</a></li>';
			}
			$page_buttons .= '
			<li>'.url('report',t('Report'),'',array('pid'=>$page['id'])).'</li>';

			if (is_mobile()) {
				$output = '<div>'.page_logo($page,'large',array('link'=>url('page/'.$page['uri']))).'</div>'.$like_link;
			}
			else {
				$output .= '
				<div id="jcow_cover_avatar_box">
					<div id="jcow_cover_avatar"><a href="'.$logo_link.'">'.page_logo($page,'large',array('link'=>url('page/'.$page['uri']))).'</a>
					</div>
				</div>
				<div class="jcow_cover_content '.$cover_bg_class.'">
					<div id="jcow_cover_name">'.h($page['name']).'
					</div>
					<div id="jcow_cover_follow">
					'.$like_link.'
						<div class="btn-group">
						  <button id="dLabel" class="btn btn-default" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    <i class="fa fa-ellipsis-h"></i>
						  </button>
						  <ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
						    '.$page_buttons.'
						  </ul>
						</div>
					</div>
				</div>
				<div id="jcow_page_menu">'.$page_menu.'</div>';
			}
			app_header('<div id="jcow_cover">'.$output.'</div>');

			$output = '';
			set_title(h($page['name']));
		}
		elseif ($page['type'] == 'group') {
			if (!$profile_css['cover_image']) {
				$cover_bg_class = 'gbox_'.strlen($page['name'])%7;
			}
			$gp_key = 'gp_'.$page['id'];
			$page['privacy'] = get_text($gp_key);
			if (!$page['var3']%2) {
				$gicon = '<i class="fa fa-globe"></i>';
			}
			else {
				$gicon = '<i class="fa fa-lock"></i>';
			}
			$res = sql_query("select * from ".tb()."page_users where uid='{$client['id']}' and pid='{$page['id']}'");
			if (sql_counts($res)) {
				$is_member = 1;
			}
			if (!$is_member) {
				if ($client['id']) {
					$join_button = '<a class="btn btn-sm btn-default join_group" href="#" groupid="'.$page['id'].'"><i class="fa fa-plus"></i> '.t('Join group').'</a>';
				}
				else {
					$join_button = '<a class="btn btn-sm btn-default" href="'.url('member/login/1').'"><i class="fa fa-plus"></i> '.t('Join group').'</a>';
				}
			}

			if (is_mobile()) {
				$output = '<div>'.page_logo($page,'large',array('link'=>url('group/'.$page['uri']))).'</div>'.$join_button;
			}
			else {
				$output .= '
				<div id="jcow_cover_avatar_box">
					<div id="jcow_cover_avatar"><a href="'.$logo_link.'">'.page_logo($page,'large',array('link'=>url('group/'.$page['uri']))).'</a>
					</div>
				</div>
				<div class="jcow_cover_content '.$cover_bg_class.'">
					<div id="jcow_cover_name"><span style="font-size:12px;vertical-align:middle">'.$gicon.'</span> '.h($page['name']).'
					</div>
					<div id="jcow_cover_follow">
					'.$join_button.'
					</div>
				</div>
				<div id="jcow_page_menu">'.$page_menu.'</div>';
			}
			app_header('<div id="jcow_cover">'.$output.'</div>');

			$output = '';
			$cover_buttons .= '<ul class="jcow_cover_buttons">';
			if ($client['id'] && $client['id'] != $owner['id']) {
				if ($is_member) {
					$cover_buttons .= '<li>'.url('group/'.$page['uri'].'/leave',t('Leave this group')).'</li>';
				}
			}
			if ($page['uid'] == $client['id']) {
				$cover_buttons .= '
				<li>'.url('groups/manage/'.$page['id'],t('Settings')).'</li>';
				$cover_buttons .= '<li>'.url('groups/logo/'.$page['id'],t('Edit group Logo')).'</li>';
				
				$res = sql_query("select count(*)as num from ".tb()."group_members_pending where gid='{$page['id']}' and !ignored");
				$row = sql_fetch_array($res);
				$cover_buttons .= '<li>'.url('groups/pending/'.$page['id'],t('Pending members').' ('.$row['num'].')').'</li>';
				
				$cover_buttons .= '<li>'.url('group/'.$page['uri'].'/managemembers',t('Manage members')).'</li>';
			}
			if (allow_access(3)) {
				$cover_buttons .= '<li>'.url('admin/useredit/'.$owner['id'],'Modify group owner').'</li>';
				$cover_buttons .= '<li><a href="#" class="deleteuser" uid="'.$owner['id'].'">Delete group owner</a></li>';
			}
			$cover_buttons .= '
			<li>'.url('report',t('Report')).'</li>
			</ul>';
			ass(array('content'=>$cover_buttons));
			$defined_tab_parent = $page['name'];
			set_title(h($page['name']));
			if (!$is_member && !$_SESSION['adsense_passed'] && $parr[1] != 'joining' && $page['var3']%2) {
				c(t('This is a closed group'));
				stop_here();
			}
		}
		$uid = $owner['id'];
		$config['is_profile'] = 1;
		if(!$owner['id']) {
			die('wrong uid');
		}

		if ( (time()-$owner['lastlogin']) > 600) {
			$owner['online'] = 0;
		}
		else {
			$owner['online'] = 1;
		}

		if ($uid == $client['id']) {
			$nav[] = '<strong>'.t('My profile').'</strong>';
		}
		else {
			$nav[] = t("{1}'s profile",'<strong>'.htmlspecialchars($owner['username']).' '.htmlspecialchars($owner['lastname']).'</strong>');
		}
	

		if ($page['type'] == 'u') {
			if ($owner['id'] != $client['id']) {
				$page['no_comment'] = 1;
			}
		}
		elseif ($page['type'] == 'group') {
			if ($client['id'] && ($owner['id'] != $client['id']) ) {
				$res = sql_query("select pid from ".tb()."page_users where pid='{$page['id']}' and uid='{$client['id']}'");
				if (!sql_counts($res)) {
					$page['no_comment'] = 1;
				}
			}
		}
		elseif ($page['type'] == 'page') {
			if ($client['id'] != $owner['id']) {
				$page['no_comment'] = 1;
			}
		}
		$owner['page'] = $page;

		


		return $owner;
	}


	function details($profile) {
		global $client;
		if ($client['id'] == $profile['id']) {
			$edit = ' <strong>'.url('account',t('Edit')).'</strong>';
		}
		if ($profile['birthmonth']) {
			$birth_info = '<dt>'.t('Birthday').'</dt>
			<dd>'.$profile['birthmonth'].'/'.$profile['birthday'].'</dd>';
		}
		$output .= '<dt>'.t('Full Name').'</dt>
			<dd>'.h($profile['fullname']).'</dd>';
		// custom fields
		for($i=1;$i<=7;$i++) {
			$col = 'var'.$i;
			$key = 'cf_var'.$i;
			$key2 = 'cf_var_value'.$i;
			$key3 = 'cf_var_des'.$i;
			$key4 = 'cf_var_label'.$i;
			$type = get_gvar($key);
			$value = get_gvar($key2);
			$des = get_gvar($key3);
			$label = get_gvar($key4);
			if ($type != 'disabled' && strlen($profile[$col])) {
				$output .= '
					<dt>'.$label.'</dt>
					<dd>'.nl2br(h($profile[$col])).'</dd>
					';
			}
		}
		$output .= '
		<dt>'.t('Last login').'</dt>
		<dd>'.get_date($profile['lastlogin']).'</dd>
		
			<dt>'.t('Gender').'</dt>
			<dd>'.gender($profile['gender']).'</dd>';
		if ($profile['birthday'] && $profile['birthyear'] && !$profile['hide_age']) {
			$output .= '
			<dt>'.t('Age').'</dt>
			<dd>'.get_age($profile['birthyear'],$profile['hide_age'],$profile['birthmonth'],$profile['birthday']).'</dd>
			';
		}


		$output .= '

			<dt>'.t('Come from').'</dt>
			<dd>'.($profile['location']).', '.get_country($profile['country']).'</dd>
			<dt>'.t('Registered').'</dt>
			<dd>'.get_date($profile['created']).'</dd>';

	
		return array('title'=>t('Details').$edit,'content'=>$output);
	}

}
	
