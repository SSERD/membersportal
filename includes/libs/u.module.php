<?php
/* ############################################################ *\
 ----------------------------------------------------------------
Jcow Software (http://www.jcow.net)
IS NOT FREE SOFTWARE
http://www.jcow.net/commercial_license
Copyright (C) 2009 - 2010 jcow.net.  All Rights Reserved.
 ----------------------------------------------------------------
\* ############################################################ */

class u extends jcow_pages {
	function __construct() {
		$this->type = 'u';
	}

	function tab_menu($owner,$page=array()) {
		return array(
			array('path'=>'u/'.$owner['username'], 'name'=>t('Wall')),
			array('path'=>'u/'.$owner['username'].'/likes', 'name'=>t('Likes'))
			);
	}

	function show_sidebar($page,$owner) {
		global $client,$all_apps;
		//ass($this->details($owner));
	}

	function likes($url = 0) {
		global $client, $content, $nav, $apps, $uhome,  $ubase, $offset, $num_per_page, $page,$config, $menuon;
		$profile = $this->settabmenu($url, 1,'u');
		$output .= stream_get($profile['page']['id'],10,0,$profile['page']['id'],'jcow_likes',1);

		$current_sub_menu['href'] = 'u/'.$profile['username'].'/likes';
		section(array('title'=>t('Likes'),'content'=>$output)
			);

	}





	function following($url) {
		global $client, $apps, $uhome,$ubase, $offset, $num_per_page, $page;
		if (!preg_match("/[0-9a-z]+/i",$url)) {
			die('wrong uid');
		}
		$profile = $this->settabmenu($url, 1,'u');
		if (!$profile['id']) die('bad uid');
		$res = sql_query("select u.id,u.username,u.avatar,u.fullname from ".tb()."followers as f left join ".tb()."accounts as u on u.id=f.fid where f.uid='{$profile['id']}' order by u.lastlogin DESC limit 50");
		while ($row = sql_fetch_array($res)) {
			$f = 1;
			$output .= '<div style="display:inline-block;height:100px;padding:0 10px;text-align:center">'.avatar($row).'<br />'.url('/u/'.$row['username'],h($row['fullname'])).'</div>';
		}
		section(array('title'=>t('Following'),'content'=>$output));
	}

	function visitors($url) {
		global $client, $apps, $uhome,$ubase, $offset, $num_per_page, $page;
		if (!preg_match("/[0-9a-z]+/i",$url)) {
			die('wrong uid');
		}
		$profile = $this->settabmenu($url, 1,'u');
		if (!$profile['id']) die('bad uid');
		$res = sql_query("select v.updated,u.username,u.avatar,u.fullname from ".tb()."page_visitors as v left join ".tb()."accounts as u on u.id=v.uid where v.pid='{$profile['page']['id']}' order by updated desc limit 50");
		$output = '<ul class="simple_list">';
		while ($row = sql_fetch_array($res)) {
			$output .= '<li style="height:55px"><div style="float:left;margin-right:5px">'.avatar($row).'</div><a href="'.url('u/'.$row['username']).'" rel="nofollow">'.h($row['fullname']).'</a><br />Time of last visit: <span style="font-size:1.5em">'.get_date($row['updated']).'</span></li>';
		}
		$output .= '</ul>';
		section(array('title'=>t('Recent visitors'),'content'=>$output));
	}

	function followers($url) {
		global $client, $apps, $uhome,$ubase, $offset, $num_per_page, $page;
		if (!preg_match("/[0-9a-z]+/i",$url)) {
			die('wrong uid');
		}
		$profile = $this->settabmenu($url, 1,'u');
		if (!$profile['id']) die('bad uid');
		$res = sql_query("select u.id,u.username,u.avatar,u.fullname from ".tb()."followers as f left join ".tb()."accounts as u on u.id=f.uid where f.fid='{$profile['id']}' order by u.lastlogin DESC limit 50");
		while ($row = sql_fetch_array($res)) {
			$f = 1;
			$output .= '<div style="display:inline-block;height:100px;padding:0 10px;text-align:center">'.avatar($row).'<br />'.url('/u/'.$row['username'],h($row['fullname'])).'</div>';
		}
		section(array('title'=>t('Followers'),'content'=>$output));
	}

}
