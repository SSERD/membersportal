<?php
/* ############################################################ *\
 ----------------------------------------------------------------
Jcow Software (http://www.jcow.net)
IS NOT FREE SOFTWARE
http://www.jcow.net/commercial_license
Copyright (C) 2009 - 2010 jcow.net.  All Rights Reserved.
 ----------------------------------------------------------------
\* ############################################################ */


if ($step == 'post') {
		set_gvar('theme_tpl',$_POST['theme_tpl']);
		if (file_exists('themes/'.$_POST['theme_tpl'].'/settings.php')) {
			include('themes/'.$_POST['theme_tpl'].'/settings.php');
		}
		if (is_array($theme_blocks)) {
			foreach ($theme_blocks as $key=>$block) {
				$key = 'theme_block_'.$key;
				if (!get_text($key)) {
					set_text($key,addslashes($block['default_value']));
				}
			}
		}
		redirect('admin/themes',1);
}
elseif ($step == 'imgpost') {
	if ($_FILES['logo']['size'] > 1024*100) {
		sys_back('Your LOGO size Exceeded limit');
	}
	if ($_FILES['landing']['size'] > 1024*500) {
		sys_back('Your Landing image size Exceeded limit');
	}
	// icon
	if (strlen($_FILES['favicon']['tmp_name'])>0 && $_FILES['favicon']['tmp_name'] != "none") {
		list($width, $height) = getimagesize($_FILES['favicon']['tmp_name']);
		if ($width != 32 || $height != 32) {
			sys_back('Square LOGO must be 32X32 px');
		}
		if ($newicon = save_file($_FILES['favicon'])) {
			if ($oldicon = get_gvar('custom_icon')) {
				@unlink($oldicon);
			}
			set_gvar('custom_icon',$newicon);
		}
	}

	// logo
	if (strlen($_FILES['logo']['tmp_name'])>0 && $_FILES['logo']['tmp_name'] != "none") {
		if ($newlogo = save_file($_FILES['logo'])) {
			if ($oldlogo = get_gvar('custom_logo')) {
				@unlink($oldlogo);
			}
			set_gvar('custom_logo',$newlogo);
		}
	}
	// landing
	if (strlen($_FILES['landing']['tmp_name'])>0 && $_FILES['landing']['tmp_name'] != "none") {
		if ($newimg = save_file($_FILES['landing'])) {
			if ($oldimg = get_gvar('custom_landing')) {
				@unlink($oldimg);
			}
			set_gvar('custom_landing',$newimg);
		}
	}
	redirect('admin/themes',1);
}
		if ($handle = opendir('themes')) {
			while (false !== ($file = readdir($handle))) {
				if (is_dir('themes/' .$file) && $file != '.' && $file != '..' && $file != '.svn' ) {
					$themes[] = $file;
				}
			}
			closedir($handle);
		}
		section_content('
		<style>
		.theme_preview {
			height:160px;
			width:150px;
			float:left;
			border: #eee 1px solid;
			margin: 5px;
			text-align:center;
		}
		.theme_preview img {
			border: #eee 1px solid;
		}
		</style>
		<div>Theme Folder: [your_jcow_installation]/themes/[theme_name]</div>
		<form method="post" action="'.url('admin/themes/post').'">');
		if (is_array($themes)) {
			foreach ($themes as $theme) {
				$selected = '';
				if ($theme == get_gvar('theme_tpl')) {
					$selected = 'checked';
					$actived = '<strong>Actived</strong>';
				}
				else {
					$actived = 'Active';
				}
				section_content('<div class="theme_preview">
				<label for="theme'.$theme.'">
				<img src="'.uhome().'/themes/'.$theme.'/preview.jpg" /><br />
				'.$theme.'<br />
				<input type="radio" name="theme_tpl" value="'.$theme.'" id="theme'.$theme.'" '.$selected.' />'.$actived.'
				</label>
				</div>');
			}
		}
		section_content('
		<div class="br"></div>
		<input type="submit" class="btn btn-primary" value="Save Change" />
		</form>');
		section_close('Themes');

		c('
			<a name="img"></a>
			<p class="text-warning">Please note: not all themes apply to custom LOGO and Landing image. If your changes does not effect, you need to edit the theme files menually.</p>
			<form method="post" name="form1" action="'.url('admin/themes/imgpost').'" enctype="multipart/form-data">
			<fieldset>
					<legend>Square Icon</legend>');
		if ($favicon = get_gvar('custom_icon')) {
			c('<img src="'.$favicon.'" style="max-width:200px;height:auto" />');
		}
		c('
					<p>
					Square LOGO, used for site icon
					<input name="favicon" type="file" /> (Must be 32X32px)
					</p>
					</fieldset>


					<fieldset>
					<legend>LOGO</legend>');
		if ($logo = get_gvar('custom_logo')) {
			c('<img src="'.$logo.'" style="max-width:200px;height:auto" />');
		}
		c('
					<p>
					Upload new LOGO
					<input name="logo" type="file" /> (Max size: 100kb)
					</p>
					</fieldset>


		<fieldset>
					<legend>Landing Image (Home page for guests)</legend>');
		if ($landing = get_gvar('custom_landing')) {
			c('<img src="'.$landing.'" style="max-width:400px;height:auto" />');
		}
		c('<p>
					Upload new Landing image
					<input name="landing" type="file" /> (Max width: 530px; Max size: 500kb)
					</p>
					</fieldset>


					<p>
					<input class="btn btn-primary" type="submit" value=" Submit " /> 
					</p>
					</form>
					');
		section_close('Upload your own Logo and Landing page image');


