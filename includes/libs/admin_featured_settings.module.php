<?php
/* ############################################################ *\
 ----------------------------------------------------------------
Jcow Software (http://www.jcow.net)
IS NOT FREE SOFTWARE
http://www.jcow.net/commercial_license
Copyright (C) 2009 - 2010 jcow.net.  All Rights Reserved.
 ----------------------------------------------------------------
\* ############################################################ */

$rps_str = get_text('recommend_pages');
$rps = explode(',',$rps_str);
$pages = array();
if (count($rps)>0) {
	foreach($rps as $page) {
		$page = explode(':',$page);
		$page_id = $page[0];
		if (is_numeric($page_id) && $page_id>0) {
			$pages[$page_id] = $page[1];
		}
	}
}

if ($_POST['step'] == 'post') {
	set_text('featuredpage_header',$_POST['featuredpage_header']);
	set_text('featuredpage_sidebar',$_POST['featuredpage_sidebar']);
	set_gvar('fp_enable_loginbox',$_POST['fp_enable_loginbox']);
	set_gvar('featuredpage_sidebar_story',$_POST['featuredpage_sidebar_story']);
	set_gvar('fp_enable_override',$_POST['fp_enable_override']);
	redirect('admin/featured_pages',1);
}
elseif ($step == 'delete') {
	if(is_numeric($id)) {
		unset($pages[$id]);
		$new_str = '';
		foreach($pages as $key=>$val) {
			$new_str .= strlen($new_str) ? ','.$key.':'.$val : $key.':'.$val;
		}
		set_text('recommend_pages',$new_str);
	}
	redirect('admin/featurepages',1);
}
else {
	
	if (get_gvar('fp_enable_loginbox')) {
		$fp_enable_loginbox = 'checked';
	}
	if (get_gvar('fp_enable_override')) {
		$fp_enable_override = 'checked';
	}
	if (get_gvar('featuredpage_sidebar_story')) {
		$featuredpage_sidebar_story = 'checked';
	}
	if (count($pages)<50) {
		c('
		<h2>Featured Page settings</h2>
		<form action="" method="post">
		<p>
		<input type="checkbox" name="fp_enable_loginbox" '.$fp_enable_loginbox.' value=1 /> <strong>Enable Login Box</strong> - Display a login box for guest in the sidebar.
		<p>
		<p>
		<input type="checkbox" name="fp_enable_override" '.$fp_enable_override.' value=1  /> <strong>Override Guest Landing Page</strong> - If you enable this, when guests go to homepage, they will be redirected to the Featured page
		<p>
		<p>
		<strong>Header of the page</strong>:<br />
		<textarea name="featuredpage_header" style="height:200px;width:600px">'.get_text('featuredpage_header').'</textarea><br />
		<ul><li>Max width:600px</li><li>HTML enabled</li></ul>
		</p>
		
		<p>
		<strong>Sidebar content on Featured page</strong>:<br />
		<input type="checkbox" name="featuredpage_sidebar_story" '.$featuredpage_sidebar_story.' />Apply to "viewstory" pages. (Display this sidebar to all blog/image/video pages.<br />
		<textarea name="featuredpage_sidebar" style="height:600px;width:400px">'.get_text('featuredpage_sidebar').'</textarea><br />
		<ul>
		<li>HTML enabled</li>
		<li>Default inline ads is disabled for this sidebar. If you want to insert sidebar ads, you can insert the codes here</li>
		</ul>
		</p>
		<input type="submit" value=" Save changes "  /><br />
		<input type="hidden" name="step" value="post" />
		
		</form>');
	}

}