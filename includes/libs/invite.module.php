<?php
/* ############################################################ *\
 ----------------------------------------------------------------
Jcow Software (http://www.jcow.net)
IS NOT FREE SOFTWARE
http://www.jcow.net/commercial_license
Copyright (C) 2009 - 2010 jcow.net.  All Rights Reserved.
 ----------------------------------------------------------------
\* ############################################################ */

class invite{

	function __construct() {
		global $client,$config;
		clear_as();
		need_login();
		set_title('Invite');
		nav(url('invite',t('Invite')));
		if (!$config['smtp_enabled'] && !get_gvar('smtp_enabled')) {
			c('To use invitation, please click "Admin CP" and set up your own SMTP info');
			stop_here();
		}
	}

	function index() {
		global $content, $db, $nav, $client, $uhome, $locations, $current_sub_menu;
		$current_sub_menu['href'] = 'invite';
		if (jedition == 'cloud') {
			$res = sql_query("select count(*) as num from jcow_invites");
			$row = sql_fetch_array($res);
			if ($row['num'] >= 100) {
				c('Sorry, each network can only send out 100 invitations');
				stop_here();
			}
		}

		$encoded_url = urlencode(uhome());


		c('

		<form method="post" action="'.url('invite/post').'" >
		<!--
		<p>
		<img src="'.uhome().'/files/icons/msn.gif" />'.url('invite/import_contacts',t('Click here to import your msn contacts')).'
		</p>
		-->
					<p>
					'.label(t('To').' (Email address)').'
					<input name="emails" size="80" value="'.$contacts.'" class="form-control" /><br />
					<span>'.t('Multiple email addresses should be Separated with commas').'(,)</span>
					</p>
					
					<p>
					'.label(t('Message').' (Optional)').'
					<textarea name="message" rows="5" class="form-control"></textarea></p>
	
		<p>
					<input type="submit" class="button" value="'.t('Send invitations').'" />
					</p>
					</form>
					');
		section_close(t('Send invitation'));

		$res = sql_query("select * from ".tb()."invites where uid='{$client['id']}' "." order by id DESC LIMIT 30");
		c('<table class="table_simple">
		<tr><th>Email address</th><th>Time</th><th>Status</th></tr>');
		while ($invite = sql_fetch_array($res) ) {
			$invite['status'] = $invite['status'] ? '<font color="green">Joined</font>' : 'Sent';
			c('<tr><td>'.$invite['email'].'</td><td>'.get_date($invite['created']).'</td><td>'.$invite['status'].'</td></tr>');
		}
		c('</table>');
		section_close(t('Hostories'));
		
		
	}
	
	function import_contacts() {
		global $current_sub_menu;
		$current_sub_menu['href'] = 'invite';

		c('
			<form method="post" action="'.url('invite/index').'">
			<fieldset style="background:url(\''.uhome().'/files/images/msn.jpg\') right top no-repeat;">
			<legend>'.t('Invite your MSN friends').'</legend>
			<p>
			'.label(t('MSN username')).'
			<input type="text" name="msn_user" size="15" />@hotmail.com
			</p>
			<p>
			'.label(t('MSN password')).'
			<input type="password" name="msn_pass" size="15" />
			</p>
			<p>
			<input type="hidden" name="import" value="msn" />
			<input type="submit" class="button" value="Import" />
			</p>
			</fieldset>
			</form>');
	}

	function post() {
		global $db, $client, $uhome, $ubase, $config;
		/*
		if (jedition == 'cloud') {
			$res = sql_query("select count(*) as num from jcow_invites");
			$row = sql_fetch_array($res);
			if ($row['num'] >= 100) {
				c('Sorry, each network can only send out 100 invites');
				stop_here();
			}
		}
		*/
		if (!$_POST['emails']) {
			sys_back(t('Please fill all the required blanks'));
		}

		
		$emails = explode(',',$_POST['emails']);
		$i = 0;
		$sents = $ignores = 0;
		foreach ($emails as $email) {
			//check
			$res = sql_query("select * from `".tb()."invites` where email='$email' ");
			$res2 = sql_query("select * from `".tb()."accounts` where email='$email' ");
			if (!sql_counts($res) && !sql_counts($res2)) {
				if (function_exists('jc_send_invite')) {
					jc_send_invite();
				}
				$sents++;
				$rand = get_rand(15);
				sql_query("insert into `".tb()."invites` (uid,email,created,code) values('{$client['id']}','$email',".time().",'$rand')");
				$iid = insert_id();
				$url = url('member/signup','','',array('iid'=>$iid,'code'=>$rand));
				$message = get_gvar('site_name').'<br />
				'.$_POST['message'].'<br />
				'.t('Click the following link to Join').'<br />
			<a href="'.$url.'">'.$url.'</a><br />';
				$email = trim($email);
				$email = str_replace("\r\n","",$email);
				if ($i > 50) {
					redirect(url('invite'),t('Invitations have been sent!'));
				}
				$message .= '<br /><br />invited by '.$client['fullname'];
				@jcow_mail($email,t('you are invited to join {1}',get_gvar('site_name')),$message,$client['email']);
				$i++;
			}
			else {
				$ignores++;
			}
		}
		$total = $ignores+$sents;
		redirect(url('invite/histories'),t('{1} submitted',$total).' ('.$sents.' '.t('sents').', '.$ignores.' '.t('ignores').')');
	}

	function histories() {
		global $current_sub_menu, $client;
		$current_sub_menu['href'] = 'invite/histories';
		$res = sql_query("select * from ".tb()."invites where uid='{$client['id']}' "." order by id DESC LIMIT 100");
		c('<table class="table_simple">
		<tr><th>Email address</th><th>Time</th><th>Status</th></tr>');
		while ($invite = sql_fetch_array($res) ) {
			$invite['status'] = $invite['status'] ? '<font color="green">Joined</font>' : 'Sent';
			c('<tr><td>'.$invite['email'].'</td><td>'.get_date($invite['created']).'</td><td>'.$invite['status'].'</td></tr>');
		}
		c('</table>');

	}
	
}