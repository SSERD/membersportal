<?php
/* ############################################################ *\
 ----------------------------------------------------------------
Jcow Software (http://www.jcow.net)
IS NOT FREE SOFTWARE
http://www.jcow.net/commercial_license
Copyright (C) 2009 - 2010 jcow.net.  All Rights Reserved.
 ----------------------------------------------------------------
\* ############################################################ */
// boot
error_reporting(E_ERROR | E_PARSE);
ini_set('display_errors', 1);
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
require_once './my/config.php';
require_once './includes/libs/common.inc.php';
require_once './includes/libs/db.inc.php';
if ($_GET['act'] == 'jcow_update_new' && is_numeric($_GET['uid'])) {
	$conn=sql_connect($db_info['host'], $db_info['user'], $db_info['pass'], $db_info['dbname']);
	sql_query("SET NAMES UTF8");
	$res = sql_query("select count(*) as num from `jcow_im` where to_id='{$_GET['uid']}' and !hasread");
	$row = sql_fetch_array($res);
	$msg_new = $row['num'] ? $row['num'] : '';

	$res = sql_query("select count(*) as num from `jcow_notes` where uid='{$_GET['uid']}' and !hasread");
	$row = sql_fetch_array($res);
	$note_new = $row['num'] ? $row['num'] : '';

	$res = sql_query("select count(*) as num from `jcow_friend_reqs` where fid='{$_GET['uid']}'");
	$row = sql_fetch_array($res);
	$frd_new = $row['num'] ? $row['num'] : '';
	if ($row['num']) {
		$frd_link = url('friends/requests');
	}
	else {
		$frd_link = url('friends');
	}
	$arr = array('msg_new'=>$msg_new,'note_new'=>$note_new,'frd_new'=>$frd_new,'frd_link'=>$frd_link);
	echo json_encode($arr);
	exit;
}
?>