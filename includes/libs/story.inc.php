<?php
/* ############################################################ *\
 ----------------------------------------------------------------
Jcow Software (http://www.jcow.net)
IS NOT FREE SOFTWARE
http://www.jcow.net/commercial_license
Copyright (C) 2009 - 2010 jcow.net.  All Rights Reserved.
 ----------------------------------------------------------------
\* ############################################################ */
class story{
	var $name = '';
	var $flag = '';
	var $header = '';
	var $footer = '';
	var $cid = 0;
	var $write_story = '';
	var $submit = '';
	var $savechanges = '';
	var $social_bookmarks = 1;
	var $top_stories = 0;
	var $stories_from_author = 0;
	var $stories_from_cat = 0;
	var $allow_vote = 0;
	var $who_voted = 1;
	var $tags = 0;
	var $default_thumb = '';
	var $about_the_author = 0;
	var $writepost = 'writestorypost';
	var $photos = 0;
	var $disable_category = 0;

	// list
	var $list_type = 'ul';
	var $list_elements = array('title','created','username');
	var $list_atts = array('Title','Created','User');

	// view detail
	var $view_elements = array('title','created','username','content');
	var $view_atts = array('Title','Created','Name','Content');

	// comments
	var $comment_type = 'ul';
	var $comment_elements = array('title','created','username','content');

	// insert/edit form
	var $story_form_elements = array('title','content');
	var $stories = 'Stories';

	// access
	var $story_write = 2;
	var $story_edit = 2;
	var $story_delete = 2;
	var $comment_write = 2;
	var $comment_delete = 2;
	
	// labels
	var $label_content = '';
	var $label_title = '';
	var $label_comment = '';
	var $label_entry = 'entries';
	// redirect
	var $redirect_writestorypost = '';

	// hooks
	var $hook;
	
	// acts
	var $act_write = '';
	
	function story() {
		GLOBAL $content, $parr, $sub_menu, $sub_menu_title, $current_app, $client, $ss, $menuon;
		if (!$this->label_content) {
			$this->label_content = t('Content');
		}
		if (!$this->label_title) {
			$this->label_title = t('Title');
		}
		if (!$this->label_comment) {
			$this->label_comment = t('Comments');
		}
		if (!$this->submit) {
			$this->submit = t('Submit');
		}
		if (!$this->savechanges) {
			$this->savechanges = t('Save changes');
		}
		if ($this->allow_vote) {
			$this->vote_options['rating'] = t('Rating');
		}
		$this->name = $parr[0];
		$menuon = $this->name;
		$this->flag = $current_app['flag'];
		$sub_menu_title = t('Categories');
		/*
		$res = sql_query("select c.* from `".tb()."story_categories` as c where  app='{$this->name}' order by weight");
		if (!$this->index) {
			$sub_menu[] = array('href'=>$this->name.'/liststories/all', 'name'=>t('All').' '.$current_app['flag']);
		}
		while ($row = sql_fetch_array($res)) {
			$sub_menu[] = array('href'=>$this->name.'/liststories/'.$row['id'],'name'=>$row['name'],'description'=>$row['description']);
		}
		if (count($sub_menu) < 2) {
			$sub_menu = array();
			$this->disable_category = 1;
		}
		*/
		$sub_menu = array();
		$this->disable_category = 1;
	}
	

	function hooks($array) {
		foreach ($array as $val) {
			$this->hook[$val] = 1;
		}
	}

	function set_current_sub_menu($cid) {
		global $current_sub_menu;
		$current_sub_menu['href'] = $this->name.'/liststories/'.$cid;
	}
	function mine() {
		$this->liststories('mine');
	}
	function newest() {
		$this->liststories('newest');
	}
	function featured() {
		$this->liststories('featured');
	}
	function index() {
		$this->liststories('all');
	}
	function discover() {
		$this->liststories('discover');
	}
	function friends() {
		$this->liststories('friends');
	}
	function following() {
		$this->liststories('following');
	}
	function mynetwork() {
		$this->liststories('mynetwork');
	}
	function mylikes() {
		$this->liststories('mylikes');
	}
	function mytopics() {
		$this->liststories('mytopics');
	}
	function hot() {
		$this->liststories('hot');
	}
	function texts() {
		$this->liststories('texts');
	}
	function all() {
		$this->liststories('all');
	}


	// 文章列表
	function liststories($uri) {
		GLOBAL $db,$num_per_page,$page,$ubase,$offset,$content,$client,$title,$sub_menu, $cat_id, $uhome, $current_sub_menu,$parr,$from_url,$config;
		$num_per_page = 25;
		$offset = ($page-1)*$num_per_page;
		$num_per_page = $num_per_page+1;
		if (preg_match("/^page_/i",$uri)) {
			$arr = explode('_',$uri);
			$page_id = $arr[1];
			if (!preg_match("/^[0-9]+$/i",$page_id)) die('bad uri');
			$res = sql_query("select * from ".tb()."pages where id='{$page_id}'");
			$jcow_page = sql_fetch_array($res);
			if (!$jcow_page['id']) die('wrong page id');
			if ($jcow_page['type'] == 'u') {
				include_once('modules/u/u.php');
				if (!$user = u::settabmenu($jcow_page['uri'],1,'u')) die('wrong uname');
				$title = t("{1}'s {2}",$user['username'],$this->label_entry);
				$nav[] = url($this->name.'/liststories/page_'.$user['username'],$user['username']);
				//c(stream_get($page_id,10,0,$page_id,$this->name,1));
				//stop_here();
				if ($this->name == 'images') {
					$res = sql_query("SELECT s.*,u.username,u.fullname,u.avatar FROM `".tb()."stories` as s left join `".tb()."accounts` as u on u.id=s.uid where s.app='{$this->name}' and s.page_id='{$jcow_page['id']}' and s.photos>0 ORDER by s.id DESC LIMIT $offset,$num_per_page ");
				}
				else {
					$res = sql_query("SELECT s.*,u.username,u.fullname,u.avatar FROM `".tb()."stories` as s left join `".tb()."accounts` as u on u.id=s.uid where s.app='{$this->name}' and s.page_id='{$jcow_page['id']}' ORDER by s.id DESC LIMIT $offset,$num_per_page ");
				}
				$pwhere = " and uid='{$user['id']}' ";
				
			}
			elseif ($jcow_page['type'] == 'page') {
				include_once('modules/page/page.php');
				if (!$user = page::settabmenu($jcow_page['uri'],1,$jcow_page['type'])) die('owner not found');
				$title = h($jcow_page['name'].' - '.$this->label_entry);
				//c(stream_get($page_id,10,0,$page_id,$this->name,1));
				//stop_here();
				$nav[] = url($this->name.'/liststories/page_'.$uri,$jcow_page['name']);
				if ($this->name == 'images') {
					$res = sql_query("SELECT s.*,u.username,u.fullname,u.avatar FROM `".tb()."stories` as s left join `".tb()."accounts` as u on u.id=s.uid where s.app='{$this->name}' and s.page_id='{$jcow_page['id']}' and s.photos>0 ORDER by s.id DESC LIMIT $offset,$num_per_page ");
				}
				else {
					$res = sql_query("SELECT s.*,u.username,u.fullname,u.avatar FROM `".tb()."stories` as s left join `".tb()."accounts` as u on u.id=s.uid where s.app='{$this->name}' and s.page_id='{$jcow_page['id']}' ORDER by s.id DESC LIMIT $offset,$num_per_page ");
				}
				$pwhere = " and page_id='{$jcow_page['id']}' ";
			}
			elseif ($jcow_page['type'] == 'group') {
				include_once('modules/group/group.php');
				if (!$user = group::settabmenu($jcow_page['uri'],1,$jcow_page['type'])) die('owner not found');
				$title = h($jcow_page['name'].' - '.$this->label_entry);
				//c(stream_get($page_id,10,0,$page_id,$this->name,1));
				//stop_here();
				$nav[] = url($this->name.'/liststories/page_'.$uri,$jcow_page['name']);
				if ($this->name == 'images') {
					$res = sql_query("SELECT s.*,u.username,u.fullname,u.avatar FROM `".tb()."stories` as s left join `".tb()."accounts` as u on u.id=s.uid where s.app='{$this->name}' and s.page_id='{$jcow_page['id']}' and s.photos>0 ORDER by s.id DESC LIMIT $offset,$num_per_page ");
				}
				else {
					$res = sql_query("SELECT s.*,u.username,u.fullname,u.avatar FROM `".tb()."stories` as s left join `".tb()."accounts` as u on u.id=s.uid where s.app='{$this->name}' and s.page_id='{$jcow_page['id']}' ORDER by s.id DESC LIMIT $offset,$num_per_page ");
				}
				$pwhere = " and page_id='{$jcow_page['id']}' ";
			}
			$uri = 'liststories/'.$uri;
		}
		else {
			$hot_num = get_gvar('jcow_hot_likes');
				if (!is_numeric($hot_num) || $hot_num <1) {
					$hot_num = 3;
				}
			/*
			if ($client['id'] && strlen($this->write_story)) {
				button($this->name.'/writestory',$this->write_story);
			}
			*/
			//c(feed_activity_get($client['id'],10,0,0,1,'following',array('app'=>$this->name));
			//stop_here();
			if ($uri == 'recommandations') {
				if ($client['id']) {
					$uids = $client['id'];
					$fuids = array($client['id']);
					$res2 = sql_query("select f.fid from ".tb()."followers as f left join ".tb()."accounts as u on u.id=f.fid where f.uid='{$client['id']}' and u.disabled<2 order by u.lastlogin desc limit 50");
					while ($row = sql_fetch_array($res2)) {
						if (!in_array($row['fid'],$fuids)) {
							$uids .= ','.$row['fid'];
							$fuids[] = $row['fid'];
						}
					}
					$res2 = sql_query("select f.fid from ".tb()."friends as f left join ".tb()."accounts as u on u.id=f.fid where f.uid='{$client['id']}' order by u.lastlogin desc limit 50");
					while ($row = sql_fetch_array($res2)) {
						if (!in_array($row['fid'],$fuids)) {
							$uids .= $uids ? ','.$row['fid'] : $row['fid'];
							$fuids[] = $row['fid'];
						}
					}
					if ($uids) {
						$uwhere = ' or s.uid in ('.$uids.') ';
					}
				}
				if ($this->name == 'images') {
					$res = sql_query("SELECT s.*,u.username,u.fullname,u.avatar FROM `".tb()."stories` as s 
						left join ".tb()."streams as st on st.id=s.stream_id
						left join `".tb()."accounts` as u on u.id=s.uid where s.app='{$this->name}' and s.page_type in ('u','page') and (s.featured or st.likes>=$hot_num $uwhere) ORDER by s.id DESC LIMIT $offset,$num_per_page ");
				}
				else {
					$res = sql_query("SELECT s.*,u.username,u.fullname,u.avatar FROM `".tb()."stories` as s 
						left join ".tb()."streams as st on st.id=s.stream_id
						left join `".tb()."accounts` as u on u.id=s.uid where s.app='{$this->name}' and s.page_type in ('u','page') and (s.featured or st.likes>=$hot_num $uwhere) ORDER by s.id DESC LIMIT $offset,$num_per_page ");
				}

			}
			elseif ($uri == 'discover') {
				if (!$client['id']) redirect('member/login/1');
				$page_ids = $friendsids = array();
				$page_ids[] = $client['page']['id'];
				$res = sql_query("select f.fid,p.id as pageid from ".tb()."followers as f left join ".tb()."pages as p on (p.uid=f.fid and p.type='u') where f.uid='{$client['id']}' order by p.updated desc limit 30");
				while ($row = sql_fetch_array($res)) {
					if (is_numeric($row['pageid']))
						$page_ids[] = $row['pageid'];
				}
				$res = sql_query("select u.pid from ".tb()."page_users as u left join ".tb()."pages as p on p.id=u.pid where u.uid='{$client['id']}'  limit 50");
				while($jcow_page = sql_fetch_array($res)) {
					if (is_numeric($jcow_page['pid']))
						$page_ids[] = $jcow_page['pid'];
				}
				$page_ids = implode(',',$page_ids);
				if ($this->name == 'images') {
					$res = sql_query("SELECT s.*,u.username,u.fullname,u.avatar,p.logo as page_logo,p.uri as page_uri,p.name as page_name FROM `".tb()."stories` as s left join `".tb()."accounts` as u on u.id=s.uid left join jcow_pages as p on p.id=s.page_id left join ".tb()."streams as st on st.id=s.stream_id where s.page_id not in ($page_ids)  and s.app='{$this->name}' and s.photos>0 and st.type=0 ORDER by s.id DESC LIMIT $offset,$num_per_page ");
				}
				else {
					$res = sql_query("SELECT s.*,u.username,u.fullname,u.avatar,p.logo as page_logo,p.uri as page_uri,p.name as page_name FROM `".tb()."stories` as s left join `".tb()."accounts` as u on u.id=s.uid  left join ".tb()."pages as p on p.id=s.page_id where s.page_id not in ($page_ids) and s.app='{$this->name}' ORDER by s.id DESC LIMIT $offset,$num_per_page ");
				}
			}
			elseif ($uri == 'following') {
				if (!$client['id']) {
					need_login();
				}
				else {
					// followed
					$page_ids = array();
					$friendsids = array($client['id']);
					$page_ids[] = $client['page']['id'];
					$res = sql_query("select f.fid,p.id,p.description,p.type,p.id as pageid from ".tb()."followers as f left join ".tb()."pages as p on (p.uid=f.fid and p.type='u') where f.uid='{$client['id']}' order by p.updated desc limit 30");
					while ($row = sql_fetch_array($res)) {
						if (is_numeric($row['pageid']))
							$page_ids[] = $row['pageid'];
					}
					$res = sql_query("select * from jcow_friends where uid='{$client['id']}' limit 50");
					while ($f = sql_fetch_array($res)) {
						$friendsids[] = $f['fid'];
					}
					$p_sql = " (!s.var5 or s.uid in (".implode(',',$friendsids).")) ";
					$res = sql_query("select u.pid from ".tb()."page_users as u left join ".tb()."pages as p on p.id=u.pid where u.uid='{$client['id']}'  limit 50");
					while($jcow_page = sql_fetch_array($res)) {
						if (is_numeric($jcow_page['pid']))
							$page_ids[] = $jcow_page['pid'];
					}
					$page_ids = implode(',',$page_ids);
				}

				if ($this->name == 'images') {
					$res = sql_query("SELECT s.*,u.username,u.fullname,u.avatar,p.logo as page_logo,p.uri as page_uri,p.name as page_name FROM `".tb()."stories` as s left join `".tb()."accounts` as u on u.id=s.uid left join jcow_pages as p on p.id=s.page_id left join ".tb()."streams as st on st.id=s.stream_id where s.page_id in ($page_ids)  and s.app='{$this->name}' and $p_sql and s.photos>0 ORDER by s.id DESC LIMIT $offset,$num_per_page ");
				}
				else {
					$res = sql_query("SELECT s.*,u.username,u.fullname,u.avatar,p.logo as page_logo,p.uri as page_uri,p.name as page_name FROM `".tb()."stories` as s left join `".tb()."accounts` as u on u.id=s.uid  left join ".tb()."pages as p on p.id=s.page_id where s.page_id in ($page_ids) and s.app='{$this->name}' and $p_sql ORDER by s.id DESC LIMIT $offset,$num_per_page ");
				}
			}
			elseif ($uri == 'mynetwork') {
				if (!$client['id']) {
					need_login();
				}
				else {
					// friends
					$page_ids = array($client['page']['id']);
					$friendsids = array($client['id']);
					$res = sql_query("select f.fid,p.id as pageid from ".tb()."friends as f left join ".tb()."pages as p on p.uid=f.fid where f.uid='{$client['id']}' order by p.updated desc limit 20");
					while ($row = sql_fetch_array($res)) {
						$friendsids[] = $row['fid'];
					}
					$p_sql = " (!s.var5 or s.uid in (".implode(',',$friendsids).")) ";
					// followed
					$res = sql_query("select f.fid,p.id,p.description,p.type,p.id as pageid from ".tb()."followers as f left join ".tb()."pages as p on (p.uid=f.fid and p.type='u') where f.uid='{$client['id']}' order by p.updated desc limit 20");
					while ($row = sql_fetch_array($res)) {
						if (is_numeric($row['pageid']))
							$page_ids[] = $row['pageid'];
					}
					// pages
					$res = sql_query("select u.pid from ".tb()."page_users as u left join ".tb()."pages as p on p.id=u.pid where u.uid='{$client['id']}' order by p.updated desc limit 20");
					while($jcow_page = sql_fetch_array($res)) {
						if (is_numeric($jcow_page['pid']))
							$page_ids[] = $jcow_page['pid'];
					}
					$page_ids = implode(',',$page_ids);
				}

				if ($this->name == 'images') {
					$res = sql_query("SELECT s.*,u.username,u.fullname,u.avatar,p.logo as page_logo,p.uri as page_uri,p.name as page_name FROM `".tb()."stories` as s left join `".tb()."accounts` as u on u.id=s.uid left join jcow_pages as p on p.id=s.page_id left join ".tb()."streams as st on st.id=s.stream_id where s.page_id in ($page_ids)  and s.app='{$this->name}' and $p_sql AND s.photos>0 ORDER by s.id DESC LIMIT $offset,$num_per_page ");
				}
				else {
					$res = sql_query("SELECT s.*,u.username,u.fullname,u.avatar,p.logo as page_logo,p.uri as page_uri,p.name as page_name FROM `".tb()."stories` as s left join `".tb()."accounts` as u on u.id=s.uid  left join ".tb()."pages as p on p.id=s.page_id where s.page_id in ($page_ids) and s.app='{$this->name}' ORDER by s.id DESC LIMIT $offset,$num_per_page ");
				}
			}
			elseif ($uri == 'friends') {
				if (!$client['id']) redirect('member/login/1');
				$res2 = sql_query("select f.fid from ".tb()."friends as f left join ".tb()."accounts as u on u.id=f.fid where f.uid='{$client['id']}' order by u.lastlogin desc limit 50");
				while ($row = sql_fetch_array($res2)) {
					$uids .= $uids ? ','.$row['fid'] : $row['fid'];
				}
				if ($uids) {
					$res = sql_query("SELECT s.*,u.username,u.fullname,u.avatar FROM `".tb()."stories` as s left join `".tb()."accounts` as u on u.id=s.uid where s.app='{$this->name}' and s.uid in ('$uids') ORDER by s.id DESC LIMIT $offset,$num_per_page ");
					$pwhere = ' and uid in ('.$uids.') ';
				}
			}
			elseif ($uri == 'mine') {
				if (!$client['id']) redirect('member/login/1');
				if ($this->name == 'images') {
					$res = sql_query("SELECT s.*,u.username,u.fullname,u.avatar FROM `".tb()."stories` as s left join `".tb()."accounts` as u on u.id=s.uid where s.app='{$this->name}' and s.uid='{$client['id']}' and s.photos>0 ORDER by s.id DESC LIMIT $offset,$num_per_page ");
				}
				else {
					$res = sql_query("SELECT s.*,u.username,u.fullname,u.avatar FROM `".tb()."stories` as s left join `".tb()."accounts` as u on u.id=s.uid where s.app='{$this->name}' and s.uid='{$client['id']}' ORDER by s.id DESC LIMIT $offset,$num_per_page ");
				}
				$pwhere = " and uid='{$client['id']}' ";
			}
			elseif (is_numeric($uri)) {
				if($cat = valid_category($uri, $this->name)) {
					$cat_id = $cid = $cat['id'];
					$title = htmlspecialchars($cat['name']);
					nav(h($cat['name']));
					$pwhere = " and cid='$cid' ";
					$res = sql_query("SELECT s.*,u.fullname,u.username,u.fullname,u.avatar FROM `".tb()."stories` as s left join `".tb()."accounts` as u on u.id=s.uid where s.app='{$this->name}' and s.cid='$cid' and u.disabled<2 ORDER by s.id DESC LIMIT $offset,$num_per_page ");
				}
				else {
					die('wrong cid');
				}
			}
			elseif ($uri == 'newest') {
				if ($this->name == 'images') {
					$photo_sql = " and s.photos>0 ";
				}
				$res = sql_query("SELECT s.*,u.username,u.fullname,u.avatar,p.logo as page_logo,p.name as page_name,p.uri as page_uri FROM `".tb()."stories` as s left join `".tb()."accounts` as u on u.id=s.uid left join jcow_pages as p on p.id=s.page_id where s.app='{$this->name}' $photo_sql and s.page_type in ('u','page')  ORDER by s.id DESC LIMIT $offset,$num_per_page ");
			}
			elseif ($uri == 'mylikes') {
				need_login();
				$res = sql_query("SELECT s.*,u.username,u.fullname,u.avatar FROM `".tb()."stories` as s left join `".tb()."accounts` as u on u.id=s.uid where s.stream_id in(select stream_id from ".tb()."liked where uid={$client['id']}) and s.app='{$this->name}' ORDER by s.id DESC LIMIT $offset,$num_per_page ");
			}
			elseif ($uri == 'mytopics') {
				need_login();
				$res = sql_query("SELECT s.*,u.username,u.fullname,u.avatar FROM ".tb()."topic_ids as ti left join `".tb()."stories` as s on s.id=ti.sid left join `".tb()."accounts` as u on u.id=s.uid where ti.tid in(select tid from ".tb()."topics_followed where uid={$client['id']}) and s.app='{$this->name}' ORDER by s.id DESC LIMIT $offset,$num_per_page ");
			}
			elseif ($uri == 'featured') {
				$this->tag_cloud(12);
				$res = sql_query("SELECT s.*,u.username,u.fullname,u.avatar,p.logo as page_logo,p.name as page_name,p.uri as page_uri FROM `".tb()."stories` as s 
						left join ".tb()."streams as st on st.id=s.stream_id
						left join `".tb()."accounts` as u on u.id=s.uid left join jcow_pages as p on p.id=s.page_id where s.app='{$this->name}' and s.featured>0 ORDER by s.id DESC LIMIT $offset,$num_per_page ");
				/*
				if (allow_access(3)) {
						c('<div style="padding:5px;text-align:right">Admin tool: '.url($this->name.'/newest','View all entries').'</div>');
					}
					*/
			}
			elseif ($uri == 'images') {
				$res = sql_query("SELECT s.*,u.username,u.fullname,u.avatar FROM `".tb()."stories` as s left join `".tb()."accounts` as u on u.id=s.uid where s.app='images' and s.page_type in ('u','page') and s.photos>0 ORDER by s.id DESC LIMIT $offset,$num_per_page ");
			}
			elseif ($uri == 'texts') {
				$res = sql_query("SELECT s.*,u.username,u.fullname,u.avatar FROM `".tb()."stories` as s left join `".tb()."accounts` as u on u.id=s.uid where s.app='texts'  ORDER by s.id DESC LIMIT $offset,$num_per_page ");
			}
			elseif ($uri == 'videos') {
				$res = sql_query("SELECT s.*,u.username,u.fullname,u.avatar FROM `".tb()."stories` as s left join `".tb()."accounts` as u on u.id=s.uid where s.app='videos' and s.page_type in ('u','page')  ORDER by s.id DESC LIMIT $offset,$num_per_page ");
			}
			elseif ($uri == 'music') {
				$res = sql_query("SELECT s.*,u.username,u.fullname,u.avatar FROM `".tb()."stories` as s left join `".tb()."accounts` as u on u.id=s.uid where s.app='music' and s.page_type in ('u','page')  ORDER by s.id DESC LIMIT $offset,$num_per_page ");
			}
			elseif ($uri == 'tag') {
				$this->tag_cloud(12);
				if (!is_numeric($parr[3])) {
					c('bad tag');
					stop_here();
				}
				$res2 = sql_query("select * from jcow_tags where id='{$parr[3]}'");
				$tag = sql_fetch_array($res2);
				if (!$tag['id']) {
					c('no this tag');
					stop_here();
				}
				c('<h1>'.t('Tag').': '.h($tag['name']).'</h1>');
				section_close();
				$res = sql_query("SELECT s.*,u.username,u.fullname,u.avatar,p.logo as page_logo,p.name as page_name FROM jcow_tag_ids as t left join `".tb()."stories` as s on s.id=t.sid left join `".tb()."accounts` as u on u.id=s.uid left join jcow_pages as p on p.id=s.page_id where t.tid='{$tag['id']}' and s.page_type in ('u','page')  ORDER by t.sid DESC LIMIT $offset,$num_per_page ");
			}
			elseif ($uri == 'hot') {
				$this->tag_cloud(12);
				if ($this->name == 'images') {
					$photo_sql = " and s.photos>0 ";
				}
				$res = sql_query("SELECT s.*,u.username,u.fullname,u.avatar,p.logo as page_logo,p.name as page_name FROM `".tb()."stories` as s 
						left join ".tb()."streams as st on st.id=s.stream_id
						left join `".tb()."accounts` as u on u.id=s.uid left join jcow_pages as p on p.id=s.page_id where s.app='{$this->name}' and s.id>
						(
						select IFNULL(
						(select id from jcow_stories where app='{$this->name}'  order by id desc limit 1000,1)
						,0)
						) $photo_sql
						 ORDER by (s.digg*3600*12+s.featured*3600*48+s.created) desc, s.id DESC LIMIT $offset,$num_per_page ");
						
			}
			else { //all

				$this->tag_cloud(12);
				if (!$client['id']) {
					$p_sql = " !s.var5 ";
					$g_sql = " p.type!='group' ";
				}
				else {
					$friendsids = array($client['id']);
					$res = sql_query("select * from jcow_friends where uid='{$client['id']}' limit 50");
					while ($f = sql_fetch_array($res)) {
						$friendsids[] = $f['fid'];
					}
					$p_sql = " (!s.var5 or s.uid in (".implode(',',$friendsids).")) ";
					$mygids = array();
					$res = sql_query("select pu.pid from jcow_page_users as pu left join jcow_pages as p on p.id=pu.pid where pu.uid='{$client['id']}' and p.type='group'");
					while ($g = sql_fetch_array($res)) {
						$mygids[] = $g['pid'];
					}
					if (count($mygids)) {
						$g_sql = " (p.type!='group' or p.id in (".implode(',',$mygids).")) ";
					}
					else {
						$g_sql = " p.type!='group' ";
					}
				}
				if ($this->name == 'images') {
					$res = sql_query("SELECT s.*,u.username,u.fullname,u.avatar,p.logo as page_logo,p.name as page_name,p.uri as page_uri FROM `".tb()."stories` as s left join `".tb()."accounts` as u on u.id=s.uid left join jcow_pages as p on p.id=s.page_id left join ".tb()."streams as st on st.id=s.stream_id where s.app='{$this->name}' and $g_sql and s.photos>0 and $p_sql ORDER by s.id DESC LIMIT $offset,$num_per_page ");
				}
				else {
					$res = sql_query("SELECT s.*,u.username,u.fullname,u.avatar,p.logo as page_logo,p.name as page_name,p.uri as page_uri FROM `".tb()."stories` as s left join `".tb()."accounts` as u on u.id=s.uid  left join ".tb()."pages as p on p.id=s.page_id left join ".tb()."streams as st on st.id=s.stream_id where s.app='{$this->name}' and $g_sql and $p_sql ORDER by s.id DESC LIMIT $offset,$num_per_page ");
				}
						
			}
			if (method_exists($this,'hook_liststories')) {
				$this->hook_liststories($uri);
			}
		}

		if ($res) {
			$i=1;
			if ($this->name == 'images') {
				section_content('<div class="grid" >');
				while ($row = sql_fetch_array($res)) {
					if ($jcow_page['type'] != 'page' && $jcow_page['type'] != 'group') {
						$res2 = sql_query("select * from jcow_streams where id='{$row['stream_id']}'");
						$stream = sql_fetch_array($res2);
						if (!stream_allow_access($stream)) {
							continue;
						}
					}
					if ($i<$num_per_page) {
						if (strlen($row['thumbnail'])) {
							if ($row['page_type'] == 'page') {
								$user = url('page/'.$row['page_uri'],h($row['page_name']));
							}
							else {
								$user = url('u/'.$row['username'],h($row['fullname']));
							}
							c('<div class="grid-item">
							<a href="'.url($this->name.'/viewstory/'.$row['id']).'">
							<img src="'.$row['thumbnail'].'" />
							</a>
							<a href="'.url($this->name.'/viewstory/'.$row['id']).'" class="jcow_gallery_title">'.h($row['title']).'</a>
							 <div class="sub">'.t('by {1}',$user).'</div>
							</div>');
						}
					}
					else {
						$got_next = 1;
					}
					$i++;
				}
				section_content('</div>');
				
			}
			elseif ($this->name == 'videos') {
				section_content('<div class="jcow_gallery">');
				while ($row = sql_fetch_array($res)) {
					if ($jcow_page['type'] != 'page' && $jcow_page['type'] != 'group') {
						$res2 = sql_query("select * from jcow_streams where id='{$row['stream_id']}'");
						$stream = sql_fetch_array($res2);
						if (!stream_allow_access($stream)) {
							continue;
						}
					}
					if ($i<$num_per_page) {
						section_content($this->theme_list_gallery($row));
						}
					else {
						$got_next = 1;
					}
					$i++;
				}
				section_content('</div>');
				
			}
			else {
				c(' ');
				while ($row = sql_fetch_array($res)) {
					if ($jcow_page['type'] != 'page' && $jcow_page['type'] != 'group') {
						$res2 = sql_query("select * from jcow_streams where id='{$row['stream_id']}'");
						$stream = sql_fetch_array($res2);
						if (!stream_allow_access($stream)) {
							continue;
						}
					}
					if ($i<$num_per_page) {
						section_content($this->theme_list_lines($row));
					}
					else {
						$got_next = 1;
					}
					section_close();
					$i++;
				}
				
			}
			if ($i == 1) {
				$config['hide_ad'] = 1;
			}

			// pager
			
			$paras = $ubase.$this->name;
			if ($parr[1]) {
				$paras .= '/'.$parr[1];
			}
			if ($parr[2]) {
				$paras .= '/'.$parr[2];
			}
			if ($parr[3]) {
				$paras .= '/'.$parr[3];
			}
			c(pager($offset,$got_next,$paras));
			section_close();
		}
		else {
			c(t('No entry'));
		}
		//
		//$smarty->assign('content',$content);
	}
	
	function theme_list_lines($row) {
		global $client,$uhome;
		if ($row['page_type'] == 'page') {
			$res = sql_query("select * from ".tb()."pages where id='{$row['page_id']}'");
			$page = sql_fetch_array($res);
			if (strlen($page['logo'])) {
				$page['avatar'] = $page['logo'];
			}
			else {
				$page['avatar'] = 'undefined.jpg';
			}
			$avatar = url('page/'.$page['uri'],'
	<img src="'.$uhome.'/'.uploads.'/avatars/s_'.$page['avatar'].'" class="avatar"  />');
		}
		else {
			$avatar = avatar($row);
		}
		$output = '';
		//$output .= '<div class="story userbox_'.$row['uid'].'" '.$row['bgimg'].'>';
		if (!$row['thumbnail'] && $this->default_thumb) {
			$row['thumbnail'] = $this->default_thumb;
		}
		$output .= '<div style="overflow:hidden">';
		if ($row['photos']) {
			$output .= '<a href="'.url($this->name.'/viewstory/'.$row['id']).'" style="display:block;width:150px;height:120px;margin:5px;overflow:hidden;float:left;background:url('.$row['thumbnail'].') no-repeat center top;background-size:150px auto;"></a>';
		}
		if ($row['page_type'] == 'page') {
			$user = url('page/'.$row['page_uri'],h($row['page_name']));
		}
		else {
			$user = url('u/'.$row['username'],h($row['fullname']));
		}
		$output .= '<div class="story_header">'.$this->list_title($row).'</div>';
		$output .= '<div class="sub">'.strip_tags(utf8_substr($row['content'],200,0)).'.. '.url($this->name.'/viewstory/'.$row['id'],t('Read more')).'</div>';
		$output .='<div class="tab_things">
		<div class="tab_thing">'.$user.'</div>';
		$output .= $this->theme_story_footer($row);
		if (method_exists($this,'hook_list_lines')) {
			$output .= $this->hook_list_lines($row);
		}
		if ($client['id'] == $row['uid'] || allow_access(3)) {
			if ($this->photos) {
				$output .= '<div class="tab_thing">'.url($this->name.'/managephotos/'.$row['id'], '<i class="fa fa-image"></i> '.t('Manage Photos')).'</div>';
			}
			$output .= '<div class="tab_thing">'.url($this->name.'/editstory/'.$row['id'], t('Edit')).'</div>';
			$output .= '<div class="tab_thing"><a class="deletestory" href="#" sid="'.$row['id'].'" markspam="0">Delete</a></div>';
		}
		if (allow_access(3)) {
			
			$output .= '<div class="tab_thing"><a class="deleteuser" href="#" uid="'.$row['uid'].'" markspam="1">Delete user</a></div>';
		}
		$output .= '</div>';
		
		//$output .= likes_get($row['stream_id']);
		$output .= '</div>';
		return $output;
	}

	function theme_list_gallery($row) {
		global $client;
		if (strlen($row['thumbnail'])) {
			if ($row['page_type'] == 'page') {
				$user = url('page/'.$row['page_uri'],h($row['page_name']));
			}
			else {
				$user = url('u/'.$row['username'],h($row['fullname']));
			}
			return '<div class="jcow_gallery_pin jcow_gallery_'.$this->name.'">
			<a href="'.url($this->name.'/viewstory/'.$row['id']).'" class="jcow_gallery_img" style="background:url('.$row['thumbnail'].') center bottom no-repeat;background-size:100% auto;">
			</a>
			<a href="'.url($this->name.'/viewstory/'.$row['id']).'" class="jcow_gallery_title">'.h($row['title']).'</a>
			 <div class="sub">'.t('by {1}',$user).'</div>
			</div>';
		}
	}
	function theme_story_footer($row) {
		$output .= '<div class="tab_thing">'.$this->list_created($row).'</div>';
		//$output .= '<div class="tab_thing">'.$this->list_username($row).'</div>';
		$output .= '<div class="tab_thing">'.$this->list_views($row).'</div>';
		return $output;
	}
	
	
	function story_line($row) {
			$line[] = $this->list_title($row);
			$line[] = $this->list_thumbnail($row);
			$line[] = $this->list_created($row);
			//$line[] = $this->list_username($row);
			$line[] = url($this->name.'/deletestory/'.$row['id'],'Delete');
			$line[] = url($this->name.'/editstory/'.$row['id'],'Edit');
			return $line;
	}

	// TAGs
	function tag($tid=0) {
		$this->liststories('tag');
		/*
		GLOBAL $nav,$num_per_page,$page,$ubase,$offset,$content,$title,$sub_menu, $cat_id, $uhome;
		$res = sql_query("select * from `".tb()."tags` where id='$tid'");
		if ($otag = sql_fetch_array($res)) {
			set_title($this->flag.' Tag: '.htmlspecialchars($otag['name']));
			$nav[] = 'Tag: <strong>'.url($this->name.'/tag/'.$otag['id'],h($otag['name'])).'</strong>';
			set_page_title($this->flag.' Tag: '.htmlspecialchars($otag['name']));
			$res = sql_query("select s.id,s.uid,s.created,s.title,u.avatar,u.username from `".tb()."tag_ids` as t left join `".tb()."stories` as s on s.id=t.sid left join `".tb()."accounts` as u on u.id=s.uid where t.tid='{$otag['id']}' ".dbhold('t')." order by t.sid DESC limit 30");
			while ($row = sql_fetch_array($res)) {
				$row['content'] = url($this->name.'/viewstory/'.$row['id'],htmlspecialchars($row['title']));
				c(user_post($row,0));
			}
		}
		else {
			die('tag not found');
		}
		*/
	}
	
	// 阅读文章
	function viewstory($sid) {
		GLOBAL $db,$ubase,$uhome,$nav,$content, $title, $page_title, $page, $client,$cat_id, $num_per_page, $offset, $config;
		enreport();
		if ($this->name != 'videos') {
			enable_jcow_inline_ad();
		}
		clear_as();
		$res = sql_query("select s.*,u.birthyear,u.gender,u.location,u.avatar,u.username,u.fullname,u.disabled,u.created as ucreated,u.lastlogin,u.forum_posts,u.followers from `".tb()."stories` as s left join `".tb()."accounts` as u on u.id=s.uid where s.id='$sid' ");
		$row = sql_fetch_array($res);
		if ($row['disabled'] == 3) die('marked as Spam');
		if (!$row['id']) die('wrong sid');
		$res = sql_query("select * from ".tb()."pages where id='{$row['page_id']}'");
		$jcow_page = sql_fetch_array($res);
		if (!$jcow_page['type']) die('unknown page id');

		if ($jcow_page['type'] == 'u' && $jcow_page['description'] == 'private') {
			if ($client['id']) {
				$res = sql_query("select * from jcow_friends where uid='{$client['id']}' and fid='{$jcow_page['uid']}'");
				if (sql_counts($res)) {
					$is_friend = 1;
				}
			}
		}
		if ($jcow_page['type'] == 'group') {
			include_once('modules/group/group.php');
			group::settabmenu($row['page_id'],1);
		}
		$res = sql_query("select * from jcow_streams where id='{$row['stream_id']}'");
		$stream = sql_fetch_array($res);
		if (strlen($row['title'])) {
			$title = $this->title_prefix.$row['title'];
		}
		if (!$row['id']) {
			die(':)');
		}
		if ($row['cid']) {
			$cat = valid_category($row['cid']);
			$cat_id = $row['cid'];
			$closed = $row['closed'];
			$this->set_current_sub_menu($row['id']);
		}
		$this->check_private($stream['type'],$row['uid']);
			// 更新文章阅读数目
			sql_query("update `".tb()."stories` set views=views+1 where id='$sid'");
			/*
			if (!$this->disable_category) {
				$nav[] = url($this->name.'/liststories/'.$cat['id'],$cat['name']);
			}
			*/
			if (strlen($row['title'])) {
				$nav[] = $page_title = htmlspecialchars($row['title']);
			}

			
			// table view
			$story .= $this->story_content($row);
			
			if (strlen($row['tags'])) {
				$story .= $this->show_tags($row);
			}
			

			$story .= '<div id="sp_block_content_bottom">'.$config['sp_content_bottom'].show_ad('sp_block_content_bottom').'</div>';


			if (is_array($this->story_opts)) {
				$story .= '<script>
					$(document).ready( function(){
						$("#add_to_favorite").click(function() {
							$("#add_to_favorite").replaceWith("<img id=\'add_to_favorite\' src=\''.$uhome.'/files/loading.gif\' width=16 height=16 />");
							$.post("'.$uhome.'/index.php?p=jquery/favoriteadd",{sid:$("#story_id").val()},function(data) {
								$("#add_to_favorite").replaceWith(data);
							},\'html\');
						});
				});
				</script>
				<table border="0"><tr>';
				$story .= '</tr></table>';
			}

			if ($this->social_bookmarks && !get_gvar('private_network')) {
				$encoded_url = urlencode(url($this->name.'/viewstory/'.$sid));
				$encoded_title = urlencode($row['title']);
			}
			c($story);
			/*
			$res = sql_query("select c.*,u.avatar,u.fullname,u.username from `".tb()."comments` as c left join `".tb()."accounts` as u on u.id=c.uid where c.target_id='{$row['id']}' ORDER BY c.id ASC LIMIT 50");
			if (sql_counts($res)) {
				c('<div style="height:20px;font-size:20px;margin:50px 0 30px 0">'.t('Comments').'</div>');
			}
			while ($comment = sql_fetch_array($res)) {
				if ($client['id'] && $client['id'] != $comment['uid']) {
					$res2 = sql_query("select id from ".tb()."pages where uid='{$comment['uid']}' and type='u'");
					$row2 = sql_fetch_array($res2);
					$pageid = $row2['id'];
					$res2 = sql_query("select * from ".tb()."followers where uid='{$client['id']}' and fid='{$comment['uid']}' limit 1");
					if (!sql_counts($res2)) {
						$follow_button = '<span><a class="jcow_button dofollow" page_id="'.$pageid.'" href="#"><img src="'.uhome().'/files/icons/add.png" /> '.t('Follow').'</a></span>';
					}
					else {
						$follow_button = '';
					}
				}
				if ($client['id']) {
					if (allow_access(3) || $client['id'] == $comment['uid']) {
						$comment['opt'] = ' '.url($this->name.'/deletecomment/'.$comment['id'],t('Delete')).' | <a href="'.url($this->name.'/editcomment/'.$comment['id']).'">'.t('Edit').'</a>';
					}
				}
				//$cmt_form = story_like_form($comment['stream_id'],array('reply_link'=>1));
				$cmt_form = likes_get($comment['stream_id']).comment_get($comment['stream_id'],3).comment_form($comment['stream_id'],t('Reply'));
				c('<div class="user_post_1 userbox_1">
					<div class="user_post_avatar">'.avatar($comment).'</div>
					<div class="user_post_content">
						<div class="user_post_h">
							<div class="user_post_h_l"><a rel="nofollow" href="'.url('u/'.$comment['username']).'">'.h($comment['fullname']).' <span>@'.$comment['username'].'</span></a> '.$follow_button.'</div>
							<div class="user_post_h_r">'.get_date($comment['created']).' '.$comment['opt'].'</div>
						</div>
						'.nl2br(decode_bb(h($comment['message']))).'
						'.$cmt_form.'
					</div>
					</div>');
			}
			
			section_close();
			c('
				<script>
				$(document).ready( function(){
					author_uname = "'.$row['username'].'";
					$(".reply_comment").click( function() {
						username = $(this).attr("username");
						if (username != author_uname) {
							$("#comment_content").val(
								$("#comment_content").val()+" @"+username
								);
					}
						$("#comment_content").focus();
						return false;
					});
				});
			</script>
			<form method="post" action="'.$ubase.$this->name.'/writecommentpost">
						<p>
						<textarea name="comment_content" id="comment_content" rows="12" style="width:480px">@'.$row['username'].' </textarea>
						</p>
						<input type="hidden" name="sid" value="'.$row['id'].'" />
						<div style="width:480px;text-align:right">
						<input style="font-size:15px" type="submit" value="'.t('Submit').'" />
						</div>
						</form>');
			section_close(t('Your comment'));
			*/
			if (strlen(get_gvar('jcow_inline_banner_ad')) && !$config['hide_ad']) {
				c(get_gvar('jcow_inline_banner_ad'));
				global $adbanner_displayed;
				$adbanner_displayed = 1;
			}
			if (method_exists($this,'hook_story_comments')) {
				$story .= $this->hook_story_comments($row);
			}
			else {
				c('<div class="hr"></div>'.
					comment_get($row['stream_id'],100).comment_form($row['stream_id']).likes_get($row['stream_id'])
					);
			}
	}

	function show_tags($row) {
		if (strlen($row['tags'])) {
			$res = sql_query("select i.tid,t.name from ".tb()."tag_ids as i left join ".tb()."tags as t on t.id=i.tid where i.sid='{$row['id']}'");
			if (sql_counts($res)) {
				$output .= '<span style="overflow:hidden">';
				while ($row = sql_fetch_array($res)) {
					$output .= '<a href="'.url($this->name.'/liststories/tag/'.$row['tid']).'" class="jcow_tags">'.h($row['name']).'</a>';
				}
				$output .= '</span>';
			}
		}
		return $output;
	}
	
	function comment_author($row) {
		$output = '<div class="post_author">';
		$output .= avatar($row).'<br />'.url('u/'.$row['username'],$row['username']);
		$output .= '</div>';
		return $output;
	}

	// content 
	function story_content($row) {
		global $client, $nav, $defined_current_tab;
		if (strlen($row['title'])) {
			$title_link = url($this->name.'/viewstory/'.$row['id'],$this->title_prefix.h($row['title']));
			}
		$res = sql_query("select * from ".tb()."pages where id='{$row['page_id']}'");
		$page = sql_fetch_array($res);
		$defined_current_tab = $this->name.'/liststories/page_'.$page['id'];
		$res = sql_query("select * from jcow_streams where id='{$row['stream_id']}'");
		$stream = sql_fetch_array($res);
			$story = '<div class="tab_things">'.$this->theme_story_footer($row);
			if ($client['id'] == $row['uid'] || allow_access(3)) {
				if ($this->photos) {
					$managephoto = '<div class="tab_thing">'.url($this->name.'/managephotos/'.$row['id'], '<i class="fa fa-image"></i> '.t('Manage Photos')).'</div>';
				}
				if ($this->name == 'images' && strlen($row['var2'])) {
					$managephoto = '';
				}
				$story .= $managephoto;
				$story .= '<div class="tab_thing">'.url($this->name.'/editstory/'.$row['id'], t('Edit')).'</div>';
				$story .= '<div class="tab_thing">'.url($this->name.'/deletestory/'.$row['id'], t('Delete')).'</div>';
			}
			if (allow_access(3)) {
				/*
				if ($stream['type'] == 0) {
					$story .= '<div class="tab_thing"><a href="#" class="stream_hot_btn" sid="'.$stream['id'].'"><i class="fa fa-fire"></i> Make it hot</a></div>';
				}
				elseif ($stream['type'] ==2) {
					$story .= '<div class="tab_thing"><a href="#" class="stream_hot_btn" sid="'.$stream['id'].'"><i class="fa fa-fire"></i> Undo make hot</a></div>';
				}
				*/
				if (!$row['featured']) {
					$story .= '<div class="tab_thing"><a href="#" class="story_feature_btn" sid="'.$row['id'].'"><i class="fa fa-fire"></i> Feature this</a></div>';
				}
				else{
					$story .= '<div class="tab_thing"><a href="#" class="story_feature_btn" sid="'.$row['id'].'"><i class="fa fa-fire"></i> Un-feature this</a></div>';
				}
				
			}

			$story .= '</div>';
			if ($client['id'] && $client['id'] != $row['uid']) {
				$res2 = sql_query("select * from ".tb()."followers where uid='{$client['id']}' and fid='{$row['uid']}' limit 1");
				if (!sql_counts($res2)) {
					$follow_button = '<span><input type="hidden" value="'.$row['uid'].'" /><a class="jcow_button dofollowuser" href="#"><img src="'.uhome().'/files/icons/add.png" /> '.t('Follow').'</a></span>';
				}
				else {
					$follow_button = '<i>'.t('Following').'</i>';
				}
			}
			$story .= '<input type="hidden" value="'.$row['id'].'" name="story_id" id="story_id" />';
			if ($page['type'] == 'page') {
				if ($client['id']) {
					$res = sql_query("select * from ".tb()."page_users where uid='{$client['id']}' and pid='{$page['id']}'");
					if (sql_counts($res)) {
						$like_link = '<span><a class="jcow_button dofollow" href="#" page_id="'.$page['id'].'">'.t('Un-follow').'</a></span>';
					}
					else {
						$like_link = '<span><a class="jcow_button dofollow" href="#" page_id="'.$page['id'].'"><i class="fa fa-plus"></i> '.t('Follow').'</a></span>';
					}
				}
				$story = '
				<table>
			<tr><td width="60">
			'.page_logo($page).'</td>
			<td align="left">
			<a href="'.url('page/'.$page['uri']).'">'.h($page['name']).'</a> '.$like_link.'
			<div style="font-size:20px;font-weight:bold">'.$title_link.'</div>
			</td></tr>
			</table>
			'.$story;
			}
			else {
				$story = '
			<table>
		<tr><td width="60">
		'.avatar($row).'</td>
		<td align="left">
		<a href="'.url('u/'.$row['username']).'">'.h($row['fullname']).' <span class="sub">(@'.$row['username'].')</sub></a>
		<div style="font-size:20px;font-weight:bold">'.$title_link.'</div>
		</td></tr>
		</table>
		'.$story;
			}
			$story .= '<div id="ad_block_content_top">'.show_ad('ad_block_content_top').'</div>';
			if (method_exists($this,'hook_viewstory')) {
				$story .= $this->hook_viewstory($row);
			}

			$story .= $this->view_content($row);
			if (method_exists($this,'hook_viewstorybottom')) {
				$story .= $this->hook_viewstorybottom($row);
			}
			return $story;
	}
	
	
	function check_private($stream_type,$uid) {
		if ($stream_type == 1 && !privacy_access(1,$uid)) {
				c(t('Sorry, this content is {1}','<strong>'.t('Friends only').'</strong>'));
				stop_here();
		}
	}

	function comment_line($row) {
			$line[] = $this->comment_created($row);
			$line[] = $this->comment_username($row);
			if ($client['id'] == 1) {
				$line[] = url($this->name.'/deletecomment/'.$row['id'],'Delete');			
			}
			$line[] = $this->comment_content($row);
			return $line;
	}

	function writestory($page_id=0) {
		//do_auth(explode('|',get_gvar('permission_add')));
		clear_as();
		limit_posting(1,0,'story');
		GLOBAL $ubase,$nav, $title, $client, $sub_menu, $ass,$current_app,$cat_id;
		if ($page_id) {
			$page = $this->check_page_access($page_id);
			$page_id = $page['id'];
			$this->tags = 0;
		}
		else {
			$page_id = $client['page']['id'];
			$page = $this->check_page_access($page_id);
		}
		if ($page['type'] != 'u') {
			c('<h2>'.h($page['name']).'</h2>');
		}
		$cat_id = $cid;
		$sub_menu = $ass = '';
		$nav[] = $this->write_story;
		$this->set_current_sub_menu($cid);
		$title = $this->write_story;
		section_content('<div class="form"><form action="'.$ubase.$this->name.'/'.$this->writepost.'" class="ajaxform" method="post"  enctype="multipart/form-data">');
		if (!$this->disable_category) {
			c($this->story_form_cat($cid));
		}
		section_content($this->writestory_form_elements($row));

		if (method_exists($this,'hook_writestory')) {
			c( $this->hook_writestory($cid));
		}
		if ($page['type'] == 'u' && !$this->disable_privacy) {
			section_content(privacy_form($row));
		}
		/*
		if ($this->photos && $this->name != 'photos') {
			section_content('<p>'.label(t('Photos')).'<input type="checkbox" name="photos" value="1" />'.t('Upload photos on next step').'</p>';
		}
		*/
		section_content('<p><input class="button" type="submit" value="'.$this->submit.'" /></p>');
		section_content('<input type="hidden" name="page_id" value="'.$page_id.'" /></form></div>');
	}
	
	// 处理评论表单
	function writecommentpost() {
		do_auth($this->comment_write);
		GLOBAL $db,$client,$ubase;
		limit_posting(1,0,'story');
		$timeline = time();
		//get_r(array('sid','content'));
		if (!$story = valid_story($_POST['sid'])) {
			sys_back('wrong sid');
		}
		if ($story['closed']) {
			sys_back('topic closed');
		}
		if (strlen($_POST['comment_content']) < 10) {
			sys_back(t('Your message is too short'));
		}
		if ($res = sql_query("insert into `".tb()."comments` (target_id,message,uid,created) VALUES ('".$_POST['sid']."','".$_POST['comment_content']."','{$client['id']}',$timeline)")) {
			$pid = insert_id();
			$posturl = url($this->name.'/viewstory/'.$story['id']);
			$posturl = '<a href="'.$posturl.'">'.addslashes(h($story['title'])).'</a>';
			$attachment = array(
					'cwall_id' => 'none',
					'des' => stripslashes(h(utf8_substr($_POST['form_content'],60)))
					);
			$app = array('name'=>'comments','id'=>$pid);
			//$stream_id = stream_publish(addslashes(t('Comment')).': '.$posturl,$attachment,$app,$client['id']);
			$stream_id = stream_publish(addslashes(t('Comment')).': '.$posturl,'',$app,$client['id']);
			sql_query("update ".tb()."comments set stream_id='$stream_id' where id='$pid'");

			$return = parse_mentions($_POST['comment_content']);
			add_mentions($return['mentions'],$stream_id);
			sql_query("update `".tb()."stories` set comments=comments+1 where id='".$_POST['sid']."'");
			
		}
		redirect($ubase.$this->name.'/viewstory/'.$_POST['sid'],1);
	}
	
	// 文章表单
	

	function check_page_access($page_id) {
		global $client;
		$res = sql_query("select * from ".tb()."pages where id='{$page_id}'");
		$page = sql_fetch_array($res);
		if (!$page['id']) die('wrong page id');
		if ($page['type'] == 'u') {
			if ($page['uid'] != $client['id']) {
				die('access denied');
			}
		}
		elseif($page['type'] == 'page') {
			if ($page['uid'] != $client['id']) {
				die('only page owner can post');
			}
		}
		return $page;
	}
	
	function story_form_cat($cid) {
		global $current_app;
		$output = '<p>'.label('Category').'<select name="cid" class="inputText">';
		if ($current_app['cat_group']) {
			$res = sql_query("select g.* from `".tb()."story_cat_groups` as g where  app='{$this->name}' order by weight");
			while ($group = sql_fetch_array($res)) {
				$output .= '<optgroup label="'.$group['name'].'">';
				$res2 = sql_query("select c.* from `".tb()."story_categories` as c where  gid={$group['id']} order by weight");
				while($row = sql_fetch_array($res2)) {
					if(is_numeric($cid) && $cid == $row['id']) {
						$row['selected'] = 'selected';
					}
					$output .='<option value="'.$row['id'].'" '.$row['selected'].' >'.$row['name'].'</option>';
					if(is_numeric($cid) && $cid == $row['id']) {
						$row['on'] = 1;
					}
				}
				$output .='</optgroup>';
			}
		}
		else {
			$res = sql_query("select c.* from `".tb()."story_categories` as c where  app='{$this->name}' order by weight DESC");
			while($row = sql_fetch_array($res)) {
				if(is_numeric($cid) && $cid == $row['id']) {
					$row['selected'] = 'selected';
				}
				$output .='<option value="'.$row['id'].'" '.$row['selected'].' >'.$row['name'].'</option>';
				if(is_numeric($cid) && $cid == $row['id']) {
					$row['on'] = 1;
				}
			}
		}
		$output .='</select></p>';
		return $output;
	}

	function writestory_form_elements($row) {
		global $parr,$client;
		if (!$client['id']) return '';
		$foo = $this->story_form_title($row);
		$foo .= $this->story_form_content($row);
		if ($parr[0] == 'blogs' && $parr[1] == 'writestory') {
			$res = sql_query("select * from jcow_pages where uid='{$client['id']}' and type='page'");
			while ($page = sql_fetch_array($res)) {
				$opt .= '<option value="'.$page['id'].'">'.h($page['name']).'</option>';
			}
			if ($opt) {
				$foo .= '<p>'.t('Post as').' <select name="opt_page_id"><option value="'.$client['page']['id'].'">'.h($client['fullname']).'</option>'.$opt.'</select></p>';
			}
		}
		if ($this->tags) {
			$foo .= $this->story_form_tags($row);
		}
		return $foo;
	}
	
	// edit story
	function editstory($sid) {
		GLOBAL $ubase,$content,$sub_menu, $ass, $client;
		if ($client['disabled']) {
			c(t('You can not edit post before being verified'));
			stop_here();
		}
		clear_as();
		$sub_menu = $ass = '';
		if (!$story = valid_story($sid)) {
			 ('Wrong sid');
		}
		if ($story['page_type'] == 'group') {
			$this->tags = 0;
		}

		do_auth($this->story_edit, $story['uid']);
		$this->set_current_sub_menu($story['cid']);

		section_content('<div class="form">
		<form action="'.$ubase.$this->name.'/editstorypost" method="post">');
		if (!$this->disable_category) {
			section_content($this->story_form_cat($story['cid']));
		}
		c($this->writestory_form_elements($story));
		if (method_exists($this,'hook_editstory')) {
			section_content($this->hook_editstory($story));
		}
		if ($story['page_type'] == 'u' && !$this->disable_privacy) {
			section_content(privacy_form($story));
		}
		section_content('<input type="hidden" name="sid" value="'.$story['id'].'" />');
		section_content('<p><input class="button" type="submit" value="'.$this->savechanges.'" /></p>');
		section_content('</form></div>');

		
		
	}
	
	function writestorypost() {
		do_auth($this->story_write);
		GLOBAL $db,$client,$ubase;
		if ($client['disabled']) {
			c(t('You can not edit post before being verified'));
			stop_here();
		}
		//get_r(array('title','content','cid'));
		$timeline = time();
		//if (!valid_category($_POST['cid'])) {
		//	sys_back('wrong cid');
		//}
		if (!$_POST['title'] || !$_POST['form_content']) {
			form_stop(t('Please fill all requried blanks'));
		}
		if (is_numeric($_POST['opt_page_id'])) {
			$_POST['page_id'] = $_POST['opt_page_id'];
		}
		if (!is_numeric($_POST['page_id'])) form_stop("need a page id");
		$page = $this->check_page_access($_POST['page_id']);

		$story = array(
			'cid' => $_POST['cid'],
			'page_id'=>$_POST['page_id'],
			'page_type'=>$page['type'],
			'title' => $_POST['title'],
			'var5' => $_POST['privacy'],
			'content' => $this->convert_content_before_insert($_POST['form_content']),
			'uid' => $client['id'],
			'created' => time(),
			'tags'=>$_POST['tags'],
			'closed' => $_POST['closed'],
			'app' => $this->name
			);

		if (method_exists($this,'hook_writestorypost') ) {
			section_content($this->hook_writestorypost($story));
		}

		if ($this->allow_vote) {
			foreach ($this->vote_options as $key=>$vla) {
				$ratings[$key] = array('score'=>0,'users'=>0);
			}
			$story['rating'] = serialize($ratings);
		}
		limit_posting(0,0,'story');

		if (sql_insert($story, tb().'stories')) {
			$sid = $story['id'] = insert_id();
			if (method_exists($this,'hook_writestorypostdone')) {
				section_content($this->hook_writestorypostdone($story));
			}
			if (strlen($_POST['tags'])) {
				parse_save_tags($_POST['tags'],$sid,$this->name);
			}

			// write act
			if (strlen($this->act_write)) {
				$attachment = array(
					'cwall_id' => $this->name.$sid,
					'uri' => $this->name.'/viewstory/'.$sid,
					'name' => $_POST['title']
					);
				$app = array('name'=>$this->name,'id'=>$sid);
				$args = array(
					'message'=>addslashes($this->act_write),
					'link' => $this->name.'/viewstory/'.$sid,
					'name' => stripslashes($_POST['title']),
					'app' => $this->name,
					'aid' => $sid
					);
				if ($this->hide_feed == 1) {
					$hide = 1;
				}
				else {
					$hide = 0;
				}
				$stream_id = jcow_page_feed($_POST['page_id'],$args,$client['id'],$hide);

				$set_story['id'] = $sid;
				$set_story['stream_id'] = $stream_id;
				sql_update($set_story,tb()."stories");
			}
			$return = parse_mentions($_POST['form_content']);
			add_mentions($return['mentions'],$stream_id);
			if (!$this->redirect_writestorypost) {
				if ($this->tags) {
					save_tags($stags,$sid,$this->name);
				}
				if ($this->name == 'images') {
					form_go(url($this->name.'/managephotos/'.$sid));
				}
				else {
					form_go(url($this->name.'/viewstory/'.$sid));
				}
			}
			else {
				form_go(url($this->redirect_writestorypost));
			}
		}

	}
	
	// 处理文章编辑
	function editstorypost() {
		GLOBAL $db,$client,$ubase;
		//get_r(array('title','content','sid'));
		$timeline = time();
		//get_r(array('sid','title','content'));
		if (!$story = valid_story($_POST['sid'])) {
			sys_back('wrong sid');
		}
		do_auth($this->story_edit, $story['uid']);
		$setstory = array(
			'id' => $_POST['sid'],
			'title' => $_POST['title'],
			'cid' => $_POST['cid'],
			'content' => $this->convert_content_before_insert($_POST['form_content']),
			'updated' => time(),
			'tags' => $_POST['tags'],
			'closed' => $_POST['closed']
			);
		if (method_exists($this,'hook_editstorypost')) {
			$this->hook_editstorypost($setstory);
		}
		$res = sql_query("select type from jcow_streams where id='{$story['stream_id']}'");
		$stream = sql_fetch_array($res);
		if ($stream['type'] != 1) {
			parse_save_tags($_POST['tags'],$story['id'],$this->name);
		}
		/*
		if ($this->tags) {
			save_tags($stags,$_POST['sid'],$this->name);
		}
		*/
		sql_update($setstory, tb().'stories', array('id'=>$_POST['sid']));
		redirect($ubase.$this->name.'/viewstory/'.$_POST['sid']);
	}

	//
	function managephotos($sid) {
		GLOBAL $ubase,$content,$sub_menu, $ass;
		clear_as();
		do_auth( explode('|',get_gvar('permission_upload')) );
		$sub_menu = $ass = '';
		if (!$story = valid_story($sid)) {
			 ('Wrong sid');
		}

		c('<form action="'.$ubase.$this->name.'/managephotos_upload" method="post" enctype="multipart/form-data">
						<script  type="text/javascript">
						$(document).ready( function(){
								$("#add_another_photo").click(function() {
									$("#add_another_photo_box").before("<tr><td><input type=\"file\" name=\"photos[]\" /></td><td><input type=\"text\" size=\"35\" name=\"descriptions[]\" /></td></tr>");
								});
						});
	</script>		
		 				<fieldset>
						<legend>'.t('Upload image').'</legend>

						<table border="0">
						<tr><td>'.t('Photo').'</td><td>'.t('Description').'</td></tr>
		<tr><td><input type="file" name="photos[]" /></td><td><input type="text" size="35" name="descriptions[]" /></td></tr>
		
		<tr id="add_another_photo_box"><td colspan="2"></td></tr>
		</table>
						<input type="submit" class="button" value="'.t('Upload').'" />
						</fieldset>
						<input type="hidden" name="sid" value="'.$story['id'].'" />
						[ '.url($this->name.'/viewstory/'.$story['id'],t('Finished')).' ] 
				</form>');
		c('<h1>'.url($this->name.'/viewstory/'.$story['id'],h($story['title'])).'</h1>');
		do_auth($this->story_edit, $story['uid']);
		set_page_title(t('Photos of {1}','"'.url($this->name.'/viewstory/'.$sid,htmlspecialchars($story['title'])).'"'));
		$this->set_current_sub_menu($story['cid']);
		

		// current pics
		$res = sql_query("select * from `".tb()."story_photos` where sid='{$story['id']}' "." order by id ASC");
		while ($row = sql_fetch_array($res)) {
			$photos[] = $row;
		}
		$i = 1;
		if (is_array($photos)) {
			c('<fieldset><legend>'.t('Uploaded images').'</legend>');
			if (get_class($this) == 'texts') {
				c('<div style="width:100%;clear:both">
				'.t('You can put images to texts by inserting {1}','<strong>{image1}</strong>,<strong>{image2}</strong>,<strong>{image3}</strong>,..').'
				</div>');
			}
			c('	<ul class="gallery">');
			$images = unserialize($story['text1']);
			foreach ($photos as $photo) {
				c('<li>{image'.$i.'}<br /><img src="'.uhome().'/'.$photo['thumb'].'" /><br />'.url($this->name.'/managephotos_delete/'.$photo['id'],t('Delete')));
				if ($story['thumbnail'] != $photo['thumb']) {
					c(' | '.url($this->name.'/managephotos_thumb/'.$photo['id'],t('Use as thumbnail')));
				}
				else {
					c(' ('.t('Thumbnail').')');
				}

				c('</li>');
				$i++;
			}

			c('</ul></fieldset>');
		}

	}

	function managephotos_upload() {
		GLOBAL $db,$client,$ubase;
		$timeline = time();
		if (!$story = valid_story($_POST['sid'])) {
			sys_back('wrong sid');
		}
		do_auth($this->story_edit, $story['uid']);
		// up pic
		foreach ($_FILES['photos']['tmp_name'] as $key=>$file_tmp_name) {
			if ($file_tmp_name) {
				$uploaded = 1;
				$photo = array('name'=>$_FILES['photos']['name'][$key],
				'tmp_name'=>$file_tmp_name,
				'type'=>$_FILES['photos']['type'][$key],
				'size'=>$_FILES['photos']['size'][$key]);
				list($width, $height) = getimagesize($photo['tmp_name']);
				if ($width <= 1200) {
					$uri = save_file($photo);
				}
				else {
					$height = floor(1200*$height/$width);
					$uri = save_thumbnail($photo, 1200,0);
				}
				$story['photos']++;
				if ($width <= 180) {
					$thumb = $uri;
				}
				else {
					$thumb = save_thumbnail($photo, 180, 230);
				}
				$size = $photo['size'];
				sql_query("insert into `".tb()."story_photos` (sid,uri,des,thumb,size) values( {$story['id']},'$uri','".$_POST['descriptions'][$key]."','$thumb','$size')");
			}
		}
		
		if ($uploaded) {
			$set_story['thumbnail'] = $thumb;
			$set_story['id'] = $story['id'];
			$set_story['photos'] = $story['photos'];
			sql_update($set_story, tb().'stories');
			
			if (strlen($this->act_write)) {
				$res = sql_query("select thumb from ".tb()."story_photos where sid='{$story['id']}' order by id DESC limit 2");
				while ($photo = sql_fetch_array($res)) {
					$thumbs[] = $photo['thumb'];
					$pics = ' ('.$set_story['photos'].')';
				}
				$attachment = array(
						'cwall_id' => $this->name.$story['id'],
						'uri' => $this->name.'/viewstory/'.$story['id'],
						'name' => addslashes($story['title']),
						'thumb' => $thumbs
						);
				$app = array('name'=>$this->name,'id'=>$story['id']);
				$stream_id = stream_update(addslashes($this->act_write),$attachment,$app, $story['stream_id']);
			}

			redirect($ubase.$this->name.'/managephotos/'.$_POST['sid'], 1);
		}
		else {
			die('sorry,failed to upload pic');
		}
	}
	function managephotos_delete($pid) {
		GLOBAL $client,$ubase;
		$res = sql_query("select * from `".tb()."story_photos` where id='$pid' ");
		$photo = sql_fetch_array($res);
		if (!$photo['id']) {
			die('no this photo');
		}
		if (!$story = valid_story($photo['sid'])) {
			sys_back('wrong sid');
		}
		do_auth($this->story_edit, $story['uid']);
		//delete
		sql_query("delete from `".tb()."story_photos` where id='$pid' ");
		@unlink($photo['uri']);
		@unlink($photo['thumb']);
		//story thumb
		if ($story['thumbnail'] == $photo['thumb']) {
			$res = sql_query("select * from `".tb()."story_photos` where sid='{$story['id']}' "." limit 1");
			$ophoto = sql_fetch_array($res);
			if ($ophoto['id']) {
				sql_query("update `".tb()."stories` set thumbnail='{$ophoto['thumb']}' where id={$story['id']} ");
			}
			else {
				sql_query("update `".tb()."stories` set thumbnail='' where id={$story['id']} ");
			}
		}
		sql_query("update `".tb()."stories` set photos=photos-1 where id={$story['id']} ");

		if (strlen($this->act_write)) {
			$res = sql_query("select thumb from ".tb()."story_photos where sid='{$story['id']}' order by id DESC limit 2");
			while ($photo = sql_fetch_array($res)) {
				$thumbs[] = $photo['thumb'];
				$pics = ' ('.$set_story['photos'].')';
			}
			$attachment = array(
					'cwall_id' => $this->name.$story['id'],
					'uri' => $this->name.'/viewstory/'.$story['id'],
					'name' => addslashes($story['title']),
					'thumb' => $thumbs
					);
			$app = array('name'=>$this->name,'id'=>$story['id']);
			$stream_id = stream_update(addslashes($this->act_write),$attachment,$app, $story['stream_id']);
		}
		redirect($ubase.$this->name.'/managephotos/'.$story['id'], 1);
	}

	function managephotos_thumb($pid) {
		GLOBAL $client,$ubase;
		$res = sql_query("select * from `".tb()."story_photos` where id='$pid' ");
		$photo = sql_fetch_array($res);
		if (!$photo['id']) {
			die('no this photo');
		}
		if (!$story = valid_story($photo['sid'])) {
			sys_back('wrong sid');
		}
		do_auth($this->story_edit, $story['uid']);
		sql_query("update ".tb()."stories set thumbnail='{$photo['thumb']}' where id='{$story['id']}'");
		redirect($ubase.$this->name.'/managephotos/'.$story['id'], 1);
	}

	// 删除文章
	function deletestory($sid) {
		GLOBAL $db,$client,$ubase;
		if (!$story = valid_story($sid)) {
			sys_back('wrong sid');
		}
		do_auth($this->story_delete, $story['uid']);
		$res = sql_query("DELETE from `".tb()."stories` where id='$sid'");
		if ($res) {
			sql_query("delete from ".tb()."streams where id='{$story['stream_id']}'");
			sql_query("delete from ".tb()."votes where sid='{$story['stream_id']}'");
			//delete photos
			$res2 = sql_query("select * from ".tb()."story_photos where sid='$sid'");
			while ($photo = sql_fetch_array($res2)) {
				@unlink($photo['uri']);
				@unlink($photo['thumb']);
				sql_query("delete from ".tb()."story_photos where id='{$photo['id']}'");
			}
		}

		$cat = valid_category($story['cid']);
		redirect($ubase.$this->name);
	}

	function newdeletestory($sid=0,$markspammer=0) {
		GLOBAL $db,$client,$ubase;
		if (!$story = valid_story($sid)) {
			sys_back('wrong sid');
		}
		do_auth($this->story_delete, $story['uid']);
		$res = sql_query("DELETE from `".tb()."stories` where id='$sid'");
		if ($res) {
			sql_query("delete from ".tb()."streams where id='{$story['stream_id']}'");
			//delete photos
			$res2 = sql_query("select * from ".tb()."story_photos where sid='$sid'");
			while ($photo = sql_fetch_array($res2)) {
				@unlink($photo['uri']);
				@unlink($photo['thumb']);
				sql_query("delete from ".tb()."story_photos where id='{$photo['id']}'");
			}
		}
		if ($markspammer && allow_access(3) && $story['uid'] != 1) {
			sql_query("update ".tb()."accounts set disabled=3 where id='{$story['uid']}'");
		}
		echo '<font color="red">Deleted</font>';

		exit;
	}

	// 推荐
	function feature_story($sid) {
		GLOBAL $db,$client,$ubase;
		if (!$story = valid_story($sid)) {
			sys_back('wrong sid');
		}
		do_auth(3);
		$res = sql_query("UPDATE `".tb()."stories` set featured=1 where id='$sid' ");
		redirect($this->name.'/viewstory/'.$story['id']);
	}
	function unfeature_story($sid) {
		GLOBAL $db,$client,$ubase;
		if (!$story = valid_story($sid)) {
			sys_back('wrong sid');
		}
		do_auth(3);
		$res = sql_query("UPDATE `".tb()."stories` set featured=0 where id='$sid' ");
		//save_topics(array(),$sid,'');//clear topics
		redirect($this->name.'/viewstory/'.$story['id']);
	}

	function deletecomment($cid) {
		global $client;
		need_login();
		if (!$comment = valid_comment($cid)) {
			sys_back('wrong cid');
		}
		if ($comment['uid'] != $client['id'] && !allow_access(3)) die('access denied');
		sql_query("DELETE from `".tb()."comments` where id='$cid'");
		sql_query("delete from ".tb()."streams where id='{$comment['stream_id']}'");
		sql_query("update ".tb()."stories set comments=comments-1 where id='{$comment['target_id']}'");
		redirect($this->name.'/viewstory/'.$comment['target_id']);
	}

	function editcomment($cid) {
		global $client;
		need_login();
		if (!$comment = valid_comment($cid)) {
			sys_back('wrong cid');
		}
		if ($comment['uid'] != $client['id'] && !allow_access(3)) die('access denied');
		$comment_limit_hours = get_gvar('comment_limit_hours');
		if (!$comment_limit_hours) $comment_limit_hours = 48;
		$timeline = time() - $comment_limit_hours*3600;
		if ($comment['created']<$timeline && !allow_access(3)) {
			sys_back('Sorry, you can only edit comments posted in <strong>'.$comment_limit_hours.'</strong> hours');
		}
		c('<form method="post" action="'.$ubase.$this->name.'/editcommentpost/'.$cid.'">
						<p>
						<textarea name="comment_content" rows="15" style="width:480px">'.h($comment['message']).' </textarea>
						</p>
						<div style="width:480px;text-align:right">
						<input style="font-size:15px" type="submit" value="'.t('Save changes').'" />
						</div>
						</form>');
	}
	function editcommentpost($cid) {
		global $client;
		need_login();
		if (!$comment = valid_comment($cid)) {
			sys_back('wrong cid');
		}
		if ($comment['uid'] != $client['id'] && !allow_access(3)) die('access denied');
		$comment_limit_hours = get_gvar('comment_limit_hours');
		if (!$comment_limit_hours) $comment_limit_hours = 48;
		$timeline = time() - $comment_limit_hours*3600;
		if ($comment['created']<$timeline && !allow_access(3)) {
			sys_back('Sorry, you can only edit comments posted in <strong>'.$comment_limit_hours.'</strong> hours');
		}
		if (strlen($_POST['comment_content']) < 10) {
			sys_back(t('Your message is too short'));
		}
		sql_query("update ".tb()."comments set message='".$_POST['comment_content']."' where id='$cid'");
		redirect($this->name.'/viewstory/'.$comment['target_id'],1);
	}


	// story form
	function story_form_title($row = array()) {
		if (strlen($_POST['title']))
			$row['title'] = $_POST['title'];
		return '<p>'.label($this->label_title).'<input type="text" class="form-control" style="width:550px" name="title" value="'.htmlspecialchars($row['title']).'" /></p>';
	}
	function story_form_tags($row = array()) {
		if (is_numeric($row['stream_id']) && $row['stream_id']) {
			$res = sql_query("select type from jcow_streams where id='{$row['stream_id']}'");
			$stream = sql_fetch_array($res);
			if ($stream['type'] == 1) {
				return '';
			}
		}
		return '
		<p>'.label(t('Tags')).'
		<input type="text" size="25" id="tags" name="tags" value="'.h($row['tags']).'" /> <span class="sub">('.t('Separated with commas').')</span>
		</p>';
	}
	function story_form_content($row = array()) {
		global $uhome;
		if (strlen($_POST['form_content']))
			$row['content'] = $_POST['form_content'];
		return '<p>'.label($this->label_content).'
	<textarea name="form_content" rows="28"  class="form-control" >'.htmlspecialchars($row['content']).'</textarea>
	<span class="sub">'.t('Support Markdown format').' <a href="https://help.github.com/articles/markdown-basics/" target="_blank" >'.t('Help').'</a></span>
	</p>';
	}

	function tinymce_form() {
		if ($_SESSION['mobile_style'])
		return '';
		return '

	<script language="javascript" type="text/javascript">
	$(document).ready(function(){
		tinyMCE.init({
		theme : "advanced",
		mode : "exact",
		document_base_url: "'.uhome().'",
		elements : "form_content",
		plugins : "inlinepopups,autoresize",
		language : "en",
		theme_advanced_buttons1 : "code,bold,italic,underline,bullist,undo,redo,link,unlink,forecolor,removeformat,removeformat,cleanup",
		theme_advanced_buttons2 : "",
		theme_advanced_buttons3 : "",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_styles : "Code=codeStyle;Quote=quoteStyle",
		content_css : "'.uhome().'/js/tiny_mce/content.css",
		entity_encoding : "raw",
		add_unload_trigger : false,
		extended_valid_elements : "textarea[cols|rows|disabled|name|readonly|class]",

		force_br_newlines : true,
		forced_root_block : "",
		force_p_newlines : false,
		  remove_linebreaks  : false,
		  relative_urls  :  false,
		  plaintext_create_paragraphs : false,
		  paste_create_paragraphs : false,
			

		});
	});
	</script>
	';
	}

	// settings
	function get_header() {
		return '';
	}

	function get_footer() {
		return '';
	}

	// views
	function view_title($row) {
		return '<h1>'.$row['title'].'</h1>';
	}
	function view_created($row) {
		return '<p>'.get_date($row['created']).'</p>';
	}
	function view_username($row) {
		return '<p>'.url('user/'.$row['uid'],htmlspecialchars($row['username'])).'</p>';
	}
	function view_content($row) {
		global $client,$Parsedown;
		if ($this->name == 'images') {
			if (strlen($row['var2'])) {
				$output .= '<div><a rel="facebox" href="'.$row['var2'].'"><img src="'.$row['var2'].'" style="max-width:100%;height:auto" /></a></div>';
				if (preg_match("/^http/",$row['var3'])) {
					$output .= '<div>Found on <a href="'.h($row['var3']).'" target="_blank" rel="nofollow">'.h($row['var3']).'</a></div>';
				}
				else {
					$output .= '<div>'.h($row['var3']).'</div>';
				}
			}
			else {
				$output  .= '
				<div >';
				$res = sql_query("select * from `".tb()."story_photos` where sid='{$row['id']}' ORDER by id DESC");
				$img_num = sql_counts($res);
				if ($img_num >1) {
					$img_half = ' width="250" height="auto" ';
				}
				while ($photo = sql_fetch_array($res)) {
					$output .= '
					<img class="jcow_image" src="'.uhome().'/'.$photo['uri'].'" style="max-width:100%;height:auto" />
					<div>'.h($photo['des']).'</div>';
				}
				$output .= '</div>';
			}
			$row['content'] = Parsedown::instance()->setBreaksEnabled(true)->setMarkupEscaped(true)->text($row['content']);
		}
		else {
			//$row['content'] = h($row['content']);
			$res = sql_query("select * from `".tb()."story_photos` where sid='{$row['id']}' ORDER by id ASC");
			$i = 1;
			while ($photo = sql_fetch_array($res)) {
				$img = '<img src="'.uhome().'/'.$photo['uri'].'" '.$img_half.' alt="'.h($photo['des']).'" />';
				$key = '\{image'.$i.'\}';
				$row['content'] = preg_replace('/'.$key.'/',$img,$row['content'],1,$count);
				if (!$count) {
					$output .= '<img src="'.uhome().'/'.$photo['uri'].'" '.$img_half.' alt="'.h($photo['des']).'" />';
				}
				$i++;
			}
			$row['content'] = Parsedown::instance()->setBreaksEnabled(true)->setMarkupEscaped(true)->text($row['content']);
			
		}
		$output .= '<div class="story_content">';
		$output .= $row['content'];
		$output .= '</div>';
		return $output;
	}

	function show_content($content) {
		global $config;
		$search_array = array(
			"/\[code](.*)\[\/code]/isU",
            "/(?i)\b((?:https?:\/\/|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?«»“”‘’]))/"
		);
		$replace_array = array(
			"<pre><code>\\1</code></pre>",
            '<a href="$1" rel="nofollow">$1</a>'
		);
		return preg_replace($search_array,$replace_array,nl2br(h($content)));
		/*
		if ($this->name == 'blogs') {
			return preg_replace("/\[code](.*)\[\/code]/isU","<pre><code>\\1</code></pre>",$content);
		}
		else {
			return $content;
		}
		*/
	}

	// comments
	function comment_title($row) {
		return '<h3>'.$row['title'].'</h3>';
	}
	
	function comment_created($row) {
		return get_date($row['created']);
	}
	
	function comment_signature($row) {
		return htmlspecialchars($row['signature']);
	}

	function comment_username($row) {
		return url('user/'.$row['uid'],htmlspecialchars($row['username']));

	}

	function comment_content($row) {
		/* block signature
		return '
		<div class="post_content">
		'.nl2br(htmlspecialchars($row['content'])).'
		<div class="tab_things">'.get_date($row['created']).'</div>
		<div class="signature">'.$this->comment_signature($row).'</div>
		</div>';
		*/
		return '<i>'.get_date($row['created']).' '.t('Wrote').':</i><br />
		'.nl2br(htmlspecialchars($row['content'])).'
		';

	}


	// listings
	function list_title($row) {
		/*
		if ($this->name == 'blogs') {
			
			if (strlen($row['var1'])>3) {
				if (!preg_match("/^http/i",$row['var1'])) {
					$row['var1'] = 'http://'.$row['var1'];
				}
				$row['var1'] = h($row['var1']);
				return '<a href="'.$row['var1'].'" target="_blank" rel="nofollow">'.h($row['title']).'<img src="files/icons/links.png" /></a>';
			}
			
		}*/
		return $sticky.'<a href="'.url($this->name.'/viewstory/'.$row['id']).'" class="link">'.htmlspecialchars($row['title']).'</a>';
	}
	
	function list_thumbnail($row) {
		global $uhome;
		if ($row['thumbnail']) {
			return '<img src="'.$row['thumbnail'].'" />';
		}
		else {
			return false;
		}
	}
	
	function list_views($row) {
		return $row['views'].' '.t('Views');
	}

	function list_comments($row) {
		//return $row['comments'].' '.t('Comments');
		return '';
	}

	function list_votes($row) {
		$row['votes'] = $row['digg'] + $row['dugg'];
		if ($row['digg']) {
			return t('Rating').': '.ceil(($row['digg']*100)/$row['votes']).'%';
		}
		else {
			return false;
		}
	}
	
	function list_summary($row) {
		$row['content'] = preg_replace("/\[\w+](.*)\[\/\w+]/isU","\\1",$row['content']);
		return strip_tags(utf8_substr($row['content'],150),'<img>');
	}
	
	function list_created($row) {
		return get_date($row['created']);
	}

	function list_username($row) {
		return t('Posted by {1}',url('u/'.$row['username'],htmlspecialchars($row['username'])) );

	}
	
	// init
	function init() {
		global $nav, $current_app;
		$nav[] = $current_app['flag'];
	}
	
	
	// stories from the author
	function stories_from_author($story) {
		global $current_app,$uhome;
		$res = sql_query("select * from `".tb()."stories` where uid='{$story['uid']}' and app='{$current_app['name']}' "." order by id DESC LIMIT 5");
		while ($row = sql_fetch_array($res)) {
			if (!$row['thumbnail'] && $this->default_thumb) {
				$row['thumbnail'] = $this->default_thumb;
			}
			if ($row['thumbnail']) {
				$output .= '<table><tr><td width="25"><a href="'.url($current_app['name'].'/viewstory/'.$row['id']).'"><img src="'.$uhome.'/'.$row['thumbnail'].'" width="50" height="50" /></a></td>
				<td>'.get_date($row['created']).'<br />'.url($current_app['name'].'/viewstory/'.$row['id'],$row['title']).'</td></tr></table>';
			}
			else {
				$output .= '<table><tr><td>
				'.url($current_app['name'].'/viewstory/'.$row['id'],$row['title']).'
				<br />'.get_date($row['created']).'</td></tr></table>';
			}
		}
		ass(array('title'=>$this->stories_from_author,'content'=>$output));
	}
	
	// stories from the author
	function stories_from_cat($cid, $title) {
		global $current_app,$uhome;
		if ($cid > 0) {
			$where = " and cid='$cid' ";
		}
		$res = sql_query("select * from `".tb()."stories` where  app='{$current_app['name']}' $where order by id DESC LIMIT 10");
		while ($row = sql_fetch_array($res)) {
			if (!$row['thumbnail'] && $this->default_thumb) {
				$row['thumbnail'] = $this->default_thumb;
			}
			if ($row['thumbnail']) {
				$output .= '<table><tr><td width="25"><a href="'.url($current_app['name'].'/viewstory/'.$row['id']).'"><img src="'.$uhome.'/'.$row['thumbnail'].'" width="50" height="50" /></a></td>
				<td>'.url($current_app['name'].'/viewstory/'.$row['id'],$row['title']).'</td></tr></table>';
			}
			else {
				$output .= '<ul class="simple_list"><li>
				'.url($current_app['name'].'/viewstory/'.$row['id'],$row['title']).'</li></ul>';
			}
		}
		ass(array('title'=>$title,'content'=>$output));
	}
	
	// who voted
	/*
	function who_voted($sid) {
		global $current_app;
		$res = sql_query("select v.*,u.firstname from `".tb()."votes` as v LEFT JOIN `".tb()."accounts` AS u ON u.id=v.uid where v.sid='$sid' ORDER BY v.created DESC LIMIT 30");
		$output = '<ul class="float_left">';
		while ($row = sql_fetch_array($res)) {
			$output .= '<li>'.url('u/'.$row['uid'],htmlspecialchars($row['username'])).'</li>';
		}
		$output .= '</ul>';
		ass(array('title'=>t('People who voted'),'content'=>$output));
	}
	*/
	

	// tag cloud
	function tag_cloud($num=10) {
		$res = sql_query("select * from `".tb()."tags` where  app='{$this->name}' and num>0 order by num DESC LIMIT $num");
		if (sql_counts($res)) {
			while ($row = sql_fetch_array($res)) {
				$tag_cloud .= ' '.url($this->name.'/liststories/tag/'.$row['id'],htmlspecialchars($row['name'])).' <span class="sub">('.$row['num'].')</span> ';
			}
		}
		if (strlen($tag_cloud)) {
			ass(
				array('title' => t('Tags'), 'content'=>$tag_cloud)
				);
		}
	}

	function vote_form() {
		return '
			<p>
						'.label(t('Rate')).'
						<select name="vote">
						<option value="0">'.t('Not Rate').'</option>
						<option value="+3">+3('.t('Excellent').')</option>
						<option value="+2">+2('.t('Very good').')</option>
						<option value="+1" selected>+1('.t('Good').')</option>
						<option value="-1">-1('.t('Poor').')</option>
						<option value="-2">-2('.t('Terrible').')</option>
						</select>';
	}

	function convert_content_before_insert($content) {
		return $content;
		if (!file_exists('js/tiny_mce/jquery.tinymce.js') || $_SESSION['mobile_style']) {
			return nl2br(
				preg_replace('/<a /i','<a rel="nofollow external" ',convert_html($content))
			);
		}
		else {
			return preg_replace('/<a /i','<a rel="nofollow external" ',convert_html($content));
		}
		
		
	}

}


function theme_list_inul($arr) {
	if (!$arr)
		$arr = array();
	$data = '<ul>';
	foreach ($arr as $item) {
		$data .= '<li>';
		foreach ($item as $key=>$val) {
			$data .= $val.' ';
		}
		$data .= '</li>';
	}
	$data .= '</ul>';
	return $data;
}

function theme_list_intable($arr, $atts) {
	if (!$arr)
		$arr = array();
	$data = '<table class="stories"><tr>';
	foreach ($atts as $att) {
		$data .= '<th>'.$att.'</th>';
	}
	$data .= '</tr>';
	foreach ($arr as $item) {
		$data .= '<tr>';
		foreach ($item as $key=>$val) {
			$data .= '<td>'.$val.'</td>';
		}
		$data .= '</tr>';
	}
	$data .= '</table>';
	return $data;
}


function theme_comments_inul($arr) {
	if (!$arr)
		$arr = array();
	$data = '<ul>';
	foreach ($arr as $item) {
		$data .= '<li>';
		foreach ($item as $key=>$val) {
			$data .= $val.' ';
		}
		$data .= '</li>';
	}
	$data .= '</ul>';
	return $data;
}

// 检测cid
function valid_category($cid,$app=0) {
	GLOBAL $db;
	if (!$app || $app == 'groups')
		$res = sql_query("select * from `".tb()."story_categories` where id='$cid' ");
	else
		$res = sql_query("select * from `".tb()."story_categories` where id='$cid' and app='$app' ");
	if(sql_counts($res)) {
		return sql_fetch_array($res);
	}
	else {
		return false;
	}
}

// 检测sid
function valid_story($sid) {
	GLOBAL $db;
	$res = sql_query("select * from `".tb()."stories` where id='$sid' ");
	if(sql_counts($res)) {
		return sql_fetch_array($res);
	}
	else {
		return false;
	}
}

// check comment
function valid_comment($cid) {
	GLOBAL $db;
	$res = sql_query("select * from `".tb()."comments` where id='$cid' ");
	if(sql_counts($res)) {
		return sql_fetch_array($res);
	}
	else {
		return false;
	}
}

// save tags
function save_tags($tags,$sid,$app='') {
	//save_topics($$tags,$sid,$app);
	if (is_array($tags)) {
		$timeline = time();
		$res = sql_query("select * from `".tb()."tag_ids` where sid='$sid' ");
		while ($row = sql_fetch_array($res)) {
			sql_query("update `".tb()."tags` set num=num-1 where id='{$row['tid']}' ");
		}
		sql_query("delete from `".tb()."tag_ids` where sid='$sid' ");
		foreach ($tags as $tag) {
			if (!preg_match("/[<>]/",$tag) && strlen($tag)) {
				//$tag = ucwords($tag);
				$res = sql_query("select * from `".tb()."tags` where name='$tag' and app='$app'");
				if ($row = sql_fetch_array($res)) {
					sql_query("insert into `".tb()."tag_ids` (tid,sid) values ('{$row['id']}','$sid')");
					sql_query("update `".tb()."tags` set num=num+1 where id='{$row['id']}' ");
				}
				else {
					sql_query("insert into `".tb()."tags` (name,num,app) values ('$tag',1,'$app')");
					$tid = insert_id();
					sql_query("insert into `".tb()."tag_ids` (tid,sid) values('$tid','$sid')");
				}
			}
		}
	}
}

function save_topics($tags,$sid,$app='') {
	if (is_array($tags)) {
		$timeline = time();
		$res = sql_query("select * from `".tb()."topic_ids` where sid='$sid' ");
		while ($row = sql_fetch_array($res)) {
			sql_query("update `".tb()."topics` set stories=stories-1 where id='{$row['tid']}' ");
		}
		sql_query("delete from `".tb()."topic_ids` where sid='$sid' ");
		foreach ($tags as $tag) {
			if (!preg_match("/[<>]/",$tag) && strlen($tag)) {
				//$tag = ucwords($tag);
				$res = sql_query("select * from `".tb()."topics` where name='$tag'");
				if ($row = sql_fetch_array($res)) {
					sql_query("insert into `".tb()."topic_ids` (tid,sid) values ('{$row['id']}','$sid')");
					sql_query("update `".tb()."topics` set stories=stories+1,updated='$timeline' where id='{$row['id']}' ");
				}
				else {
					sql_query("insert into `".tb()."topics` (name,stories,updated) values ('$tag',1,'$timeline')");
					$tid = insert_id();
					sql_query("insert into `".tb()."topic_ids` (tid,sid) values('$tid','$sid')");
				}
			}
		}
	}
}
// topics selector
function topics_autocomplete_js() {
	$res = sql_query("select name from ".tb()."topics order by featured DESC,stories DESC limit 30");
	if (sql_counts($res)) {
		while($row = sql_fetch_array($res)) {
			$topics[] = json_encode($row['name']);
			$availabletags = implode(',',$topics);
		}
		return '
		<script>
		$(function() {
			var availableTags = [
				'.$availabletags.'
			];
			function split( val ) {
				return val.split( /,\s*/ );
			}
			function extractLast( term ) {
				return split( term ).pop();
			}

			$( ".topicbox" )
				.bind( "keydown", function( event ) {
					if ( event.keyCode === $.ui.keyCode.TAB &&
							$( this ).data( "autocomplete" ).menu.active ) {
						event.preventDefault();
					}
				})
				.autocomplete({
					minLength: 0,
					source: function( request, response ) {
						// delegate back to autocomplete, but extract the last term
						response( $.ui.autocomplete.filter(
							availableTags, extractLast( request.term ) ) );
					},
					focus: function() {
						// prevent value inserted on focus
						return false;
					},
					select: function( event, ui ) {
						var terms = split( this.value );
						// remove the current input
						terms.pop();
						// add the selected item
						terms.push( ui.item.value );
						// add placeholder to get the comma-and-space at the end
						terms.push( "" );
						this.value = terms.join( ", " );
						return false;
					}
				});
			});
		</script>';
	}
	else {
		return '';
	}
}


function check_auto_approve() {
	return false;
	if (!$auto_approve_roles = get_gvar('auto_approve_roles')) {
		$auto_approve_roles = 3;
	}
	else {
		$auto_approve_roles = explode('|',$auto_approve_roles);
	}
	if (allow_access($auto_approve_roles)) {
		return true;
	}
	else {
		return false;
	}
}
function parse_save_tags($topics,$sid,$app='') {
	global $client;
	if ($_POST['privacy'] == 1) return ''; // disable for private story
	$topics = explode(',',$topics);
	$stags = array();
	if (is_array($topics)) {
		foreach ($topics as $topic) {
			$topic = trim($topic);
			if (strlen($topic) > 0 && strlen($topic) < 30) {
				$stags[] = $topic;
			}
		}
	}
	$res = sql_query("select app,featured from jcow_stories where id='$sid'");
	$story = sql_fetch_array($res);
	if ($story['page_type'] == 'group') {
		$res = sql_query("select * from jcow_pages where id='{$story['page_id']}'");
		$page = sql_fetch_array($res);
		if ($page['var3']%2) {
			return '';
		}
	}
	save_tags($stags,$sid,$app);
	return implode(',',$stags);
}

function parse_save_topics($topics,$sid,$app='') {
	$topics = explode(',',$topics);
	$stags = array();
	if (is_array($topics)) {
		foreach ($topics as $topic) {
			$topic = trim($topic);
			if (strlen($topic) > 0 && strlen($topic) < 30) {
				$stags[] = $topic;
			}
		}
	}
	save_topics($stags,$sid,$app='');
	return implode(',',$stags);
}