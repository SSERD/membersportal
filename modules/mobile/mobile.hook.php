<?php
/* ############################################################ *\
 ----------------------------------------------------------------
@package	Jcow Social Networking Script.
@copyright	Copyright (C) 2009 - 2010 jcow.net.  All Rights Reserved.
@license	see http://jcow.net/license
 ----------------------------------------------------------------
\* ############################################################ */
function mobile_boot() {
	global $parr,$gvars;
	if(detect_mobile()) {
		$_SESSION['mobile_detected'] = 1;
	}
	else {
		$_SESSION['mobile_detected'] = 0;
		$_SESSION['mobile_style'] = 0;
	}
	if ($_SESSION['mobile_detected'] && !$_SESSION['pc_style'] && $parr[0] != 'mobile') {
		$_SESSION['mobile_style'] = 1;
		if ($parr[0] != 'admin') {
			$gvars['jcow_inline_banner_ad'] == '';
			if (strlen($gvars['jcow_inline_mobile_ad'])) {
				$gvars['jcow_inline_banner_ad'] = $gvars['jcow_inline_mobile_ad'];
			}
		}
	}
	
}
function detect_mobile() {
	//return true;
	if(preg_match('/(alcatel|amoi|android|avantgo|blackberry|benq|cell|cricket|docomo|elaine|htc|iemobile|iphone|ipad|ipaq|ipod|j2me|java|midp|mini|mmp|mobi|motorola|nec-|nokia|palm|panasonic|philips|phone|playbook|sagem|sharp|sie-|silk|smartphone|sony|symbian|t-mobile|telus|up\.browser|up\.link|vodafone|wap|webos|wireless|xda|xoom|zte)/i', $_SERVER['HTTP_USER_AGENT']))
    return true;
    else
    return false;
}

function display_application_mobile($data) {
	global $clear_as, $client, $config, $parr, $adside_displayed, $adbanner_displayed;
    $output .= '<div class="content_full">';

	if (is_array($data['sections'])) {
		if (!$config['is_profile_page']) {
			$output .= '<div class="block">'.new_tab_menu().'</div>';
		}
		$i=0;
		if (count($data['sections']) == 1 && !$adbanner_displayed && !$config['hide_ad'] && strlen(get_gvar('jcow_inline_banner_ad'))) {
			$output .= '<div class="block">'.get_gvar('jcow_inline_banner_ad').'</div>';
			$adbanner_displayed = 1;
		}
		foreach ($data['sections'] as $section) {
			if ($config['is_profile_page'] && $i == 1) {
				$output .= '<div class="block">'.new_tab_menu().'</div>';
			} 
			$output .= '<div class="block">';
			if (strlen($section['title'])) {
				$output .= '<div class="block_title">'.$section['title'].'</div>';
			}
			$output .= '<div class="block_content">'.$section['content'].'</div>
			</div>';
			$i++;
			if ($i == 1 && !$adbanner_displayed && !$config['hide_ad'] && strlen(get_gvar('jcow_inline_banner_ad'))) {
				$output .= '<div class="block">'.get_gvar('jcow_inline_banner_ad').'</div>';
				$adbanner_displayed = 1;
			}
		}
	}
    if (!$clear_as) {
		$i = $adside_displayed = 0;		
		if (is_array($data['blocks'])) {
			foreach($data['blocks'] as $block) {
				if (is_array($block)) {
					$output .= '
					<div class="block">';
					if ($block['title']) {
						$output .= '<div class="block_title">'.$block['title'].'</div>';
					}
					$output .= '<div class="block_content">
					'.$block['content'].
					'</div>
					</div>
					';
					$i++;
				}
			}
		}
	}
	$output .= '
	
	</div><!-- end of appmain -->
	'; // end of app_main


	$output .= $data['app_footer'];
	return $output;
}