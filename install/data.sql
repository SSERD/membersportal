CREATE TABLE IF NOT EXISTS `jcow_accounts` (
  `id` int(11) NOT NULL auto_increment,
  `fbid` bigint(20) NOT NULL default '0',
  `email` varchar(120) NOT NULL default '',
  `lastact` int(11) NOT NULL default '0',
  `created` int(11) NOT NULL default '0',
  `username` varchar(25) NOT NULL default '',
  `fullname` varchar(30) NOT NULL default '',
  `password` varchar(32) NOT NULL default '',
  `level` tinyint(4) NOT NULL default '0',
  `points` int(11) NOT NULL default '0',
  `avatar` varchar(50) NOT NULL default '',
  `signature` text,
  `blurbs` text,
  `profile_permission` tinyint(4) NOT NULL default '0',
  `location` varchar(100) NOT NULL default '',
  `lastlogin` int(11) NOT NULL default '0',
  `ipaddress` varchar(30) NOT NULL default '',
  `chpass` varchar(10) NOT NULL default '',
  `disabled` tinyint(4) NOT NULL default '0',
  `intr` text,
  `gender` tinyint(4) NOT NULL default '0',
  `about_me` text,
  `birthyear` int(4) NOT NULL default '0',
  `birthmonth` tinyint(2) NOT NULL default '0',
  `birthday` tinyint(2) NOT NULL default '0',
  `hide_age` tinyint(1) NOT NULL default '0',
  `reg_code` varchar(8) NOT NULL default '',
  `forum_posts` int(11) NOT NULL default '0',
  `featured` tinyint(1) NOT NULL default '0',
  `roles` varchar(255) NOT NULL default '',
  `country` varchar(50) NOT NULL default '',
  `locale` varchar(50) NOT NULL default '',
  `state` varchar(50) NOT NULL default '',
  `jcowsess` char(12) NOT NULL default '',
  `token` varchar(32) NOT NULL default '',
  `wall_id` int(11) NOT NULL default '0',
  `followers` int(11) NOT NULL default '0',
  `settings` text,
  `var1` varchar(255) NOT NULL default '',
  `var2` varchar(255) NOT NULL default '',
  `var3` varchar(255) NOT NULL default '',
  `var4` varchar(255) NOT NULL default '',
  `var5` varchar(255) NOT NULL default '',
  `var6` varchar(255) NOT NULL default '',
  `var7` varchar(255) NOT NULL default '',
  `pass` varchar(32) NOT NULL default '',
  `hide_me` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `username` (`username`),
  KEY `lastlogin` (`lastlogin`),
  KEY `email` (`email`),
  KEY `fbid` (`fbid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;



CREATE TABLE IF NOT EXISTS `jcow_blacks` (
  `id` int(11) NOT NULL auto_increment,
  `uid` int(11) NOT NULL default '0',
  `bid` int(11) NOT NULL default '0',
  `bname` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `uid` (`uid`,`bid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;



CREATE TABLE IF NOT EXISTS `jcow_cache` (
  `ckey` varchar(50) collate utf8_unicode_ci NOT NULL default '',
  `content` text,
  `expired` int(11) NOT NULL default '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



CREATE TABLE IF NOT EXISTS `jcow_comments` (
  `id` int(11) NOT NULL auto_increment,
  `target_id` varchar(20) NOT NULL default '',
  `uid` int(11) NOT NULL default '0',
  `message` text,
  `created` int(11) NOT NULL default '0',
  `stream_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `target_id` (`target_id`),
  KEY `stream_id` (`stream_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `jcow_favorites` (
  `id` int(11) NOT NULL auto_increment,
  `uid` int(11) NOT NULL default '0',
  `fuid` int(11) NOT NULL default '0',
  `fapp` varchar(100) character set utf8 NOT NULL default '',
  `fsid` int(11) NOT NULL default '0',
  `created` int(11) NOT NULL default '0',
  `title` varchar(100) character set utf8 NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `uid` (`uid`,`fuid`,`fsid`,`created`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;


CREATE TABLE IF NOT EXISTS `jcow_followers` (
  `uid` int(11) NOT NULL default '0',
  `fid` int(11) NOT NULL default '0',
  KEY `uid` (`uid`,`fid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE IF NOT EXISTS `jcow_forums` (
  `id` int(11) NOT NULL auto_increment,
  `weight` int(11) NOT NULL default '0',
  `parent_id` int(11) NOT NULL default '0',
  `name` varchar(255) NOT NULL default '',
  `type_pic` varchar(255) NOT NULL default '',
  `description` tinytext,
  `rules` text,
  `forum_type` varchar(50) NOT NULL default '0',
  `threads` int(11) NOT NULL default '0',
  `posts` int(11) NOT NULL default '0',
  `lastpostname` varchar(32) NOT NULL default '',
  `lastposttopicid` int(11) NOT NULL default '0',
  `lastposttopic` varchar(70) NOT NULL default '',
  `lastpostcreated` int(11) NOT NULL default '0',
  `moderator` varchar(255) NOT NULL default '',
  `settings` text,
  `fmembers` int(11) NOT NULL default '0',
  `image` varchar(250) NOT NULL default '',
  `read_roles` varchar(255) NOT NULL default '',
  `upload_roles` varchar(255) NOT NULL default '',
  `thread_roles` varchar(255) NOT NULL default '',
  `reply_roles` varchar(255) NOT NULL default '',
  `moderators` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `belong_id` (`parent_id`),
  KEY `order_num` (`weight`),
  KEY `type_class` (`forum_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;



INSERT INTO `jcow_forums` (`id`, `weight`, `parent_id`, `name`, `type_pic`, `description`, `rules`, `forum_type`, `threads`, `posts`, `lastpostname`, `lastposttopicid`, `lastposttopic`, `lastpostcreated`, `moderator`, `settings`, `fmembers`, `image`, `read_roles`, `upload_roles`, `thread_roles`, `reply_roles`, `moderators`) VALUES
(7, 1, 0, 'General Category', '', '', '', 'category', 0, 0, '', 0, '', 0, '', '', 0, '', '', '', '', '', ''),
(8, 1, 7, 'General Forum', '', 'This is a general forum', '', 'forum', 0, 0, '', 0, '', 0, '', '', 0, '', '1|2', '2', '2', '2', '');



CREATE TABLE IF NOT EXISTS `jcow_forum_attachments` (
  `id` int(11) NOT NULL auto_increment,
  `pid` int(11) NOT NULL default '0',
  `tid` int(11) NOT NULL default '0',
  `uri` varchar(100) NOT NULL default '',
  `des` varchar(255) NOT NULL default '',
  `size` int(11) NOT NULL default '0',
  `orginal_name` varchar(255) NOT NULL default '',
  `downloads` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `pid` (`pid`),
  KEY `tid` (`tid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;



CREATE TABLE IF NOT EXISTS `jcow_forum_polls` (
  `id` int(11) NOT NULL auto_increment,
  `tid` int(11) NOT NULL default '0',
  `question` varchar(100) NOT NULL default '',
  `created` int(11) NOT NULL default '0',
  `options` text,
  `timeout` int(11) NOT NULL default '0',
  `options_per_user` tinyint(4) NOT NULL default '0',
  `voters` text,
  `total` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `tid` (`tid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;



CREATE TABLE IF NOT EXISTS `jcow_forum_posts` (
  `id` int(11) NOT NULL auto_increment,
  `tid` bigint(11) NOT NULL default '0',
  `uid` int(11) NOT NULL default '0',
  `title` varchar(255) NOT NULL default '',
  `message` text,
  `created` int(10) NOT NULL default '0',
  `ip` varchar(30) NOT NULL default '',
  `is_first` tinyint(4) NOT NULL default '0',
  `attach` int(11) NOT NULL default '0',
  `bbcode_off` tinyint(4) NOT NULL default '0',
  `emote_off` tinyint(4) NOT NULL default '0',
  `got_attach` tinyint(4) NOT NULL default '0',
  `stream_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `tid` (`tid`),
  KEY `author_id` (`uid`),
  KEY `stream_id` (`stream_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `jcow_forum_threads` (
  `id` int(11) NOT NULL auto_increment,
  `fid` int(11) NOT NULL default '0',
  `old_fid` int(11) NOT NULL default '0',
  `pid` int(11) NOT NULL default '0',
  `userid` int(11) NOT NULL default '0',
  `username` varchar(50) NOT NULL default '',
  `topic` varchar(255) NOT NULL default '',
  `views` int(11) NOT NULL default '0',
  `posts` int(11) NOT NULL default '0',
  `closed` smallint(1) NOT NULL default '0',
  `created` int(11) NOT NULL default '0',
  `lastpostusername` varchar(255) NOT NULL default '0',
  `lastpostcreated` int(10) NOT NULL default '0',
  `icon` tinyint(4) NOT NULL default '0',
  `thread_type` tinyint(1) NOT NULL default '0',
  `thread_lock` tinyint(1) NOT NULL default '0',
  `got_poll` tinyint(11) NOT NULL default '0',
  `got_attach` tinyint(4) NOT NULL default '0',
  `stressed` tinyint(4) NOT NULL default '0',
  `digg` int(11) NOT NULL default '0',
  `dugg` int(11) NOT NULL default '0',
  `votes` text,
  PRIMARY KEY  (`id`),
  KEY `fid` (`fid`),
  KEY `thread_type` (`thread_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;



CREATE TABLE IF NOT EXISTS `jcow_friends` (
  `uid` int(11) NOT NULL default '0',
  `fid` int(11) NOT NULL default '0',
  `created` int(11) NOT NULL default '0',
  KEY `uid` (`uid`,`fid`),
  KEY `fid` (`fid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE IF NOT EXISTS `jcow_friend_reqs` (
  `uid` int(11) NOT NULL default '0',
  `fid` int(11) NOT NULL default '0',
  `created` int(11) NOT NULL default '0',
  `msg` varchar(200)  NOT NULL default '',
  KEY `uid` (`uid`,`fid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE IF NOT EXISTS `jcow_groups` (
  `id` int(11) NOT NULL auto_increment,
  `uri` varchar(30) NOT NULL default '',
  `name` varchar(100) NOT NULL default '',
  `slogan` varchar(200) NOT NULL default '',
  `creatorid` int(11) NOT NULL default '0',
  `creator` varchar(50) NOT NULL default '',
  `description` text,
  `members` int(11) NOT NULL default '0',
  `created` int(11) NOT NULL default '0',
  `logo` varchar(100) NOT NULL default '',
  `private` tinyint(4) NOT NULL default '0',
  `needapproval` tinyint(4) NOT NULL default '0',
  `posts` int(11) NOT NULL default '0',
  `topics` int(11) NOT NULL default '0',
  `lastptime` int(11) NOT NULL default '0',
  `lastpname` varchar(50) NOT NULL default '',
  `password` varchar(32) NOT NULL default '',
  `custom_css` text,
  `style_ids` varchar(50) NOT NULL default '',
  `category` char(2) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `creatorid` (`creatorid`),
  KEY `uri` (`uri`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;



CREATE TABLE IF NOT EXISTS `jcow_group_categories` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL default '',
  `groups` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;



CREATE TABLE IF NOT EXISTS `jcow_group_members` (
  `gid` int(11) NOT NULL default '0',
  `uid` int(11) NOT NULL default '0',
  `created` int(11) NOT NULL default '0',
  `nickname` varchar(20) NOT NULL default '',
  `about_me` text,
  `hide_profile` tinyint(1) NOT NULL default '0',
  KEY `gid` (`gid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE IF NOT EXISTS `jcow_group_members_pending` (
  `uid` int(11) NOT NULL default '0',
  `gid` int(11) NOT NULL default '0',
  `created` int(11) NOT NULL default '0',
  `ignored` tinyint(4) NOT NULL default '0',
  KEY `uid` (`uid`,`gid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE IF NOT EXISTS `jcow_group_polls` (
  `id` int(11) NOT NULL auto_increment,
  `tid` int(11) NOT NULL default '0',
  `question` varchar(100) NOT NULL default '',
  `created` int(11) NOT NULL default '0',
  `options` text,
  `timeout` int(11) NOT NULL default '0',
  `options_per_user` tinyint(4) NOT NULL default '0',
  `voters` text,
  `total` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `tid` (`tid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;



CREATE TABLE IF NOT EXISTS `jcow_group_postcats` (
  `id` int(11) NOT NULL auto_increment,
  `gid` int(11) NOT NULL default '0',
  `name` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `gid` (`gid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;



CREATE TABLE IF NOT EXISTS `jcow_group_posts` (
  `id` int(11) NOT NULL auto_increment,
  `gid` int(11) NOT NULL default '0',
  `tid` bigint(11) NOT NULL default '0',
  `uid` int(11) NOT NULL default '0',
  `username` varchar(50) NOT NULL default '',
  `rtid` int(11) NOT NULL default '0',
  `rid` int(11) NOT NULL default '0',
  `rname` varchar(100) NOT NULL default '',
  `message` text,
  `created` int(10) NOT NULL default '0',
  `ip` varchar(30) NOT NULL default '',
  `attach` int(11) NOT NULL default '0',
  `bbcode_off` tinyint(4) NOT NULL default '0',
  `emote_off` tinyint(4) NOT NULL default '0',
  `got_attach` tinyint(4) NOT NULL default '0',
  `topic` varchar(100) NOT NULL default '',
  `is_first` tinyint(4) NOT NULL default '0',
  `replies` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `tid` (`tid`),
  KEY `uid` (`uid`),
  KEY `gid` (`gid`),
  KEY `rtid` (`rtid`),
  KEY `rid` (`rid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=100 ;



CREATE TABLE IF NOT EXISTS `jcow_group_topics` (
  `id` int(11) NOT NULL auto_increment,
  `gid` int(11) NOT NULL default '0',
  `old_fid` int(11) NOT NULL default '0',
  `pid` int(11) NOT NULL default '0',
  `uid` int(11) NOT NULL default '0',
  `username` varchar(50) NOT NULL default '',
  `topic` varchar(255) NOT NULL default '',
  `views` int(11) NOT NULL default '0',
  `posts` int(11) NOT NULL default '0',
  `closed` smallint(1) NOT NULL default '0',
  `created` int(11) NOT NULL default '0',
  `lastpostusername` varchar(255) NOT NULL default '0',
  `lastpostcreated` int(11) NOT NULL default '0',
  `icon` tinyint(4) NOT NULL default '0',
  `thread_type` tinyint(1) NOT NULL default '0',
  `thread_lock` tinyint(1) NOT NULL default '0',
  `got_poll` tinyint(11) NOT NULL default '0',
  `got_attach` tinyint(4) NOT NULL default '0',
  `stressed` tinyint(4) NOT NULL default '0',
  `digg` int(11) NOT NULL default '0',
  `dugg` int(11) NOT NULL default '0',
  `votes` text,
  PRIMARY KEY  (`id`),
  KEY `gid` (`gid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=32 ;



CREATE TABLE IF NOT EXISTS `jcow_gvars` (
  `gkey` varchar(50) NOT NULL default '',
  `gvalue` text,
  KEY `gkey` (`gkey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



INSERT INTO `jcow_gvars` (`gkey`, `gvalue`) VALUES
('theme_folder', 'themes/default'),
('cf_var1', 'disabled'),
('cf_var2', 'disabled'),
('cf_var3', 'disabled'),
('cf_var4', 'disabled'),
('cf_var5', 'disabled'),
('cf_var6', 'disabled'),
('cf_var7', 'disabled'),
('jcow_version', '4.0'),
('app_music_disable', '0'),
('story_access', 'all'),
('profile_access', 'all'),
('site_slogan', 'This is a Social Network'),
('ad_block_content_top', ''),
('ad_block_content_bottom', ''),
('site_name', 'My Jcow Network'),
('site_email', 'name@domain.com'),
('block_top', ''),
('block_bottom', ''),
('only_invited', '0'),
('session_lived', '1267784005'),
('permission_etheme', '2'),
('permission_atheme', '2|11'),
('private_network', '0'),
('theme_tpl', 'default'),
('theme_css', '1.css'),
('hide_ad_roles', '3'),
('acc_verify', '0'),
('permission_upload', '2'),
('permission_comment', '2'),
('permission_add', '2'),
('permission_browse', '1|2'),
('permission_feed', '1|2'),
('theme_block_adsbar', 'Go to "Admin CP" - "Themes" - "Manage Blocks" to edit this message.'),
('limit_posting_num', '5'),
('app_music', '0');


CREATE TABLE IF NOT EXISTS `jcow_invites` (
  `id` int(11) NOT NULL auto_increment,
  `uid` int(11) NOT NULL default '0',
  `email` varchar(255) NOT NULL default '',
  `status` tinyint(4) NOT NULL default '0',
  `created` int(11) NOT NULL default '0',
  `code` varchar(15) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;



CREATE TABLE IF NOT EXISTS `jcow_langs` (
  `lang_from` varchar(255) NOT NULL default '',
  `lang_to` text,
  `lang` varchar(20) NOT NULL default '',
  KEY `lang_from` (`lang_from`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE IF NOT EXISTS `jcow_messages` (
  `id` int(11) NOT NULL auto_increment,
  `subject` varchar(100) NOT NULL default '',
  `message` text,
  `from_id` int(11) NOT NULL default '0',
  `to_id` int(11) NOT NULL default '0',
  `created` int(11) NOT NULL default '0',
  `hasread` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `from_id` (`from_id`,`to_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;




CREATE TABLE IF NOT EXISTS `jcow_profiles` (
  `id` int(11) NOT NULL default '0',
  `style_ids` varchar(255) NOT NULL default '',
  `custom_css` text,
  `background` varchar(100) NOT NULL default '',
  `videoid` int(11) NOT NULL default '0',
  `favorites` int(11) NOT NULL default '0',
  `views` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `jcow_profile_comments` (
  `id` int(11) NOT NULL auto_increment,
  `uid` int(11) NOT NULL default '0',
  `target_id` int(11) NOT NULL default '0',
  `message` text,
  `created` int(11) NOT NULL default '0',
  `stream_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `stream_id` (`stream_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;



CREATE TABLE IF NOT EXISTS `jcow_roles` (
  `id` int(11) NOT NULL default '0',
  `name` varchar(100) character set utf8 NOT NULL default '',
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



INSERT INTO `jcow_roles` (`id`, `name`) VALUES
(1, 'Guest'),
(2, 'General member'),
(3, 'Administrator');




CREATE TABLE IF NOT EXISTS `jcow_stories` (
  `id` int(11) NOT NULL auto_increment,
  `cid` int(11) NOT NULL default '0',
  `sticky` tinyint(4) NOT NULL default '0',
  `closed` tinyint(4) NOT NULL default '0',
  `title` varchar(120) NOT NULL default '',
  `thumbnail` varchar(255) NOT NULL default '',
  `content` text,
  `uid` int(11) NOT NULL default '0',
  `created` int(11) NOT NULL default '0',
  `lastreply` int(11) NOT NULL default '0',
  `lastreplyuname` varchar(50) NOT NULL default '',
  `lastreplyuid` int(11) NOT NULL default '0',
  `updated` int(11) NOT NULL default '0',
  `views` int(11) NOT NULL default '0',
  `comments` int(11) NOT NULL default '0',
  `stream_id` int(11) NOT NULL default '0',
  `app` varchar(50) NOT NULL default '',
  `digg` int(11) NOT NULL default '0',
  `dugg` int(11) NOT NULL default '0',
  `photos` int(11) NOT NULL default '0',
  `tags` varchar(255) NOT NULL default '',
  `featured` tinyint(4) NOT NULL default '0',
  `var1` varchar(255) NOT NULL default '',
  `var2` varchar(255) NOT NULL default '',
  `var3` varchar(255) NOT NULL default '',
  `var4` varchar(255) NOT NULL default '',
  `var5` varchar(255) NOT NULL default '',
  `text1` text,
  `text2` text,
  `blob1` blob,
  `rating` text,
  `page_id` int(11) NOT NULL default '0',
  `page_type` VARCHAR( 25 ) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `app` (`app`),
  KEY `uid` (`uid`),
  KEY `page_id` (`page_id`),
  KEY `cid` (`cid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=79 ;



CREATE TABLE IF NOT EXISTS `jcow_story_categories` (
  `id` int(11) NOT NULL auto_increment,
  `gid` int(11) NOT NULL default '0',
  `name` varchar(150) NOT NULL default '',
  `description` text,
  `weight` int(11) NOT NULL default '0',
  `app` varchar(50) NOT NULL default '',
  `var1` varchar(255) NOT NULL default '',
  `var2` varchar(255) NOT NULL default '',
  `var3` varchar(255) NOT NULL default '',
  `var4` varchar(255) NOT NULL default '',
  `var5` varchar(255) NOT NULL default '',
  `uri` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `app` (`app`),
  KEY `weight` (`weight`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;


CREATE TABLE IF NOT EXISTS `jcow_story_cat_groups` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL default '',
  `app` varchar(50) NOT NULL default '',
  `weight` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;



CREATE TABLE IF NOT EXISTS `jcow_story_photos` (
  `id` int(11) NOT NULL auto_increment,
  `sid` int(11) NOT NULL default '0',
  `uri` varchar(100) NOT NULL default '',
  `des` varchar(255) NOT NULL default '',
  `thumb` varchar(100) NOT NULL default '',
  `size` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `sid` (`sid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=48 ;



CREATE TABLE IF NOT EXISTS `jcow_streams` (
  `id` int(11) NOT NULL auto_increment,
  `message` text,
  `wall_id` int(11) NOT NULL default '0',
  `uid` int(11) NOT NULL default '0',
  `attachment` text,
  `created` int(11) NOT NULL default '0',
  `type` tinyint(1) NOT NULL default '0',
  `app` varchar(20) NOT NULL default '',
  `aid` int(11) NOT NULL default '0',
  `hide` tinyint(1) NOT NULL default '0',
  `likes` int(11) NOT NULL default '0',
  `dislikes` int(11) NOT NULL default '0',
  `stuff_id` int(11) NOT NULL default '0',
  `data` text,
  PRIMARY KEY  (`id`),
  KEY `app` (`app`),
  KEY `aid` (`aid`),
  KEY `stuff_id` (`stuff_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1;


CREATE TABLE IF NOT EXISTS `jcow_disliked` (
  `id` int(11) NOT NULL auto_increment,
  `uid` int(11) NOT NULL default '0',
  `stream_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `uid` (`uid`,`stream_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;




CREATE TABLE IF NOT EXISTS `jcow_tags` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL default '',
  `app` varchar(25) NOT NULL default '',
  `num` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;



CREATE TABLE IF NOT EXISTS `jcow_tag_ids` (
  `tid` int(11) NOT NULL default '0',
  `sid` int(11) NOT NULL default '0',
  KEY `tid` (`tid`,`sid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




CREATE TABLE IF NOT EXISTS `jcow_texts` (
  `tkey` varchar(50) NOT NULL default '',
  `tvalue` text,
  KEY `tkey` (`tkey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



INSERT INTO `jcow_texts` (`tkey`, `tvalue`) VALUES
('welcome_pm', 'Hello %username%!\r\nThank you for your registeration!\r\nPlease invite your friends to join our community.'),
('welcome_email', 'Dear %username%,\r\nWelcome to %sitelink%!\r\nYour login email is: %email%\r\nOur URL is:\r\n%sitelink%'),
('welcome_msg', 'Welcome to our Community!'),
('rules_conditions', 'none'),
('footermsg', 'Your footer here..'),
('locations', 'Afghanistan  \r\nAlbania  \r\nAlgeria  \r\nAmerican Samoa  \r\nAndorra  \r\nAngola  \r\nAnguilla  \r\nAntarctica  \r\nAntigua and Barbuda  \r\nArgentina  \r\nArmenia  \r\nAruba  \r\nAustralia  \r\nAustria  \r\nAzerbaidjan  \r\nBahamas  \r\nBahrain  \r\nBangladesh  \r\nBarbados  \r\nBelarus  \r\nBelgium  \r\nBelize  \r\nBenin  \r\nBermuda  \r\nBhutan  \r\nBolivia  \r\nBosnia-Herzegovina  \r\nBotswana  \r\nBouvet Island  \r\nBrazil  \r\nBrunei Darussalam  \r\nBulgaria  \r\nBurkina Faso  \r\nBurundi  \r\nCambodia  \r\nCameroon  \r\nCanada  \r\nCape Verde  \r\nCayman Islands  \r\nCentral African Republic  \r\nChad  \r\nChile  \r\nChina  \r\nChristmas Island  \r\nCocos Islands  \r\nColombia  \r\nComoros  \r\nCongo  \r\nCook Islands  \r\nCosta Rica  \r\nCroatia  \r\nCuba  \r\nCyprus  \r\nCzech Republic  \r\nDenmark  \r\nDjibouti  \r\nDominica  \r\nDominican Republic  \r\nEast Timor  \r\nEcuador  \r\nEgypt  \r\nEl Salvador  \r\nEquatorial Guinea  \r\nEstonia  \r\nEthiopia  \r\nFalkland Islands  \r\nFaroe Islands  \r\nFiji  \r\nFinland  \r\nFormer Czechoslovakia  \r\nFrance  \r\nFrench Guyana  \r\nGabon  \r\nGambia  \r\nGeorgia  \r\nGermany  \r\nGhana  \r\nGibraltar  \r\nGreat Britain  \r\nGreece  \r\nGreenland  \r\nGrenada  \r\nGuadeloupe  \r\nGuam  \r\nGuatemala  \r\nGuinea  \r\nGuinea Bissau  \r\nGuyana  \r\nHaiti  \r\nHonduras  \r\nHong Kong  \r\nHungary  \r\nIceland  \r\nIndia  \r\nIndonesia  \r\nIran  \r\nIraq  \r\nIreland  \r\nIsrael  \r\nItaly  \r\nIvory Coast  \r\nJamaica  \r\nJapan  \r\nJordan  \r\nKazakhstan  \r\nKenya  \r\nKiribati  \r\nKuwait  \r\nKyrgyzstan  \r\nLaos  \r\nLatvia  \r\nLebanon  \r\nLesotho  \r\nLiberia  \r\nLibya  \r\nLiechtenstein  \r\nLithuania  \r\nLuxembourg  \r\nMacau  \r\nMacedonia  \r\nMadagascar  \r\nMalawi  \r\nMalaysia  \r\nMaldives  \r\nMali  \r\nMalta  \r\nMarshall Islands  \r\nMartinique  \r\nMauritania  \r\nMauritius  \r\nMayotte  \r\nMexico  \r\nMicronesia  \r\nMoldavia  \r\nMonaco  \r\nMongolia  \r\nMontserrat  \r\nMorocco  \r\nMozambique  \r\nMyanmar  \r\nNamibia  \r\nNauru  \r\nNepal  \r\nNetherlands  \r\nNetherlands Antilles  \r\nNeutral Zone  \r\nNew Caledonia  \r\nNew Zealand  \r\nNicaragua  \r\nNiger  \r\nNigeria  \r\nNiue  \r\nNorfolk Island  \r\nNorth Korea  \r\nNorway  \r\nOman  \r\nPakistan  \r\nPalau  \r\nPanama  \r\nPapua New Guinea  \r\nParaguay  \r\nPeru  \r\nPhilippines  \r\nPitcairn Island  \r\nPoland  \r\nPolynesia  \r\nPortugal  \r\nPuerto Rico  \r\nQatar  \r\nReunion  \r\nRomania  \r\nRussian Federation  \r\nRwanda  \r\nSaint Helena  \r\nSaint Lucia  \r\nSaint Vincent and Grenadines  \r\nSamoa  \r\nSan Marino  \r\nSaudi Arabia  \r\nSenegal  \r\nSeychelles  \r\nSierra Leone  \r\nSingapore  \r\nSlovak Republic  \r\nSlovenia  \r\nSolomon Islands  \r\nSomalia  \r\nSouth Africa  \r\nSouth Korea  \r\nSpain  \r\nSri Lanka  \r\nSudan  \r\nSuriname  \r\nSwaziland  \r\nSweden  \r\nSwitzerland  \r\nSyria  \r\nTadjikistan  \r\nTaiwan  \r\nTanzania  \r\nThailand  \r\nTogo  \r\nTokelau  \r\nTonga  \r\nTrinidad and Tobago  \r\nTunisia  \r\nTurkey  \r\nTurkmenistan  \r\nTuvalu  \r\nUganda  \r\nUkraine  \r\nUnited Arab Emirates  \r\nUnited Kingdom  \r\nUnited States  \r\nUruguay  \r\nUzbekistan  \r\nVanuatu  \r\nVatican City State  \r\nVenezuela  \r\nVietnam  \r\nVirgin Islands (British)  \r\nVirgin Islands (USA)  \r\nWallis and Futuna Islands  \r\nWestern Sahara  \r\nYemen  \r\nYugoslavia  \r\nZaire  \r\nZambia  \r\nZimbabwe');


CREATE TABLE IF NOT EXISTS `jcow_tmp` (
  `tkey` varchar(70) NOT NULL default '',
  `tcontent` text,
  KEY `tkey` (`tkey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE IF NOT EXISTS `jcow_var_cache` (
  `name` varchar(60) NOT NULL default '',
  `content` varchar(255) NOT NULL default '',
  `created` int(11) NOT NULL default '0',
  KEY `name` (`name`,`created`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE IF NOT EXISTS `jcow_votes` (
  `sid` int(11) NOT NULL default '0',
  `created` int(11) NOT NULL default '0',
  `rate` int(11) NOT NULL default '0',
  `uid` int(11) NOT NULL default '0',
  KEY `sid` (`sid`,`uid`),
  KEY `created` (`created`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE IF NOT EXISTS `jcow_forum_subscribes` (
  `uid` int(11) NOT NULL default '0',
  `tid` int(11) NOT NULL default '0',
  KEY `uid` (`uid`,`tid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `jcow_chatrooms` (
  `id` int(11) NOT NULL auto_increment,
  `uid` int(11) NOT NULL default '0',
  `fid` int(11) NOT NULL default '0',
  `content` text,
  `updated` int(11) NOT NULL default '0',
  `created` int(11) NOT NULL default '0',
  `request_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `uid` (`uid`,`fid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `jcow_reports` (
  `id` int(11) NOT NULL auto_increment,
  `uid` int(11) NOT NULL default '0',
  `url` varchar(255) NOT NULL default '',
  `message` text,
  `hasread` tinyint(1) NOT NULL default '0',
  `created` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;



CREATE TABLE IF NOT EXISTS `jcow_reports` (
  `id` int(11) NOT NULL auto_increment,
  `uid` int(11) NOT NULL default '0',
  `url` varchar(255) NOT NULL default '',
  `message` text,
  `hasread` tinyint(1) NOT NULL default '0',
  `created` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `jcow_messages_sent` (
  `id` int(11) NOT NULL auto_increment,
  `subject` varchar(100) NOT NULL default '',
  `message` text,
  `from_id` int(11) NOT NULL default '0',
  `to_id` int(11) NOT NULL default '0',
  `created` int(11) NOT NULL default '0',
  `hasread` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `from_id` (`from_id`,`to_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `jcow_liked` (
  `id` int(11) NOT NULL auto_increment,
  `uid` int(11) NOT NULL default '0',
  `stream_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `uid` (`uid`),
  KEY `stream_id` (`stream_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `jcow_subscr` (
  `id` varchar(32) NOT NULL default '',
  `item_number` varchar(32) NOT NULL default '',
  `status` varchar(25) NOT NULL default '',
  `uid` int(11) NOT NULL default '0',
  `timeline` int(11) NOT NULL default '0',
  KEY `id` (`id`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE IF NOT EXISTS `jcow_menu` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL default '',
  `tab_name` varchar(50) NOT NULL default '',
  `weight` int(11) NOT NULL default '0',
  `path` varchar(255) NOT NULL default '',
  `app` varchar(50) NOT NULL default '',
  `actived` tinyint(1) NOT NULL default '0',
  `type` varchar(25) NOT NULL default '',
  `protected` tinyint(1) NOT NULL default '0',
  `allowed_roles` text,
  `icon` varchar(255) NOT NULL default '',
  `parent` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=105 ;



INSERT INTO `jcow_menu` VALUES (14,'My feed','In my network',1,'feed','feed',1,'app',0,'1','',''),(18,'My account','My information',20,'account','account',1,'personal',0,'','',''),(19,'Avatar','',34,'account/avatar','account',1,'tab',0,'','','account'),(21,'Privacy','',36,'account/privacy','account',1,'tab',0,'','','account'),(22,'Password','',37,'account/cpassword','account',1,'tab',0,'','','account'),(23,'Invite','',20,'invite','invite',1,'app',0,'','',''),(101,'Images','Hot',42,'images','images',1,'community',0,'1,2','',''),(105,'Settings','',43,'account/settings','account',1,'tab',0,'1,2','','account'),(106,'Messages','',44,'message/inbox','message',1,'app',0,'1,2','',''),(107,'New','',45,'images/newest','images',1,'tab',0,'1,2','','images'),(108,'In my network','',46,'images/following','images',1,'tab',0,'1,2','','images'),(109,'My images','',47,'images/mine','images',1,'personal',0,'1,2','','jcow_base'),(132,'Members','Browse',48,'browse','browse',1,'crossplatform',1,'1,2','','');



CREATE TABLE IF NOT EXISTS `jcow_modules` (
  `name` varchar(50) NOT NULL default '',
  `actived` tinyint(1) NOT NULL default '0',
  `hooking` tinyint(4) NOT NULL default '0',
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



INSERT INTO `jcow_modules` VALUES ('browse',1,0),('feed',1,0),('dashboard',1,1),('account',1,0),('admin',1,1),('u',1,0),('member',1,0),('follow',1,0),('friends',1,0),('jquery',1,0),('language',1,0),('message',1,0),('notifications',1,0),('preference',1,0),('report',1,0),('rss',1,0),('search',1,0),('invite',1,0),('apps',1,0),('blacklist',1,0),('update',1,0),('images',1,1),('forums',0,1),('forumadmin',0,0),('ads',0,1),('music',0,1),('blogs',0,1),('page',0,0),('group',0,0),('groups',0,1),('videos',0,1),('events',0,1),('pages',0,1),('questions',0,1),('chatbar',0,1),('cache',0,1),('fblogin',0,0),('mobile',0,1);




CREATE TABLE IF NOT EXISTS `jcow_banned` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(100) NOT NULL default '',
  `ip1` varchar(3) NOT NULL default '',
  `ip2` varchar(3) NOT NULL default '',
  `ip3` varchar(3) NOT NULL default '',
  `ip4` varchar(3) NOT NULL default '',
  `created` int(11) NOT NULL default '0',
  `expired` int(11) NOT NULL default '0',
  `operator` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;



CREATE TABLE IF NOT EXISTS `jcow_pages` (
  `id` int(11) NOT NULL auto_increment,
  `uri` varchar(30) NOT NULL default '',
  `uid` int(11) NOT NULL default '0',
  `views` int(11) NOT NULL default '0',
  `logo` varchar(100) NOT NULL default '',
  `name` varchar(100) NOT NULL default '',
  `style_ids` text,
  `custom_css` text,
  `background` varchar(100) NOT NULL default '',
  `type` varchar(25) NOT NULL default '',
  `description` text,
  `users` int(11) NOT NULL default '0',
  `updated` int(11) NOT NULL default '0',
  `var1` varchar(255) NOT NULL default '',
  `var2` varchar(255) NOT NULL default '',
  `var3` varchar(255) NOT NULL default '',
  `text1` text,
  `text2` text,
  PRIMARY KEY  (`id`),
  KEY `uid` (`uid`),
  KEY `uri` (`uri`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;



CREATE TABLE IF NOT EXISTS `jcow_page_users` (
  `id` int(11) NOT NULL auto_increment,
  `pid` int(11) NOT NULL default '0',
  `uid` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `pid` (`pid`,`uid`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE IF NOT EXISTS `jcow_user_crafts` (
  `uid` int(11) NOT NULL default '0',
  `hash` varchar(5) NOT NULL default '',
  `created` int(11) NOT NULL default '0',
  KEY `uid` (`uid`,`created`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE IF NOT EXISTS `jcow_disliked` (
  `id` int(11) NOT NULL auto_increment,
  `uid` int(11) NOT NULL default '0',
  `stream_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `uid` (`uid`,`stream_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;


CREATE TABLE IF NOT EXISTS `jcow_footer_pages` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  `link_name` varchar(255) NOT NULL default '',
  `content` text,
  `weight` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


INSERT INTO `jcow_footer_pages` (`id`, `name`, `link_name`, `content`, `weight`) VALUES
(1, 'About Us', 'About Us', 'You(admin) can edit this page from "Admin CP"-"Footer Pages".', 1),
(2, 'Contact Us', 'Contact Us', 'You(admin) can edit this page from "Admin CP"-"Footer Pages".', 2);


CREATE TABLE IF NOT EXISTS `jcow_pending_review` (
  `id` int(11) NOT NULL auto_increment,
  `uid` int(11) NOT NULL default '0',
  `content` text,
  `uri` varchar(255) NOT NULL default '',
  `post_id` varchar(50) NOT NULL default '',
  `created` int(11) NOT NULL default '0',
  `ignored` tinyint(4) NOT NULL default '0',
  `stream_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `uid` (`uid`),
  KEY `post_id` (`post_id`),
  KEY `stream_id` (`stream_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `jcow_limit_posting` (
  `uid` int(11) NOT NULL default '0',
  `created` int(11) NOT NULL default '0',
  `act` varchar(50) NOT NULL default '',
  KEY `uid` (`uid`,`created`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `jcow_mentions` (
  `id` int(11) NOT NULL auto_increment,
  `uid` int(11) NOT NULL default '0',
  `stream_id` int(11) NOT NULL default '0',
  `wall_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;



CREATE TABLE IF NOT EXISTS `jcow_topics` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL default '',
  `stories` int(11) NOT NULL default '0',
  `followers` int(11) NOT NULL default '0',
  `updated` int(11) NOT NULL default '0',
  `featured` tinyint(4) NOT NULL default '0',
  `description` text,
  PRIMARY KEY  (`id`),
  KEY `name` (`name`),
  KEY `stories` (`stories`),
  KEY `featured` (`featured`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;



CREATE TABLE IF NOT EXISTS `jcow_topics_followed` (
  `uid` int(11) NOT NULL default '0',
  `tid` int(11) NOT NULL default '0',
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE IF NOT EXISTS `jcow_topic_ids` (
  `tid` int(11) NOT NULL default '0',
  `sid` int(11) NOT NULL default '0',
  KEY `tid` (`tid`,`sid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `jcow_page_visitors` (
  `pid` int(11) NOT NULL default '0',
  `uid` int(11) NOT NULL default '0',
  `updated` int(11) NOT NULL default '0',
  KEY `pid_uid` (`pid`,`uid`),
  KEY `pid_updated` (`pid`,`updated`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `jcow_status_photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sid` int(11) NOT NULL default '0',
  `pid` int(11) NOT NULL default '0',
  `uri` varchar(100) NOT NULL default '',
  `des` varchar(255) NOT NULL default '',
  `thumb` varchar(100) NOT NULL default '',
  `size` int(11) NOT NULL default '0',
  PRIMARY KEY (`id`),
  KEY `sid` (`sid`),
  KEY `pid` (`pid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `jcow_spammers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userkey` varchar(255) NOT NULL default '',
  `type` varchar(50) NOT NULL default '',
  `created` int(11) NOT NULL default '0',
  `status` varchar(50) NOT NULL default '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `userkey` (`userkey`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `jcow_spammers_cloud` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userkey` varchar(255) NOT NULL default '',
  `type` varchar(50) NOT NULL default '',
  `created` int(11) NOT NULL default '0',
  `status` varchar(50) NOT NULL default '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `userkey` (`userkey`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `jcow_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL default '0',
  `question` text,
  `answer` text,
  `created` int(11) NOT NULL default '0',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `jcow_im` (
`id` int(11) NOT NULL default '0',
  `from_id` int(11) NOT NULL default '0',
  `to_id` int(11) NOT NULL default '0',
  `message` text,
  `created` int(11) NOT NULL default '0',
  `hasread` tinyint(4) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `jcow_notes` (
`id` int(11) NOT NULL default '0',
  `uid` int(11) NOT NULL default '0',
  `created` int(11) NOT NULL default '0',
  `message` text,
  `from_uid` int(11) NOT NULL default '0',
  `hasread` tinyint(4) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


ALTER TABLE `jcow_im`
 ADD PRIMARY KEY (`id`), ADD KEY `from_id` (`from_id`,`to_id`);

ALTER TABLE `jcow_notes`
 ADD PRIMARY KEY (`id`), ADD KEY `uid` (`uid`);


ALTER TABLE `jcow_im`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
ALTER TABLE `jcow_notes`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;


