<?php
/* ############################################################ *\
 ----------------------------------------------------------------
@package	Jcow Social Networking Script.
@copyright	Copyright (C) 2009 - 2010 jcow.net.  All Rights Reserved.
@license	see http://jcow.net/license
 ----------------------------------------------------------------
\* ############################################################ */
if (file_exists('includes/libs/admin_a.hook.php')) {
	include 'includes/libs/admin_a.hook.php';
}
else {
	include 'includes/libs/admin.hook.php';
}