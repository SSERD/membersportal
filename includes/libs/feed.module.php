<?php
/* ############################################################ *\
 ----------------------------------------------------------------
Jcow Software (http://www.jcow.net)
IS NOT FREE SOFTWARE
http://www.jcow.net/commercial_license
Copyright (C) 2009 - 2010 jcow.net.  All Rights Reserved.
 ----------------------------------------------------------------
\* ############################################################ */

class feed{
	function __construct() {
		global $content, $db, $apps, $client, $settings, $menuon,$config;
		$menuon = 'feed';
	}
	function index($page = 0) {
		set_title(t('News feed'));
		enable_jcow_inline_ad();
		global $client,$captcha,$tab_menu;
		$tab_menu = array();
		if (!$client['id']) {
			global $content, $db, $apps, $client, $settings,$tab_menu,$parr;
			enable_jcow_inline_ad();
			c(feed_activity_get(0,15,0,0,1,'latest').' ');
			$hooks = check_hooks('widget');
			$widgets = array();
			if (is_array($hooks)) {
				foreach ($hooks as $hook) {
					$hook_func = $hook.'_widget';
					$hook_func($widgets);
				}
			}
			foreach($widgets as $key=>$widget) {
				$callback = $widget['callback'];
				if (function_exists($callback)) {
					$return = $callback();
					ass(array('title'=>h($widget['name']),
						'content'=>$return));
				}
			}
			stop_here();
		}
		if (!get_gvar('custom_logo') && allow_access(3)) {
			c(url('admin/themes#img','<i class="fa fa-upload"></i> Upload your own site LOGO'));
			section_close();
		}
		if ($client['id'] && $client['disabled']) {
			c(alert_verify());
		}
		if ($client['avatar'] == 'undefined.jpg') {
			$uf[] = url('account/avatar', t('Avatar picture'));
		}

	
		$res = sql_query("select fid from ".tb()."friends where uid='{$client['id']}' order by rand() limit 20");
		while ($row = sql_fetch_array($res)) {
			$uids[] = $row['fid'];
		}
		if (count($uids)>0) {
			$i=0;
			$res = sql_query("select distinct f.fid,u.* from jcow_friends as f left join jcow_accounts as u on u.id=f.fid where f.uid in (".implode(',',$uids).") order by rand() limit 30");
			while ($user = sql_fetch_array($res)) {
				if ($user['id']>0 && $i < 3 && $user['id'] != $client['id']) {
					$res2 = sql_query("select * from jcow_friends where uid='{$client['id']}' and fid='{$user['id']}' ");
					$res3 = sql_query("select * from jcow_friend_reqs where uid='{$client['id']}' and fid='{$user['id']}' ");
					if (!sql_counts($res2) && !sql_counts($res3)) {
						$res2 = sql_query("select count(*) as num from jcow_friends where uid='{$user['id']}' and fid in 
							(select fid from jcow_friends where uid='{$client['id']}')
							");
						$row = sql_fetch_array($res2);
						if ($user['avatar']) {
							$user['avatar'] = uploads.'/avatars/'.$user['avatar'];
						}
						$people_suggest .= listing_box(
							array('name'=>h($user['fullname']),'img'=>$user['avatar'],'link'=>url('u/'.$user['username']),'des'=>'<div style="white-space: nowrap;">'.t('{1} mutual friend(s)',$row['num']).'</div><span><a href="#" class="btn btn-xs btn-default doadd" userid="'.$user['id'].'" ><i class="fa fa-user-plus"></i> '.t('Add Friend').'</a></span>'
						));
						$i++;
					}
				}
			}
		}
		c(stream_form($client['id'],array(),'',array('fromhome'=>1)));
		section_close();
		if ($people_suggest) {
			if (is_mobile()) {
				c('<div style="font-weight:bold;color:#999">'.t('People you may know').'</div>'.$people_suggest);
				section_close();
			}
			else {
				ass(array('title'=>t('People you may know'),'content'=>$people_suggest));
			}
		}
		//
		
		if ($client['settings']['feed_filter'] == 'following') {
			$sd_btns = '<span><a href="'.url('feed/setdefault/latest').'">'.t('Latest').'</a></span><span class="sd_on">'.t('Following').'</span><span><a href="'.url('feed/setdefault/new').'">'.t('New').'</a></span>';
		}
		elseif ($client['settings']['feed_filter'] == 'new') {
			$sd_btns = '<span><a href="'.url('feed/setdefault/latest').'">'.t('Latest').'</a></span><span><a href="'.url('feed/setdefault/following').'">'.t('Following').'</a></span><span class="sd_on">'.t('New').'</span>';
		}
		else {
			$sd_btns = '<span class="sd_on">'.t('Latest').'</span><span><a href="'.url('feed/setdefault/following').'">'.t('Following').'</a></span><span><a href="'.url('feed/setdefault/new').'">'.t('New').'</a></span>';
		}
		
		if (!is_mobile()) {
			c('<div class="set_default_btns">
				<label>'.t('Filter').'</label>'.$sd_btns.'
				</div>');
			section_close('',array('pure'=>1));
		}
		
		if ($client['settings']['feed_filter'] == 'following') {
			c(feed_activity_get($client['id'],10,0,0,1,'following'));
		}
		elseif ($client['settings']['feed_filter'] == 'new') {
			c(activity_get(0,10,0,0,1));
		}
		else {
			c(feed_activity_get(0,10,0,0,1,'latest').' ');
		}

		$hooks = check_hooks('widget');
		$widgets = array();
		if (is_array($hooks)) {
			foreach ($hooks as $hook) {
				$hook_func = $hook.'_widget';
				$hook_func($widgets);
			}
		}
		foreach($widgets as $key=>$widget) {
			$callback = $widget['callback'];
			if (function_exists($callback)) {
				$return = $callback();
				ass(array('title'=>h($widget['name']),
					'content'=>$return));
			}
		}
	}
	function setdefault($filter = 'hot') {
		if (preg_match("/^[a-z]+$/",$filter)) {
			save_u_settings(array('feed_filter'=>$filter));
		}
		redirect(url('feed'));
	}

	function explore() {
		global $client;
		$num = 10;
		c(feed_activity_get(0,$num,0,0,1,'hot').' ');
		if (module_actived('topics')) {
			$res = sql_query("select * from ".tb()."topics  where featured>0 order by stories limit 20");
			while ($tag = sql_fetch_array($res)) {
				$tags .= '<a href="'.url('topics/topic/'.$tag['id']).'" class="jcow_tags">'.h($tag['name']).'</a>';
			}
			ass(array('title'=>url('topics',t('Community tags')),'content'=>$tags));
		}
	}

	function share($stream_id) {
		global $client;
		if (!$client['id']) die('need login');
		// selector
		$res = sql_query("select * from jcow_pages where uid='{$client['id']}' and type='u'");
		$page = sql_fetch_array($res);
		$options .= '<option value="'.$page['id'].'">'.t('My Profile').'</option>';
		$options .= '<optgroup label="Groups">';
		$res = sql_query("select p.* from ".tb()."page_users as u left join ".tb()."pages as p on p.id=u.pid where u.uid='{$client['id']}' and p.type='group' order by p.updated DESC limit 100");
		while ($group = sql_fetch_array($res) ) {
			$options .= '<option value="'.$group['id'].'">'.h($group['name']).'</option>';
		}
		$options .= '</optgroup>';
		$options .= '<optgroup label="Pages">';
		$res = sql_query("select p.* from ".tb()."page_users as u left join ".tb()."pages as p on p.id=u.pid where u.uid='{$client['id']}' and p.type='page' order by p.updated DESC limit 100");
		while ($page = sql_fetch_array($res) ) {
			$options .= '<option value="'.$page['id'].'">'.h($page['name']).'</option>';
		}
		$options .= '</optgroup>';

		if (is_numeric($stream_id) && $stream_id > 0) {
			$res = sql_query("select s.*,u.username,u.avatar,p.uid as wall_uid from ".tb()."streams as s left join ".tb()."accounts as u on u.id=s.uid left join ".tb()."pages as p on p.id=s.wall_id where s.id='$stream_id' and s.hide!=1 ");
			$stream = sql_fetch_array($res);
			$stream['attachment'] = unserialize($stream['attachment']);
			if ($stream['type'] == 1) {
				if (!$client['id']) {
					echo t('Access denied');
					exit;
				}
				$res = sql_query("select * from jcow_friends where uid='{$client['id']}' and fid='{$stream['uid']}'");
				if (!sql_counts($res)) {
					echo t('Access denied');
					exit;
				}
			}
			echo '<div class="block"><div class="block_content">
			<div style="background:#eee">
			Share to: 
				<div style="width:60%;display:inline-block">
				 <select name="pageid" class="input-sm form-control pageid">'.$options.'</select>
				</div>
			
			<textarea class="form-control share_msg" rows=2 placeholder="'.t('Say something about this').'"></textarea>
			</div>
				<div class="stream_quote">
				'.stream_display($stream,'preview',0,0,0,array('share_preview'=>1)).'
				</div>
			<div><button class="btn btn-block btn-primary share_post" sid="'.$stream_id.'">Post</button></div>
			</div></div>';
		}
		else {
			echo 'feed not found';
		}
		exit;
	}

	function sharepost($stream_id) {
		global $client;
		if (!$client['id']) die('need login');
		if (!is_numeric($_POST['pageid'])) die('no pageid');
		if (is_numeric($stream_id) && $stream_id > 0) {
			$res = sql_query("select s.*,u.username,u.avatar,p.uid as wall_uid from ".tb()."streams as s left join ".tb()."accounts as u on u.id=s.uid left join ".tb()."pages as p on p.id=s.wall_id where s.id='$stream_id' and s.hide!=1 ");
			$stream = sql_fetch_array($res);
			if (!$stream['id']) die('stream not found');
			$res = sql_query("select * from jcow_pages where id='{$_POST['pageid']}'");
			$page = sql_fetch_array($res);
			if (!$page['id']) die('page not found');
			if ($page['type'] == 'u' || $page['type'] == 'page') {
				if ($page['uid'] != $client['id']) {
					die('access denied');
				}
			}
			sql_query("insert into jcow_streams(message,wall_id,uid,created,app,aid) values('{$_POST['msg']}','{$_POST['pageid']}','{$client['id']}','".time()."','repost',$stream_id )");

			echo '<div style="padding:100px 0;text-align:center;font-size:20px"><i class="fa fa-lg fa-check"></i> Shared successfully!</div>';
		}
		else {
			echo 'feed not found';
		}
		exit;
	}

	function savepages() {
		global $client;
		need_login();
		if (!count($_POST['pageids'])) {
			sys_back('No page selected');
		}
		foreach($_POST['pageids'] as $page_id) {
			$res = sql_query("select * from ".tb()."pages where id='$page_id'");
			$page = sql_fetch_array($res);
			if ($page['id']) {
				if ($page['type'] == 'u') {
					$res2 = sql_query("select * from ".tb()."accounts where id='{$page['uid']}'");
					$user = sql_fetch_array($res2);
					if ($user['id']) {
						$res3 = sql_query("select * from ".tb()."followers where uid='{$client['id']}' and fid='{$user['id']}' limit 1");
						if (!sql_counts($res3)) {
							$follow['uid'] = $client['id'];
							$follow['fid'] = $user['id'];
							sql_insert($follow, tb().'followers');
							sql_query("update ".tb()."accounts set followers=followers+1 where id='{$user['id']}'");
						}
					}
				}
				else {
					$res2 = sql_query("select * from ".tb()."page_users where pid='{$page['id']}' and uid='{$client['id']}'");
					if (!sql_counts($res2)) {
						sql_query("insert into ".tb()."page_users (uid,pid) value ('{$client['id']}','{$page['id']}')");
						sql_query("update ".tb()."pages set users=users+1 where id='{$page['id']}'");
					}
				}
			}
		}
		redirect('feed',1);
	}

	function mentions() {
		need_login();
		global $content, $db, $apps, $client, $settings;
		$lastread_key = 'feed_mr'.$client['id'];
		$timeline = set_tmp($lastread_key,time());
		$num = 10;
		feed_activity_get($client['id'],$num,0,0,1,'mentions');
		ass(array('title'=>t('My account'),'content'=>dashboard_my_account()));

	}

	function likes() {
		need_login();
		global $content, $db, $apps, $client, $settings;
		$num = 10;
		c(feed_activity_get($client['id'],$num,0,0,1,'likes'));
	}

	function view($stream_id=0) {
		global $client;
		if (is_numeric($stream_id) && $stream_id > 0) {
			$res = sql_query("select s.*,u.username,u.avatar,p.uid as wall_uid from ".tb()."streams as s left join ".tb()."accounts as u on u.id=s.uid left join ".tb()."pages as p on p.id=s.wall_id where s.id='$stream_id' and s.hide!=1 ");
			$stream = sql_fetch_array($res);
			$stream['attachment'] = unserialize($stream['attachment']);
			if ($stream['type'] == 1) {
				if (!$client['id']) {
					c(t('Access denied'));
					stop_here();
				}
				$res = sql_query("select * from jcow_friends where uid='{$client['id']}' and fid='{$stream['uid']}'");
				if (!sql_counts($res)) {
					c(t('Access denied'));
					stop_here();
				}
			}
			c(stream_display($stream,'preview',0,0,200));
		}
	}


	function hot($page = 0) {
		global $content, $db, $apps, $client, $settings,$tab_menu,$parr;
		need_login();
		enable_jcow_inline_ad();
		if ($client['id']) {
			c(stream_form($client['id'],array(),'',array('fromhome'=>1)));
			section_close();
			ass(array('title'=>t('My account'),'content'=>dashboard_my_account()));
		}
		c(feed_activity_get(0,15,0,0,1,'hot').' ');
	}

	function latest($page = 0) {
		global $content, $db, $apps, $client, $settings,$tab_menu,$parr;
		need_login();
		enable_jcow_inline_ad();
		if ($client['id']) {
			c(stream_form($client['id'],array(),'',array('fromhome'=>1)));
			section_close();
			ass(array('title'=>t('My account'),'content'=>dashboard_my_account()));
		}
		c(feed_activity_get(0,15,0,0,1,'latest').' ');
	}

	function feedmoreactivities() {
		global $client;
		if ($_POST['type'] == 'latest') {
			echo feed_activity_get(0,$_POST['num'],$_POST['offset'],$_POST['target_id'],0,$_POST['type'],array('direct_output'=>1));
		}
		else {
			do_auth(2);
			echo feed_activity_get($client['id'],$_POST['num'],$_POST['offset'],$_POST['target_id'],0,$_POST['type'],array('direct_output'=>1));
		}
		exit;
	}
	function all($page = 0) {
		global $client;
		if ($client['id']) {
			ass(array('title'=>t('My account'),'content'=>dashboard_my_account()));
		}
		enable_jcow_inline_ad();
		set_title(t('News feed'));
		global $content, $db, $apps, $client, $settings;
		if ($client['id']) {
			c(stream_form($client['id'],array(),'',array('fromhome'=>1)));
			section_close();
		}
		$num = 10;
		c(activity_get(0,$num,0,0,1));
	}
}


