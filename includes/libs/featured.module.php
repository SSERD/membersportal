<?php
/* ############################################################ *\
 ----------------------------------------------------------------
Jcow Software (http://www.jcow.net)
IS NOT FREE SOFTWARE
http://www.jcow.net/commercial_license
Copyright (C) 2009 - 2010 jcow.net.  All Rights Reserved.
 ----------------------------------------------------------------
\* ############################################################ */

class featured{
	function featured() {
		global $content, $db, $apps, $client, $settings, $menuon,$config,$top_title;
		$config['hide_sidebar_ad'] = 1;
		$top_title = '';
		$menuon = 'feed';
	}

	function index($page = 0) {
		global $content, $db, $apps, $client, $settings,$tab_menu;
		if (!$client['id'] && get_gvar('fp_enable_loginbox')) {
			$output = '
<form method="post" name="loginform" id="form1" action="'.url('member/loginpost').'" >
			'.t('Username or Email').':<br />
			<input type="text" size="10" name="username" style="width:220px" /><br />
							'.t('Password').':<br />
			<input type="password" size="10" name="password" style="width:220px" /><br />
			<div class="sub">( '.url('member/chpass',t('Forgot password?')).' )</div>
			<label for="remember_me">
			<input type="checkbox" name="remember_me" value="1" id="remember_me" /> '.t('Remember me').'
			</label><br />
			<input type="submit" value="'.t('Login').'" style="font-size:25px" /> <strong>'.url('member/signup',t('Create an account')).'</strong>
			</form>
			<script language="javascript">document.loginform.username.focus();</script>';
			if (get_gvar('fb_id')) {
				$output .= '<div style="margin:20px 0;text-align:center">'.url('fblogin','<img src="'.uhome().'/modules/fblogin/button.png" />').'</div>';
			}
			ass(array('title'=>t('Login'),'content'=>$output));
			}
		$sidebar = get_text('featuredpage_sidebar');
		if (strlen($sidebar)) {
			ass(array('content'=>$sidebar));
		}
		$header = get_text('featuredpage_header');
		if (strlen($header)) {
			c($header);
			section_close();
		}
		c(featured_story_get(0,12,0,0,1));
	}

	function morestory() {
		echo featured_story_get(0,$_POST['num'],$_POST['offset'],0,0,$_POST['type']);
		exit;
	}
}


function featured_story_get($uid,$num = 12,$offset=0,$target_id=0,$pager=0,$type='') {
	global $client, $adbanner_displayed,$config;
	$extra = $num+1;
	$i = 1;
	if ($offset > 1000 || $num > 50) return ''; // to avoid from high server load;
	if (!is_numeric($offset)) die('wrong act p');
	$res = sql_query("select st.*,u.username,u.avatar,p.uid as wall_uid from ".tb()."stories as s left join ".tb()."streams as st on st.id=s.stream_id left join ".tb()."accounts as u on u.id=s.uid left join ".tb()."pages as p on p.id=s.page_id where s.featured>0 order by s.id desc limit $offset,$extra");
	
	$i=1;
	while($row = sql_fetch_array($res)) {
		if ($i <= $num) {
			$row['attachment'] = unserialize($row['attachment']);
			$output .= stream_display($row,'preview','',$target_id);
		}
		if ($offset == 0 && $i == 1 && !$config['hide_ad']) {
			$output .= '<div style="padding-left:80px">'.get_gvar('jcow_inline_banner_ad').'</div>';
			$adbanner_displayed = 1;
			}
		$i++;
	}
	if ($pager && $i > $num) {
		$uid = str_replace(',','_',$uid);
		if ($offset == 0) {
			$output .= '<div id="morestream_box"></div>
				<div>
				<script>
				$(document).ready(function(){
					$(".block_content").on("click",".morestream_button",function() {
						$(this).hide();
						$("#morestream_box").html("<img src=\"'.uhome().'/files/loading.gif\" /> Loading");
						$.post("'.uhome().'/index.php?p=featured/morestory",
									{offset:$("#stream_offset").val(),num:'.$num.'},
									  function(data){
										var currentVal = parseInt( $("#stream_offset").val() );
										$("#stream_offset").val(currentVal + '.$num.');
										$("#morestream_box").before(data);
										if (data.length>50) {
											$(".morestream_button").show();
										}
										$("#morestream_box").html("");
										},"html"
									);
						return false;
					});
				});
				</script>
	
				<input type="hidden" id="stream_offset" value="'.$num.'" /><a href="#" class="morestream_button"><strong>'.t('See More').'</strong></a>
			</div>';
		}
		else {
		$output .= '
			<a href="#" class="morestream_button"><strong>'.t('See More').'</strong></a>
			';
			}
	}
	return $output;
}