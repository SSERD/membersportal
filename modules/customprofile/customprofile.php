<?php
/* ############################################################ *\
 ----------------------------------------------------------------
Jcow Software (http://www.jcow.net)
IS NOT FREE SOFTWARE
http://www.jcow.net/commercial_license
Copyright (C) 2009 - 2010 jcow.net.  All Rights Reserved.
 ----------------------------------------------------------------
\* ############################################################ */


class customprofile {
	function customprofile() {
		global $tab_menu, $nav, $config, $client, $blocks, $styles, $custom_css, $menuon;
		$menuon = 'myprofile';
		$config['hide_ad'] = 1;
		$blocks['right'] = '';
		if (!$client['id']) {
			redirect(url('member/login'));
		}
		set_title(t('Customize my profile theme'));
		
		$nav[] = url('customprofile',t('customprofile'));
	}
	
	function index($page_id=0) {
		global $nav, $client, $uhome;
		if (!$page_id) {
			$page_id = $client['page']['id'];
		}
		set_subtitle(t('Custom my profile theme'));
		do_auth(explode('|',get_gvar('permission_etheme')));
		$res = sql_query("SELECT * FROM `".tb()."pages` where id='$page_id' and uid='{$client['id']}'");
		$profile = sql_fetch_array($res);
		if (!$profile['id']) die('access denied');
		$custom = unserialize($profile['custom_css']);
		section_content('<script type="text/javascript">
	    jQuery(document).ready(function($) {
	    	$("input[name=\'wallpaper\']").click(
	    		function() {
	    		$("#custom_wallpaper").toggle("fast");
	    		}
	    		);
			$("input[name=\'generalpage\']").click(
	    		function() {
	    		$("#custom_generalpage").toggle("fast");
	    		}
	    		);
			$("input[name=\'bheader\']").click(
	    		function() {
	    		$("#custom_bheader").toggle("fast");
	    		}
	    		);
	    })
	  </script>');
		if ($custom['wallpaper']) {
			$wallpaper = 'checked';
		}
		else {
			$toggle_wallpaper = ' style="display:none" ';
		}
		if ($custom['wallpaper_repeat_x']) {
			$repeat_x = 'checked';
		}
		if ($custom['wallpaper_repeat_y']) {
			$repeat_y = 'checked';
		}
		$wallpaper_bg_color = $custom['wallpaper_bg_color'];
		if ($custom['wallpaper_bg_image']) {
			$delete_act = url('customprofile/backgrounddel/'.$page_id,t('Delete Current Image'));
		}
		if ($custom['cover_image']) {
			$delete_cover = url('customprofile/coverdel/'.$page_id,t('Delete Cover Image'));
		}
		if ($custom['wallpaper_bg_position'] == 'left') {
			$pleft = 'selected';
		}
		elseif ($custom['wallpaper_bg_position'] == 'right') {
			$pright = 'selected';
		}
		else {
			$pcenter = 'selected';
		}
		// general page
		if ($custom['generalpage']) {
			$generalpage = 'checked';
		}
		else {
			$toggle_generalpage = ' style="display:none" ';
		}
		if ($custom['generalpage_transparent']) {
			$generalpage_transparent = 'checked';
		}
		$generalpage_bg_color = $custom['generalpage_bg_color'];
		$generalpage_font_color = $custom['generalpage_font_color'];
		$generalpage_link_color = $custom['generalpage_link_color'];

		// block header
		if ($custom['bheader']) {
			$bheader = 'checked';
		}
		else {
			$toggle_bheader = ' style="display:none" ';
		}
		$bheader_bg_color = $custom['bheader_bg_color'];
		$bheader_font_color = $custom['bheader_font_color'];
		$bheader_link_color = $custom['bheader_link_color'];

		if ($custom['musicplayer']) {
			$musicplayer = 'checked';
		}
		else {
			$musicplayer = '';
		}
		if ($custom['hide_avatar']) {
			$hide_avatar_check = 'checked';
		}
		else {
			$hide_avatar_check = '';
		}
		
		if ($profile['type'] == 'u') {
			c('<p>&lt;&lt;'.url('u/'.$client['username'],h($client['fullname'])).'</p>');
		}
		else {
			c('<p>&lt;&lt;'.url($profile['type'].'/'.$profile['uri'],h($profile['name'])).'</p>');
		}

		section_content('
		<link rel="stylesheet" media="screen" type="text/css" href="'.uhome().'/js/colorpicker/css/colorpicker.css" />
<script type="text/javascript" src="'.uhome().'/js/colorpicker/js/colorpicker.js"></script>

<script>

$(document).ready(function() {
	$(\'#wallpaper_bg_color,#cover_text_color\').ColorPicker({
		onSubmit: function(hsb, hex, rgb, el) {
			$(el).val(hex);
			$(el).ColorPickerHide();
		},
		onBeforeShow: function () {
			$(this).ColorPickerSetColor(this.value);
		}
	})
	.bind(\'keyup\', function(){
		$(this).ColorPickerSetColor(this.value);
	});
});
</script>
');
		section_content('
		<form action="'.url('customprofile/post').'" method="post"  enctype="multipart/form-data">
		<h2>'.t('Cover').'</h2>
		<div>
		<p>
		<label>'.t('Upload cover image').'</label>
		<input type="file" name="cover_img" />
		 '.$delete_cover.'
		</p>
		
		<p>
		<label>'.t('Text color').'</label>
		<input type="text" name="cover_text_color" id="cover_text_color" size="8" value="'.$custom['cover_text_color'].'" />
		</p>
		
		<p>
		<label for="hide_avatar">
		<input type="checkbox" id="hide_avatar" name="hide_avatar" '.$hide_avatar_check.' value=1 /> '.t('Hide avatar pic').'</label>
		</p>
		
		</div>');	
		
		if ($profile['type'] == 'u') {
			c('<h2>'.t('Music player').'</h2>
			<div>
			<label for="check_musicplayer">
			<input type="checkbox" id="check_musicplayer" name="musicplayer" '.$musicplayer.' value=1 /> '.t('Play my liked music in my profile page.').'
			<label>
			</div>');
			}
		


		c('<h2>'.t('Wallpaper').'</h2>
		<div>
		<label for="check_wallpaper"><input type="checkbox" id="check_wallpaper" name="wallpaper" value="1"  '.$wallpaper.' />
		'.t('Enable custom Wall Paper').'</label>
		<div id="custom_wallpaper" '.$toggle_wallpaper.' >
		<p>
		<label>'.t('Upload background image').'</label>
		<input type="file" name="bg_img" />
		 '.$delete_act.'
		</p>
		<p>
		<label>'.t('Position').'</label>
		<select name="position">
		<option value="left" '.$pleft.' >Left</option>
		<option value="right" '.$pright.' >Right</option>
		<option value="center" '.$pcenter.' >Center</option>
		</select>
		</p>
		<p>
		<label>Repeat-X</label>
		<input type="checkbox" name="repeat_x" value="1" '.$repeat_x.' />Yes
		</p>
		<p>
		<label>Repeat-Y</label>
		<input type="checkbox" name="repeat_y" value="1" '.$repeat_y.'  />Yes
		</p>
		<p>
		<label>'.t('Background color').'</label>
		<input type="text" name="wallpaper_bg_color" id="wallpaper_bg_color" size="8" value="'.$wallpaper_bg_color.'" />
		<br /><span>This color will fill the space out of the background image</span>
		</p>
		</div>
		</div>


		<p>
		<input type="hidden" name="page_id" value="'.$page_id.'" />
		<input type="submit" class="button" value="'.t('Save changes').'" />
		</p>
		</form>
			');
		c('<img src="'.uhome().'/files/theme_help.gif" align="right" />');
	}

	function post() {
		global $nav, $client, $current_sub_menu, $uhome;
		if (!preg_match("/^[0-9a-z]{6}$/i",$_POST['wallpaper_bg_color'])) {
			$_POST['wallpaper_bg_color'] = 'eeeeee';
		}
		if (!preg_match("/^[0-9a-z]{6}$/i",$_POST['cover_text_color'])) {
			$_POST['cover_text_color'] = '';
		}
		$page_id = $_POST['page_id'];
		$res = sql_query("SELECT * FROM `".tb()."pages` where id='$page_id' and uid='{$client['id']}'");
		$profile = sql_fetch_array($res);
		if (!$profile['id']) die('access denied');
		$arr = unserialize($profile['custom_css']);

		$arr['wallpaper'] = $_POST['wallpaper'] ? 1:0;
		$arr['wallpaper_bg_color'] = $_POST['wallpaper_bg_color'];
		$arr['cover_text_color'] = $_POST['cover_text_color'];
		$arr['wallpaper_bg_position'] = $_POST['position'];
		$arr['wallpaper_repeat_x'] = $_POST['repeat_x'];
		$arr['wallpaper_repeat_y'] = $_POST['repeat_y'];
		
		// up pic
		if ($_FILES['bg_img']['tmp_name']) {
			if ($uri = save_file($_FILES['bg_img'])) {
				if ($arr['wallpaper_bg_image']) {
					@unlink($arr['wallpaper_bg_image']);
				}
				$arr['wallpaper_bg_image'] = $uri;
			}
		}
		// up cover
		if ($_FILES['cover_img']['tmp_name']) {
			if ($uri = save_file($_FILES['cover_img'])) {
				if ($arr['cover_image']) {
					@unlink($arr['cover_image']);
				}
				$arr['cover_image'] = $uri;
			}
		}
		$arr['musicplayer'] = $_POST['musicplayer'];
		$arr['hide_avatar'] = $_POST['hide_avatar'];

		$custom_css = serialize($arr);
		sql_query("update `".tb()."pages` set custom_css='$custom_css' where id='$page_id'");
		if ($profile['type'] == 'u') {
			$profile['uri'] = $client['username'];
		}
		redirect(url($profile['type'].'/'.$profile['uri']),1);

	}

	function backgrounddel($page_id=0) {
		global $nav, $client, $current_sub_menu, $uhome;
		$res = sql_query("SELECT * FROM `".tb()."pages` where id='$page_id' and uid='{$client['id']}'");
		$profile = sql_fetch_array($res);
		if (!$profile['id']) die('access denied');
		$arr = unserialize($profile['custom_css']);
		if (strlen($arr['wallpaper_bg_image'])) {
			unlink($arr['wallpaper_bg_image']);
			$arr['wallpaper_bg_image'] = '';
		}
		$custom_css = serialize($arr);
		sql_query("update `".tb()."pages` set custom_css='$custom_css' where id='{$client['page']['id']}'");
		redirect(url('customprofile/'.$profile['id']),1);

	}
	
	function coverdel($page_id=0) {
		global $nav, $client, $current_sub_menu, $uhome;
		$res = sql_query("SELECT * FROM `".tb()."pages` where id='$page_id' and uid='{$client['id']}'");
		$profile = sql_fetch_array($res);
		if (!$profile['id']) die('access denied');
		$arr = unserialize($profile['custom_css']);
		if (strlen($arr['cover_image'])) {
			@unlink($arr['cover_image']);
		}
		$arr['cover_image'] = '';
		$custom_css = serialize($arr);
		sql_query("update `".tb()."pages` set custom_css='$custom_css' where id='{$client['page']['id']}'");
		redirect(url('customprofile/'.$profile['id']),1);

	}

}

function get_style_list($dirname) {
	if ($handle = opendir($dirname)) {
		while (false !== ($file = readdir($handle))) {
			if (is_dir($dirname . '/' .$file) && $file != '.' && $file != '..' && $file != '.svn' ) {
				$dirs[] = $file;
			}
		}
		closedir($handle);
		
		if (is_array($dirs)) {
			asort($dirs);
			return $dirs;
		}
		else {
			return 0;
		}
	}
}