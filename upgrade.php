<?php

// db
require_once './my/config.php';
if (!is_array($db_info)) {
	header("location:install.php");
	exit;
}
include './includes/libs/common.inc.php';
require_once './includes/libs/db.inc.php';
$conn=sql_connect($db_info['host'], $db_info['user'], $db_info['pass'], $db_info['dbname']);
sql_query("SET NAMES UTF8");


echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head><title>Upgrade</title></head>
<body>';


sql_query("CREATE TABLE IF NOT EXISTS `jcow_page_visitors` (
  `pid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `updated` int(11) NOT NULL,
  KEY `pid_uid` (`pid`,`uid`),
  KEY `pid_updated` (`pid`,`updated`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
");


sql_query("CREATE TABLE IF NOT EXISTS `jcow_story_comments` (
  `id` int(11) NOT NULL auto_increment,
  `sid` int(11) NOT NULL default '0',
  `uid` int(11) NOT NULL default '0',
  `target_uid` int(11) NOT NULL,
  `cid` int(11) NOT NULL,
  `title` varchar(150) NOT NULL default '',
  `content` text NOT NULL,
  `created` int(11) NOT NULL default '0',
  `app` varchar(50) NOT NULL default '',
  `pending` tinyint(1) NOT NULL default '0',
  `vote` tinyint(4) NOT NULL default '0',
  `stream_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `sid` (`sid`,`uid`),
  KEY `target_uid` (`target_uid`),
  KEY `stream_id` (`stream_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;
");


$result = sql_query("SHOW COLUMNS FROM ".tb()."invites");
while ($row = sql_fetch_array($result)) {
	if ($row['Field'] == 'code') {
		$invidecode = 'exists';
	}
}
if ($invidecode != 'exists') {
	sql_query(" 
	ALTER TABLE ".tb()."invites ADD `code` VARCHAR( 15 ) NOT NULL;
	");
	echo 'jcow_invites: add "code"<hr />';
}


sql_query("
	CREATE TABLE IF NOT EXISTS `jcow_topics` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL,
  `stories` int(11) NOT NULL,
  `followers` int(11) NOT NULL,
  `updated` int(11) NOT NULL,
  `featured` tinyint(4) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `name` (`name`),
  KEY `stories` (`stories`),
  KEY `featured` (`featured`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1
	");
	sql_query("
	CREATE TABLE IF NOT EXISTS `jcow_topics_followed` (
  `uid` int(11) NOT NULL,
  `tid` int(11) NOT NULL,
  KEY `uid` (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8
	");
	sql_query("
	CREATE TABLE IF NOT EXISTS `jcow_topic_ids` (
  `tid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  KEY `tid` (`tid`,`sid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
	");	
	
sql_query("
CREATE TABLE IF NOT EXISTS `jcow_status_photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sid` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `uri` varchar(100) NOT NULL,
  `des` varchar(255) NOT NULL,
  `thumb` varchar(100) NOT NULL,
  `size` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sid` (`sid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
");

sql_query("
CREATE TABLE IF NOT EXISTS `jcow_spammers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userkey` varchar(255) NOT NULL,
  `type` varchar(50) NOT NULL,
  `created` int(11) NOT NULL,
  `status` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `userkey` (`userkey`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
");

sql_query("
CREATE TABLE IF NOT EXISTS `jcow_spammers_cloud` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userkey` varchar(255) NOT NULL,
  `type` varchar(50) NOT NULL,
  `created` int(11) NOT NULL,
  `status` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `userkey` (`userkey`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
");

sql_query("
CREATE TABLE IF NOT EXISTS `jcow_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `question` text NOT NULL,
  `answer` text NOT NULL,
  `created` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
");

$result = sql_query("SHOW COLUMNS FROM ".tb()."topics");
while ($row = sql_fetch_array($result)) {
	if ($row['Field'] == 'featured') {
		$topic_featured = 'exists';
	}
	if ($row['Field'] == 'description') {
		$topic_description = 'exists';
	}
}
if ($topic_featured != 'exists') {
	$sql = " 
	ALTER TABLE `jcow_topics` ADD `featured` TINYINT NOT NULL ;
	";
	sql_query($sql);
	$sql = " 
	ALTER TABLE `jcow_topics` ADD INDEX ( `featured` ) ;
	";
	sql_query($sql);
	echo 'jcow_topics: add "featured"<hr />';
}
if ($topic_description != 'exists') {
	$sql = " 
	ALTER TABLE `jcow_topics` ADD `description` TEXT NOT NULL ;
	";
	sql_query($sql);
	echo 'jcow_topics: add "description"<hr />';
}



$result = sql_query("SHOW INDEX FROM ".tb()."topics_followed");
while ($row = sql_fetch_array($result)) {
	if ($row['Column_name'] == 'tid') {
		$topics_followed_indexed = 'exists';
	}
}
if ($topics_followed_indexed != 'exists') {
	sql_query(" ALTER TABLE `jcow_topics_followed` ADD INDEX ( `tid` ); ");
	echo 'jcow_topics_followed: alert index for "tid"<hr />';
}


$result = sql_query("SHOW INDEX FROM ".tb()."cache");
while ($row = sql_fetch_array($result)) {
	if ($row['Column_name'] == 'ckey') {
		$cache_indexed = 'exists';
	}
}
if ($cache_indexed != 'exists') {
	sql_query(" ALTER TABLE `jcow_cache` ADD INDEX ( `ckey` ) ; ");
	echo 'jcow_cache: alert index for "ckey"<hr />';
}

$result = sql_query("SHOW COLUMNS FROM ".tb()."pages");
while ($row = sql_fetch_array($result)) {
  if ($row['Field'] == 'var1') {
    $jcow93 = 'passed';
  }
}
if ($jcow93 != 'passed') {
  sql_query("ALTER TABLE ".tb()."pages ADD `var1` VARCHAR( 255 ) NOT NULL");
  sql_query("ALTER TABLE ".tb()."pages ADD `var2` VARCHAR( 255 ) NOT NULL");
  sql_query("ALTER TABLE ".tb()."pages ADD `var3` VARCHAR( 255 ) NOT NULL");
  sql_query("ALTER TABLE `jcow_pages` ADD `text1` TEXT NOT NULL");
  sql_query("ALTER TABLE `jcow_pages` ADD `text2` TEXT NOT NULL");
  echo 'jcow pages added var and text <hr />';
}

$result = sql_query("SHOW COLUMNS FROM `jcow_page_users`");
while ($row = sql_fetch_array($result)) {
  if ($row['Field'] == 'id') {
    $jcow94 = 'passed';
  }
}
if ($jcow94 != 'passed') {
  sql_query("ALTER TABLE `jcow_page_users` ADD `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST");
  echo 'jcow_page_users added id and auto increment <hr />';
}

$result = sql_query("SHOW TABLES LIKE 'jcow_im'");
$num = sql_counts($result);
if ($num>0) {
    $jcow10 = 'passed';
}
if ($jcow10 != 'passed') {
  sql_query("CREATE TABLE IF NOT EXISTS `jcow_im` (
`id` int(11) NOT NULL,
  `from_id` int(11) NOT NULL,
  `to_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `created` int(11) NOT NULL,
  `hasread` tinyint(4) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8");
  sql_query("CREATE TABLE IF NOT EXISTS `jcow_notes` (
`id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `created` int(11) NOT NULL,
  `message` text NOT NULL,
  `from_uid` int(11) NOT NULL,
  `hasread` tinyint(4) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8");
  sql_query("ALTER TABLE `jcow_im`
 ADD PRIMARY KEY (`id`), ADD KEY `from_id` (`from_id`,`to_id`)");
  sql_query("ALTER TABLE `jcow_notes`
 ADD PRIMARY KEY (`id`), ADD KEY `uid` (`uid`)");
  sql_query("ALTER TABLE `jcow_im`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1");
  sql_query("ALTER TABLE `jcow_notes`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1");
  echo 'Jcow 10.0 db upgraded <hr />';
}

$result = sql_query("SHOW COLUMNS FROM `jcow_streams`");
while ($row = sql_fetch_array($result)) {
  if ($row['Field'] == 'data') {
    $jcow11 = 'passed';
  }
}
if ($jcow11 != 'passed') {
  sql_query("ALTER TABLE `jcow_streams` ADD `data` TEXT NOT NULL ;");
  echo 'jcow_streams added "data" <hr />';
}


echo '
<div style="color:green;font-size:25px;">
Finished!<br />Please Delete this file Now!</div>
</body></html>';