<?php

class events extends story{
	public $list_type = 'ul';
	function __construct() {
		global $nav,$ubase;
		parent::story();
		$this->photos = 1;
		$this->act_write = t('Created an event');
		$this->write_story = t('Start new event');
		set_menu_path('events');

	}

	function index() {
		global $client;
		if ($client['id']) {
			button($this->name.'/writestory',t('Start new event'));
			if ($subscribed_events = subscribed_events(5)) {
				c($subscribed_events);
			}
			else {
				c('<div class="sub">'.t('No events coming up').'</div>');
			}
			section_close(t('Subscribed'));

			if ($innetwork_events = innetwork_events(5)) {
				c($innetwork_events);
			}
			else {
				c('<div class="sub">'.t('No events coming up').'</div>');
			}
			section_close(t('In my network'));

			if (strlen($client['location']) && strlen($client['country'])) {
				if ($innetwork_events = nearby_events(5)) {
					c($innetwork_events);
				}
				else {
					c('<div class="sub">'.t('No events coming up').'</div>');
				}
			}
			else {
				c(url('account','<i class="fa fa-location-arrow"></i> '.t('Please enter your City first')));
			}
			section_close(t('Nearby'));
		}
		if ($upcoming_events = events_upcoming(10)) {
			c($upcoming_events);
		}
		else {
			c('<div class="sub">'.t('No events coming up').'</div>');
		}
		section_close(t('All Events coming up'));

		if (!$past_events = past_events(5)) {
			$past_events = '<div class="sub">'.t('No events past').'</div>';
		}
		ass(array('title'=>t('Past events'),'content'=>$past_events));
	}

	
	function convert_content_before_insert($content) {
		return nl2br(h($content));
		
	}
	function hook_writestory($row) {
		$output = '
		<script>
		$(document).ready( function(){
			$("#datepicker").datepicker({
				onSelect: function(date, instance) {
						$("#event_date").val(date);
				}
			});
		});

		</script>
		<div style="padding-left:20px">
			<div type="text" id="datepicker"></div>
			<input type="hidden" name="date" id="event_date" value="'.date('m/d/Y').'" />
			
							<p>'.label(t('Time')).timeselector().'
							</p>
							<p>
								'.google_places(array(),'').'
								</p>
			
		</div>
';
		return $output;
	}
	
	function hook_writestorypost(&$story) {
		global $client;
		
		$timeline = strtotime($_POST['date'].' '.$_POST['time'].' GMT')-$client['timezone']*3600;
		$story['var1'] = $_POST['location'];
		$story['var2'] = $timeline;
		$story['var3'] = $_POST['placeid'];
		$story['var4'] = $_POST['locality'];
		$story['var5'] = $_POST['country'];
		$uids[] = $client['id'];
		$text1 = serialize($uids);
		$story['text1'] = $text1;
	}
	
	function hook_viewstory($story) {
		global $client;
		if ($story['var2'] < time()) {
			$story['past'] = 1;
		}
		if ($story['past']) {
			$gflag = t('Went');
		}
		else {
			$gflag = t('Going');
		}
		if (!$story['past']) {
			$output .= '<div style="padding:5px">';
			if ($client['id']) {
				$output .= avatar($client,'small');
				$res = sql_query("select * from jcow_votes where sid='{$story['id']}' and uid='{$client['id']}'");
				$row = sql_fetch_array($res);
				if (!$row['rate']) {
					$output .= '
					<span><a href="#" class="btn btn-sm btn-default events_engage" eventid="'.$story['id'].'" engage="1"><i class="fa fa-plus"></i> '.$gflag.'</a></span>
					<span><a href="#" class="btn btn-sm btn-default events_engage" eventid="'.$story['id'].'" engage="2">'.t('Interested').'</a></span>';
				}
				else {
					if ($row['rate'] == 1) {
						$output .= '<i class="fa fa-check"></i> '.$gflag;
					}
					else {
						$output .= '<i class="fa fa-check"></i> '.t('Interested');
					}
					$output .= ' <span><a href="#" class="btn btn-xs btn-default events_engage" eventid="'.$story['id'].'" engage="0">'.t('Cancel').'</a></span>';
				}
			}
			$output .= '</div>';
		}
		$res = sql_query("select count(*) as num from jcow_votes where sid='{$story['id']}' and rate=1");
		$row = sql_fetch_array($res);
		if ($row['num']) {
			$output .= '<h4>'.$gflag.' ('.$row['num'].')</h4>';
			$output .= '<div>';
			$res = sql_query("select u.* from jcow_votes as v left join jcow_accounts as u on u.id=v.uid where v.sid='{$story['id']}' and v.rate=1 limit 20");
			while ($user = sql_fetch_array($res)) {
				$output .= avatar($user,'small');
			}
			$output .= '</div>';
		}

		$res = sql_query("select count(*) as num from jcow_votes where sid='{$story['id']}' and rate=2");
		$row = sql_fetch_array($res);
		if ($row['num']) {
			$output .= '<h4>'.t('Interested').' ('.$row['num'].')</h4>';
			$output .= '<div>';
			$res = sql_query("select u.* from jcow_votes as v left join jcow_accounts as u on u.id=v.uid where v.sid='{$story['id']}' and v.rate=2 limit 10");
			while ($user = sql_fetch_array($res)) {
				$output .= avatar($user,'small');
			}
			$output .= '</div>';
		}


		if (strlen($story['var3'])) {
			$output .= google_map_show($story['var3']);
		}
		$output .= '
				<p>
				<strong>'.t('Location').':</strong> '.h($story['var1']).'
				</p>
				<p>
				<strong>'.t('Time').':</strong> '.get_date($story['var2']).'
				</p>
				';
		return $output;
	}
	
	function joinit($sid) {
		global $client;
		need_login();
		$story = valid_story($sid);
		$uids = unserialize($story['text1']);
		if (is_array($uids)) {
			if (in_array($client['id'],$uids)) {
				sys_back(t('You have joined this event'));
			}
		}
		$uids[] = $client['id'];
		$text1 = serialize($uids);
		sql_query("update `".tb()."stories` set text1='$text1' where id={$story['id']}");
		redirect('events/viewstory/'.$story['id'],1);
	}
	function leaveit($sid) {
		global $client;
		need_login();
		$story = valid_story($sid);
		$uids = unserialize($story['text1']);
		foreach ($uids as $uid) {
			if ($uid != $client['id']) {
				$nuids[] = $uid;
			}
		}
		$text1 = serialize($nuids);
		sql_query("update `".tb()."stories` set text1='$text1' where id={$story['id']}");
		redirect('events/viewstory/'.$story['id'],1);
	}
	
	function story_form_content($row = array()) {
		return '<p>'.label(t('Description')).'<textarea name="form_content" rows="3" class="form-control">'.htmlspecialchars($row['content']).'</textarea></p>';
	}

	function theme_story_footer($row) {
		$output .= '<div class="tab_thing">'.$this->list_created($row).'</div>';
		$output .= '<div class="tab_thing">'.$this->list_username($row).'</div>';
		if (time() > $row['var2']) {
			$begintime = '<font color="red">'.get_date($row['var2'],'date').'</font>';
		}
		else {
			$begintime = '<font color="green">'.get_date($row['var2'],'date').'</font>';
		}
		$output .= '<div class="tab_thing">'.t('Begin').': '.$begintime.'</div>';
		return $output;
	}

	function ajax_engage() {
		global $client;
		if (!$client['id'] || !is_numeric($_POST['sid'])) die();
		$res = sql_query("select * from jcow_stories where id='{$_POST['sid']}' and app='events'");
		$story = sql_fetch_array($res);
		if (!$story['id']) die();
		$res = sql_query("select * from jcow_votes where sid='{$story['id']}' and uid='{$client['id']}'");
		$row = sql_fetch_array($res);
		if ($row['sid']) {
			if ($_POST['engage'] == 1 || $_POST['engage'] == 2) {
				sql_query("update jcow_votes set rate='{$_POST['engage']}' where sid='{$row['sid']}' and uid='{$client['id']}'");
			}
			else {
				sql_query("delete from jcow_votes where sid='{$row['sid']}' and uid='{$client['id']}'");
				echo t('Canceled');
			}
		}
		else {
			if ($_POST['engage'] == 1 || $_POST['engage'] == 2) {
				sql_query("insert into jcow_votes(sid,uid,rate,created) values('{$story['id']}','{$client['id']}','{$_POST['engage']}',".time().")");
			}
		}
		if ($_POST['engage'] == 1) echo '<i class="fa fa-check"></i> '.t('Going');
		if ($_POST['engage'] == 2) echo '<i class="fa fa-check"></i> '.t('Interested');
		exit;
	}

	function ajax_form($page_type='') {
		global $client;
		if (!$client) die('login');
		echo '	
		<script>
		$(document).ready( function(){
			$("#event_title").focus();
			 $( "#datepicker" ).datepicker();
			$("#form_submit").click(function() {
				if ($(document).find("#placename")[0] && !$(document).find("#placename").val()) {
					alert("'.t('Please select location from droplist').'");
					return false;
				}
			});
		});

		</script>
		<table border="0">
		<tr><td><strong>'.t('Name').'</td><td><input type="text" id="event_title" name="event_title" size="60" /></td></tr>
		<tr><td><strong>'.t('Where').'</td><td>'.google_places(array(),'').'</td></tr>
		<tr><td><strong>'.t('When').'</td><td><input type="text" id="datepicker" name="date" size="20" /> '.timeselector().'</td></tr>
		<tr><td><strong>'.t('Image').'</td><td><input type="file" name="event_image" /></td></tr>
		</table>
		<textarea name="form_content" id="form_content" class="form-control" rows="3" placeholder="'.t('Description').'"></textarea>';
		exit;
	}

	function ajax_post() {
		global $client;
		if (!$client) die('login');
		if (!$_POST['event_title']) events::ajax_error(t('Please input a Title'));
		$vote_options['rating'] = t('Rating');
		foreach ($vote_options as $key=>$vla) {
			$ratings[$key] = array('score'=>0,'users'=>0);
		}
		$page = story::check_page_access($_POST['page_id']);
		$story = array(
			'cid' => 0,
			'page_id' => $_POST['page_id'],
			'page_type'=>$page['type'],
			'title' => $_POST['event_title'],
			'content' => $_POST['form_content'],
			'uid' => $client['id'],
			'created' => time(),
			'app' => 'events',
			'rating' => serialize($ratings)
			);
		$timeline = strtotime($_POST['date'].' '.$_POST['time'].' GMT')-$client['timezone']*3600;
		$story['var1'] = $_POST['location'];
		$story['var2'] = $timeline;
		$story['var3'] = $_POST['placeid'];
		$story['var4'] = $_POST['locality'];
		$story['var5'] = $_POST['country'];
		$uids[] = $client['id'];
		$text1 = serialize($uids);
		$story['text1'] = $text1;
		if (sql_insert($story, tb().'stories')) {
			$sid = $story['id'] = insert_id();
			$set_story['id'] = $sid;
			save_tags($stags,$sid,'events');
			if ($_FILES['event_image']['tmp_name']) {
				$file_tmp_name = $_FILES['event_image']['tmp_name'];
				$photo = array('name'=>$_FILES['event_image']['name'],
				'tmp_name'=>$file_tmp_name,
				'type'=>$_FILES['event_image']['type'],
				'size'=>$_FILES['event_image']['size']);
				list($width, $height) = getimagesize($photo['tmp_name']);
				if ($width <= 500) {
					$uri = save_file($photo);
				}
				else {
					$height = floor(500*$height/$width);
					$uri = save_thumbnail($photo, 500,0);
				}
				$set_story['photos'] = 1;
				$thumb = save_thumbnail($photo, 200, 300);
				$set_story['thumbnail'] = $thumb;
				$size = $photo['size'];
				sql_query("insert into `".tb()."story_photos` (sid,uri,des,thumb,size) values( {$story['id']},'$uri','','$thumb','$size')");
			}
			// write act
			$attachment = array(
				'cwall_id' => 'events'.$sid,
				'uri' => 'events/viewstory/'.$sid,
				'name' => $_POST['event_title']
				);
			$app = array('name'=>'events','id'=>$sid);
			$stream_id = stream_publish(t('started an event'),$attachment,$app,$client['id'],$_POST['page_id']);
			$set_story['stream_id'] = $stream_id;
			sql_update($set_story,tb()."stories");

			if ($page['type'] == 'u') {
				$return = parse_mentions($_POST['description']);
					add_mentions($return['mentions'],$stream_id);
			}

			$stream = array('id'=>$stream_id,'username'=>$client['username'],'avatar'=>$client['avatar'],'wall_id'=>$page['id'],'wall_uid'=>$page['uid'],'app'=>'events','uid'=>$client['id'],'created'=>time(),'aid'=>$story['id']);
			echo json_encode(array('formdata'=>'reset','streamdata'=>stream_display($stream,'preview')));
		}
		else {
			events::ajax_error('failed to add event');
			
		}
		exit;
	}

	function ajax_error($msg) {
		echo '<div style="color:red">'.$msg.'</div>';
		echo events::ajax_form();
		exit;
	}
}

function subscribed_events($num=5) {
	global $client;
	if (!$client['location']) return '';
	$res = sql_query("select s.* from jcow_votes as v left join jcow_stories as s on s.id=v.sid where v.uid='{$client['id']}' and s.app='events'");
	while ($event = sql_fetch_array($res)) {
		$output .= events_list($event);
	}
	return $output;
}

function innetwork_events($num=5) {
	global $client;
	if (!$client['location']) return '';
	$timeline = time();
	$pids = array($client['page']['id']);
	//frd
	$res = sql_query("select p.id from jcow_friends as f left join jcow_pages as p on (p.uid=f.fid and p.type='u') where f.uid='{$client['id']}' order by rand() limit 50");
	while ($row = sql_fetch_array($res)) {
		if ($row['id'] > 0) {
			$pids[] = $row['id'];
		}
	}
	$res = sql_query("select * from jcow_stories where app='events' and page_id in (".implode(',',$pids).") order by id DESC limit $num");
	while ($event = sql_fetch_array($res)) {
		$output .= events_list($event);
	}
	return $output;
}

function nearby_events($num=5) {
	global $client;
	if (!$client['location']) return '';
	$timeline = time();
	$res = sql_query("select * from jcow_stories where app='events' and var4='".safe($client['location'])."' order by id DESC limit $num");
	while ($event = sql_fetch_array($res)) {
		$output .= events_list($event);
	}
	return $output;

	}
function events_upcoming($num = 5) {
	$timeline = time();
	$res = sql_query("select * from jcow_stories where app='events' and var2>$timeline order by id DESC limit $num");
	while ($event = sql_fetch_array($res)) {
		$output .= events_list($event);
	}
	return $output;
}
function past_events($num = 5) {
	$timeline = time() - 3600*24;
	$res = sql_query("select * from jcow_stories where app='events' and var2<$timeline order by id DESC limit $num");
	while ($event = sql_fetch_array($res)) {
		$event['content'] = '';
		$output .= events_list($event);
	}
	return $output;
}
function events_list($event) {
	if ($event['var2'] > time() ) {
		$hl = 'class="hl"';
	}
	return listing_box(
							array('name'=>h($event['title']),'img'=>$event['thumbnail'],'link'=>url('events/viewstory/'.$event['id']),'des'=>'<div style="white-space: nowrap;">'.h(utf8_substr($event['content'],80)).'</div>
								<div '.$hl.'><strong>'.get_date($event['var2']).'</strong> - '.h($event['var1'].', '.$event['var4']).'</div>'
						));
}




