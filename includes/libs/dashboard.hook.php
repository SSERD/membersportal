<?php
/* ############################################################ *\
 ----------------------------------------------------------------
@package	Jcow Social Networking Script.
@copyright	Copyright (C) 2009 - 2010 jcow.net.  All Rights Reserved.
@license	see http://jcow.net/license
 ----------------------------------------------------------------
\* ############################################################ */

function dashboard_widget(&$widgets) {
	global $parr;
	//if ($parr[0] != 'feed') return '';
	if (!$GLOBALS['client']['id']) return '';
	$widgets['dashboard_my_account'] = array(
		'name'=>t('My account'),
		'callback'=>'dashboard_my_account'
		);
	if (!get_gvar('disable_birthday')) {
		$widgets['dashboard_friends_birthday'] = array(
			'name'=>t('Birthdays coming up'),
			'callback'=>'dashboard_friends_birthday'
			);
	}
}


function dashboard_my_account() {
		global $client, $new_apps;
		if (!$client['id']) return false;
		$res = sql_query("select * from `".tb()."pages` where uid='{$client['id']}' and type='u'");
		$row = sql_fetch_array($res);
		if ($client['avatar'] == 'undefined.jpg') {
			$uf[] = url('account/avatar', t('Avatar picture'));
		}
		$profile_views = $row['views'];
		$res = sql_query("select count(*) as num from ".tb()."followers where uid='{$client['id']}'");
		$row = sql_fetch_array($res);
		$client['following'] = $row['num'];
		$content .= '
		<ul class="jcow_sidebar_links">
		<li>'.url('u/'.$client['username'],
			t('My Profile').' <i>('.t('{1} views',$profile_views).')</i>'
			).'</li>
		<li>'.url('u/'.$client['username'].'/following',t('{1} following','<strong>'.$client['following'].'</strong>')).'</li>
			<li>'.url('u/'.$client['username'].'/followers',t('{1} followers','<strong>'.$client['followers'].'</strong>')).'</li>
		<li>'.url('preference',t('Preference')).'</li>';
		foreach ($new_apps as $item) {
			if ($item['type'] == 'personal') {
				$content .= '<li>
				<a href="'.url($item['path']).'">'.t($item['name']).'</a>
				</li>';
			}
		}
		$content .= '</ul>';
		
		return $content;
	}


function dashboard_friends_birthday() {
	global $client;
	$current = time() + $client['timezone']*3600;
	$m = gmdate('n',$current);
	$d = gmdate('j',$current);
	$next = $m+1;
	if ($m<10) $m = '0'.$m;
	if ($next > 12) $next = '01';
	if ($d > 20) {
		$nextm = " or (f.uid='{$client['id']}' and birthmonth='$next' and birthday<$d) ";
	}
	$res = sql_query("select u.* from ".tb()."followers as f left join ".tb()."accounts as u on u.id=f.fid where (f.uid='{$client['id']}' and u.birthmonth='$m' and u.birthday>=$d) $nextm  order by u.id desc limit 15");
	$content = '<ul class="simple_list">';
	while ($user = sql_fetch_array($res)) {
		$total++;
		if ($user['birthmonth'] < 10) $user['birthmonth'] = '0'.$user['birthmonth'];
		if ($user['birthday'] < 10) $user['birthday'] = '0'.$user['birthday'];
		$content .= '<li>'.avatar($user,25).url('u/'.$user['username'],$user['username']).' - Birthday: <strong>'.$user['birthmonth'].'-'.$user['birthday'].'</strong></li>';
	}
	$content .= '</ul>';
	if (!$total) $content = t('none');
	return $content;
}
