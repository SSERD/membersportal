<?php
/* ############################################################ *\
 ----------------------------------------------------------------
@package	Jcow Social Networking Script.
@copyright	Copyright (C) 2009 - 2010 jcow.net.  All Rights Reserved.
@license	see http://jcow.net/license
 ----------------------------------------------------------------
\* ############################################################ */


function events_menu() {
	$items = array();
	$items['events'] = array(
		'name'=>'Events',
		'type'=>'community'
	);
	return $items;
}

?>