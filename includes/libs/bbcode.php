<?php
/* ############################################################ *\
 ----------------------------------------------------------------
Jcow Software (http://www.jcow.net)
IS NOT FREE SOFTWARE
http://www.jcow.net/commercial_license
Copyright (C) 2009 - 2010 jcow.net.  All Rights Reserved.
 ----------------------------------------------------------------
\* ############################################################ */
// use this function after htmlspecialchars()


function decode_bb($msg){
        $search_array = array(
            "/\[quote](.*)\[\/quote]/isU",
            "/\[quote=(.*)](.*)\[\/quote]/isU",
            "/\[img]([^'\"\?\&\+]*(\.gif|jpg|jpeg|png|bmp))\[\/img]/iU",
            "/\[img=([^'\"\?\&\+]*(\.gif|jpg|jpeg|png|bmp))]([^'\"]*)\[\/img]/iU",
            
            "/\[b](.*)\[\/b]/isU",
            "/\[i](.*)\[\/i]/isU",
            "/\[u](.*)\[\/u]/isU",
            '/https?:\/\/[\w\-\.!~#?&=+\*\'"\/]+/'
        );

        $replace_array = array(
            "<div class=\"quote\">\\1</div>",
            "<div class=\"quote\">\\1<br />\\2</div>",
            " <img src=\"\\1\" alt=\"User's Image\" onload=\"if(this.width>580) {this.resized=true; this.width=580; this.alt='Click here to open new window';}\" onmouseover=\"this.style.cursor='hand';\" onclick=\"window.open('\\1');\" /> ",
            " <img src=\"\\1\" alt=\"\\3\" onload=\"if(this.width>580) this.width=580;\" onmouseover=\"this.style.cursor='hand';\" onclick=\"window.open('\\1');\" /> ",
            
            "<b>\\1</b>",
            "<i>\\1</i>",
            "<u>\\1</u>",
            '<a href="$0" rel="nofollow">$0</a>'
        );
        $msg = preg_replace($search_array, $replace_array, $msg);
        if(preg_match_all("/\[code](.*)\[\/code]/isU", $msg, $match)){
            foreach($match[1] as $key=>$a){
                $msg = str_replace($match[0][$key], "<pre class='prettyprint'>".
                    str_replace("<br />", "", $a)
                    ."</pre>",$msg
                    );
            }
        }
		return $msg;
}