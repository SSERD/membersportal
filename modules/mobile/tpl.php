<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">	
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<base href="<?php echo uhome()?>/" />
<style type="text/css" media="all">@import "<?php echo $uhome;?>/modules/mobile/style.css";</style>
<title><?php echo $title?> - <?php echo get_gvar('site_name')?></title>
<meta name="Generator" content="Powered by Jcow" />
<link rel="shortcut icon" href="<?php echo uhome().'/themes/'.get_gvar('theme_tpl').'/ico.gif';?>" type="image/x-icon" />
<body>

<div id="header">
<div id="logo"><?php echo url('mobile',get_gvar('site_name'))?></div>
<?php
if ($client['id']) {
	if(!$friendslink = mobile_f_request())
		$friendslink = url('mobile/friends',t('Friends'));

	echo '<div id="menu">
	'.url('mobile',t('Home')).'  '.$friendslink.'  '.url('mobile/message',t('Inbox').msg_unread() ).' '.url('mobile/notifications',t('Notifications').note_unread()).'
	</div>';
}
	?>
</div>



<?php
if (is_array($notices)) {
	foreach ($notices as $notice) {
		echo '<div class="notice">'.$notice.'</div>';
	}
}
if (is_array($sections)) {
		foreach ($sections as $section) {
			echo '<div class="block">';
			if (strlen($section['title'])) {
				echo  '<div class="block_title">'.$section['title'].'</div>';
			}
			echo  '<div class="block_content">'.$section['content'].'</div>
			</div>';
		}
	}
?>


<div id="footer">
<?php
if ($client['id']) {
	echo mobile_avatar($client,'icon').' '.url('mobile/u/'.$client['username'],h($client['fullname'])).'<br />'.
		url('account',t('Edit profile')).' | '.url('mobile/member/logout',t('Logout') );
}
echo '<div style="padding:5px;text-align:center">'.t('Interface').': <strong>'.t('Simplified').'</strong> | '.url('mobile/setoriginal',t('Original')).'</div>';
?>
</div>

</body>
</html>