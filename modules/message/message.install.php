<?php
/* ############################################################ *\
 ----------------------------------------------------------------
@package	Jcow Social Networking Script.
@copyright	Copyright (C) 2009 - 2010 jcow.net.  All Rights Reserved.
@license	see http://jcow.net/license
 ----------------------------------------------------------------
\* ############################################################ */

function message_menu() {
	$items = array();
	$items['message/inbox'] = array(
		'name'=>'Messages',
		'type'=>'app'
	);
	return $items;
}


?>