<?php
/* ############################################################ *\
 ----------------------------------------------------------------
Jcow Software (http://www.jcow.net)
IS NOT FREE SOFTWARE
http://www.jcow.net/commercial_license
Copyright (C) 2009 - 2010 jcow.net.  All Rights Reserved.
 ----------------------------------------------------------------
\* ############################################################ */

class notifications{
	
	function notifications() {
		global $client, $menuon;
		$menuon = 'message';
		hide_ad();
	}
	function index() {
		redirect('notifications/more');
	}
	function more() {
		global $content, $db, $client, $offset, $num_per_page, $page, $ubase, $nav;
		if (!$client['id']) {
			die('please login');
		}
		sql_query("update jcow_notes set hasread=1 where uid='{$client['id']}' and hasread=0");
		$res = sql_query("SELECT n.*,u.username,u.avatar,u.fullname FROM ".tb()."notes as n  left join `".tb()."accounts` as u on u.id=n.from_uid where n.uid='{$client['id']}' ORDER by n.created DESC LIMIT 0,20 ");
		$out .= '
		<ul class="simple_list">';
		$i=0;
		while ($row = sql_fetch_array($res)) {
			if ($row['from_uid']) {
				$row['note_img'] = '<div style="float:left;width:30px">
					'.avatar($row,'small').'
					</div>';
				}
			$out .= '<li style="height:60px">'.$row['note_img'].$row['message'].'<br /><span class="sub">'.get_date($row['created']).' </span></li>';
			$i++;
		}
		$out .= '</ul>';
		if ($i>0) {
			c( $out);
		}
		else {
			c(t('None'));
		}
	}

	function ajax() {
		global $content, $db, $client, $offset, $num_per_page, $page, $ubase, $nav;
		if (!$client['id']) {
			die('please login');
		}
		sql_query("update jcow_notes set hasread=1 where uid='{$client['id']}' and hasread=0");
		$res = sql_query("SELECT n.*,u.username,u.avatar,u.fullname FROM ".tb()."notes as n  left join `".tb()."accounts` as u on u.id=n.from_uid where n.uid='{$client['id']}' ORDER by n.created DESC LIMIT 0,10 ");
		$out .= '
		<ul class="simple_list">';
		$i=0;
		while ($row = sql_fetch_array($res)) {
			if ($row['from_uid']) {
				$row['note_img'] = '<div style="float:left;width:30px">
					'.avatar($row,'small').'
					</div>';
				}
			$out .= '<li style="height:60px">'.$row['note_img'].$row['message'].'<br /><span class="sub">'.get_date($row['created']).' </span></li>';
			$i++;
		}
		$out .= '</ul>';
		if ($i>0) {
			echo $out;
			if ($i > 9) {
				echo '<div style="text-align:right">'.url('notifications/more',t('See more')).'</div>';
			}
		}
		else {
			echo t('None');
		}
		exit;
	}

}