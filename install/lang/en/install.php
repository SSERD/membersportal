<?php
$msg = array(
		'about' => 'Only One Step',
		'installtion' => 'Installtion',
		'database_info' => 'Config',
		'dbversion' => 'Database Version',
		'dbhost' => 'DB Host',
		'dbuser' => 'DB Username',
		'dbpass' => 'DB Password',
		'dbname' => 'DB name',
		'libs_info' => 'Libs',
		'others' => 'Others',
		'language' => 'Interface',
		'begin_install' => 'I agree to this License Agreement, Begin Install',
		'help' => 'If you got any problem, do not hesitate to contact us: <a href="http://www.jcow.net">jcow.net</a>',
		'success' => 'Congratulations! You have installed Jcow Network successfully! <br />Please delete the file: <strong>install.php</strong><br /><strong>The first account will be the admin</strong>.',
		'need_writable' => 'These files or folders need to be set writable(777)',
		'return' => 'Return',
		'check_db_info' => 'Database connection Error, Please return to previous step and check the info',
		'check_lib_db_info' => 'Database connection Error, Please check the db_connect.php in your libs',
		'check_libs' => 'Failed to read lib files, please type a correct libs folder',
		'got_errors' => 'Got Errors',
		'already_installed' => 'The Module has already been installed in the selected database.',
		'need_module' => 'You need to install these modules first',
		'site_url' => 'Website Base Href'
	);

?>