<?php
/* ############################################################ *\
 ----------------------------------------------------------------
Jcow Software (http://www.jcow.net)
IS NOT FREE SOFTWARE
http://www.jcow.net/commercial_license
Copyright (C) 2009 - 2010 jcow.net.  All Rights Reserved.
 ----------------------------------------------------------------
\* ############################################################ */

function featured_menu() {
	$items = array();
	$items['featured'] = array(
		'name'=>'Featured stories',
		'tab_name'=>'Featured',
		'type'=>'community'
	);
	return $items;
}
?>