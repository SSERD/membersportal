<?php

echo '
<link href="'.uhome().'/modules/videos/style.css" media="screen" rel="stylesheet" type="text/css" />
<style>
				#appmain {
					width: 645px;
					float: right;
				}
				#appside {
					width: 310px;
					float: left;
				}
				.jcow_story_video {
					width:130px;
					height:100px;
					}
				#appside .block_title {
					margin: 5px;
					}
				#appside .block_content {
					padding: 2px;
					}
				.home_blog_entries {
					border-bottom: #eeeeee 1px solid;
					padding:3px;
					margin:2px;
					}
				.home_blog_entries img {
					vertical-align:middle;
					}
				.home_status {
					margin:3px;
					float:left;
					width:130px;
					}
				.hot_topics  {
					margin:3px;
					background:#eeeeee;
					float:left;
					width:130px;
					}
</style>
<div id="jcow_app_container" >
<div id="jcow_content" class="content_general" >';

echo '
<div class="block">
<div class="block_title">'.t('Community activities').'</div>
<div class="block_content">
<script type="text/javascript" src="'.uhome().'/js/jquery.vtricker.js"></script>
<script>
jQuery(document).ready(function($) {
	$("#recent_activites").fadeIn();
	$(\'#recent_activites\').vTicker({
	   speed: 800,
	   pause: 5000,
	   showItems: 4,
	   animation: \'fade\',
	   mousePause: false,
	   height: 350,
	   direction: \'down\'
	});
			});
</script>
<style>
#recent_activites li{
	margin:0;
	padding:0;
	}
</style>
<div id="recent_activites" style="display:none">
<ul>
';

$res = sql_query("SELECT s.*,u.username,u.avatar from `".tb()."streams` as s left join ".tb()."accounts as u on u.id=s.uid left join ".tb()."pages as p on p.id=s.wall_id where s.hide!=1 and p.type!='group' and (u.forum_posts>0 or u.id=1) and 
	s.id NOT IN (SELECT id FROM ".tb()."pending_review where stream_id=s.id)
		order by s.id desc limit 30");
while ($stream = sql_fetch_array($res)) {
	$stream['attachment'] = unserialize($stream['attachment']);
	echo '<li>'.strip_tags(stream_display($stream,'simple'),'<img><table><tr><td><div>').'</li>';
}
echo '</ul>
<div style="position:absolute;left:0;bottom:0px;height:20px;width:100%;background:url('.uhome().'/files/common_css/fade.png) repeat-x"> </div></div>

</div></div>';



echo '</div>';// end appmain


// ##################### sidebar ########################
echo '<div id="jcow_side">';

echo '
				<div class="block">
				<div class="block_title">'.t('Login').'</div>
				<div class="block_content">
<form method="post" name="loginform" id="form1" action="'.url('member/loginpost').'" >
			'.t('Username or Email').':<br />
			<input type="text" size="10" name="username" style="width:220px" /><br />
							'.t('Password').':<br />
			<input type="password" size="10" name="password" style="width:220px" /><br />
			<div class="sub">( '.url('member/chpass',t('Forgot password?')).' )</div>
			<label for="remember_me">
			<input type="checkbox" name="remember_me" value="1" id="remember_me" /> '.t('Remember me').'
			</label><br />
			<input type="submit" value="'.t('Login').'" style="font-size:25px" /> <strong>'.url('member/signup',t('Create an account')).'</strong>
			</form>
			<script language="javascript">document.loginform.username.focus();</script>';
			if (get_gvar('fb_id')) {
				echo '<div style="margin:20px 0;text-align:center">'.url('fblogin','<img src="'.uhome().'/modules/fblogin/button.png" />').'</div>';
			}

			echo '
				</div>
				</div>
				';
$res = sql_query("select count(*) as num from ".tb()."accounts");
$row = sql_fetch_array($res);
$num_users = $row['num'];

$res = sql_query("select count(*) as num from ".tb()."friends");
$row = sql_fetch_array($res);
$num_friendships = $row['num'];

$res = sql_query("select count(*) as num from ".tb()."followers");
$row = sql_fetch_array($res);
$num_connections = $row['num']+$num_friendships;

$res = sql_query("select count(*) as num from ".tb()."streams");
$row = sql_fetch_array($res);
$num_streams = $row['num'];

$res = sql_query("select count(*) as num from ".tb()."stories");
$row = sql_fetch_array($res);
$num_stories = $row['num'];

echo '<div class="block">
				<div class="block_title">'.t('Statistics').'</div>
				<div class="block_content">
				<table width="90%" cellspacing=0" cellpadding="5">
				<tr><td>'.t('Members').'</td><td align="right">'.$num_users.'</td></tr>
				<tr style="background:#eeeeee"><td>'.t('Friendships').'</td><td align="right">'.$num_friendships.'</td></tr>
				<tr><td>'.t('Connections').'</td><td align="right">'.$num_connections.'</td></tr>
				<tr style="background:#eeeeee"><td>'.t('Streams').'</td><td align="right">'.$num_streams.'</td></tr>
				<tr><td>'.t('Stories').'</td><td align="right">'.$num_stories.'</td></tr>
				</table>
				</div>
		</div>';


echo '</div>';// sidebar
echo '</div>';//jcow_app_container