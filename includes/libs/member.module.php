<?php
/* ############################################################ *\
 ----------------------------------------------------------------
Jcow Software (http://www.jcow.net)
IS NOT FREE SOFTWARE
http://www.jcow.net/commercial_license
Copyright (C) 2009 - 2010 jcow.net.  All Rights Reserved.
 ----------------------------------------------------------------
\* ############################################################ */

class member{
	function __construct() {
		global $config;
		$config['hide_ad'] = 1;
		$config['hidemenu'] = 1;
		clear_as();
	}
	function test() {
		echo 'db:';
		exit;
	}
	function testpost() {
		form_go(url('member/signup'));
	}
	function login($need_login = 0, $iid = 0) {
		global $db, $nav, $client, $config,$captcha;
		set_title('Log In');
		if ($client['id']) redirect(my_jcow_home());
		if ($need_login) {
			sys_notice(t('You need to login to do this'));
		}
		$nav[] = url('member/login','Login');
		section_content('
<form method="post" name="form1" class="ajaxform" action="'.url('member/loginpost').'" >
								<div class="form-group">
								<input type="text" size="10" name="username" autocorrect="off" autocapitalize="off" class="form-control" value="'.h($_POST['username']).'"  placeholder="'.t('Username or Email').'"/><div class="sub">'.url('member/signup',t('Register an account')).'</div>
								</div>
								<div class="form-group">
								<input type="password" size="10" class="form-control" name="password" style="min-width:120px"  value="'.h($_POST['password']).'" placeholder="'.t('Password').'" />
								<div class="sub">'.url('member/chpass',t('Forgot password?')).'</div>
								</div>
								');
			
			c('
								<div class="checkbox">
								<label><input type="checkbox" name="remember_me" id="remember_me" value="1" '.$remember_check.' /> '.t('Remember me').'</label>
								</div>
								<div class="form-group">
								<button type="submit" class="btn btn-lg btn-primary">'.t('Login').'</button>
								'.$bakg.'
								</div>
								</p>
								</form>

								
								<script language="javascript" >
					document.form1.username.focus();
					</script>
			');
			c('<div style="height:2px;margin:10px 0;background:#eee"></div>
				<div><a href="'.url('member/signup').'" class="btn btn-default"><i class="fa fa-user-plus"></i> '.t('Create an account').'</a></div>');
			if (get_gvar('fb_id')) {
				c('<div style="display:inline-block;padding:5px 5px 5px 0"><a href="'.url('fblogin').'" class="btn btn-primary"><i class="fa fa-facebook"></i> '.t('Connect with Facebook').'</a></div>');
			}
			if (get_gvar('oauth_google_id') && get_gvar('oauth_google_secret')) {
				c( '<div style="display:inline-block;padding:5px 5px 5px 0"><a href="'.url('oauth/google').'" class="btn btn-danger"><i class="fa fa-google fa-lg"></i> '.t('Connect with Google').'</a></div> ');
			}
		section_close(t('Log in'));

	}

	function loginpost() {
		global $db, $client, $ss, $ubase,$sid, $captcha,$config;
		if ($client['id']) {
			form_stop('already logged in');
		}

		$lfkey = substr(md5('lf'.$client['ip']),0,10);
		$res = sql_query("select count(*) as num from ".tb()."tmp where tkey='$lfkey'");
		$lf = sql_fetch_array($res);
		if ($lf['num'] > 10) {
			$res = sql_query("select max(tcontent) from jcow_tmp where tkey='$lfkey'");
			$row = sql_fetch_array($res);
			$timeline = time() + 3600*3;
			if ((time() - $row['tcontent']) < 7200) {
				form_stop('You have attempt too many times, please wait for a while');
			}
			else {
				sql_query("delete from ".tb()."tmp where tkey='$lfkey'");
			}
		}
		$password = md5($_POST['password'].'jcow');
		$res = sql_query("select * from `".tb()."accounts` where  (email='".$_POST['username']."' or username='".$_POST['username']."') and password='$password'  limit 1");
		if (sql_counts($res)) {
			$newss = get_rand(12);
			$row = sql_fetch_array($res);
			if (is_mobile()) {
				$token = md5($row['password'].'jm');
				form_array(array('act'=>'loginsuccess','jmuser'=>$row['id'],'jmtoken'=>$token));
			}
			if ($_POST['remember_me']) {
				sql_query("UPDATE `".tb()."accounts` SET jcowsess='$newss',ipaddress='".addslashes($client['ip'])."' WHERE id='{$row['id']}' ");
				setcookie('jcowss', $newss, time()+3600*24*365,"/");
				setcookie('jcowuid', $row['id'], time()+3600*24*365,"/");
			}
			$_SESSION['uid'] = $row['id'];
			set_tmp($togkey,'deleteit');
			if (strlen($_COOKIE['j_return_url'])) {
				setcookie('j_return_url', '', time()+3600*24*365,"/");
				//form_go(url($_COOKIE['j_return_url']));
				form_go(my_jcow_home());
			}
			else {
				form_go(my_jcow_home());
			}
			exit;
		}
		else {
			form_stop(t('Wrong account or password'));
			sql_query("insert into ".tb()."tmp(tkey,tcontent) values('$lfkey',".time().")");
		}
		set_title('Login');
		if ($_POST['remember_me']) {
			$remember_check = 'checked';
		}
	}

	function logout() {
		check_pin();
		global $content, $db, $nav, $sid, $client,$ss,$ubase, $config;
		if ($client['id']) {
			$hooks = check_hooks('logout');
			if ($hooks) {
				foreach ($hooks as $hook) {
					$hook_func = $hook.'_logout';
					$hook_func();
				}
			}
			 $_SESSION['uid'] = 0;
			 setcookie('jcowuid', '', time()+3600*24*365,"/");
			 setcookie('jcowss', '', time()+3600*24*365,"/");
			 redirect(uhome());
		}
		redirect(uhome());
	}

	function activate($uid=0,$code='') {
		global $client;
		if (is_numeric($uid)) {
			$res = sql_query("select * from ".tb()."accounts where id='$uid'");
			$user = sql_fetch_array($res);
			if (strlen($user['reg_code']) && $user['reg_code'] == $code) {
				sql_query("update ".tb()."accounts set disabled=0,reg_code='' where id='{$user['id']}'");
				$actived = 1;
			}
		}
		if ($actived) {
			c('<p style="font-size:2em">'.t('account activated successfully').'!</p>');
			if (!$client['id']) {
				c('<p>'.url('member/login',t('Login now')).'</p>');
			}
		}
		else {
			c('<p style="font-size:2em">Failed</p>');
		}
	}

	function chpass() {
		global $client;
		if ($client['id']) {
			die('already logged in');
		}
		c('<h2>'.t('Get back my password').'</h2>
		<form method="post" name="form1" action="'.url('member/chpasspost').'" >
					<p>
					'.label(t('Email')).'
					<input type="text" name="email" />
					<div class="sub">'.t('The email address you registered with').'</div>
					</p>
					<p>
					<input class="button" type="submit" value="'.t('Submit').'" />
					</p>
					</form>
					<script language="javascript" >
					document.form1.email.focus();
					</script>');
	}
	
	function chpasspost() {
		if(!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i", $_POST['email'])) {
			sys_back(t('Unavailable email address'));
		}
		$res = sql_query("select * from `".tb()."accounts` where email='{$_POST['email']}'");
		$row = sql_fetch_array($res);
		if (!$row['id']) {
			sys_back(t('No user registered with this email'));
		}
		$code = get_rand(10);
		sql_query("update `".tb()."accounts` set chpass='$code' where id='{$row['id']}'");
		$link = url('member/chpassdo/'.$code,t('Click Here'));
		jcow_mail($_POST['email'],t('Get your password'),$link);
		c(t('A verification email has been sent to you, please check your email inbox'));
	}

	function chpassdo($code = '') {
		if (!preg_match("/^[0-9a-z]+$/i",$code)) die('not a valid code');
		$res = sql_query("select * from `".tb()."accounts` where chpass='{$code}'");
		$user = sql_fetch_array($res);
		if (!sql_counts($res)) die('wrong code');
		c('<form method="post" name="form1" action="'.url('member/chpassdopost').'" >
					<p>
					'.label(t('Pick a new password')).'
					<input type="password" name="password" />
					</p>
					<p>
					<input type="hidden" name="passcode" value="'.$code.'" />
					<input type="hidden" name="id" value="'.$user['id'].'" />
					<input class="button" type="submit" value="'.t('Submit').'" />
					</p>
					</form>');
	}

	function chpassdopost() {
		$code = $_POST['passcode'];
		if (!preg_match("/^[0-9a-z]+$/i",$code)) die('not a valid code');
		$res = sql_query("select * from `".tb()."accounts` where chpass='{$code}'");
		if (!sql_counts($res)) die('wrong code');
		$newpass = md5($_POST['password'].'jcow');
		sql_query("update ".tb()."accounts set chpass='',password='$newpass' where id='{$_POST['id']}' and chpass='{$code}'");
		redirect('member/login',t('Your password has been changed'));
	}


	function signup() {
		global $db, $client, $uhome, $config, $captcha,$notices;
		set_title(t('Sign up'));
		if ($client['id']) redirect('feed');
		$reg_limit_ip = get_gvar('reg_limit_ip');
		if (get_gvar('signup_closed') == 2) {
			form_stop(t('Sorry, currently we are not accepting new members'));
		}
		
		$code = $_GET['code'];
		$iid = $_GET['iid'];
		if (!$iid && $_SESSION['jcow_reg_iid']>0) {
			$iid = $_SESSION['jcow_reg_iid'];
		}
		else {
			$_SESSION['jcow_reg_iid'] = $iid;
		}

		if (!$code && strlen($_SESSION['jcow_reg_code']) == 15) {
			$code = $_SESSION['jcow_reg_code'];
		}
		else {
			$_SESSION['jcow_reg_code'] = $code;
		}
		if (function_exists('jc_check_members')) {
			jc_check_members();
		}
		
		/*
		if (!strlen($iid) || !strlen($code)) {
			if ($_POST['request_i']) {
				$error = '';
				if (!$_POST['myemail'] || !$_POST['myname']) {
					$error = t('Please fill in all the required blanks');
				}
				elseif(!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i", $_POST['myemail'])) {
					$error = t('Unavailable email address');
				}
				else {
					//check
					$res = sql_query("select * from `".tb()."invites` where email='{$_POST['myemail']}' ");
					$res2 = sql_query("select * from `".tb()."accounts` where email='{$_POST['myemail']}' ");
					if (sql_counts($res) || sql_counts($res2)) {
						$error = t('The Email address already exists in system');
					}
				}
				if (strlen($error)) {
					sys_notice($error);
				}
				else {
					//insert
					$email = $_POST['myemail'];
					$rand = get_rand(15);
					sql_query("insert into `".tb()."invites` (uid,email,created,code) values('0','$email',".time().",'$rand')");
					$iid = insert_id();
					$url = url('member/signup','','',array('iid'=>$iid,'code'=>$rand));
					$message = "Dear ".$_POST['myname']."<br />
					<a href=\"$url\">".t('Click here to continue registration')."</a>
					<br />
					<br />
					".t("If link not working, paste this into your browser")."<br />
					$url
					<br />";
					@jcow_mail($email,
						t('Welcome to').' ['.get_gvar('site_name').']',
						$message);
					c('<p style="font-size:15px">'.t('A welcome Email has been sent to you, please check your email inbox.').'<br />
					('.t('If you do not find the email in your inbox, please check your spam filter or bulk email folder.').')</p>');
					stop_here();
				}
			}
			c('<form method="post" name="form1" action="'.url('member/signup').'" >
					<div id="signup_request_box">
						<div class="signup_request_label">'.t('Your Name').'</div>
						<div class="signup_request_input"><input name="myname" value="'.h($_POST['myname']).'" /></div>

						<div class="signup_request_label">'.t('Email Address').'</div>
						<div class="signup_request_input"><input name="myemail" value="'.h($_POST['myemail']).'" />
							<span>'.t('Invitation will be sent to this address').'</span>
						</div>
						<div id="signup_request_submit">
						<input type="submit" value="'.t('Continue').'" />
						</div>
						<input type="hidden" name="request_i" value="1" />');

			if (get_gvar('fb_id')) {
				c('<br /><br />'.url('fblogin','<img src="'.uhome().'/modules/fblogin/button.png" />'));
			}
					c('</div>
					</form>
					<script language="javascript" >
					document.form1.myname.focus();
					</script>');
			
			stop_here();
		}
		*/
		if ($iid) {
			$res = sql_query("select * from ".tb()."invites where id='$iid' and code='$code'");
			$invite = sql_fetch_array($res);
			if (!$invite['id']) {
				form_stop(c('Not a valid invitation ID'));
			}
			elseif($invite['status']) {
				$_SESSION['jcow_reg_iid'] = $_SESSION['jcow_reg_code'] = '';
				form_stop(c('Invitation Key expired'));
			}
		}
		else
			$invite = array();
		if (get_gvar('signup_closed') == 1 && !$invite['id']) {
			form_stop(t('Sorry, this community is invite-only'));
		}
		if (is_numeric($reg_limit_ip)) {
			$res = sql_query("select count(*) as num from ".tb()."accounts where ipaddress='{$client['ip']}'");
			$row = sql_fetch_array($res);
			if ($row['num'] >= $reg_limit_ip) {
				form_stop(t('Sorry, only {1} registrations allowed per IP','<strong>'.$reg_limit_ip.'</strong>'));
			}
		}
		

		if ($_POST['onpost']) {

			$errors = array();
			$resp = null;
			if (!is_mobile() && !$captcha['disabled'] && !strlen($_SESSION['fb_email'])) {
				if (!recaptcha_valid()) {
					$errors[] = t('Recaptcha validation failed');
				}
			}
			
			if (!$_POST['agree_rules']) {
				$errors[] = t('You must agree to our rules for signing up');
			}
			if (strlen($_SESSION['fb_email'])) {
				$_POST['email'] = $_SESSION['fb_email'];
			}
			if (strtolower($_COOKIE['cfm']) != strtolower($_POST['confirm_code'])) {
				$errors[] = t('The string you entered for the code verification did not match what was displayed');
			}
			$_POST['username'] = strtolower($_POST['username']);
			if (strlen($_POST['username']) < 4 || strlen($_POST['username']) > 18 || !preg_match("/^[0-9a-z]+$/i",$_POST['username'])) {
				$errors[] = t('Username').': '.t('from 4 to 18 characters, only 0-9,a-z');
			}
			if (!$_POST['username'] || !$_POST['email'] || !$_POST['password'] || !strlen($_POST['fullname'])) {
				$errors[] = t('Please fill in all the required blanks');
			}
			elseif(!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i", $_POST['email'])) {
				$errors[] = t('Unavailable email address');
			}
			if ($client['ip'] == '127.0.0.1' || preg_match("/^10\.0/",$client['ip']) || preg_match("/^192\.168/",$client['ip']) || preg_match("/^172\.16/",$client['ip'])) {
				$is_localnetwork = 1;
			}

			/*
			if (!get_gvar('disable_birthday')) {
				if (!$_POST['birthmonth'] || !$_POST['birthday'] || !strlen($_POST['birthyear'])) {
					$errors[] = t('Please fill in the Birthday fields');
				}
			}
			*/
			for($i=1;$i<=7;$i++) {
					$col = 'var'.$i;
					$key = 'cf_var'.$i;
					$key2 = 'cf_var_value'.$i;
					$key3 = 'cf_var_des'.$i;
					$key4 = 'cf_var_label'.$i;
					$key5 = 'cf_var_required'.$i;
					$ctype = get_gvar($key);
					if ($ctype != 'disabled' && get_gvar($key5)) {
						if (!strlen($_POST[$col])) {
							$errors[] = t('Please fill in all the required blanks');
						}
					}
				}
			
			$password = md5($_POST['password'].'jcow');
			$timeline = time();
			$res = sql_query("select * from `".tb()."accounts` where email='{$_POST['email']}'");
			if (sql_counts($res)) {
				$errors[] = t('You have registered with this email address before.');
			}
			$res = sql_query("select * from `".tb()."accounts` where username='{$_POST['username']}'");
			if (sql_counts($res)) {
				$errors[] = t('The Username has already been used');
			}
			if (get_gvar('verify_approval')) {
				if (strlen(get_text('community_question')) && !strlen($_POST['answer'])) {
					$errors[] = t('You must answer the community question');
				}
			}

			if (!count($errors)) {
				
				$reg_code = '';
				$verify_note = '';
			
				// member
				if ($_POST['hide_age']) {
					$hide_age = 1;
				}
				else {
					$hide_age = 0;
				}
				$newss = get_rand(12);
				if (get_gvar('verify_approval')) {
					$member_disabled = 1;
				}
				else {
					$member_disabled =0;
				}
				if (strlen($_SESSION['fb_avatar'])) {
					$avatar = $_SESSION['fb_avatar'];
					$fb_id = $_SESSION['fb_id'];
				}
				else {
					$avatar = $fb_id = '';
				}
				sql_query("insert into `".tb()."accounts` (about_me,avatar,fbid,disabled,gender,location,country,birthyear,birthmonth,birthday,hide_age,password,email,username,fullname,created,lastlogin,ipaddress,var1,var2,var3,var4,var5,var6,var7,reg_code) values('{$_POST['about_me']}','$avatar','$fb_id',$member_disabled,'{$_POST['gender']}','{$_POST['location']}','{$_POST['country']}','{$_POST['birthyear']}','{$_POST['birthmonth']}','{$_POST['birthday']}','{$hide_age}','$password','".$_POST['email']."','{$_POST['username']}','{$_POST['fullname']}',$timeline,$timeline,'{$client['ip']}','{$_POST['var1']}','{$_POST['var2']}','{$_POST['var3']}','{$_POST['var4']}','{$_POST['var5']}','{$_POST['var6']}','{$_POST['var7']}','{$reg_code}')");
				//sql_query("insert into `".tb()."accounts` (about_me,disabled,gender,location,birthyear,birthmonth,birthday,hide_age,password,email,username,fullname,created,lastlogin,ipaddress,var1,var2,var3,var4,var5,var6,var7,reg_code) values('{$_POST['about_me']}',$member_disabled,'{$_POST['gender']}','{$_POST['location']}','{$_POST['birthyear']}','{$_POST['birthmonth']}','{$_POST['birthday']}','{$hide_age}','$password','".$_POST['email']."','{$_POST['username']}','{$_POST['fullname']}',$timeline,$timeline,'{$client['ip']}','{$_POST['var1']}','{$_POST['var2']}','{$_POST['var3']}','{$_POST['var4']}','{$_POST['var5']}','{$_POST['var6']}','{$_POST['var7']}','{$reg_code}')");
				$uid = insert_id();
				if ($uid == 1) {
					sql_query("update ".tb()."accounts set roles='3',disabled=0,reg_code='' where id='$uid'");
				}
				sql_query("insert into `".tb()."pages` (uid,uri,type) values($uid,'{$_POST['username']}','u')");
				$page_id = insert_id();
				if (get_gvar('verify_approval')) {
					sql_query("insert into ".tb()."requests (uid,question,answer,created) values('{$uid}',' ','{$_POST['answer']}','".time()."') ");
				}
				if ($invite['id']>0) {
					sql_query("update ".tb()."invites set status=1 where id='{$invite['id']}'");
					if ($invite['uid']) {
						$res = sql_query("select * from ".tb()."accounts where id='{$invite['uid']}'");
						$invitor = sql_fetch_array($res);
						$invitor_roles = explode('|',$invitor['roles']);
						if (is_array($invitor_roles) && in_array('3',$invitor_roles)) {
							sql_query("update ".tb()."accounts set disabled=0 where id='$uid'");
							$member_disabled = 0;
						}
						// add follow
						sql_query("insert into `".tb()."followers` (uid,fid) values ($uid,{$invite['uid']})");
						sql_query("insert into `".tb()."followers` (uid,fid) values ({$invite['uid']},$uid)");
						sql_query("update ".tb()."accounts set followers=followers+1 where id in ($uid,{$invite['uid']})");
						
						// add friends
						sql_query("insert into `".tb()."friends` (uid,fid,created) values ($uid,{$invite['uid']},".time().")");
						sql_query("insert into `".tb()."friends` (uid,fid,created) values ({$invite['uid']},$uid,".time().")");
					}
				}
				if (get_gvar('is_demo')) {
					$res = sql_query("select id from jcow_accounts where avatar!='' order by id DESC limit 5");
					while ($user = sql_fetch_array($res)) {
						add_friend($uid,$user['id']);
					}
					sql_query("insert into jcow_ad_funds(uid,amount) values('$uid',50)");
				}
				// add stream
				if (!get_gvar('verify_approval')) {
					if (strlen($_POST['sayhi'])) {
						$sayhi = $_POST['sayhi'];
					}
					else {
						$sayhi = t('Joined this network');
					}
					stream_publish($sayhi,'','',$uid,$page_id);
				}



				// welcome email
				$welcome_email = nl2br(get_text('welcome_email'));
				$welcome_email = str_replace('%username%',$_POST['username'],$welcome_email);
				$welcome_email = str_replace('%email%',$_POST['email'],$welcome_email);
				$welcome_email = str_replace('%password%',$_POST['password'],$welcome_email);
				$welcome_email = str_replace('%sitelink%',url(uhome(),h(get_gvar('site_name')) ),$welcome_email);
				/* removed from 9.3, 
				if (strlen($reg_code)) {
					$welcome_email = '<p>'.t('To complete the activation of your account please click the following link').':<br /><a href="'.url('member/activate/'.$uid.'/'.$reg_code).'">'.url('member/activate/'.$uid.'/'.$reg_code).'</a></p>
					'.$welcome_email;
				}
				*/
				
				@jcow_mail($_POST['email'], 'Welcome to "'.h(get_gvar('site_name')).'"!', $welcome_email);
				$_SESSION['login_cd'] = 3;
				// suggested pages
				if ($_POST['followed_pages']) {
					$page_ids = explode(',',$_POST['followed_pages']);
					foreach ($page_ids as $page_id) {
						if (is_numeric($page_id) && $page_id > 0) {
							$res = sql_query("select * from jcow_pages where id='$page_id'");
							$page = sql_fetch_array($res);
							if ($page['type'] == 'u') {
								if ($page['uid'] != $invite['uid']) {
									sql_query("insert into `".tb()."followers` (uid,fid) values ($uid,{$page['uid']})");
									sql_query("update ".tb()."accounts set followers=followers+1 where id='{$page['uid']}'");
								}
							}
							elseif ($page['type'] == 'page') {
								sql_query("insert into ".tb()."page_users (uid,pid) value ('{$uid}','{$page['id']}')");
								sql_query("update ".tb()."pages set users=users+1 where id='{$page['id']}'");
							}
							elseif ($page['type'] == 'group') {
								if ($page['var3']>=2) {
									sql_query("insert into ".tb()."group_members_pending (uid,gid,created) value ('{$uid}','{$page['id']}',".time().")");
									send_note($page['uid'],t('{1} wants to join your group',url('u/'.$_POST['username'],h($_POST['username']))).': <strong>'.url('group/'.$page['uri'],h($page['name'])).'</strong>' );
								}
								else {
									sql_query("insert into ".tb()."page_users (uid,pid) value ('{$uid}','{$page['id']}')");
									sql_query("update ".tb()."pages set users=users+1 where id='{$page['id']}'");
								}
							}
						}
					}
				}
				// update members
				$res = sql_query("select count(*) as num from jcow_accounts");
				$row = sql_fetch_array($res);
				set_gvar('members_num',$row['num']);
				if (is_mobile()) {
					$token = md5($password.'jm');
					form_array(array('act'=>'signupsuccess','jmuser'=>$uid,'jmtoken'=>$token));
				}
				//login
				$_SESSION['uid'] = $uid;
				$_SESSION['jcow_reg_code'] = $_SESSION['jcow_reg_iid'] = '';
				$_SESSION['fb_id'] = $_SESSION['fb_email'] = $_SESSION['fb_avatar'] =  '';
				form_go(url('account/avatar'));
				exit;
			}
			else {
				foreach ($errors as $error) {
					$error_msg .= '<li>'.$error.'</li>';
				}
				form_stop(t('Errors').':<ul>'.$error_msg.'</ul>');
			}
		}

		set_title('Signup');
			if (get_gvar('pm_enabled')) {
				c('<strong>'.t('Join Us').'</strong><br />
				'.t('Membership pricing').':<ul>');
				if ($pm_1m = get_gvar('pm_1m')) {
					c('<li>'.$pm_1m.' '.get_gvar('pm_currency').' '.t('Per month').'</li>');
				}
				if ($pm_3m = get_gvar('pm_3m')) {
					c('<li>'.$pm_3m.' '.get_gvar('pm_currency').' '.t('Per Annua').'</li>');
				}
				if ($pm_12m = get_gvar('pm_12m')) {
					c('<li>'.$pm_12m.' '.get_gvar('pm_currency').' '.t('Per Yeal').'</li>');
				}
				c('</ul>');
				section_close(t('Paid membership'));
			}
				
			c('
<style>
.sf_field {
	margin-bottom:10px;
	padding-left:10px;
}
.sf_title{
	font-weight:bold;
	font-size:20px;
	padding:5px;
	margin:15px 0;
	}
.sp_title {
	margin:10px 0;
	width:500px;
	border-bottom:#eeeeee 2px solid;
}
.sp_title span{
	display:inline-block;
	background:#eeeeee;
	color:#333333;
	font-weight:bold;
	padding:5px;
}
.sp_box {
	overflow:hidden;width:200px;height:72px;margin-right:3px;display:inline-block;
}
.sp_img {
	float:left;
}
.sp_content {
	padding-left:60px
}
.sp_name {
	height:15px;overflow:hidden;font-weight:bold
}
</style>

<h2>'.t('Sign up').'</h2>
		<form method="post" action="'.url('member/signup').'"  class="ajaxform form-inline">
');
$page_u_ids = array_filter(explode(',',get_text('suggest_u')));
$page_p_ids = array_filter(explode(',',get_text('suggest_p')));
$page_g_ids = array_filter(explode(',',get_text('suggest_g')));
$users = $pages = $groups = array();
if (count($page_u_ids) || count($page_p_ids) || count($page_g_ids)) {
	$got_suggestion = 1;
}
if ($got_suggestion && !$_POST['page_picked'] && !is_mobile()) {
	c('<script>
		$(function() {
			$(document).on("click",".reg_follow",function() {
				var pid = $(this).attr("page_id");
				$("#followed_pages").val($("#followed_pages").val()+","+pid);
				$(this).before("<i class=\"fa fa-check\"></i>");
				$(this).removeClass("btn-primary").addClass("btn-success");
				$(this).prop("disabled",true);
				$("#reg_next").prop("disabled",false);
				return false;
			});
			$(document).on("click",".go_next",function() {
				$("#suggestions_box").hide();
				$("#main_box").animate({width:"toggle"},350);
				return false;
			});
		});
		</script>
		');
	c('<div id="suggestions_box"><div class="sf_title">'.t('Step 1/2: Pick interests').'</div>');
	if (count($page_u_ids)) {
		c('<div class="sp_title"><span>'.t('People').':</span></div>');
		$res = sql_query("select id,uid from ".tb()."pages where id in (".implode(',',$page_u_ids).")");
		while ($page = sql_fetch_array($res)) {
			$res2 = sql_query("select fullname,email,id,avatar,followers from ".tb()."accounts where id='{$page['uid']}'");
			$user = sql_fetch_array($res2);
			if ($user['id']) {
				c('<div class="sp_box"><div class="sp_img">'.avatar($user,'small',array('nolink'=>1)).'</div>
					<div class="sp_content">
						<div class="sp_name">'.h($user['fullname']).'</div><div class="sub">'.t('{1} followers',$user['followers']).'</div><button class="btn btn-xs btn-primary reg_follow" page_id="'.$page['id'].'"><i class="fa fa-plus"></i> '.t('Follow').'</button>
					</div>
					</div>');
			}
		}
	}

	if (count($page_p_ids)) {
		$res = sql_query("select id,name,logo,users from ".tb()."pages where id in (".implode(',',$page_p_ids).")");
		while($page = sql_fetch_array($res)) {
			if ($page['id']) {
				$pages[] = $page;
			}
		}
		if (count($pages)) {
			c('<div class="sp_title"><span>'.t('Pages').':</span></div>');
			foreach ($pages as $page) {
				c('<div class="sp_box"><div class="sp_img">'.page_logo($page,'small',array('nolink'=>1)).'</div>
					<div class="sp_content">
						<div class="sp_name">'.h($page['name']).'</div><div class="sub">'.t('{1} likes',$page['users']).'</div><a href="#" class="btn btn-xs btn-primary reg_follow" page_id="'.$page['id'].'"><i class="fa fa-plus"></i> '.t('Like').'</a>
					</div>
					</div>');
			}
		}
	}

	if (count($page_g_ids)) {
		$res = sql_query("select id,name,logo,users from ".tb()."pages where id in (".implode(',',$page_g_ids).")");
		while($page = sql_fetch_array($res)) {
			if ($page['id']) {
				$groups[] = $page;
			}
		}
		if (count($groups)) {
			c('<div class="sp_title"><span>'.t('Groups').':</span></div>');
			foreach ($groups as $page) {
				c('<div class="sp_box"><div class="sp_img">'.page_logo($page,'small',array('nolink'=>1)).'</div>
					<div class="sp_content">
						<div class="sp_name">'.h($page['name']).'</div><div class="sub">'.t('{1} members',$page['users']).'</div><button class="btn btn-xs btn-primary reg_follow" page_id="'.$page['id'].'"><i class="fa fa-plus"></i> '.t('Join').'</button>
					</div>
					</div>');
			}
		}
	}
	c('<div style="text-align:right">
		<button id="reg_next" class="btn btn-success btn-lg go_next" disabled>'.t('Next step').' <i class="fa fa-long-arrow-right"></i></button><br />
		<a href="#" class="go_next" >'.t('Skip').'</a>
		</div>
		</div>');
	$hide_main_box = 'style="display:none"';
}
if (strlen($_SESSION['fb_email'])) {
	if ($_SESSION['fb_avatar']) {
		$fb_avatar = '<div style="padding-left:20px"><img src="'.uhome().'/'.uploads.'/avatars/'.$_SESSION['fb_avatar'].'"  /></div>';
		if (!strlen($_POST['fullname'])) {
			$_POST['fullname'] = $_SESSION['fb_fullname'];
		}
	}
	$email_field = '<input type="email"  id="form_email" class="form-control" name="email" value="'.h($_SESSION['fb_email']).'" style="width:362px" disabled />';
}
else {
	$email_field = '<input type="email"  id="form_email" class="form-control" name="email" value="'.h($_POST['email']).'" style="width:362px" placeholder="'.t('Email Address').'" />';
}
c('
<div id="main_box" '.$hide_main_box.'>
<div class="sf_title">'.t('Details').'</div>
'.$fb_avatar.'
<div class="sf_field"><input type="text" id="form_fullname" name="fullname" value="'.h($_POST['fullname']).'" style="width:362px" class="form-control" placeholder="'.t('Full Name').'" /></div>
<div class="sf_field">'.$email_field.'</div>

<div class="sf_field">
<input type="text"  class="form-control" id="form_username" style="width:180px" autocorrect="off" autocapitalize="off" name="username" value="'.h($_REQUEST['username']).'" placeholder="'.t('Username').' (0-9a-z)" /> 
<input type="password" name="password"  id="form_password" class="form-control" style="width:180px" value="'.h($_REQUEST['password']).'"  placeholder="'.t('Password').'" />
</div>');

if (!get_gvar('disable_birthday')) {
		c('<div class="sf_field">'.t('Birth').'<br />
					<select name="birthmonth" id="birthmonth" class="fpost">');
					c('<option value="">'.t('Month').':</option>');
					for ($i=1;$i<13;$i++) {
						if ($i<10)$j='0'.$i;else $j=$i;$iss='';
						if ($_REQUEST['birthmonth'] == $j) $iss='selected';
						c('<option value="'.$j.'" '.$iss.' >'.$j.'</option>');
					}
					c('</select>
					<select name="birthday" id="birthday" class="fpost">');
					c('<option value="">'.t('Day').':</option>');
					for ($i=1;$i<=31;$i++) {
						if ($i<10)$j='0'.$i;else $j=$i;$iss='';
						if ($_REQUEST['birthday'] == $j) $iss='selected';
						c('<option value="'.$j.'" '.$iss.'>'.$j.'</option>');
					}
					c('</select>
					
					<select name="birthyear" id="birthyear" class="fpost">
					');
					$year_from = gmdate("Y",time()) - 8;
					$year_to = gmdate("Y",time()) - 100;
					if ($_REQUEST['birthyear'])
						$yearkey = $_REQUEST['birthyear'];
					c('<option value="">'.t('Year').':</option>');
					for ($i=$year_from;$i>$year_to;$i--) {
						$selected = '';
						if ($yearkey == $i)
							$selected = 'selected';
						c('<option value="'.$i.'" '.$selected.'>'.$i.'</option>');
					}
					if ($row['hide_age']) $hide_age = 'checked';
					c('
					</select>
					
					 
					 <label for="hide_age"><input id="hide_age" type="checkbox" name="hide_age" value="1" '.$hide_age.' /> '.t('Hide my age').'</label>
					 </div>
					');
}

					if (!isset($_POST['gender']) || $_POST['gender'] == 2) {
						$gender2 = 'checked';
					}
					elseif ($_POST['gender'] == 0) {
						$gender0 = 'checked';
					}
					else {
						$gender1 = 'checked';
					}
c('<div class="sf_field">'.t('Gender').'<br />
					<label for="gender_1"><input id="gender_1" type="radio" name="gender" value="1" '.$gender1.' /> '.t('Male').'</label> 
					<label for="gender_0"><input id="gender_0" type="radio" name="gender" value="0" '.$gender0.' /> '.t('Female').'</label>
					<label for="gender_0"><input id="gender_0" type="radio" name="gender" value="2" '.$gender2.' /> '.t('Hide').'</label></div>
					');
		
c('<div class="form-group"><label>'.t('City').'</label><br />
					'.google_places(array('placename'=>$_POST['location'],'country'=>$_POST['country'])).'</div>

			'); 
			
			
					// custom fields 
					$profile = array();
					for($i=1;$i<=7;$i++) {
						$col = 'var'.$i;
						$key = 'cf_var'.$i;
						$key2 = 'cf_var_value'.$i;
						$key3 = 'cf_var_des'.$i;
						$key4 = 'cf_var_label'.$i;
						$key5 = 'cf_var_required'.$i;
						$ctype = get_gvar($key);
						$value = get_gvar($key2);
						$des = get_gvar($key3);
						$label = get_gvar($key4);
						$required = get_gvar($key5);
						if ($required) $required = '*';
						if ($ctype != 'disabled') {
							if ($ctype == 'text') {
								if (strlen($profile[$col])) {
									$value = htmlspecialchars($profile[$col]);
								}
								if (strlen($_POST[$col])) {
									$value = h($_POST[$col]);
								}
								c('<div class="form-group">
								<label>'.$required.$label.'</label><br /><input type="text" name="'.$col.'" class="form-control" value="'.$value.'" placeholder="'.$des.'" /></div>');
							}
							elseif ($ctype == 'textarea') {
								if (strlen($profile[$col])) {
									$value = htmlspecialchars($profile[$col]);
								}
								if (strlen($_POST[$col])) {
									$value = h($_POST[$col]);
								}
								c('<div class="form-group"><label>'.$required.$label.'</label><br />
								<textarea class="form-control" name="'.$col.'" placeholder="'.$des.'" />'.$value.'</textarea><br /></div>');
							}
							elseif ($ctype == 'select_box') {
								$tarr = explode("\r\n",$value);
								c('<div class="form-group"><label>'.$label.'</label><br />
								<select name="'.$col.'">
								');
								if (strlen($_POST[$col])) {
									$value = h($_POST[$col]);
								}
								foreach ($tarr as $val) {
									if ($val == $value) {
										$selected = 'selected';
									}
									else {
										$selected = '';
									}
									c('<option value="'.$val.'" '.$selected.'>'.$val.'</option>');
								}
								c('</select><br /><span class="sub">'.$des.'</span></div>');
							}
						}
					}
					/* removed from 9.7.1
			c('<div class="sf_field">'.t('About me').'<br />
				<textarea name="aboutme" class="form-control" cols="50">'.h($_POST['about_me']).'</textarea></div>');*/
			if (get_gvar('verify_approval')) {
				$question = get_text('community_question');
				if (strlen($question)) {
					
					c('
					<div class="sf_title">'.t('community question').'</div>

					<div class="form-group"><label>*'.$question.'</label><br />
				<textarea name="answer" rows="2" cols="50" placeholder="'.t('Only admin can see your answer').'">'.h($_POST['answer']).'</textarea></div>');
				}
			}
			
			if (!get_gvar('verify_approval')) {
			c('
<div class="form-group"><label>'.t('Say HI').'</label><br />
				<textarea name="sayhi" rows="3" cols="50" placeholder="'.h(t('Say something to the community')).'" class="form-control">'.h($_POST['sayhi']).'</textarea>
				</div>');
		}
		
c('
<div class="sf_title">'.t('Finish').'</div>');
			
			c('<div class="sf_field">'.t('Terms and Privacy Policy').'
			<div style="width:95%;height:100px;overflow:scroll;border:white 2px solid;padding:5px;">
			'.nl2br(h(get_text('rules_conditions'))).'
			</div>
			</div>
			<div class="sf_field">
			<input type="hidden" name="g" value="'.h($_REQUEST['g']).'" />
			<input type="hidden" name="onpost" value="1" />
			<input type="hidden" name="page_picked" value="1" />
			<input type="hidden" name="followed_pages" value="'.h($_POST['followed_pages']).'" id="followed_pages" />
			<label for="agree_rules"><input type="checkbox" id="agree_rules" name="agree_rules" value="1" checked /> '.t('I have read, and agree to abide by the Terms and Privacy Policy.').'</label>
			');
			if (!is_mobile() && !$captcha['disabled'] && !strlen($_SESSION['fb_email'])) {
				c('<div class="sf_field">
				'.recaptcha_get_html($captcha['publickey'],$captchaerror).'
				</div>');
			}
			c('

						<input type="submit" value="'.t('Signup Now').'"  class="btn btn-primary" />
			</div>
</div>
		</form>
<style>
.form-group {
	width:100%;
	padding-left:15px;
}
.form-inline .form-control {
	width:70%;
}
</style>
			');

	}

	function need_verify() {
		global $client;
		if (!$client['id']) die('plz login');
		set_title('Not verified');
		c('Your account is pending');
	}
}