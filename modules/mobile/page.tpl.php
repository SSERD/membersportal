<?php
/* ############################################################ *\
Copyright (C) 2009 - 2010 jcow.net.  All Rights Reserved.
------------------------------------------------------------------------
The contents of this file are subject to the Common Public Attribution
License Version 1.0. (the "License"); you may not use this file except in
compliance with the License. You may obtain a copy of the License at
http://www.jcow.net/celicense. The License is based on the Mozilla Public
License Version 1.1, but Sections 14 and 15 have been added to cover use of
software over a computer network and provide for limited attribution for the
Original Developer. In addition, Exhibit A has been modified to be consistent
with Exhibit B.

Software distributed under the License is distributed on an "AS IS" basis,
 WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.
------------------------------------------------------------------------
The Original Code is Jcow.

The Original Developer is the Initial Developer.  The Initial Developer of the
Original Code is jcow.net.

\* ############################################################ */

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">	

<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta name="Generator" content="Powered by Jcow" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php echo $auto_redirect?>
<style type="text/css" media="all">@import "<?php echo $uhome;?>/files/common_css/style.css";</style>
<style type="text/css" media="all">@import "<?php echo $uhome;?>/modules/mobile/page.css";</style>
<link rel="shortcut icon" href="<?php echo $uhome;?>/themes/<?php echo $theme_tpl;?>/ico.gif" type="image/x-icon" />
<?php echo $tpl_vars['javascripts'];?>
<script>
$(document).bind("mobileinit", function () {
    $.mobile.ajaxEnabled = false;
    $.mobile.keepNative = "select,input";
});
</script>

<link rel="stylesheet" href="//code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css" type="text/css" />
<script src="//code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
<title><?php echo $title?> - <?php echo get_gvar('site_name')?></title>
<?php
if (strlen(get_gvar('site_keywords'))) {
	echo '<meta name="keywords" content="'.get_gvar('site_keywords').'" />';
}
?>
<?php echo $header?>
</head>

<body>

<div id="mobile_box" data-role="page">
    <div data-role="panel" id="jcow_mobile_menu1" data-position="left" data-display="push" >
<?php
echo '<ul data-role="listview" data-theme="b">';
$top_apps = $more_apps = array();
if (is_array($my_apps)) {
	foreach ($my_apps as $item) {
		if ($item['path'] != 'account') {
			$total_length += strlen(t($item['name']));
				$top_apps[] = $item;
		}
	}
}

if (is_array($new_apps)) {
	foreach ($new_apps as $item) {
		if ($item['path'] != 'account') {
			$top_apps[] = $item;
		}
	}
}
foreach ($top_apps as $item) {
	$icon = uhome().'/modules/'.$item['app'].'/icon.png';
	if ($item['icon']) $icon = $item['icon'];
	echo '<li ><a href="'.url($item['path']).'" >
				<img src="'.$icon.'" class="ui-li-icon">'.t($item['name']).'</a></li>';
}
echo '</ul>';
?>
    </div>

<div data-role="panel" id="jcow_mobile_menu2" data-position="right" data-display="push" >
	<ul data-role="listview">
<?php
if (!$client['id']) {
	echo '
	<li>'.url('member/login',t('Login')).'</li>
	<li>'.url('member/signup',t('Sign up')).'</li>
	';
}
else {
	echo '
	<li><a href="'.my_jcow_home().'" >'.t('Home').'</a></li>
	<li>'.url('u/'.$client['uname'],t('Profile')).'</li>
	<li>'.$friendslink.'</li>
	<li>'.url('message',t('Inbox').msg_unread() ).'</li>
	<li>'.url('notifications',t('Notifications').note_unread()).'</li>
	<li>'.url('account',t('Account')).'</li>
	<li>'.url('member/logout',t('Logout') ).'</li>
	';
}
?>
</ul>
</div>

<div data-role="header" data-position="fixed"  data-theme="b">
    <a href="#jcow_mobile_menu1" id="jcow_mobile_menu1_button" class=" ui-btn-left ui-btn ui-btn-inline  ui-corner-all ui-btn-icon-left ui-icon-bars ui-btn-icon-notext" style="border:#cccccc 1px solid">jcow menu1</a>
<h1><?php echo h(get_gvar('site_name'));?></h1>
    <a href="#jcow_mobile_menu2" id="jcow_mobile_menu2_button" class="ui-btn-right ui-btn ui-btn-inline ui-mini ui-corner-all ui-btn-icon-left ui-icon-gear ui-btn-icon-notext" style="border:#cccccc 1px solid">jcow menu2</a>
</div>







<!-- #################### structure ################## -->

<div id="jcow_main" class="ui-content">
<?php


if (is_array($notices)) {
	foreach ($notices as $notice) {
		echo '<div class="notice">'.$notice.'</div>';
	}
}
if ($top_title) {
	echo '<div style="padding:0 0 10px 30px;background:url('.uhome().'/modules/'.$application.'/icon.png) 9px 5px no-repeat;font-size:1.5em">'.$top_title.'</div>';
}

echo $app_header;

if (is_array($buttons)) {
		echo '<div style="padding-left:10px;"><ul class="buttons">';
		foreach ($buttons as $val) {
			echo '<li>'.$val.'</li>';
		}
		echo '</ul></div>';
	}

/* 
The "display_application_content" is the output of applications. 
The default Width is 780px. 
You may not change the Width, otherwise some applications can not be displayed correctly 
*/

if ($application != 'home') {
	display_application_content();
}
else {
	include 'modules/mobile/home.tpl.php';
}
echo '

';


?>




</div><!-- end jcow_application -->

<div id="footer">

<?php
// footer pages
$footer_pages = get_footer_pages();
if (count($footer_pages)) {
	echo '<div id="footer_pages">'.implode(' | ',$footer_pages).'</div>';
}
echo $tpl_vars['language_selection'];
echo $tpl_vars['footer']
?>
<br /><br />
<!-- YOU ARE NOT ALLOWED TO REMOVE jcow_attribution() WITHOUT A BRANDING REMOVAL LICENSE -->
<?php echo jcow_attribution();?>


<?php echo $footer;?>
</div>

</div><!-- end mobile box-->
</body>
</html>