<?php
/* ############################################################ *\
 ----------------------------------------------------------------
Jcow Software (http://www.jcow.net)
IS NOT FREE SOFTWARE
http://www.jcow.net/commercial_license
Copyright (C) 2009 - 2010 jcow.net.  All Rights Reserved.
 ----------------------------------------------------------------
\* ############################################################ */

global $client,$offset,$num_per_page, $page,$captcha;

if ($act == 'login') {
	$atempts = get_tmp($togkey);
	if (!$atempts) $atempts = 0;
	if ($client['id'] > 0) {
		redirect('mobile');
	}
	if ($att == 's') {
		sys_notice(t('Congratulations! You have successfully signed up. You can now login with your account'));
	}
	if ($_POST['step'] == 'post') {
		if ($atempts > 3 && !get_gvar('disable_recaptcha_login')) {
			if ($_POST["recaptcha_challenge_field"]) {
				if (!recaptcha_valid()) {
						$hold = 1;
				}
			}
			else {
				c('<script language="javascript" >
				$(document).ready( function(){
									$("#recaptcha_response_field").focus();
			});
									</script>');
				$hold = 1;
			}
		}
		if (!$hold) {
			$password = md5($_POST['password'].'jcow');
			$res = sql_query("select * from `".tb()."accounts` where  (email='".$_POST['username']."' or username='".$_POST['username']."') and password='$password'  limit 1");
			if (sql_counts($res)) {
				$newss = get_rand(12);
				$row = sql_fetch_array($res);
				if ($_POST['remember_me']) {
					sql_query("UPDATE `".tb()."accounts` SET jcowsess='$newss' WHERE id='{$row['id']}' ");
					setcookie('jcowss', $newss, time()+3600*24*365,"/");
					setcookie('jcowuid', $row['id'], time()+3600*24*365,"/");
				}
				$_SESSION['uid'] = $row['id'];
				set_tmp($togkey,'deleteit');
				redirect(url('mobile'));
			}
			else {
				$atempts++;
				$_SESSION['login_cd']--;
				set_tmp($togkey,$atempts);
				sys_notice(t('Wrong account or password'));
			}
		}
	}

	set_title(t('Login'));
	if ($client['id']) redirect('mobile');
	section_content('
			<script language="javascript" >
			$(document).ready( function(){
								$("#recaptcha_response_field").attr("tabindex",3);
		});
								</script>
		<form method="post" name="form1" id="form1" action="'.url('mobile/member/login').'" >
								'.t('Username or Email').':<br />
								<input type="text" size="10" name="username" style="width:120px" value="'.h($_POST['username']).'" tabindex=1 /><br /><br />

								'.t('Password').':<br />
								<input type="hidden" name="step" value="post" />
								<input type="password" size="10" name="password" style="width:120px"  value="'.h($_POST['password']).'" tabindex=2 />
										
								');
		if ($atempts > 3 && !get_gvar('disable_recaptcha_login')) {
			c( recaptcha_get_html($captcha['publickey'],''));
		}
		
		c('
							<div class="sub">
							( <input type="checkbox" name="remember_me" value="1" '.$remember_check.' tabindex=4 /> '.t('Remember me').' ) 
							</div>
							<input type="submit" value="'.t('Login').'" tabindex=5 /> 
							<br /><br />
							</form>
<div class="sub">'.url('mobile/member/signup',t('Register an account')).'</div>
<div class="sub">'.url('mobile/member/chpass',t('Forgot password?')).'</div>
							<script language="javascript" >
				document.form1.username.focus();
				</script>
		');
	section_close(t('Log in'));

}

// ############################# SIGN UP ###############################
elseif ($act == 'signup') {
	if ($client['id'] > 0) {
		redirect('mobile');
	}
		if ($_POST['onpost']) {
			if (get_gvar('only_invited')) {
				$hold = 1;
			}
			if ($_POST['iid']) {
				$res = sql_query("select * from ".tb()."invites where id='{$_POST['iid']}'");
				$invite = sql_fetch_array($res);
				if ($invite['id']) {
					if ($hold) {
						$hold = 0;
						if ($invite['email'] != $_POST['email']) {
							$errors[] = t('You must use the Email address from which you received the invitation');
						}
					}
				}

			}
			if ($hold) {
				die('<p>'.
					t('Only invited people can Sign up this network. If you are invited, please click the link in the inviting email.').
					'</p>');
			}
			if (!get_gvar('disable_recaptcha_reg')) {
				if (!recaptcha_valid()) {
						$errors[] = t('Wrong Verification code');
				}
			}
			if (!$_POST['agree_rules']) {
				$errors[] = t('You must agree to our rules for signing up');
			}
			
			//get_r(array('username','password','password2','email','agree','confirm_code','location'));
			if (strtolower($_COOKIE['cfm']) != strtolower($_POST['confirm_code'])) {
				$errors[] = t('The string you entered for the code verification did not match what was displayed');
			}
			$_POST['username'] = strtolower($_POST['username']);
			if (strlen($_POST['username']) < 4 || strlen($_POST['username']) > 18 || !preg_match("/^[0-9a-z]+$/i",$_POST['username'])) {
				$errors[] = t('Username').': '.t('from 4 to 18 characters, only 0-9,a-z');
			}
			if (preg_match('/</',$_POST['fullname'])) {
				$errors[] = 'Unavailable Full name format';
			}

			if (!$_POST['email'] || !$_POST['fullname'] || !$_POST['username']) {
				$errors[] = t('Please fill in all the required blanks');
			}
			else {
				for($i=1;$i<=7;$i++) {
					$col = 'var'.$i;
					$key = 'cf_var'.$i;
					$key2 = 'cf_var_value'.$i;
					$key3 = 'cf_var_des'.$i;
					$key4 = 'cf_var_label'.$i;
					$key5 = 'cf_var_required'.$i;
					$ctype = get_gvar($key);
					if ($ctype != 'disabled' && get_gvar($key5)) {
						if (!strlen($_POST[$col])) {
							$errors[] = t('Please fill in all the required blanks');
						}
					}
				}
			}
			if(!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i", $_POST['email'])) {
				$errors[] = t('Unavailable email address');
			}

			$password = md5($_POST['password'].'jcow');
			$timeline = time();
			$res = sql_query("select * from `".tb()."accounts` where email='{$_POST['email']}'");
			if (sql_counts($res)) {
				$errors[] = t('You have registered with this email address before.');
			}
			$res = sql_query("select * from `".tb()."accounts` where username='{$_POST['username']}'");
			if (sql_counts($res)) {
				$errors[] = t('The Username has already been used');
			}

			if (!is_array($errors)) {
				if (get_gvar('acc_verify') == 1) {
					$reg_code = get_rand(6,'0123456789');
					$verify_note = t('Verification Code: {1}',$reg_code)."\r\n<br />";
				}
				else {
					$reg_code = '';
					$verify_note = '';
				}
				// member
				if ($_POST['hide_age']) {
					$hide_age = 1;
				}
				else {
					$hide_age = 0;
				}
				$newss = get_rand(12);
				if (get_gvar('pm_enabled') || get_gvar('acc_verify')) {
					$member_disabled = 1;
				}
				else {
					$member_disabled = 0;
				}
				sql_query("insert into `".tb()."accounts` (about_me,disabled,gender,location,birthyear,birthmonth,birthday,hide_age,password,email,username,fullname,created,lastlogin,ipaddress,var1,var2,var3,var4,var5,var6,var7,reg_code) values('{$_POST['about_me']}',$member_disabled,'{$_POST['gender']}','{$_POST['location']}','{$_POST['birthyear']}','{$_POST['birthmonth']}','{$_POST['birthday']}','{$hide_age}','$password','".$_POST['email']."','{$_POST['username']}','{$_POST['fullname']}',$timeline,$timeline,'{$client['ip']}','{$_POST['var1']}','{$_POST['var2']}','{$_POST['var3']}','{$_POST['var4']}','{$_POST['var5']}','{$_POST['var6']}','{$_POST['var7']}','{$reg_code}')");
				$uid = insert_id();
				if ($uid == 1) {
					sql_query("update ".tb()."accounts set roles='3' where id='$uid'");
				}
				sql_query("insert into `".tb()."pages` (uid,uri,type) values($uid,'{$_POST['username']}','u')");
				$page_id = insert_id();
				if ($_POST['iid']) {
					sql_query("update ".tb()."invites set status=1 where id='{$_POST['iid']}'");
				}

				stream_publish(t('Signed Up','','','',1),'','',$uid,$page_id);

				// welcome email
				$welcome_email = nl2br(get_text('welcome_email'));
				$welcome_email = str_replace('%username%',$_POST['username'],$welcome_email);
				$welcome_email = str_replace('%email%',$_POST['email'],$welcome_email);
				$welcome_email = str_replace('%password%',$_POST['password'],$welcome_email);
				$welcome_email = str_replace('%sitelink%',url(uhome(),h(get_gvar('site_name')) ),$welcome_email);
				@jcow_mail($_POST['email'], 'Welcome to "'.h(get_gvar('site_name')).'"!', $verify_note.$welcome_email);
				$_SESSION['login_cd'] = 3;
				redirect(url('mobile/member/login/s'));
				exit;
			}
			else {
				foreach ($errors as $error) {
					$error_msg .= '<li>'.$error.'</li>';
				}
				sys_notice(t('Errors').':<ul>'.$error_msg.'</ul>');
			}
		}

		if (get_gvar('only_invited')) {
			$hold = 1;
		}
		if ($iid) {
			$res = sql_query("select * from ".tb()."invites where id='{$iid}'");
			$invite = sql_fetch_array($res);
			if ($invite['id']) {
				$hold = 0;
				$use_i_email = t('You must use the Email address from which you received the invitation');
				$iid_field = '<input type="hidden" name="iid" value="'.$iid.'" />';
			}
		}
		if ($hold) {
			c(t('Sorry, only invited people can sign up'));
			stop_here();
		}

		set_title('Signup');
			if (get_gvar('pm_enabled')) {
				c('<strong>'.t('Join Us').'</strong><br />
				'.t('Membership pricing').':<ul>');
				if ($pm_1m = get_gvar('pm_1m')) {
					c('<li>'.$pm_1m.' '.get_gvar('pm_currency').' '.t('Per month').'</li>');
				}
				if ($pm_3m = get_gvar('pm_3m')) {
					c('<li>'.$pm_3m.' '.get_gvar('pm_currency').' '.t('Per Annua').'</li>');
				}
				if ($pm_12m = get_gvar('pm_12m')) {
					c('<li>'.$pm_12m.' '.get_gvar('pm_currency').' '.t('Per Yeal').'</li>');
				}
				c('</ul>');
				section_close(t('Paid membership'));
			}
					c('
<script>
$(document).ready( function(){
	objrow = $("tr.row1 td::first-child");
	objrow.attr("valign","top");
	objrow.attr("align","right");
	});
</script>
		<form method="post" action="'.url('mobile/member/signup').'" >
<strong>'.t('Passport').'</strong><br />
*'.t('Email Address').' '.$use_i_email.':<br />
					<input type="text" size="20" name="email" value="'.h($_REQUEST['email']).'" class="fpost" style="width:180px" />
					<br />
					<span class="sub">('.$invite_msg.t("We won't display your Email Address.").')</span>
<br /><br />

*'.t('Username').'/'.t('Nickname').':<br />
					<input type="text" size="18" class="fpost" name="username" value="'.h($_REQUEST['username']).'" style="width:180px" /><br />
					<span class="sub">('.t('4 to 18 characters, made up of 0-9,a-z').')</span>
<br /><br />


*'.t('Password').':<br />
					<input type="password" name="password"  class="fpost" value="'.h($_REQUEST['password']).'" style="width:180px" />
<br /><br />
<strong>'.t('Personal info').'</strong><br />
*'.t('Full Name').':<br />
					<input type="text" size="20" name="fullname" value="'.h($_REQUEST['fullname']).'"  class="fpost" style="width:180px" />
<br /><br />
*'.t('Birth').':<br />
					<select name="birthyear" class="fpost">
					');
					$year_from = date("Y",time()) - 8;
					$year_to = date("Y",time()) - 100;
					if ($_REQUEST['birthyear'])
						$yearkey = $_REQUEST['birthyear'];
					else
						$yearkey = $year_from - 12;
					for ($i=$year_from;$i>$year_to;$i--) {
						$selected = '';
						if ($yearkey == $i)
							$selected = 'selected';
						c('<option value="'.$i.'" '.$selected.'>'.$i.'</option>');
					}
					if ($row['hide_age']) $hide_age = 'checked';
					c('
					</select>
					<select name="birthmonth" class="fpost">');
					for ($i=1;$i<13;$i++) {
						if ($i<10)$j='0'.$i;else $j=$i;$iss='';
						if ($_REQUEST['birthmonth'] == $j) $iss='selected';
						c('<option value="'.$j.'" '.$iss.' >'.$j.'</option>');
					}
					c('</select>
					<select name="birthday" class="fpost">');
					for ($i=1;$i<=31;$i++) {
						if ($i<10)$j='0'.$i;else $j=$i;$iss='';
						if ($_REQUEST['birthday'] == $j) $iss='selected';
						c('<option value="'.$j.'" '.$iss.'>'.$j.'</option>');
					}
					c('</select><br />
					 <input type="checkbox" name="hide_age" value="1" '.$hide_age.' />'.t('Hide my age').'
<br /><br />');
					if ($_REQUEST['gender'] == 1) {
						$gender1 = 'checked';
					}
					elseif ($_REQUEST['gender'] == 2) {
						$gender2 = 'checked';
					}
					else {
						$gender0 = 'checked';
					}

	c('
	*'.t('Gender').':<br />
	<input type="radio" name="gender" value="1" '.$gender1.' />'.t('Male').' 
	<input type="radio" name="gender" value="0" '.$gender0.' />'.t('Female').'
	<input type="radio" name="gender" value="2" '.$gender2.' />'.t('Hide').'
<br /><br />

*'.t('Come from').':<br />
					<select name="location" class="inputText">');
					$locations = explode("\r\n",get_text('locations'));
					$_REQUEST['location'] = trim($_REQUEST['location']);
					foreach($locations as $location) {
						if ($_REQUEST['location'] == trim($location)) {
							$selected = 'selected';
						}
						else {
							$selected = '';
						}
						c('<option value="'.$location.'" '.$selected.' >'.$location.'</option>');
					}
					c('</select>
<br /><br />
'.t('About me').'<br />
					<textarea rows="3" name="about_me">'.htmlspecialchars($client['about_me']).'</textarea>
<br /><br />
			'); 
			
			
					// custom fields 
					$profile = array();
					for($i=1;$i<=7;$i++) {
						$col = 'var'.$i;
						$key = 'cf_var'.$i;
						$key2 = 'cf_var_value'.$i;
						$key3 = 'cf_var_des'.$i;
						$key4 = 'cf_var_label'.$i;
						$key5 = 'cf_var_required'.$i;
						$ctype = get_gvar($key);
						$value = get_gvar($key2);
						$des = get_gvar($key3);
						$label = get_gvar($key4);
						$required = get_gvar($key5);
						if ($required) $required = '*';
						if ($ctype != 'disabled') {
							if ($ctype == 'text') {
								if (strlen($profile[$col])) {
									$value = htmlspecialchars($profile[$col]);
								}
								if (strlen($_POST[$col])) {
									$value = h($_POST[$col]);
								}
								c($required.$label.':<br />
								<input type="text" name="'.$col.'" value="'.$value.'" />
								<br /><span class="sub">'.$des.'</span>
								<br /><br />');
							}
							elseif ($ctype == 'textarea') {
								if (strlen($profile[$col])) {
									$value = htmlspecialchars($profile[$col]);
								}
								if (strlen($_POST[$col])) {
									$value = h($_POST[$col]);
								}
								c($required.$label.':<br />
								<textarea rows="3" name="'.$col.'" />'.$value.'</textarea>
								<br /><span class="sub">'.$des.'</span>
								<br /><br />');
							}
							elseif ($ctype == 'select_box') {
								$tarr = explode("\r\n",$value);
								c($label.':<br />
								<select name="'.$col.'">
								');
								if (strlen($_POST[$col])) {
									$value = h($_POST[$col]);
								}
								foreach ($tarr as $val) {
									if ($val == $value) {
										$selected = 'selected';
									}
									else {
										$selected = '';
									}
									c('<option value="'.$val.'" '.$selected.'>'.$val.'</option>');
								}
								c('</select><br /><span class="sub">'.$des.'</span>
								<br /><br />');
							}
						}
					}
			c($iid_field);

			if (!get_gvar('disable_recaptcha_reg')) {
				c(t('Image verification').':<br />
				'.recaptcha_get_html($captcha['publickey'],$captchaerror).'
				<br /><br />');
			}
			c('<strong>'.t('Rules & Conditions').'</strong>
			<div style="width:300px;height:100px;overflow:scroll;border:white 2px solid;padding:5px;">
			'.nl2br(h(get_text('rules_conditions'))).'
			</div>
			<input type="hidden" name="g" value="'.$_REQUEST['g'].'" />
			<input type="hidden" name="onpost" value="1" />
			<input type="checkbox" name="agree_rules" value="1" checked /> '.t('I have read, and agree to abide by the Rules & Conditions.').' <br />
						<input type="submit" style="background:#5BA239;color:white;font-size:1.5em;font-weight:bold" value="'.t('Signup Now').'" />
		</form>	
			');
			section_close(t('Sign up'));
}

elseif ($act == 'chpass') {
	set_title(t('Get back my password'));
	if ($client['id']) {
		die('already logged in');
	}
	c('
	<form method="post" name="form1" action="'.url('mobile/member/chpasspost').'" >
				<p>
				'.label(t('Email')).'
				<input type="text" name="email" />
				<div class="sub">'.t('The email address you registered with').'</div>
				</p>
				<p>
				<input class="button" type="submit" value="'.t('Submit').'" />
				</p>
				</form>
				<script language="javascript" >
				document.form1.email.focus();
				</script>');
	section_close(t('Get back my password'));
}

elseif ($act == 'chpasspost') {
	if(!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i", $_POST['email'])) {
		die(t('Unavailable email address'));
	}
	$res = sql_query("select * from `".tb()."accounts` where email='{$_POST['email']}'");
	$row = sql_fetch_array($res);
	if (!$row['id']) {
		die(t('No user registered with this email'));
	}
	$code = get_rand(10);
	sql_query("update `".tb()."accounts` set chpass='$code' where id={$row['id']}");
	$link = url('mobile/member/chpassdo/'.$code,t('Click Here'));
	jcow_mail($_POST['email'],t('Get your password'),$link);
	c(t('A verification email has been sent to you, please check your email inbox'));
	section_close('Sent');
}

elseif ($act == 'chpassdo') {
		if (!preg_match("/^[0-9a-z]+/i",$att)) die('not a valid code');
		$res = sql_query("select * from `".tb()."accounts` where chpass='{$att}'");
		$user = sql_fetch_array($res);
		if (!sql_counts($res)) die('wrong code');
		c('<form method="post" name="form1" action="'.url('mobile/member/chpassdopost').'" >
					<p>
					'.t('Pick a new password').':<br />
					<input type="password" name="password" />
					<input type="hidden" name="passcode" value="'.$att.'" />
					<input type="hidden" name="id" value="'.$user['id'].'" /><br />
					<input class="button" type="submit" value="'.t('Save').'" />
					</p>
					</form>');
		section_close('Password');
}

elseif ($act == 'chpassdopost') {
	$code = $_POST['passcode'];
	if (!preg_match("/^[0-9a-z]+/i",$code)) die('not a valid code');
	$res = sql_query("select * from `".tb()."accounts` where chpass='{$code}'");
	if (!sql_counts($res)) die('wrong code');
	$newpass = md5($_POST['password'].'jcow');
	sql_query("update ".tb()."accounts set chpass='',password='$newpass' where id='{$_POST['id']}' and chpass='{$code}'");
	redirect('mobile/member/login',1);
}



elseif ($act == 'logout') {
	if ($client['id']) {
		 $_SESSION['uid'] = 0;
		 setcookie('jcowuid', '', time()+3600*24*365,"/");
		 setcookie('jcowss', '', time()+3600*24*365,"/");
	}
	redirect('mobile');

}