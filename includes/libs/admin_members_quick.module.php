<?php
/* ############################################################ *\
 ----------------------------------------------------------------
Jcow Software (http://www.jcow.net)
IS NOT FREE SOFTWARE
http://www.jcow.net/commercial_license
Copyright (C) 2009 - 2010 jcow.net.  All Rights Reserved.
 ----------------------------------------------------------------
\* ############################################################ */

if (basename($_SERVER["SCRIPT_NAME"]) != 'index.php') die(basename($_SERVER["SCRIPT_NAME"]));
global $page,$client,$num_per_page,$offset;
if ($step == 'modify') {
}

elseif($step == 'changestatus') {
	if (is_array($_POST['ids'])) {
		foreach ($_POST['ids'] as $id) {
			if ($id != $client['id']) {
				$res = sql_query("select * from ".tb()."accounts where id='$id'");
				$row = sql_fetch_array($res);
				if ($row['id']) {
					sql_query("delete from ".tb()."requests where uid='{$row['id']}'");
					if (!$_POST['status']) { //verify
						if ($row['disabled']) {
							sql_query("update ".tb()."accounts set disabled='{$_POST['status']}' where id='$id'");
							@jcow_mail($row['email'], 'Account approved', 
							t('Congratulations, Your account on {1} has been approved!',url(uhome(),get_gvar('site_name')))
							);
						}
					}
					elseif ($_POST['status'] == 3) {
						jcow_deleteuser($row['id']);

					}
					elseif ($_POST['status'] == 4) {
						jcow_deleteuser($row['id']);
						sql_query("insert ignore into ".tb()."spammers (userkey,type,created) values('{$row['ipaddress']}','ip',".time().")");
						sql_query("insert ignore into ".tb()."spammers (userkey,type,created) values('{$row['email']}','email',".time().")");

					}
					else {
						sql_query("update ".tb()."accounts set disabled='{$_POST['status']}' where id='$id'");
						}
				}
			}
		}
	}
	redirect(url('admin/members_quick','','',array('page'=>$_POST['page'])),1);
}

		c('<form method="post" action="'.url('admin/members_quick').'">
		Username or Email address: 
		<input type="text" name="username" /> <input type="submit" value="'.t('Search').'" />
		</form><br />');
		if ($_POST['username']) {
			$res = sql_query("select * from `".tb()."accounts` "." where username like '%{$_POST['username']}%' or email like '%{$_POST['username']}%' order by id DESC limit 12");
		}
		else {
			$res = sql_query("select count(*) as total from `".tb()."accounts` "." where 1 $filter ");
			$row = sql_fetch_array($res);
			$total = $row['total'];
			$pb       = new PageBar($total, $num_per_page, $page);
			$pb->paras = url('admin/members_quick'.$pageb);
			$pagebar  = $pb->whole_num_bar();

			$res = sql_query("select * from `".tb()."accounts` "." order by id DESC limit $offset,$num_per_page");
		}
		c('
			<style>
			.mq_list_selected {
				background: #B8D6BF;
				border-bottom: white 1px solid;
			}
			.mq_list {
				cursor:pointer;
			}
			</style>
			<script>
			$(function() {
				$(".mq_list").click(function(event) {
					if ($(event.target).attr("class") != "mq_checkbox") {
						$(this).find("input:checkbox").click();
					}
				});
				$(".mq_checkbox").click(function() {
					$(this).parents(".mq_list").toggleClass("mq_list_selected");
					$(this).parents(".mq_list").toggleClass("row1");
					$("#mq_count").html($(".mq_checkbox:checked").length);
				});
				$("#mq_submit").click(function() {
					var len = $(".mq_checkbox:checked").length;
					if (!len) {
						alert("no user selected");
						return false;
					}
				});
	});
		</script>
		<form method="post" action="'.url('admin/members_quick/changestatus').'">
		<table class="stories">
		<tr class="table_line1">
		<td></td><td>Member</td><td>Details</td></tr>');
		while ($member = sql_fetch_array($res)) {
			$res2 = sql_query("select * from ".tb()."requests where uid='{$member['id']}'");
			$row2 = sql_fetch_array($res2);
			if ($member['disabled'] == 1) {
			$status = '<span style="color:red">Un-verified</span>';
			}
			elseif ($member['disabled'] == 2) {
				$status = '<font color="red">Suspended</font>';
			}
			elseif ($member['disabled'] == 3) {
				$status = '<font color="red">Spammer</font>';
			}
			else {
				$status = '<font color="green">Verified</font>';
			}
			$gender = $member['gender'] ? 'Male':'Female';
			if ($client['is_demo']) {
				$member['email'] = $member['ipaddress'] = '<font color="blue">*hidden for DEMO*</font>';
			}
			section_content('
			<tr class="row1 mq_list ">
			<td><input type="checkbox" name="ids[]" value="'.$member['id'].'" class="mq_checkbox" /></td>
			<td valign="top" width="60">'.avatar($member).'<br />'.$member['username'].'<br />'.h($member['fullname']). '<br />'.$status.'<br />'.url('admin/useredit/'.$member['id'],'Edit').'<br />
			<a href="#" class="deleteuser" uid="'.$member['id'].'">Delete</a>
			</td>
			<td><div style="font-size:11px"><code>General:</code> '.$gender.', 
			'.$member['ipaddress'].', 
			'.$member['email'].', 
			'.get_date($member['created']).'
			</dov>
			<div style="font-size:11px">
			<code>Field 1</code> '.h($member['var1']).'
			<code>Field 2</code> '.h($member['var2']).' 
			<code>Field 3</code> '.h($member['var3']).' 
			<code>Field 4</code> '.h($member['var4']).' 
			<code>Field 5</code> '.h($member['var5']).' 
			<code>Field 6</code> '.h($member['var6']).' 
			</div>
			</td>
			</tr>');
		}
		section_content('
		<tr class="row2"><td colspan="4">
		<span style="font-weight:bold" id="mq_count">0</span> member(s) selected<br />
		Change status to:
		<input type="radio" name="status" value="0" checked/> <span class="jcow_buttonflag">Verified</span>
		<input type="radio" name="status" value="1"/> <span class="jcow_buttonflag">Un-verified</span>
		<input type="radio" name="status" value="2"/> <span class="jcow_buttonflag">Suspend</span>
		<input type="radio" name="status" value="3"/> <span class="jcow_buttonflag">Delete</span>
		<input type="radio" name="status" value="4"/> <span class="jcow_buttonflag">Delete and Block IP</span>
	<input type="submit" value="Save" id="mq_submit" class="btn btn-primary" />
		<div class="sub">
					<strong>Verified</strong> - make user verified.<br />
					<strong>Un-verified</strong> - can not post anything.<br />
					<strong>Suspended</strong> - can not login.<br />
					<strong>Delete</strong> - user will be deleted.<br />
					<strong>Delete and Block IP</strong> - delete user. add black list.</div>
		<input type="hidden" name="page" value="'.$page.'" />
		</td></tr>
		
		</table>
		</form>');
		
		c($pagebar);
