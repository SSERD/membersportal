<?php
/* ############################################################ *\
 ----------------------------------------------------------------
Jcow Software (http://www.jcow.net)
IS NOT FREE SOFTWARE
http://www.jcow.net/commercial_license
Copyright (C) 2009 - 2010 jcow.net.  All Rights Reserved.
 ----------------------------------------------------------------
\* ############################################################ */

class search{
	
	function index() {
		c('
		<form action="'.url('search/listing').'" method="post">
		<p>
		<label>title</label> <input type="text" name="title" />
		</p>
		<p>
		<input type="submit" class="button" value="'.t('Submit').'" />
		</p>
		</form>
		');
	}

	function ajax_search() {
		$keyword = trim($_GET['q']);
		if (strlen($keyword)<1) die('keyword too short');
		$num = 0;
		// users
		$res = sql_query("select * from 
		(select id,username,avatar,fullname from jcow_accounts order by id DESC limit 1000) as t
		 where fullname like '%".safe($keyword)."%' limit 5");
		while ($user = sql_fetch_array($res)) {
			$num++;
			$response .= '<div><a href="'.url('u/'.$user['username']).'" class="search_results">'.avatar($user,25,array('nolink'=>1)).' '.h($user['fullname']).'</a></div>';
		}

		// pages&groups
		$limit = 10 - $num;
		$res = sql_query("select * from 
		(select id,uri,name,logo,type from jcow_pages order by id DESC limit 2000) as t
		 where name  like '%".safe($keyword)."%' limit $limit");
		while ($page = sql_fetch_array($res)) {
			if ($page['type'] == 'group') {
				$url = url('group/'.$page['uri']);
			}
			else {
				$url = url('page/'.$page['uri']);
			}
			$response .= '<div><a href="'.$url.'" class="search_results">'.page_logo($page,'25',array('nolink'=>1)).' '.h($page['name']).'</a></div>';
		}

		if (!$response)
			$response = t('No result');
		$response .= '<div><a href="'.url('browse').'" class="search_results">'.t('Advanced search').'</a></div>';
		echo $response;
		exit;
	}
	
	function listing() {
		global $offset, $num_per_page, $page,$client;
		$search_num = 12;
		if (strlen($_POST['title'])) {
			$title = $_POST['title'];
			$type = $_POST['type'];
		}
		else {
			$title = trim($_GET['title']);
			$type = $_GET['type'];
		}
		if ($type == 'group') {
			$arr = array('group'=>1);
			$search_num = 36;
		}
		elseif ($type == 'user') {
			$arr = array('user'=>1);
			$search_num = 36;
		}
		elseif ($type == 'page') {
			$arr = array('page'=>1);
			$search_num = 36;
		}
		else {
			$arr = array('group'=>1,'user'=>1,'page'=>1);
		}
		$display_title = stripslashes(trim($title));
		c('Searching <strong>'.h($title).'</strong><br />');
		if (strlen($title) < 3) {
			sys_back(t('Keyword is too short'));
		}
		else {
			set_title(h($title));
			if ($arr['user']) {
				$c = '<h1>'.h($display_title).'</h1>';
				$res = sql_query("select * from ".tb()."accounts where fullname like '%{$title}%'  and !disabled order by lastlogin DESC limit $search_num");
				while ($user = sql_fetch_array($res)) {
					$users .= '<li>'.url('u/'.$user['username'],$user['username']).'<br />'.avatar($user).'<br />'.h($user['username']).'</li>';
				}
				if (strlen($users)) {
					$c .= '<h2>'.t('People').'</h2>';
					$c .= '<ul class="small_avatars">'.$users.'</ul>';
				}
			}

			if ($arr['page']) {
				$c .= '<h2>'.t('Pages').'</h2>';
				$res = sql_query("select * from jcow_pages where type='page' and name  like '%{$title}%'  order by users DESC limit $search_num");
				while ($page = sql_fetch_array($res) ) {
					if ($client['id']) {
						$res2 = sql_query("select * from jcow_page_users where uid='{$client['id']}' and pid='{$page['id']}'");
						if (!sql_counts($res2)) {
							$page['follow_btn'] = '<div><a href="#" class="jcow_button dofollow" page_id="'.$page['id'].'" rel="nofollow"><i class="fa fa-plus"></i> '.t('Like').'</a></div>';
						}
					}
					$page['users']++;
					if (!$page['logo']) {
						$page['logo'] = 'logo.jpg';
					}
					$logo = url('page/'.$page['uri'],'<img src="'.uhome().'/'.uploads.'/avatars/s_'.$page['logo'].'" width="25" height="25" />');
					$i++;
					$c .= '<div  style="padding:10px;border-bottom:#eeeeee 1px solid"><table><tr><td width="60">
					'.url('page/'.$page['uri'],'<img src="'.uhome().'/'.uploads.'/avatars/s_'.$page['logo'].'" />').' </td><td>'.
						url('page/'.$page['uri'],h($page['name'])).'<div class="sub">'.t('{1} likes',$page['users']).'</div>
						'.$page['follow_btn'].'</td></tr></table>
					</div>';
				}
			}

			if ($arr['group']) {
				$c .= '<h2>'.t('Groups').'</h2>';
				$res = sql_query("select * from jcow_pages where type='group' and name  like '%{$title}%'  order by users DESC limit $search_num");
				while ($group = sql_fetch_array($res) ) {
					$group['users']++;
					if (!$group['logo']) {
						$group['logo'] = 'logo.jpg';
					}
					$logo = url('group/'.$group['uri'],'<img src="'.uhome().'/'.uploads.'/avatars/s_'.$group['logo'].'" width="25" height="25" />');
					$i++;
					$c .= '<div  style="padding:10px;border-bottom:#eeeeee 1px solid"><table><tr><td width="60">
					'.url('group/'.$group['uri'],'<img src="'.uhome().'/'.uploads.'/avatars/s_'.$group['logo'].'" />').' </td><td>'.
						url('group/'.$group['uri'],h($group['name'])).'<div class="sub">'.t('{1} members',$group['users']).'</div></td></tr></table>
					</div>';
				}
			}

			c($c);
		}
	}

	function result($hash) {
		set_title('Search result');
		c(get_cache($hash).get_gvar('ad_block_search'));
	}
	
}
