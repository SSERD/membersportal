<?php
/* ############################################################ *\
 ----------------------------------------------------------------
Jcow Software (http://www.jcow.net)
IS NOT FREE SOFTWARE
http://www.jcow.net/commercial_license
Copyright (C) 2009 - 2010 jcow.net.  All Rights Reserved.
 ----------------------------------------------------------------
\* ############################################################ */

class jquery {
	function __construct() {

	}
	
	function index() {
		exit;
	}

	function uploadimg() {
		global $client;
		if (!$client['id']) {
			die('need login');
		}
		$photos = $story['photos'];
		$file_tmp_name = $_FILES['file']['tmp_name'];
		if ($file_tmp_name) {
			$photo = array('name'=>$_FILES['file']['name'],
			'tmp_name'=>$file_tmp_name,
			'type'=>$_FILES['file']['type'],
			'size'=>$_FILES['file']['size']);

			list($width, $height) = getimagesize($file_tmp_name);
			if ($width <= 1000) {
				$uri = save_file($photo);
			}
			else {
				$height = floor(1000*$height/$width);
				$uri = save_thumbnail($photo, 1000, 0);
			}
			$photos++;
			$thumb = save_thumbnail($photo, 400, 400);
			$size = $photo['size'];
			$token = $client['id'].get_rand(5);
			sql_query("insert into `".tb()."story_photos` (sid,uri,des,thumb,size) values( 0,'$uri','$token','$thumb','$size')");
			echo json_encode(
				array('token'=>$token,'thumb'=>$thumb)
				);			
		}
		exit;
	}

	function mobile_menu() {
		global $client,$new_apps;
		if (!$client['id']) die('please login');
		c('<div class="jcow_app_menu">');
		c( '<a href="'.url('u/'.$client['username']).'">'.avatar($client,25,array('nolink'=>1)).' '.($client['fullname']).'</a>
			<a href="'.url('account').'"><i class="fa fa-pencil-square-o"></i> '.t('Edit profile').'</a>');
		c( '<div class="jcow_app_menu_title">'.t('Apps').'</div>');
		foreach ($new_apps as $item) {
			if ($item['type'] == 'crossplatform') {
				$skey = $item['path'];
				$menuon_class = check_menu_on($item['path']);
				c( '<div>
				<a href="'.url($item['path']).'" '.$menuon_class.'><img src="modules/'.$item['app'].'/icon.png" /> '.t($item['name']).'<i class="fa fa-chevron-right"></i></a>
				</div>');
			}
		}
		if (module_actived('groups')) {
			c( '<div class="jcow_app_menu_title">'.t('Groups').'</div>');
			if ($client['id']) {
				$res = sql_query("select p.* from ".tb()."page_users as u left join ".tb()."pages as p on p.id=u.pid where u.uid='{$client['id']}' and p.type='group' order by p.updated DESC limit 6");
				while ($group = sql_fetch_array($res) ) {
					if ($group['logo']) {
						$group['logo'] = '<img src="'.uploads.'/avatars/s_'.$group['logo'].'" width="25" height="25" />';
					}
					else {
						$group['logo'] = '<img src="modules/groups/icon.png" />';
					}
					c( '<a href="'.url('group/'.$group['uri']).'">'.$group['logo'].' '.h($group['name']).'</a>');
				}
				c( '<a href="'.url('groups').'" ><i class="fa fa-search-plus"></i> '.t('Browse groups').'</a>
					<a href="'.url('groups/create').'" ><i class="fa fa-plus"></i> '.t('Create group').'</a>');
			}
		}
		if (module_actived('pages')) {
			c( '<div class="jcow_app_menu_title">'.t('Pages').'</div>');
			if ($client['id']) {
				$res = sql_query("select * from ".tb()."pages where type='page' and uid='{$client['id']}' order by updated DESC limit 6");
				while ($page = sql_fetch_array($res) ) {
					if ($page['logo']) {
						$page['logo'] = '<img src="'.uploads.'/avatars/s_'.$page['logo'].'" width="25" height="25" />';
					}
					else {
						$page['logo'] = '<img src="modules/pages/icon.png" />';
					}
					c( '<a href="'.url('page/'.$page['uri']).'">'.$page['logo'].' '.h($page['name']).'</a>');
				}
				c('<a href="'.url('pages').'" ><i class="fa fa-search-plus"></i> '.t('Browse pages').'</a>
					<a href="'.url('pages/create').'" ><i class="fa fa-plus"></i> '.t('Create page').'</a>');
			}
		}
		c('<div class="jcow_app_menu_title">'.t('Others').'</div>
			<a href="#" class="jm_logout"><i class="fa fa-sign-out"></i> '.t('Logout').'</a>');
		c('</div>');
	}


	function ratestory() {
		global $client;
		if (!$client['id']) {
			die('need login');
		}
		$storyid = $_POST['sid'];
		$rate = $_POST['rate'];
		$rate = explode('&',$rate);
		foreach ($rate as $val) {
			$rate2 = explode('=',$val);
			if (is_numeric($rate2[1]) && $rate2[1] > 0) {
				$key = $rate2[0];
				$rvote[$key] = $rate2[1];
			}
		}
		if (!is_numeric($storyid)) {
			die('wrong sid');
		}
		$res = sql_query("select id,digg,dugg,rating from `".tb()."stories` where id='$storyid' ");
		$story = sql_fetch_array($res);
		if (!$story['id']) die('wrong sid');
		$ratings = unserialize($story['rating']);
		if (!is_array($ratings)) die('not array');
		$digg = $i = 0;
		foreach($ratings as $key=>$rating) {
			if ($rvote[$key] > 0) {
				$ratings[$key]['score'] = $ratings[$key]['score'] + $rvote[$key];
				$ratings[$key]['users'] = $ratings[$key]['users'] + 1;
				$digg = $digg + $rvote[$key];
				$i++;
			}
		}
		$ratings = serialize($ratings);
		if ($digg) {
			$story['digg'] = $story['digg'] + ceil($digg/$i);
			$story['dugg'] = $story['dugg'] + 1;
		}
		
		// 是否已经投过票
		$res = sql_query("select * from `".tb()."votes` where uid='{$client['id']}' and sid='{$storyid}' limit 1");
		if (!sql_counts($res)) {
			sql_query("insert into `".tb()."votes` (sid,uid,rate,created) values ('{$storyid}','{$client['id']}','$rate',".time().")");
			sql_query("update `".tb()."stories` set digg='{$story['digg']}',dugg='{$story['dugg']}',rating='$ratings' where id='{$storyid}'");
		}
		else {
			die('already voted');
		}
		echo $story['dugg'];
		ss_update();
		exit;
	}
	
	function update_status() {
		global $client;
		if (!$client['id']) {
			die('need login');
		}
		$q = "INSERT INTO `".tb()."acts` (uid,act,created,title) VALUES({$client['id']},'status',".time().",'{$_POST['statusinput']}')";
		if(sql_query($q)) {
			echo '{"status":"'.$_POST['statusinput'].'"}';
		}
		ss_update();
	}

	function comments() {
		global $num_per_page;
		$storyid = $_POST['sid'];
		if (!is_numeric($storyid)) die('bad sid');
		$res = sql_query("select * from ".tb()."stories where id='$storyid' ");
		$story = sql_fetch_array($res);
		if (!$story['id']) die('wrong sid');

		$i = 0;
		$total = $story['comments'];
		$comments_head = '<div id="comments_head"></div>';
		$res = sql_query("select c.*,u.avatar,u.signature,u.username from `".tb()."story_comments` as c left join `".tb()."accounts` as u on u.id=c.uid where c.sid='$storyid' ORDER BY c.id DESC LIMIT 0,$num_per_page");
		while ($row = sql_fetch_array($res)) {
			// list in ul
			//$row['num'] = $offset + $i;
			$comments .= user_post($row);
			$i++;
		}
		if ($i == 0) {
			$comments .= '<i>None</i><br /><br />';
		}
		else {
			$num = '1 ~ '.$i;
		}
		$comments .= '<div id="comments_foot"></div>';
		echo $comments_head.$num.$comments;
		ss_update();
		exit;
	}

	function comments_more() {
		global $num_per_page;
		$page = $_POST['apage'];
		$offset = ($page-1)*$num_per_page;
		$storyid = $_POST['sid'];
		if (!is_numeric($storyid)) die('bad sid');
		$res = sql_query("select * from ".tb()."stories where id='$storyid' ");
		$story = sql_fetch_array($res);
		if (!$story['id']) die('wrong sid');

		$i = $offset;
		$total = $story['comments'];
		$comments = '<div id="comments_head"></div>';
		$res = sql_query("select c.*,u.avatar,u.signature,u.username from `".tb()."story_comments` as c left join `".tb()."accounts` as u on u.id=c.uid where c.sid='$storyid' ORDER BY c.id DESC LIMIT $offset,$num_per_page");
		while ($row = sql_fetch_array($res)) {
			// list in ul
			//$row['num'] = $offset + $i;
			$comments .= user_post($row);
			$i++;
		}
		echo ($offset+1).' ~ '.$i.$comments;
		exit;
	}

	function comment_add() {
		GLOBAL $db,$client,$ubase;
		if (!$client['id']) {
			die('need login');
		}
		$timeline = time();
		$res = sql_query("select * from ".tb()."stories where id='{$_POST['sid']}' ");
		$story = sql_fetch_array($res);
		if (!$story['id']) {
			die('wrong sid');
		}
		if ($story['closed']) {
			die('topic closed');
		}
		if (strlen($_POST['form_content']) < 2) {
			die(t('Your message is too short'));
		}
		if ($res = sql_query("insert into `".tb()."story_comments` (sid,content,uid,cid,created,app,target_uid,vote) VALUES ('".$_POST['sid']."','".$_POST['form_content']."','{$client['id']}','{$story['cid']}',$timeline,'{$story['app']}','{$story['uid']}','{$_POST['vote']}')")) {
			sql_query("update `".tb()."stories` set comments=comments+1,lastreply=".time().",lastreplyuname='{$client['uname']}',lastreplyuid={$client['id']} where id='".$_POST['sid']."'");
		}
		$row = array(
			'avatar'=>$client['avatar'],
			'content' => nl2br(htmlspecialchars($_POST['form_content'])),
			'username' => $client['username'],
			'created' => time()
		);
		echo user_post($row);
		exit;
	}

	function favoriteadd() {
		global $client;
		if (!$client['id']) {
			die('need login');
		}
		$storyid = $_POST['sid'];
		$res = sql_query("select * from `".tb()."stories` where id='$storyid' ");
		$story = sql_fetch_array($res);
		if (!$story['id']) die('wrong sid');
		$res = sql_query("select * from ".tb()."favorites where fsid='$storyid' and uid='{$client['id']}'");
		$row = sql_fetch_array($res);
		if ($row['id']) die('Found in listings');
		$favorite = array(
			'uid' => $client['id'],
			'fuid' => $story['uid'],
			'fapp' => $story['app'],
			'fsid' => $storyid,
			'title' => addslashes($story['title']),
			'created' => time()
			);
		sql_insert($favorite, tb().'favorites');
		echo 'Saved';
		exit;
	}

	function stream_publish() {
		global $client, $config;
		if (!$client['id']) die('please login first');
		limit_posting(0,1);
			if (strlen($_POST['message'])<4) die('failed! message too short');
			$_POST['message'] = utf8_substr($_POST['message'],200);
			$_POST['message'] = parseurl($_POST['message']);
			$url_search = array(            
				"/\[url]www.([^'\"]*)\[\/url]/iU",
				"/\[url]([^'\"]*)\[\/url]/iU",
				"/\[url=www.([^'\"\s]*)](.*)\[\/url]/iU",
				"/\[url=([^'\"\s]*)](.*)\[\/url]/iU",
			);
			$url_replace = array(
				"<a href=\"http://www.\\1\" target=\"_blank\" rel=\"nofollow\">www.\\1</a>",
				"<a href=\"\\1\" target=\"_blank\" rel=\"nofollow\">\\1</a>",
				"<a href=\"http://www.\\1\" target=\"_blank\" rel=\"nofollow\">\\2</a>",
				"<a href=\"\\1\" target=\"_blank\" rel=\"nofollow\">\\2</a>"
				);
			$stream_id = stream_publish(preg_replace($url_search,$url_replace, h($_POST['message']) ),$attachment,$app,$client['id'],$_POST['page_id']);
			$arr = array(
				'id'=>$stream_id,'fullname'=>$client['fullname'],'avatar'=>$client['avatar'],'message'=>decode_bb(h(stripslashes($_POST['message']))),'attachment'=>$attachment,'username'=>$client['uname'],'created'=>time()
				);
			echo stream_display($arr,'',1);
			ss_update();
		
		exit;
	}

	function stream_delete($storyid) {
		global $client, $config;
		if (!$client['id']) die('please login first');
		$res = sql_query("select * from ".tb()."streams where id='$storyid'");
		$stream = sql_fetch_array($res);
		if (!$stream['id']) die('wrong sid');
		if ($stream['uid'] == $client['id'] || in_array('3',$client['roles']) ) {
			sql_query("update ".tb()."streams set hide=1 where id='{$stream['id']}'");
			//status photo
			$res = sql_query("select * from ".tb()."status_photos where sid='$storyid'");
			$photo = sql_fetch_array($res);
			if (strlen($photo['uri'])) {
				@unlink($photo['uri']);
				@unlink($photo['thumb']);
				sql_query("delete from ".tb()."status_photos where sid='$storyid'"); 
			}
			echo 'ok';
		}
		else {
			echo 'access denied';
		}
		exit;
	}

	function comment_publish() {
		global $client, $config;
		if (!$client['id']) die('<div class="ferror">please login first</div>');
		if ( !preg_match("/^[0-9a-z]+$/i",$_POST['target_id']) ) die('no target id');
		if (strlen($_POST['message'])<4) die('<div class="ferror">failed! message too short</div>');
		limit_posting(0,1);
		$_POST['message'] = utf8_substr($_POST['message'],160);
		
		$message = parseurl(h($_POST['message']));
		if ($_POST['img_token']) {
			$res = sql_query("select * from jcow_story_photos where sid=0 and des='{$_POST['img_token']}'");
			$photo = sql_fetch_array($res);
			if ($photo['id']) {
				sql_query("delete from jcow_story_photos where sid=0 and des='{$_POST['img_token']}'");
				$message .= '<div class="comment_img"><img src="'.$photo['thumb'].'" /></div>';
			}
		}
		if ($cid = comment_publish($_POST['target_id'],$message
			)) {
			$arr = array(
				'id'=>$cid);
			echo comment_display($arr);
		}
		exit;
	}

	function stream_hot_btn() {
		if (!allow_access(3)) {
			die("access denied");
		}
		$sid = $_POST['sid'];
		if (!is_numeric($sid)) die('no sid');
		$res = sql_query("select * from jcow_streams where id='$sid'");
		$stream = sql_fetch_array($res);
		if ($stream['type'] == 0) {
			sql_query("update jcow_streams set type=2 where id='$sid'");
		}
		else {
			sql_query("update jcow_streams set type=0 where id='$sid'");
			}
		die('<span style="color:green"><i class="fa fa-check"></i> done</span>'); 
	}
	function story_feature_btn() {
		if (!allow_access(3)) {
			die("access denied");
		}
		$sid = $_POST['sid'];
		if (!is_numeric($sid)) die('no sid');
		$res = sql_query("select * from jcow_stories where id='$sid'");
		$story = sql_fetch_array($res);
		if ($story['featured']) {
			sql_query("update jcow_stories set featured=0 where id='$sid'");
		}
		else {
			sql_query("update jcow_stories set featured=1 where id='$sid'");
		}
		die('<span style="color:green"><i class="fa fa-check"></i> done</span>'); 
	}
	function dolike() {
		global $client, $config;
		if (!$client['id']) die('<div class="ferror">please login first</div>');
		if ($client['disabled']) die('<span class="ferror">'.t('Your account pending approval').'</span>');
		if ( !preg_match("/^[0-9a-z]+$/i",$_POST['target_id']) ) die('no target id');
		if (!is_numeric($_POST['target_id'])) {
			die('wrong target id');
		}
		limit_posting(0,1);
		$res = sql_query("select * from ".tb()."streams where id='{$_POST['target_id']}'");
		$stream = sql_fetch_array($res);
		if ($stream['id']) {
			$res = sql_query("select * from ".tb()."liked where stream_id='{$_POST['target_id']}' and uid='{$client['id']}' limit 1");
			if (sql_counts($res)) { //undo like
				sql_query("delete from ".tb()."liked where stream_id='{$_POST['target_id']}' and uid='{$client['id']}'");
				sql_query("update ".tb()."streams set likes=likes-1 where id='{$_POST['target_id']}'");
				if ($stream['aid'] && $stream['uid'] != $client['id']) {
					sql_query("update jcow_stories set digg=digg-1 where stream_id='{$stream['id']}'");
				}
				echo t('Unliked').'<br />';
			}
			else { // do like
				$like = array('uid'=>$client['id'],'stream_id'=>$_POST['target_id']);
				sql_insert($like,tb().'liked');
				sql_query("update ".tb()."streams set likes=likes+1 where id='{$_POST['target_id']}'");
				if (in_array($stream['app'],array('blogs','videos','images','events'))) {
					$res = sql_query("select title from ".tb()."stories where id='{$stream['aid']}'");
					$story = sql_fetch_array($res);
					$stream['message'] = $story['title'];
				}
				if ($stream['aid'] && $stream['uid'] != $client['id']) {
					sql_query("update jcow_stories set digg=digg+1 where stream_id='{$stream['id']}'");
				}
				send_note($stream['uid'],'<i class="fa fa-thumbs-o-up"></i> '.t('{1} liked your post',h($client['fullname'])).': '.url('feed/view/'.$stream['id'],safe($stream['message'])),$client['id']);
				echo t('Liked').'<br />';
			}
		}
		else {
			die('stream not found');
		}
		exit;
	}

	function dodislike() {
		global $client, $config;
		if (!$client['id']) die('<div class="ferror">please login first</div>');
		if ( !preg_match("/^[0-9a-z]+$/i",$_POST['target_id']) ) die('no target id');
		if (!is_numeric($_POST['target_id'])) {
			die('wrong target id');
		}
		limit_posting(0,1);
		$res = sql_query("select * from ".tb()."disliked where stream_id='{$_POST['target_id']}' and uid='{$client['id']}' limit 1");
		if (sql_counts($res)) {
			sql_query("delete from ".tb()."disliked where stream_id='{$_POST['target_id']}' and uid='{$client['id']}'");
			sql_query("update ".tb()."streams set dislikes=dislikes-1 where id='{$_POST['target_id']}'");
			echo t('Undisliked').'<br />';
		}
		else {
			$like = array('uid'=>$client['id'],'stream_id'=>$_POST['target_id']);
			sql_insert($like,tb().'disliked');
			sql_query("update ".tb()."streams set dislikes=dislikes+1 where id='{$_POST['target_id']}'");
			echo t('Disliked').'<br />';
		}

		exit;
	}

	function wholike($stream_id=0,$offset=0) {
		if (!is_numeric($stream_id)) die();
		if (!$offset) $offset = 0;
		$num = 8;
		$res = sql_query("select u.* from ".tb()."liked as l left join ".tb()."accounts as u on u.id=l.uid where stream_id='$stream_id' limit $offset,".($num+1));
		$i=1;
		while ($user = sql_fetch_array($res)) {
			if ($i>$num) {
				$got_more = 1;
			}
			else {
				$users[] = $user;
			}
			$i++;
		}
		echo $this->display_users($users);
		if ($got_more) {
			$offset = $offset + $num;
			echo '<hr />
			<a href="#" onclick="jQuery.facebox({ ajax: \''.url('jquery/wholike/'.$stream_id.'/'.$offset).'\' });return false;" >'.t('More..').'</a>';
		}
		exit;
	}
	
	function out_share($story_id) {
		if (!is_numeric($story_id)) die('bad id');
		$res = sql_query("select * from ".tb()."stories where id='$story_id'");
		$story = sql_fetch_array($res);
		if (!$story['id']) die('wrong id');
		$url = url($story['app'].'/viewstory/'.$story['id']);
		$encoded_url = urlencode($url);
		if (!$_SESSION['mobile_detected']) {
			$show_url = '<input type="text" style="font-size:15px;width:500px;height:30px" value="'.$url.'" onclick="$(this).select();" />';
		}
		echo '
		<strong>Share:</strong><br />
		'.$show_url.'
		<div style="margin-top:10px">
		<a href=\'http://twitter.com/home?status='.$encoded_url.'\' target=\'_blank\'><img src=\'files/common_css/twitter-button.png\' width="32" height="32"/></a>
					<a href=\'http://www.facebook.com/sharer.php?u='.$encoded_url.'\' target=\'_blank\'><img src=\'files/common_css/facebook-button.png\'  width="32" height="32"/></a>
					<a href=\'http://plus.google.com/share?url='.$encoded_url.'\' target=\'_blank\'><img src=\'files/common_css/gplus-button.png\'  width="32" height="32"/></a>';
		if ($story['app'] == 'images') {
			echo '<a href=\'http://pinterest.com/pin/create/button/?url='.$encoded_url.'&media='.$encoded_media.'\' target=\'_blank\'><img src=\'files/common_css/pinterest-button.png\'  width="32" height="32"/></a>';
		}
		echo '</div>';
		exit;
	}
	
	function whodislike($stream_id=0,$offset=0) {
		if (!is_numeric($stream_id)) die();
		if (!$offset) $offset = 0;
		$num = 8;
		$res = sql_query("select u.* from ".tb()."disliked as l left join ".tb()."accounts as u on u.id=l.uid where stream_id='$stream_id' limit $offset,".($num+1));
		$i=1;
		while ($user = sql_fetch_array($res)) {
			if ($i>$num) {
				$got_more = 1;
			}
			else {
				$users[] = $user;
			}
			$i++;
		}
		echo $this->display_users($users);
		if ($got_more) {
			$offset = $offset + $num;
			echo '<hr />
			<a href="#" onclick="jQuery.facebox({ ajax: \''.url('jquery/whodislike/'.$stream_id.'/'.$offset).'\' });return false;" rel="facebox">'.t('More..').'</a>';
		}
		exit;
	}

	function deleteuser($uid=0,$pin='') {
		global $client;
		if (!allow_access(3)) die('access denied');
		if (!is_numeric($uid)) die('bad uid');
		$res = sql_query("select * from ".tb()."accounts where id='$uid'");
		$user = sql_fetch_array($res);
		if (!$user['id']) {
			echo 'User does not exist, or has been deleted';
			exit;
		}
		$user['roles'] = explode('|',$user['roles']);
		if (is_array($user['roles']) && in_array(3, $user['roles'])) {
			echo 'This user is in Admin role, and you can not delete an admin.';
			exit;
		}
		$lpin = substr($client['password'],-5,5);
		$arr = explode('_',$pin);
		$pin = $arr[0];
		$type = $arr[1];
		if (!strlen($pin)) {
			echo '
			<strong>'.h($user['fullname']).' ('.$user['username'].', '.$user['email'].')</strong>
			ip: '.$user['ipaddress'].'<br />
			register: '.get_date($user['created']).'<br />
			If you delete this user, all his/her posts,pages,groups will be deleted!<br />
			Are you sure to delete this user ?
			<a href="#" onclick="jQuery.facebox({ ajax: \''.url('jquery/deleteuser/'.$uid.'/'.$lpin).'\' });return false;" style="display:block;padding:5px;">Yes Delete</a> 
			<a href="#" onclick="jQuery.facebox({ ajax: \''.url('jquery/deleteuser/'.$uid.'/'.$lpin.'_spam').'\' });return false;" style="display:block;padding:5px;">Yes Delete and mark as Spammer</a>';
		}
		else {
			if ($pin != $lpin) die('pin not match');
			jcow_deleteuser($uid);
			if ($type == 'spam') {
				sql_query("insert ignore into ".tb()."spammers (userkey,type,created) values('{$user['ipaddress']}','ip',".time().")");
				sql_query("insert ignore into ".tb()."spammers (userkey,type,created) values('{$user['email']}','email',".time().")");
			}
			echo '<script>
			$(".userbox_'.$user['id'].'").hide("fast");
			</script>';
			echo 'deleted';
		}
		exit;
	}

	function allcomments($stream_id=0,$offset=0) {
		if (!is_numeric($stream_id)) die();
		if (!$offset) $offset = 0;
		$num = 12;
		$more = $num+1;
		$res = sql_query("select c.*,u.username,u.avatar from ".tb()."comments as c left join ".tb()."accounts as u on u.id=c.uid where c.stream_id='{$stream_id}' order by id desc limit $offset,".$more);
		$i=1;
		while($row = sql_fetch_array($res)) {
			if ($i <=$num) {
				$comments .= comment_display($row);
			}
			$i++;
		}
		echo $comments;
		if ($i>$num) {
			$offset = $offset + $num;
			echo '<hr />
			<a href="#" onclick="jQuery.facebox({ ajax: \''.url('jquery/allcomments/'.$stream_id.'/'.$offset).'\' });return false;" >'.t('More..').'</a>';
		}
		exit;
	}

	private function display_users($users) {
		if (is_array($users)) {
			foreach ($users as $user) {
				if (!strlen($user['fullname'])) $user['fullname'] = $user['username'];
				$output .= '<li>'.avatar($user).'<br />'.url('u/'.$user['username'],h($user['fullname'])).'</li>';
			}
		}
		return '
		<ul class="small_avatars">'.$output.'</ul>';
	}

	function profile_comment_publish() {
		global $client, $config;
		if (!$client['id']) die('please login first');
		limit_posting(0,1);
		if (!is_numeric($_POST['target_id'])) die('no target id');
		$user = valid_user($_POST['target_id']);
		if (!$user['id']) die('bad uid');
		if (strlen($_POST['message'])<4) die('failed! message too short');
		$id = profile_comment_publish($_POST['target_id'],$_POST['message']);
		$msg = 'Hi<br /><strong>'.$client['uname'].'</strong> left a comment to your profile.<br />
		Click '.url('u/'.$user['username'],'HERE').' to view the comment.';
		//jcow_mail($user['email'],$client['uname'].' left a comment to your profile',$msg);
		send_note($user['id'],t('{1} left a comment to {2}',name2profile($client['uname']),url('u/'.$user['username'],t('your profile'))) );
		mail_notice('wall_post',$user['username'],t('{1} left a comment to your profile',$client['username']),t('{1} left a comment to your profile',$client['username']) );
		// write act
		$attachment = array(
			'url' => url('u/'.$user['username']),
			'des' => utf8_substr($_POST['message'],180)
			);
		$app = array('name'=>'pcomment','id'=>$id);
		$act = t('{1} left a comment to {2}','',name2profile($user['username']) );
		$stream_id = stream_publish($act,$attachment,$app);
		sql_query("update ".tb()."profile_comments set stream_id='$stream_id' where id='$id'");

		$arr = array(
			'id'=>$id,'avatar'=>$client['avatar'],'message'=>stripslashes($_POST['message']),'username'=>$client['uname'],'created'=>time(),'stream_id'=>$stream_id
			);
		echo profile_comment_display($arr,1);
		ss_update();
		exit;
	}

	function morestream() {
		echo stream_get($_POST['page_id'],$_POST['num'],$_POST['offset'],$_POST['target_id'],$_POST['stream_app'],1,array('direct_output'=>1));
		exit;
	}
	function moreactivities() {
		if (preg_match("/_/",$_POST['uid'])) {
			$uid = explode('_',$_POST['uid']);
		}
		else
			$uid = $_POST['uid'];
		echo activity_get($uid,7,$_POST['offset'],$_POST['target_id'],'','',array('direct_output'=>1));
		exit;
	}

	function morelikestream() {
		echo stream_get($_POST['uid'],10,$_POST['offset']);
		exit;
	}

	function moreprofilecomment() {
		echo profile_comment_get($_POST['uid'],5,$_POST['offset']);
		exit;
	}
	

	function translate() {
		global $client;
		if (get_gvar('is_demo') || !allow_access(3)) die('<font color="red">access denied</font>');
		sql_query("update `".tb()."langs` set lang_to='{$_POST['tto']}' where lang_from='{$_POST['tfrom']}' and lang='{$client['lang']}' ");
		ss_update();
		echo 'saved';
		exit;
	}

	function jcow_update_new() {
		global $client;
		if (!$client['id']) die();
		$res = sql_query("select count(*) as num from `".tb()."im` where to_id='{$client['id']}' and !hasread");
		$row = sql_fetch_array($res);
		$msg_new = $row['num'] ? $row['num'] : '';

		$res = sql_query("select count(*) as num from `".tb()."notes` where uid='{$client['id']}' and !hasread");
		$row = sql_fetch_array($res);
		$note_new = $row['num'] ? $row['num'] : '';

		$res = sql_query("select count(*) as num from `".tb()."friend_reqs` where fid='{$client['id']}'");
		$row = sql_fetch_array($res);
		$frd_new = $row['num'] ? $row['num'] : '';
		if ($row['num']) {
			$frd_link = url('friends/requests');
		}
		else {
			$frd_link = url('friends');
		}

		echo '{
		  "msg_new": "'.$msg_new.'",
		  "note_new": "'.$note_new.'",
		  "frd_new": "'.$frd_new.'",
		  "frd_link": "'.$frd_link.'"
		}
		';
		exit;
	}

}

function valid_youtube_id($id) {
	if (!$data = @file_get_contents("http://gdata.youtube.com/feeds/api/videos/".$id)) {
		return false;
	}
	else {
		if (!preg_match("/xml/i",$data)) {
			return false;
		}
	}
	return true;
}