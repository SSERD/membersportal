<?php
/* ############################################################ *\
 ----------------------------------------------------------------
Jcow Software (http://www.jcow.net)
IS NOT FREE SOFTWARE
http://www.jcow.net/commercial_license
Copyright (C) 2009 - 2010 jcow.net.  All Rights Reserved.
 ----------------------------------------------------------------
\* ############################################################ */

class admin {
	function __construct() {
		do_auth(3);
		clear_as();
		clear_report();
		set_title('Admin Panel');
		global $nav, $config, $menuon;
		$menuon = 'admin';
		$config['hide_ad'] = 1;
		$nav[] = url('admin',t('Admin Panel'));

	}
	function modules() {
		global  $nav;
		$nav[] = url('admin/modules',t('Modules'));
		include "modules/admin/inc/modules.php";
	}
	function menu($step,$id) {
		global $nav;
		$nav[] = url('admin/menu',t('Menu'));
		include "modules/admin/inc/menu.php";
	}
	function blocks($step) {
		global $nav;
		$nav[] = url('admin/blocks',t('Blocks'));
		include "modules/admin/inc/blocks.php";
	}
	function update($step) {
		global $nav;
		include "modules/admin/inc/update.php";
	}
	function footer_pages($step,$id) {
		global $nav;
		include "modules/admin/inc/footer_pages.php";
	}

	function blacklist($step,$id) {
		global $nav;
		$nav[] = url('admin/blacklist',t('Blacklist'));
		include "modules/admin/inc/blacklist.php";
	}
	function stories($step,$id) {
		global $nav;
		$nav[] = url('admin/stories','Stories');
		include "includes/libs/admin_stories.module.php";
	}
	function memberqueue($step,$id) {
		global $nav;
		$nav[] = url('admin/memberqueue',t('Member Queue'));
		include "includes/libs/admin_memberqueue.module.php";
	}

	function pending_posts($step,$id) {
		global $nav;
		$nav[] = url('admin/pending_posts','Posts Queue');
		include "includes/libs/admin_pending_posts.module.php";
	}
	function cache($step,$id) {
		global $nav;
		$nav[] = url('admin/cache',t('Cache controller'));
		include "includes/libs/admin_cache.module.php";
	}
	function themes($step) {
		global $menuon, $tab_menu, $nav, $config;
		set_title('Theme');
		$nav[] = url('admin/themes','Themes');
		include "modules/admin/inc/themes.php";
	}
	function members_quick($step) {
		global $menuon, $tab_menu, $nav, $config;
		set_title('Member quick management');
		$nav[] = url('admin/members_quick','Member quick management');
		include "modules/admin/inc/members_quick.php";
	}
	function stream_monitor($step) {
		global $menuon, $tab_menu, $nav, $config;
		set_title('Stream monitor');
		$nav[] = url('admin/stream_monitor','Stream monitor');
		include "modules/admin/inc/stream_monitor.php";
	}
	function permissions($step) {
		global $menuon, $tab_menu, $nav, $config, $menu_items;
		set_title('Permissions');
		$nav[] = url('admin/permissions','Permissions');
		include "modules/admin/inc/permissions.php";
	}

	function featurepages($step='',$id=0) {
		global $menuon, $tab_menu, $nav, $config, $menu_items;
		set_title('Suggestions');
		$nav[] = url('admin/featurepages','Suggestions');
		include "includes/libs/admin_featurepages.module.php";
	}
	function featured_pages($step='',$id=0) {
		global $menuon, $tab_menu, $nav, $config, $menu_items;
		set_title('Featured page Settings');
		$nav[] = url('admin/featured_pages','Featured page Settings');
		include "includes/libs/admin_featured_settings.module.php";
	}
 	function page_categories($step='',$id=0) {
		global $menuon, $tab_menu, $nav, $config, $menu_items;
		set_title('Page categories');
		$nav[] = url('admin/page_categories','Page categories');
		include "includes/libs/admin_page_categories.module.php";
	}

	function index() {
		global $version, $client, $optional_apps, $config, $current_modules, $admin_menu;
		nav('Index');
		

		$s = str_replace('http://','',uhome());
		$res = sql_query("select count(*) as num from ".tb()."accounts");
		$member = sql_fetch_array($res);

		if (!check_license()) {
			$upgradeurl = ' (<strong><a href="http://www.jcow.net/download/">Upgrade</a></strong>)';
		}


		section_content('
			<IFRAME SRC="https://www.jcow.net/news.php?s='.urlencode($s).'&e='.urlencode($client['email']).'fn='.urlencode($client['fullname']).'&m='.$member['num'].'&v='.jversion().'&l='.jedition.'" TITLE="Jcow News" WIDTH="580" HEIGHT="180" scrolling="no">
<a href="http://www.jcow.net">Visit Jcow for News&Updates</a><br />
</IFRAME>
		');
		section_close('Jcow Updates & Security notes');

		
		$res = sql_query("select count(*) as num from ".tb()."accounts");
		$row = sql_fetch_array($res);
		$num_users = $row['num'];
		
		$res = sql_query("select count(*) as num from ".tb()."friends");
		$row = sql_fetch_array($res);
		$num_friendships = $row['num'];
		
		$res = sql_query("select count(*) as num from ".tb()."followers");
		$row = sql_fetch_array($res);
		$num_connections = $row['num']+$num_friendships;
		
		$res = sql_query("select count(*) as num from ".tb()."streams");
		$row = sql_fetch_array($res);
		$num_streams = $row['num'];
		
		$res = sql_query("select count(*) as num from ".tb()."stories");
		$row = sql_fetch_array($res);
		$num_stories = $row['num'];
		
		$res = sql_query("select count(*) as num from ".tb()."pages where type='page'");
		$row = sql_fetch_array($res);
		$num_pages = $row['num'];

		$res = sql_query("select count(*) as num from ".tb()."accounts " );
		$row = sql_fetch_array($res);
		$members = $row['num'];
		$res = sql_query("select count(*) as num from ".tb()."accounts where disabled=0" );
		$row = sql_fetch_array($res);
		$verifed_members = $row['num'];
		$res = sql_query("select count(*) as total from `".tb()."reports`");
		$row = sql_fetch_array($res);
		$reports_all = $row['total'];
		$res = sql_query("select count(*) as total from `".tb()."reports` where hasread=0");
		$row = sql_fetch_array($res);
		$reports_unread = $row['total'];

		$timeline = time()-3600*24*3;
		$res = sql_query("select count(*) as num from ".tb()."accounts where created>$timeline and disabled=1 " );
		$row = sql_fetch_array($res);
		$memberqueue = $row['num'];
		
		if (!module_actived('pages')) {
			$fan_page_status = ' <span style="font-size:10px;color:red">(Not enabled)</span>';
		}
		if (check_license()) {
			$jedition = 'Pro';
		}
		else {
			$jedition = jedition.' <a href="http://www.jcow.net" target="_blank" style="font-weight:bold;color:red">Upgrade to Pro</a>';
		}
		if (jedition == 'CE') {
			$vnotes = 'CE.
			<div style="color:red">You can have up to <strong>5</strong> members in Community Edition.  <a href="http://www.jcow.net/download" target="_blank" style="font-weight:bold;color:red"> | Upgrade to Pro for unlimited members</a></div>';
		}
		elseif (jedition == 'trial') {
			$vnotes = ' Trial';
		}
		section_content('
		<ul>
		<li>You have <strong>'.$members.'</strong> Members, 
		<strong>'.$num_friendships.'</strong> Friendships, 
		<strong>'.$num_connections.'</strong> Connections, 
		<strong>'.$num_streams.'</strong> Streams, 
		<strong>'.$num_stories.'</strong> Stories, 
		<strong>'.$num_pages.'</strong> Fan Pages'.$fan_page_status.'</li>');
		if (check_license()) {
			c('
			<li>
			<a href="'.url('admin/members_quick').'">Un-verified member: <strong>'.$memberqueue.'</strong> (recent 3 days)</a>
			</li>');
		}
		
		c('
		<li>
		<a href="'.url('admin/reports').'">Member Reports (<strong>'.$reports_unread.'</strong>/'.$reports_all.')</a>
		</li>

		<li>Your Jcow Version: <strong>'.jversion().'</strong> '.$vnotes.'</li>
		</ul>
		<style>
		.ai_items a {
			color:#0084B4;
		}
		</style>');
		if (jedition == 'trial') {
			$tool_disabled = ' tool_disabled';
			c('<div style="padding:10px;background:#FEFFE3;color:#333">Grayed tools are not available in Trial Version. <br />
				<a href="http://www.jcow.net/download">Upgrade to Full version</a> and get all tools running.</div>');
		}
		section_content('<div>

		<div class="ai_items">'
		.url('admin/config','<i class="fa fa-cogs fa-4x"></i><br />'.t('Configuration')).'</div>

		<div class="ai_items'.$tool_disabled.'">'
		.url('admin/modules','<i class="fa fa-puzzle-piece fa-4x"></i><br />'.t('Modules')).'</div>

		<div class="ai_items'.$tool_disabled.'">'
		.url('admin/featurepages','<i class="fa fa-files-o fa-4x"></i><br />Suggestions').'</div>
	
		
		<div class="ai_items">'
		.url('admin/menu','<i class="fa fa-list-alt fa-4x"></i><br />'.t('Menu')).'</div>
				
		<div class="ai_items">'
		.url('admin/themes','<i class="fa fa-cubes fa-4x"></i><br />'.t('themes')).'</div>
		
		<div class="ai_items">'
		.url('admin/members_quick','<i class="fa fa-users fa-4x"></i><br />'.t('Members')).'</div>

		<div class="ai_items">'
		.url('admin/customfields','<i class="fa fa-user fa-4x"></i><br />'.t('Member fields')).'</div>

		<div class="ai_items">'
		.url('admin/userroles','<i class="fa fa-user fa-4x"></i><br />'.t('Member roles')).'</div>

		<div class="ai_items">'
		.url('admin/permissions','<i class="fa fa-user-secret fa-4x"></i><br />'.t('Permissions')).'</div>
		
	

		<div class="ai_items">'
		.url('admin/texts','<i class="fa fa-file-text-o fa-4x"></i><br />'.t('Text')).'</div>
		
		'.$ads_link.'

		<div class="ai_items">'
		.url('admin/translate','<i class="fa fa-language fa-4x"></i><br />'.t('Translation')).'</div>


	
		<div class="ai_items">'
		.url('admin/stream_monitor','<i class="fa fa-eye fa-4x"></i><br />'.t('Stream monitor')).'</div>

		<div class="ai_items'.$tool_disabled.'">'
		.url('admin/cache','<i class="fa fa-space-shuttle fa-4x"></i><br />'.t('Cache controller')).'</div>

		<div class="ai_items'.$tool_disabled.'">'
		.url('admin/ad_blocks','<i class="fa fa-dollar fa-4x"></i><br />'.t('Ad blocks')).'</div>
		
		
		<style>
		.ai_items a {
			display:block;
			width:120px;
			height: 120px;
			padding:20px 10px;
			overflow: hidden;
			float: left;
			text-align: center;
			text-decoration:none;
		}
		.ai_items a:hover {		
			background:#eeeeee;
		}
		.tool_disabled a{
			color:#cccccc;
			cursor:not-allowed;
		}
		</style>
		<script>
		$(".tool_disabled").click(function() {
			return false;
		});
</script>');
		
		

		section_close('Admin Tools');
		
		c('<ul class="simple_list">');
		c('
		<li>'.url('admin/footer_pages', 'Footer Pages').' - Edit pages like "about us"</li>');
		if (is_array($admin_menu)) {
			foreach ($admin_menu as $item) {
				$arr = explode('/',$item['path']);
				$img = '';
				$icon = 'modules/'.$arr[0].'/icon.png';
				if (file_exists($icon)) {
					$img = '<img src="'.uhome().'/'.$icon.'" />';
				}
				section_content('<li>'.$img.' '.url($item['path'],$item['name']).'</li>');
			}
		}
		c('
		</ul>');
		section_close('Addon Tools');
	}

	function jsql() {
		nav(url('admin/jsql','Execute SQL Query') );
		if ($_POST['step'] == 2) {
			if (!$_POST['query']) {
				sys_notice('Empty query');
			}
			else {
				$query = stripslashes($_POST['query']);
				$query = remove_remarks($query);
				$pieces = split_sql_file($query, ";");
				if (is_array($pieces)) {
					foreach ($pieces as $piece) {
						$sql = trim($piece);
						if(!empty($sql) and $sql[0] != "#") {
							sql_query($sql);
						}
					}
				}
				sys_notice('Query executed!');
			}
		}
		c('
		<p>Be <strong>Very</strong> careful when using this tool!<br />
		Do not execute queries from unknown source.
		</p>
		<form method="post" action="'.url('admin/jsql').'">
		<textarea name="query" rows="10" cols="50"></textarea><br />
		<input type="hidden" name="step" value="2" />
		<input type="submit" value="Execute!" />
		</form>');
	}

	function clear_spammer_posts($clear=0) {
		nav('Clear spammer posts');
		if ($clear) {
			$res = sql_query("select * from ".tb()."accounts where disabled=3 and blurbs!='cleared'");
			while ($spammer = sql_fetch_array($res)) {
				$uid = $spammer['id'];
				sql_query("delete from ".tb()."streams where uid='$uid'");
				$res2 = sql_query("select * from ".tb()."stories where uid='$uid'");
				while ($row2 = sql_fetch_array($res2)) {
					$res3 = sql_query("select * from ".tb()."story_photos where sid='{$row2['id']}'");
					while($photo = sql_fetch_array($res3)) {
						@unlink($photo['uri']);
						sql_query("delete from ".tb()."story_photos where id='{$photo['id']}'");
					}
					sql_query("delete from ".tb()."stories where id='{$row2['id']}' and uid='$uid'");
				}
				sql_query("delete from ".tb()."messages where from_id='{$uid}'");
				sql_query("delete from ".tb()."pages where uid='{$uid}'");
				sql_query("update ".tb()."accounts set blurbs='cleared' where id='$uid'");
			}
			c('all spammer posts cleared');
		}
		else {
			$res = sql_query("select count(*) as num from ".tb()."accounts where disabled=3 and blurbs!='cleared'");
			$row = sql_fetch_array($res);
			if ($row['num']) {
				
				c('
					<p>There are <strong>'.$row['num'].'</strong> spammers that not cleared.</p>
					<p>
					If you click "clear now", all the posts from the spammers will be deleted and can not be revived.<br />
					<input type="button" value="clear now" onclick="window.location=\''.url('admin/clear_spammer_posts/1').'\'" />
					</p>');
			}
			else {
				c('all spammer posts cleared');
			}
		}
	}


	function app_disable($key) {
		set_gvar('app_'.$key, 0);
		redirect('admin',1);
	}
	function app_enable($key) {
		set_gvar('app_'.$key, 1);
		redirect('admin',1);
	}
	
	function reports() {
		nav(url('admin/reports',t('Reports') ));
		global $current_sub_menu, $apps, $story_apps,$offset, $ubase, $num_per_page, $page, $content; 
		set_title('Reports');
		sql_query("update ".tb()."reports set hasread=1");
		$res = sql_query("SELECT r.*,u.username FROM ".tb()."reports as r left join `".tb()."accounts` as u on u.id=r.uid ORDER by r.id DESC LIMIT $offset,$num_per_page ");
		c('<table class="stories" cellspacing="1">');
		section_content('<tr class="table_line1">
			<td>Reports</td>
			</tr>');
			while ($row = sql_fetch_array($res)) {
			c('<tr class="row1">
			<td>'.get_date($row['created']).', url: '.url($row['url'],$row['url']).'<br /> reported by '.url('u/'.$row['username'],$row['username']).': <span class="sub">'.h($row['message']).'</span></td>
			</tr>');
		}
		c('</table>');

		// pager
		$res = sql_query("select count(*) as total from `".tb()."reports`");
		$row = sql_fetch_array($res);
		$total = $row['total'];
		$pb       = new PageBar($total, $num_per_page, $page);
		$pb->paras = $ubase.'admin/reports';
		$pagebar  = $pb->whole_num_bar();
		c($pagebar);
	}

	
	
	function config() {
		nav(url('admin/config',t('Site configuration')) );
		if (jedition == 'CE') {
			$cd_note = ' <span style="color:red;font-size:13px">(Can not be changed in Community Edition)</span>';
			$cd_set = ' disabled ';
		}
		global $current_sub_menu, $config, $locations, $blocks, $client;
		if (!check_license()) {
			$rec_note = ' <span class="sub">(Not available in Jcow '.jedition.')</span>';
		}
		$current_sub_menu['href'] = url('admin/config');
		$question = get_text('community_question');
		if (!strlen($question)) {
			$question = 'Why you want to join this community?';
		}
		section_content('
			<form action="'.url('admin/configpost').'" method="post">
		<fieldset>
		<legend>'.t('General settings').'</legend>
		<p>
		'.label(t('Site name')).'
		<input type="text" name="site_name" value="'.get_gvar('site_name').'" />
		</p>
		
		<p>
		'.label(t('Site Slogan')).'
		<input type="text" size="70" name="site_slogan" value="'.get_gvar('site_slogan').'" />
		</p>

		<p>
		'.label(t('Site Keywords')).'
		<input type="text" size="70" name="site_keywords" value="'.get_gvar('site_keywords').'" /><br />
		<span class="sub">will be inserted to meta tag</span>
		</p>

		');

		section_content('<p> '.label(t('Webmaster email')).'<input type="text" 
		name="site_email" value="'.get_gvar('site_email').'" />
		<span class="sub">Important message will be sent to this address</span></p>');
		
		
	
		
		if (get_gvar('landing_page') == 'hotstream') {
			$landing_page_1 = 'selected';
		}
		else {
			$landing_page_0 = 'selected';
		}
		section_content('
			<p> '.label('Banner content').'
		<textarea name="jcow_banner" class="form-control" '.$cd_set.'>'.h(get_gvar('jcow_banner')).'</textarea>
		<span><strong>HTML Tag</strong> enabled.</span></p>
		<p> '.label('Footer Message'.$cd_note).'
		<textarea name="footermsg" class="form-control" '.$cd_set.'>'.h(get_text('footermsg')).'</textarea>
		<span>Displayed in the footer of your pages.</span></p>

		<p>'.label('Landing Page').'
		<select name="landing_page">
		<option value="signupform" '.$landing_page_0.' >Signup Form - Show welcome message and a sign up form</option>
		<option value="hotstream" '.$landing_page_1.' >Hot Streams - Show hot streams to guests</option>
		</select><br />
		<span>Note: If you set your site as "Private", this Landing Page setting will be ignored and will always show sign form.</span>
		</p>
		
		</fieldset>');
		if (get_gvar('friend_admin')) {
			$friend_admin = 'checked';
		}
		if (get_gvar('reg_confirm')) {
			$reg_confirm = 'checked';
		}
		if (get_gvar('private_network')) {
			$story_access_member = 'selected';
		}
		else {
			$story_access_all = 'selected';
		}
		if (get_gvar('hide_right_sidebar')) {
			$hide_right_sidebar = 'checked';
		}
		section_content('
		<fieldset>
		<legend>Privacy&Security</legend>
		<p>'.label('Network visiting').'
		<select name="private_network">
		<option value="0" '.$story_access_all.' >Public - everyone can visite this site</option>
		<option value="1" '.$story_access_member.' >Private - only members can visite this site</option>
		</select>
		</p>
		');
		
		if (get_gvar('signup_closed') == 2) {
			$signup_closed = 'selected';
		}
		elseif (get_gvar('signup_closed') == 1) {
			$signup_invite = 'selected';
		}
		else {
			$signup_closed_no = 'selected';
		}
		if (get_gvar('acc_verify') == 2) {
			$acc_verify_2 = 'selected';
		}
		elseif (get_gvar('acc_verify') == 1) {
			$acc_verify_1 = 'selected';
		}
		elseif (get_gvar('acc_verify') == 3) {
			$acc_verify_3 = 'selected';
		}
		else {
			$acc_verify_0 = 'selected';
		}
		if (get_gvar('verify_approval')) {
			$verify_approval = 'checked';
		}
		if (!$pending_post_limit = get_gvar('pending_post_limit'))
			$pending_post_limit = 10;
		if (!$reg_limit_ip = get_gvar('reg_limit_ip'))
			$reg_limit_ip = 2;
		if (get_gvar('email_c')) {
			$email_c = 'checked';
		}
		section_content('
		<p>
		'.label('Signing Up').'
		<select name="signup_closed">
		<option value="0" '.$signup_closed_no.' >Open - Free to Join</option>
		<option value="1" '.$signup_invite.' >Open - Invite Only</option>
		<option value="2" '.$signup_closed.' >Close - Not accepting new members</option>
		</select>
		</p>
		<p>
		'.label('New member verification').'
		<label for="verify_approval"><input id="verify_approval" type="checkbox" name="verify_approval" '.$verify_approval.' />Enable New member Verification.</label>'.$rec_note.'<br />
		<span>If enabled, new users can not post anything before you manually approved their membership.</span>
		</p>
		<p>
		'.label('Limit').'
		Limit <input type="text" size="2" name="reg_limit_ip" value="'.$reg_limit_ip.'" /> registrations per IP address.'.$rec_note.'<br />
		<span>Set to <strong>1</strong> for live site to stop spam. Set to <strong>999</strong> for local testing</span>
		</p>

		</fieldset>');


		section_content('<fieldset>
		<legend>Offline '.$cd_note.'</legend>');
		if (get_gvar('offline')) {
			$offline = 'checked';
		}
		section_content('
		<p>
		'.label('Website Offline').'
		<input type="checkbox" value="1" name="offline" '.$offline.' '.$cd_set.' /> '.t('Offline').'<br />
		'.t('Reason').':<br />
		<textarea name="offline_reason" class="form-control" '.$cd_set.'>'.get_gvar('offline_reason').'</textarea><br />
		'.t('Even if offline, you can still access admin area').'
		</p>
		</fieldset>');

		$miniblog_maximum = get_gvar('miniblog_maximum');
		if (!$miniblog_maximum) {
			$miniblog_maximum = 140;
		}
		if (!get_gvar('disable_bookmark')) {
			$enable_bookmark_checked = 'checked';
		}
		if (!get_gvar('disable_recaptcha_reg')) {
			$enable_recaptcha_reg_checked = 'checked';
		}
		if (!get_gvar('disable_recaptcha_login')) {
			$enable_recaptcha_login_checked = 'checked';
		}
		if (!get_gvar('disable_recaptcha_pm')) {
			$enable_recaptcha_pm_checked = 'checked';
		}
		if (!get_gvar('disable_recaptcha_req')) {
			$enable_recaptcha_req_checked = 'checked';
		}
		if (!get_gvar('disable_friendship')) {
			$enable_friendship_checked = 'checked';
		}
		if (!$jcow_hot_likes = get_gvar('jcow_hot_likes')) {
			$jcow_hot_likes = 2;
		}
		if (get_gvar('smtp_enabled')) {
			$smtp_enabled = 'checked';
		}
		$r_secret = get_gvar('recaptcha_secret');
		$smtp_username = get_gvar('smtp_username');
		$smtp_password = get_gvar('smtp_password');
		$oauth_google_secret = get_gvar('oauth_google_secret');
		if ($client['is_demo']) {
			$r_secret = '********************';
			$smtp_username = $smtp_password = '************';
			$oauth_google_secret = '**************';
		}
		section_content('
		<fieldset>
		<legend>Recaptcha'.$cd_note.'</legend>
		You can go to <a href="https://www.google.com/recaptcha" target="_blank">https://www.google.com/recaptcha</a> to get Recaptcha site key and secret key.
		<p>
		Recaptcha Site Key: <input type="text" name="recaptcha_key" size="50" value="'.h(get_gvar('recaptcha_key')).'" '.$cd_set.' />
		</p>
		<p>Recaptcha Secret Key : <input type="text" name="recaptcha_secret" size="50"  value="'.h($r_secret).'"'.$cd_set.'  /></p>

		</fieldset>

		<fieldset>
		<legend>Google MAP API</legend>
		Here you can get free Credentials keys: <a href="https://console.developers.google.com" target="_blank">https://console.developers.google.com</a>.
		<p>
		Google Credentials - Browser Key: <input type="text" name="google_browser_key" size="50" value="'.h(get_gvar('google_browser_key')).'"  />
		</p>
		</fieldset>
		<fieldset>
		<legend>Google Oauth ID'.$cd_note.'</legend>
		Here you can get free Oauth ID: <a href="https://console.developers.google.com" target="_blank">https://console.developers.google.com</a>.
		<p>
		Google Oauth client ID: <input type="text" name="oauth_google_id" size="50" value="'.h(get_gvar('oauth_google_id')).'" '.$cd_set.' />
		</p>
		<p>
		Google Oauth client secret: <input type="text" name="oauth_google_secret" size="50" value="'.h($oauth_google_secret).'" '.$cd_set.' />
		</p>
		</fieldset>

		<fieldset>
		<legend>SMTP'.$cd_note.'</legend>
		By default, Jcow send emails using the PHP function mail(). If you define and enable SMTP settings, all system notice Email will be sent via SMTP.
		<p>
		<label><input type="checkbox" name="smtp_enabled" value="1" '.$smtp_enabled.' '.$cd_set.' /> Enable SMTP</label>
		</p>
		<p>
		SMTP host: <input type="text" name="smtp_host" size="50" value="'.h(get_gvar('smtp_host')).'" '.$cd_set.' />
		</p>
		<p>SMTP port: <input type="text" name="smtp_port" size="50"  value="'.h(get_gvar('smtp_port')).'" '.$cd_set.' /></p>
		<p>SMTP username: <input type="text" name="smtp_username" size="50"  value="'.h($smtp_username).'" '.$cd_set.' /></p>
		<p>SMTP password: <input type="text" name="smtp_password" size="50"  value="'.h($smtp_password).'" '.$cd_set.' /></p>
		</fieldset>


		<fieldset>
		<legend>Others</legend>
		<p>
		Maximum length of mini-blog: <input type="text" size="5" name="miniblog_maximum" value="'.$miniblog_maximum.'" /> characters
		</p>
		<p>
		<input type="checkbox" value="1" name="enable_friendship" '.$enable_friendship_checked.' />Enable Friendship.
		</p>
		<input type="hidden" name="disable_recaptcha_req" value="1" />
		</fieldset>');
		
		
		
		section_content('<p>
		<input type="submit" value="'.t('Save changes').'" />
		</p>
		</form>
		');
	}
	function setlicensepost() {
		set_gvar('license_key', $_POST['license_key']);
		redirect(url('admin'));
	}
	function configpost() {
		$config = array( 
					'site_name' => $_POST['site_name'],
					'email' => $_POST['email'],
					'offline' => $_POST['offline'],
					'offline_reason' => $_POST['offline_reason']
					);
		set_gvar('private_network', $_POST['private_network']);
		if ($_POST['enable_friendship']) {
			set_gvar('disable_friendship', 0);
			}
		else {
			set_gvar('disable_friendship', 1);
			}
		set_gvar('botauth', $_POST['botauth']);
		set_gvar('landing_page', $_POST['landing_page']);
		//set_gvar('jcow_hot_likes', $_POST['jcow_hot_likes']);
		set_gvar('verify_approval', $_POST['verify_approval']);
		set_gvar('site_name', $_POST['site_name']);
		set_gvar('site_slogan', $_POST['site_slogan']);
		set_gvar('site_keywords', $_POST['site_keywords']);
		set_gvar('site_email', $_POST['site_email']);
		set_gvar('chat', $_POST['chat']);
		$s = serialize($config);
		set_gvar('friend_admin',$_POST['friend_admin']);
		set_gvar('reg_confirm',$_POST['reg_confirm']);
		set_gvar('reg_limit_ip',$_POST['reg_limit_ip']);
		set_gvar('only_invited',0);
		set_gvar('jcow_banner',$_POST['jcow_banner']);
		set_gvar('signup_closed',$_POST['signup_closed']);
		set_text('community_question',$_POST['community_question']);
		set_gvar('pending_post_limit',$_POST['pending_post_limit']);
		set_gvar('oauth_google_id',$_POST['oauth_google_id']);
		set_gvar('oauth_google_secret',$_POST['oauth_google_secret']);

		set_gvar('miniblog_maximum',$_POST['miniblog_maximum']);
		$disable_bookmark = $_POST['enable_bookmark'] ? 0:1;
		set_gvar('disable_bookmark',$disable_bookmark);
		if (jedition != 'trial') {
			set_gvar('recaptcha_key',$_POST['recaptcha_key']);
			set_gvar('recaptcha_secret',$_POST['recaptcha_secret']);
			set_gvar('smtp_enabled',$_POST['smtp_enabled']);
			set_gvar('smtp_host',$_POST['smtp_host']);
			set_gvar('smtp_port',$_POST['smtp_port']);
			set_gvar('smtp_username',$_POST['smtp_username']);
			set_gvar('smtp_password',$_POST['smtp_password']);
			set_gvar('offline',$_POST['offline']);
			set_gvar('reg_limit_ip',$_POST['reg_limit_ip']);
			set_gvar('offline_reason',$_POST['offline_reason']);
			set_text('footermsg',$_POST['footermsg']);
			set_gvar('google_browser_key',$_POST['google_browser_key']);
			set_gvar('google_server_key',$_POST['google_server_key']);
		}

		redirect(url('admin/config'),1);
	}
	
	function users($filter = '') {
		nav(url('admin/members_quick','Members'));
		global $current_sub_menu,$num_per_page,$offset,$page;
		section_content('
		<script>
		function sfilter() {
			var uname = document.getElementById("fusername").value;
			window.location = "'.url('admin/members_quick').'/"+uname;
			}
		</script>
		');
		c(t('Username').': 
		<input type="text" name="lastname" id="fusername" /> <input type="button" value="'.t('Search').'" onclick="javascript:sfilter();" />');
		
		section_content('<ul>');
		if (strlen($filter) && !preg_match("/^page/i",$filter)) {
			$pageb = "/$filter";
			$filter = " and username like '%$filter%' ";
		}
		else {
			$filter = '';
		}
		$res = sql_query("select * from `".tb()."accounts` "." where 1 $filter order by id DESC limit $offset,$num_per_page");
		while ($member = sql_fetch_array($res)) {
			if ($member['disabled'] == 1)
				$status = '(un-verified)';
			if ($member['disabled'] == 2)
				$status = '(suspended)';
			if ($member['disabled'] == 3)
				$status = '(spammer)';
			section_content('<li><span>'.url('admin/useredit/'.$member['id'],$member['username'].' '.$member['lastname']). '<span class="sub">'.$status.'</span></li>');
		}
		section_content('</ul>');
		
		$res = sql_query("select count(*) as total from `".tb()."accounts` "." where 1 $filter ");
		$row = sql_fetch_array($res);
		$total = $row['total'];
		$pb       = new PageBar($total, $num_per_page, $page);
		$pb->paras = url('admin/members_quick'.$pageb);
		$pagebar  = $pb->whole_num_bar();
		c($pagebar);
		$current_sub_menu['href'] = url('admin/members_quick');
	}
	
	function useredit($uid = 0) {
		global $nav,$client;
		$nav[] = url('admin/members_quick',t('Users'));
		$res = sql_query("select * from `".tb()."accounts` where id='$uid' ");
		$user = sql_fetch_array($res);
		$user['roles'] = explode('|',$user['roles']);
		if (!$user['id']) {
			die('wrong uid');
		}
		if ($user['featured']) $featured = 'checked';
		if ($user['disabled'] == 3) {
			$status = '<br />Status: <font color="red">Spammer</font>';
		}
		if ($client['is_demo']) {
			$user['email'] = $user['ipaddress'] = '<font color="blue">*hidden for DEMO*</font>';
		}
		section_content('
		<p>
		'.t('Username').': 
		<strong>'.$user['username'].'</strong>
		 ('.url('u/'.$user['username'],t('View profile')).')<br />
		Email: '.$user['email'].'<br />
		Location: '.h($user['location']).'
		</p>
		<p>
		<label>User IP</label>
		<strong>'.$user['ipaddress'].'</strong>
		'.$status.' 
		</p>
		<fieldset>
		<form action="'.url('admin/usereditpost').'" method="post">
		<p>
		'.label('User Roles'));
		$res = sql_query("select * from ".tb()."roles where (id=3 or id>9) order by id");
		while ($role = sql_fetch_array($res)) {
			$checked = '';
			if (in_array($role['id'],$user['roles']))	$checked = ' checked ';
			section_content('<input type="checkbox" name="set_roles[]" value="'.$role['id'].'" '.$checked.' />'.h($role['name']).' ');
		}
		section_content('
		</p>
		
					<p>
					'.label('Change Status').'
					<input type="radio" name="disabled" value=0 '.admin_check_status($user,0).' /> Verified 
					<input type="radio" name="disabled" value=1 '.admin_check_status($user,1).' /> Un-verified 
					<input type="radio" name="disabled" value=2 '.admin_check_status($user,2).' /> Suspended 
					<span>
					<i>Un-verified</i> - can not post.<br />
					<i>Suspended</i> - can not login.<br /></span>
					</p>
					<p>
					'.label(t('Featured')).'
					<input type="checkbox" name="set_featured" value=1 '.
					$featured.' /> Featured<br />
					<span class="sub">Featured members have more chance to be displayed.</span>
					</p>
		
		<p>
		<input type="hidden" name="uid" value="'.$user['id'].'" />
		<input type="submit" value="'.t('Save changes').'" class="button" /> 
		<a href="#" class="deleteuser" uid="'.$user['id'].'">Delete this user</a>
		</p>
		</form>
		</fieldset>');
		$res2 = sql_query("select s.*,u.username,u.avatar,p.uid as wall_uid from ".tb()."streams as s left join ".tb()."accounts as u on u.id=s.uid left join ".tb()."pages as p on p.id=s.wall_id where u.id='{$user['id']}' order by s.id desc limit 30");
		$acts = '';
		while($row2 = sql_fetch_array($res2)) {
			$attachment = unserialize($row2['attachment']);
			$att = '';
			if (count($attachment) > 1) {
				if (strlen($attachment['name'])) {
					if (strlen($attachment['uri'])) {
						$att = url($attachment['uri'],h($attachment['name']));
					}
					else {
						$att = h($attachment['name']);
					}
				}
				if (strlen($attachment['title'])) {
					$att = url($attachment['uri'],h($attachment['title']) );
				}
			}
			$acts .= '<li>'.$row2['message'].' '.$att.'</li>';
		}
		c('<strong>Recent acts from this user</strong><ul>'.$acts.'</ul>');
	}
	
	function usereditpost() {
		if ($_POST['delete']) {
			redirect('admin/members_quick',1);
		}
		else {
			if (is_array($_POST['set_roles']))
			$roles = implode('|',$_POST['set_roles']);
			$featured = $_POST['set_featured'];
			$res = sql_query("select disabled,email from ".tb()."accounts where id='{$_POST['uid']}'");
			$user = sql_fetch_array($res);
			sql_query("update `".tb()."accounts` set disabled='{$_POST['disabled']}',roles='{$roles}',featured='{$featured}' $newpass where id={$_POST['uid']} ");
			if (!$_POST['disabled'] && $user['disabled'] == 1) {
				@jcow_mail($user['email'], 'Your account on '.get_gvar('site_name').' approved!', 'Congratulations! Your account on '.get_gvar('site_name').' has been approved! You can start posting now');
			}

			redirect('admin/useredit/'.$_POST['uid'],1);
		}
	}

	function userdelete() {
		$res = sql_query("select * from `".tb()."accounts` where id='{$_POST['uid']}' ");
		$user = sql_fetch_array($res);
		if (!$user['id']) {
			sys_back('wrong uid');
		}
		$uid = $user['id'];
		/*delete forum posts*/
		sql_query("delete from ".tb()."accounts where id='$uid'");
		sql_query("delete from ".tb()."forum_threads where userid='$uid'");
		sql_query("delete from ".tb()."forum_posts where uid='$uid'");
		/*delete comments*/
		sql_query("delete from ".tb()."comments where uid='$uid'");
		/*delete follower*/
		sql_query("delete from ".tb()."followers where uid='$uid' or fid='$uid'");
		/*others*/
		sql_query("delete from ".tb()."friends where uid='$uid' or fid='$uid'");
		sql_query("delete from ".tb()."groups where creatorid='$uid'");
		sql_query("delete from ".tb()."group_members where uid='$uid'");
		sql_query("delete from ".tb()."group_members_pending where uid='$uid'");
		sql_query("delete from ".tb()."group_posts where uid='$uid'");
		sql_query("delete from ".tb()."group_topics where uid='$uid'");
		sql_query("delete from ".tb()."messages where from_id='$uid' or to_id='$uid'");
		sql_query("delete from ".tb()."profiles where id='$uid'");
		sql_query("delete from ".tb()."profile_comments where uid='$uid'");
		$res = sql_query("select id from ".tb()."stories where uid='$uid'");
		while ($story = sql_fetch_array($res)) {
			$res2 = sql_query("select uri from ".tb()."story_photos where sid='{$story['id']}'");
			while($photo = sql_fetch_array($res2)) {
				@unlink($photo['uri']);
			}
		}
		sql_query("delete from ".tb()."stories where uid='$uid'");
		sql_query("delete from ".tb()."streams where uid='$uid'");
		sql_query("delete from ".tb()."liked where uid='$uid'");
		redirect('admin/members_quick',1);
		
	}
	
	function userroles() {
		global $nav;
		$nav[] = url('admin/userroles','User roles');
		$res = sql_query("select * from ".tb()."roles "." order by id");
		section_content('<p>"User Roles" is mainly used for grouping your members so that you can '.url('adminpermissions','give different permissions to different members').'.</p>');
		section_content('<ul>');
		while ($role = sql_fetch_array($res)) {
			section_content('<li>'.h($role['name']));
			if ($role['id'] > 9) section_content(' '.url('admin/userroleedit/'.$role['id'],'Edit').' | '.url('admin/userroledelete/'.$role['id'],'Delete'));
			section_content('</li>');
		}
		section_content('</ul>');
		section_close('Current Roles');
		section_content('
		<form method="post" action="'.url('admin/userroleadd').'">
		<p><label>Role Name:</label><input type="text" name="name" />
		<input type="submit" value="Add" />
		</p>
		</form>');
		section_close('Add a Role');
	}
	
	function userroledelete($rid) {
		if ($rid < 10) die('you must not delete roles that ID under 10');
		sql_query("delete from ".tb()."roles where id='$rid' ");
		redirect('admin/userroles',1);
	}
	
	function userroleedit($rid) {
		$res = sql_query("select * from ".tb()."roles where id='$rid' ");
		$role = sql_fetch_array($res);
		section_content('
		<form method="post" action="'.url('admin/userroleeditpost').'">
		<p><label>Role Name:</label><input type="text" name="name" value="'.h($role['name']).'" />
		<input type="hidden" name="rid" value="'.$role['id'].'" />
		<input type="submit" value="Add" />
		</p>
		</form>');
		section_close('Edit Role');
	}
	
	function userroleeditpost() {
		sql_query("update ".tb()."roles set name='{$_POST['name']}' where id='{$_POST['rid']}' ");
		redirect('admin/userroles',1);
	}
	
	function userroleadd() {
		if (!$_POST['name']) sys_back('Please input a valid role name');
		$res = sql_query("select max(id) as maxid from ".tb()."roles " );
		$row = sql_fetch_array($res);
		if ($row['maxid'] < 11) $id = 11;
		else $id = $row['maxid'] + 1;
		sql_query("insert into ".tb()."roles(id,name) values($id,'{$_POST['name']}')");
		redirect('admin/userroles',1);
	}
	
	function apps() {
		nav(t('Applications'));
		global $current_sub_menu, $all_apps;
		$current_sub_menu['href'] = url('admin/apps');
		$res = sql_query("select * from ".tb()."apps "." order by weight");
		section_content('<table class="stories" cellspacing="1">');
		section_content('<form action="'.url('admin/appspost').'" method="post" >');
		section_content('<tr class="table_line1"><td width="10">Actived</td><td>Application</td><td>Display</td><td>Weight</td></tr>');
		while($app = sql_fetch_array($res)) {
			$checked = $app['status'] ? 'checked':'';
			$readonly = $app['status'] ? '':'readonly';
			$app['dname'] = $app['name'];
			if ($app['dname'] == 'members') $app['dname'] = 'member listings';
			section_content('<tr class="row1"><td><input type="checkbox" name="'.$app['name'].'active" '.$checked.' value="1" /></td>
			<td>'.$app['dname'].'</td>
			<td><input type="text" name="'.$app['name'].'flag" value="'.h($app['flag']).'" '.$readonly.' /></td>
			<td><input type="text" name="'.$app['name'].'weight" size="2" value="'.$app['weight'].'" /></td>
			</tr>');
		}
		section_content('<tr class="row2"><td colspan="4"><input type="submit" value="'.t('Save changes').'" /></td></tr>
		</form>
		</table>');
	}
	
	function appspost() {
		$res = sql_query("select * from ".tb()."apps ");
		while($app = sql_fetch_array($res)) {
			$active = $app['name'].'active';
			$active = $_POST[$active];
			$flag = $app['name'].'flag';
			$flag = $_POST[$flag];
			$weight = $app['name'].'weight';
			$weight = $_POST[$weight];
			sql_query("update ".tb()."apps set status='$active',flag='$flag',weight='$weight' where id='{$app['id']}'");
		}
		redirect('admin/apps',1);
	}
	
	function plugins() {
		nav(t('Plugins'));
		global $current_sub_menu, $apps;
		$path='plugins/';
		if ($handle = opendir($path)) {
		    c("Directory handle: $handle\n");
		    c("Files:\n");
		    while (false !== ($file = readdir($handle))) {
		        $tmppath=rawurlencode($file);
		        c("<a href=$path"."$tmppath>$file</a>\n");
		    		c("<br>");
		    }
		    while ($file = readdir($handle)) {
		        c("$file\n");
		    }
		    closedir($handle);
		}
	}
	
	function pluginspost() {
		redirect('admin/apps',1);
	}
	
	function texts() {
		nav(t('System Texts'));
		global $current_sub_menu, $config, $locations,  $blocks;
		$current_sub_menu['href'] = url('admin/texts');
		section_content('
			<form action="'.url('admin/textspost').'" method="post">
			<p>
			'.label(t('Terms and Privacy Policy')).'
			<textarea name="rules_conditions" style="width:500px" rows="7">'.h(get_text('rules_conditions')).'</textarea><br />
			<span>Used on user registration</span>
			</p>

			<p>
			'.label('Fan Pages creating rules').'
			<textarea name="create_page_rule"  style="width:500px" rows="7">'.h(get_text('create_page_rule')).'</textarea><br />
			<span>Displayed on the "Creating a Fan Page" page.  <strong>HTML</strong> tag enabled. </span>
			</p>

						<p>
			'.label('Groups creating rules').'
			<textarea name="create_group_rule"  style="width:500px" rows="7">'.h(get_text('create_group_rule')).'</textarea><br />
			<span>Displayed on the "Creating a Group" page. <strong>HTML</strong> tag enabled.</span>
			</p>

			<p>
			'.label('Words filter').'
			<textarea name="words_filter"  style="width:500px" rows="7">'.h(get_text('words_filter')).'</textarea><br />
			<span>Words in this box will be replaced by **. separated by commas.</span>
			</p>		
			
			<p>
			'.label('Welcome PM Template').'
			<textarea name="welcome_pm"  style="width:500px" rows="7">'.h(get_text('welcome_pm')).'</textarea><br />
			<span>Special word: %username%</span>
			</p>
			
			<p>
			'.label('Welcome Email Template').'
			<textarea name="welcome_email"  style="width:500px" rows="7">'.h(get_text('welcome_email')).'</textarea><br />
			<span>Special words: %username%, %email%, %sitelink%</span>
			</p>


			
			<p>
			<input type="submit" class="button" value="'.t('Save changes').'" />
			</p>
			</form>
				');
	}

	function textspost() {
		set_text('rules_conditions',$_POST['rules_conditions']);
		set_text('create_page_rule',$_POST['create_page_rule']);
		set_text('create_group_rule',$_POST['create_group_rule']);
		set_text('words_filter',$_POST['words_filter']);
		set_text('welcome_pm',$_POST['welcome_pm']);
		set_text('welcome_email',$_POST['welcome_email']);
		redirect('admin/texts',1);
	}

	function ad_blocks() {
		nav(url('admin/ad_blocks','Ad blocks'));
		if (jedition == 'trial' || jedition == 'CE') {
			c('Please <a href="https://www.jcow.net/download" target="_blank">Upgrade to Pro</a> to use this feature');
			stop_here();
		}
		global $current_sub_menu, $config, $locations;
		$current_sub_menu['href'] = url('admin/ad_blocks');
		if (!check_license()) {
			c('<div style="background:yellow;padding:5p;margin:10px">Ad blocks is not available in Jcow '.jedition.'</div>');
		}
		section_content('<form action="'.url('admin/ad_blockspost').'" method="post">');

		section_content('<fieldset><legend>Ad Options</legend>
		<label>Hide Ad to</label>');
		$hide_ad_roles = explode('|',get_gvar('hide_ad_roles'));
		$res = sql_query("select * from ".tb()."roles "." where id>1 order by id");
		while ($role = sql_fetch_array($res)) {
			$checked = '';
			if (in_array($role['id'],$hide_ad_roles))	$checked = ' checked ';
			section_content('<input type="checkbox" name="hide_ad_roles[]" value="'.$role['id'].'" '.$checked.' />'.h($role['name']).' ');
		}
		section_content('</fieldset>');

		c('<fieldset><legend>Google Adsense Crawler Access</legend>
			<label>Restricted Directory or URL</label> <input style="background:#eeeeee" type="text" size="70" value="'.url('jcowadsense').'" readonly/><br />
			<label>Parameter1</label>
			<input type="text" value="username" style="background:#eeeeee" readonly/> = 
			<input type="text" value="jcow" style="background:#eeeeee" readonly/><br />
			<label>Parameter2</label>
			<input type="text" value="password" style="background:#eeeeee" readonly/> = 
			<input type="text" name="adsense_password" value="'.get_gvar('adsense_password').'" />
			
			</fieldset>
			');
		
		section_content('<fieldset>
		<legend>Inline Banner Ad</legend>
		
		<ul>
		<li>Maximum width: <strong>500px</strong></li>
		<li>Suggested size:Auto
		</li>
		</ul>');
			c('
		<table border="0" width="100%"><tr><td width="51%">
		<textarea name="jcow_inline_banner_ad" style="width:300px;height:300px">'.h(get_gvar('jcow_inline_banner_ad')).'</textarea>
		</td>
		<td>
		<div style="width:300px;height:300px;background:#D7DCE1"><div style="clear:both">Your Jcow Page</div>
		<div style="width:170px;height:30px;background:#A0ABB8;float:left;margin:5px;clear:both">Page content</div>
		<div style="width:170px;height:40px;background:#53EE5A;float:left;margin:5px;clear:both">ADS HERE</div>
		<div style="width:170px;height:30px;background:#A0ABB8;float:left;margin:5px;clear:both">Page content</div>
		<div style="width:170px;height:30px;background:#A0ABB8;float:left;margin:5px;clear:both">Page content</div>
		</div>
		</td></tr></table>
		Available parameters:<br />
		<strong>jcow_user_id</strong>: Returns user id for online member. Returns "0" for guests.<br />
		<strong>jcow_user_username</strong>: Returns username for online member. Returns blank for guests.<br />
		<strong>jcow_user_fullname</strong>: Returns fullname for online member. Returns blank for guests.<br />
		</fieldset>');


		section_content('<fieldset>
		<legend>Inline sidebar ad</legend>
		
		<ul>
		<li>Maximum width: <strong>300</strong>px</li>
		<li>Suggested size:<br />
		<strong>300</strong>x<strong>250</strong>, 
		<strong>300</strong>x<strong>200</strong>, 
		<strong>250</strong>x<strong>250</strong>, 
		<strong>200</strong>x<strong>200</strong>
		</li>
		</ul>');
			c('
		<table border="0" width="100%"><tr><td width="51%">
		<textarea name="jcow_inline_sidebar_ad" style="width:300px;height:300px">'.h(get_gvar('jcow_inline_sidebar_ad')).'</textarea>
		</td>
		<td>
		
		<div style="width:300px;height:300px;background:#D7DCE1"><div style="clear:both">Your Jcow Page</div>
		<div style="width:70px;height:30px;background:#A0ABB8;float:right;margin:5px;clear:both">Content</div>
		<div style="width:70px;height:40px;background:#53EE5A;float:right;margin:5px;clear:both">ADS HERE</div>
		<div style="width:70px;height:30px;background:#A0ABB8;float:right;margin:5px;clear:both">Content</div>
		<div style="width:70px;height:30px;background:#A0ABB8;float:right;margin:5px;clear:both">Content</div>
		</div>
		</td></tr></table>
		Available parameters:<br />
		<strong>jcow_user_id</strong>: Returns user id for online member. Returns "0" for guests.<br />
		<strong>jcow_user_username</strong>: Returns username for online member. Returns blank for guests.<br />
		<strong>jcow_user_fullname</strong>: Returns fullname for online member. Returns blank for guests.<br />
		
		</fieldset>');
		
		
	
		
		section_content('<input type="submit" class="button" value="'.t('Save changes').'" />');
	
		section_content('
		</form>
		');
	}
	function ad_blockspost() {
		if (is_array($_POST['hide_ad_roles'])) {
			set_gvar('hide_ad_roles', implode('|',$_POST['hide_ad_roles']));
		}
		else {
			set_gvar('hide_ad_roles', '');
		}
		set_gvar('adsense_password',$_POST['adsense_password']);
		set_gvar('jcow_inline_sidebar_ad', $_POST['jcow_inline_sidebar_ad']);
		set_gvar('jcow_inline_banner_ad', $_POST['jcow_inline_banner_ad']);
		set_gvar('jcow_inline_mobile_ad', $_POST['jcow_inline_mobile_ad']);
		redirect(url('admin/ad_blocks'),1);
	}


	function customfields() {
		nav(url('admin/customfields','Custom fields'));
		global $current_sub_menu, $apps;
		$current_sub_menu['href'] = url('admin/customfields');
		if (!get_gvar('disable_birthday')) {
			$birthday_checked = 'checked';
		}
		section_content('
		<p>This setting is optional. You can ask your members some questions when they signing up or changing profile. Their answers will be displayed on their profile.</p>
		<table border="1" width="100%">
		<tr>
			<td><input type="text" size="20"  value="Smoke or not" /></td>
			<td>
			<select>
			<option value="text" >Text</option>
			<option value="select_box" selected >Select Box</option>
			<option value="disabled">Disabled</option>
			</select>
			</td>
			<td>
			<textarea  rows="2" style="width:360px">Yes
No</textarea>
			</td>
		</tr>
		</table>');
		section_close('Example');
		section_content('
		<form action="'.url('admin/customfields_post').'" method="post">
		<table border="1" width="100%" cellpadding="5">
		<tr>
		<td>Question</td>
		<td>Default Value(s)<br />
		Only required when the Field Type is <strong>Select Box</strong>.Separate options with "Enter"</td>
		</tr>
		<tr><td>Birthday<br />
		<input type="checkbox" name="birthday" value=1 '.$birthday_checked.' /> Required
		</td><td>If this is unchecked, users can not search member by age, but the signup form will be cleaner.</td></tr>');
		for($i=1;$i<=7;$i++) {
			$key = 'cf_var'.$i;
			$key2 = 'cf_var_value'.$i;
			$key3 = 'cf_var_des'.$i;
			$key4 = 'cf_var_label'.$i;
			$key5 = 'cf_var_required'.$i;
			$type = get_gvar($key);
			$value = get_gvar($key2);
			$des = get_gvar($key3);
			$label = get_gvar($key4);
			$required_field = get_gvar($key5);
			$text_selected = $select_box_selected = $disabled_selected = '';
			if ($type == 'text') {
				$text_selected = 'selected';
			}
			elseif ($type == 'select_box') {
				$select_box_selected = 'selected';
			}
			elseif ($type == 'textarea') {
				$textarea_selected = 'selected';
			}
			else {
				$disabled_selected = 'selected';
			}
			if ($required_field) {
				$required_checked = 'checked';
			}
			else {
				$required_checked = '';
			}
			section_content('<tr>
			<td>
			Field Name:<input type="text" size="20" name="'.$key4.'" value="'.$label.'" /><br />
			Field type:
			<select name="'.$key.'">
			<option value="text" '.$text_selected.'>Single-Line Text</option>
			<option value="textarea" '.$textarea_selected.'>Multi-Line Text</option>
			<option value="select_box"  '.$select_box_selected.'>Select Box</option>
			<option value="disabled"  '.$disabled_selected.'>Disabled</option>
			</select><br />
			<input type="checkbox" name="'.$key5.'" value=1 '.$required_checked.' />Required
			</td>
			<td>Options for Select Box:<br />
			<textarea name="'.$key2.'" rows="3" style="width:360px">'.$value.'</textarea><br />
			Description:<br />
			<input type="text" name="'.$key3.'" value="'.$des.'" style="width:360px" />
			</td>
			</tr>');
		}

		section_content('
		</table>
		<p><input type="submit" value="'.t('Save changes').'" /></p>
		</fieldset>
		');
		section_close('Setup');
	}
	
	function customfields_post() {
		for($i=1;$i<=7;$i++) {
			$key = 'cf_var'.$i;
			$key2 = 'cf_var_value'.$i;
			if ($_POST[$key] == 'select_box') {
				if (!strlen($_POST[$key2])) {
					sys_back(t('You must set options for select_box'));
				}
			}
		}
		
		for($i=1;$i<=7;$i++) {
			$key = 'cf_var'.$i;
			$key2 = 'cf_var_value'.$i;
			$key3 = 'cf_var_des'.$i;
			$key4 = 'cf_var_label'.$i;
			$key5 = 'cf_var_required'.$i;
			if ($_POST[$key] != 'disabled') {
				$newarr = array();
				set_gvar($key,$_POST[$key]);
				$tarr = explode(',',$_POST[$key2]);
				foreach ($tarr as $val) {
						$newarr[] = trim($val);
				}
				$newkey2 = implode(',',$newarr);
				set_gvar($key2,$newkey2);
				set_gvar($key3,$_POST[$key3]);
				set_gvar($key4,$_POST[$key4]);
				set_gvar($key5,$_POST[$key5]);
			}
			else {
				set_gvar($key,'disabled');
				set_gvar($key2);
				set_gvar($key3);
				set_gvar($key4);
				set_gvar($key5);
			}
		}
		if (!$_POST['birthday']) {
			set_gvar('disable_birthday',1);
		}
		else {
			set_gvar('disable_birthday',0);
		}
		redirect('admin/customfields',1);
	}
	function translatedel($delkey) {
		global $langs_enabled;
		$newlangs = array();
		foreach ($langs_enabled as $val) {
			if ($val != $delkey) {
				$newlangs[] = $val;
			}
		}
		$newlang = implode(',',$newlangs);
		set_gvar('langs_enabled',$newlang);
		redirect('admin/translate',1);
	}

	function translateinout() {
		global $langs;
		if ($_POST['step'] == 'export') {
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename='.$_POST['lang'].'.txt');
			header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			$res = sql_query("select * from ".tb()."langs where lang='{$_POST['lang']}'");
			while ($row = sql_fetch_array($res)) {
				echo $row['lang'].' :transfrom: '.$row['lang_from'].' :transto: '.$row['lang_to']."\r\n";
			}
			exit;
		}
		if ($_POST['step'] == 'import') {
			if ($src = $_FILES['lang_src']['tmp_name']) {
				$data = fread(fopen($src, 'r'), filesize($src));
				if (!preg_match("/transto/i",$data)) {
					sys_notice('Please select a correct language file');
				}
				else {
					$lines = explode("\r\n",$data);
					foreach ($lines as $line) {
						if (preg_match("/transfrom/i",$line)) {
							$trans = explode(':transto:',$line);
							$lang_to = addslashes(trim($trans[1]));
							$lang_s = explode(':transfrom:',$trans[0]);
							$lang_code = trim($lang_s[0]);
							$lang_from = addslashes(trim(  $lang_s[1]         ));
							if(strlen($lang_code) == 2) {
								$res = sql_query("select * from ".tb()."langs where lang_from='{$lang_from}' and lang='{$lang_code}'");
								if (sql_counts($res)) {
									sql_query("update ".tb()."langs set lang_to='{$lang_to}' where lang_from='{$lang_from}' and lang='{$lang_code}'");
								}
								else {
									sql_query("insert into ".tb()."langs (lang,lang_from,lang_to) values('{$lang_code}','$lang_from','$lang_to')");
								}
							}
						}
					}
					redirect('admin/translate',1);
				}
			}
			else {
				sys_notice('Please select a language file');
			}
		}
	}

	function translate() {
		nav(url('admin/translate','Translate'));
		global $current_sub_menu, $client, $langs_enabled, $page, $lang_options, $langs;
		if (is_array($langs_enabled) && count($langs_enabled)) {
			c('<p>Language Options:<ul>');
			foreach ($langs_enabled as $val) {
				c('<li>'.$langs[$val].' <span class="sub">| '.url('admin/translatedel/'.$val,'Delete').'</li>');
			}
			c('</ul></p>');
		}
		sql_query("delete from ".tb()."langs where lang_from=''");
		c('<p><form method="post" action="'.url('admin/translateadd').'">Add a language: <select name="new_lang">');
		foreach ($langs as $key=>$lang) {
			c('<option value="'.$key.'">'.$lang.'</option>');
		}
	c('</select>
	<input type="submit" value="Add" />
	</form>
	</p>');
		$offset = ($page-1)*50;
		$current_sub_menu['href'] = url('admin/translate');
		$key = $client['lang'];
	if (count($langs_enabled) > 0) {
		section_content('<hr />
		<table width="90%" border="1">
		<tr><td width="50%">Export Language<br />
		<form method="post" action="'.url('admin/translateinout').'">
		<select name="lang">');
		foreach ($langs_enabled as $val) {
			c('<option value="'.$val.'">'.$langs[$val].'</option>');
		}
		c('</select>
		<input type="hidden" name="step" value="export" />
		<input type="submit" value="Export" />
		</form>
		</td>
		<td>Import Language<br />
		<form method="post" action="'.url('admin/translateinout').'" enctype="multipart/form-data">
		<input type="file" name="lang_src" />
		<input type="hidden" name="step" value="import" />
		<input type="submit" value="Import" />
		</form>
		</td>
		</tr>
		</table>
		<p style="font-size:20px">You are translating <strong>'.$lang_options[$key].'</strong>');
		if (count($lang_options) > 1) {
				foreach ($lang_options as $key=>$lang) { 
					if ($client['lang'] == $key) { 
						$lang_pres[] = $lang; 
					} 
					else { 
						$lang_pres[] = url('language/post/'.$key,$lang); 
					}
				} 
				$lang_select = implode(' | ', $lang_pres);
				c(' (Switch to '.$lang_select.')<br />');
		}
		section_content('</p>');

		section_content('
		<script>
				$(document).ready( function(){
						$(".trans_text").keydown(function(e) {
							if(e.ctrlKey && e.keyCode == 13) {
								var cbox = $(this).next();
								var mbox = $(this);
								var tbox = $(this).prev();
								cbox.html("<img src=\"'.uhome().'/files/loading.gif\" /> Submitting");
								$.post("'.uhome().'/index.php?p=jquery/translate",
								{tto:mbox[0].value,tfrom:tbox[0].value},
								  function(data){
									cbox.html(data);
									},"html"
								);
								return false;
							}
						});
				});
				</script>');
		section_close();
		$res = sql_query("select * from `".tb()."langs` where lang='{$client['lang']}' "." and lang_to='' order by lang_from limit 1000");
		while ($row = sql_fetch_array($res)) {
			/*if(!strlen($row['lang_to'])) {
				$row['lang_to'] = $row['lang_from'];
			}*/

			section_content('<form class="translate_form">
			<span class="sub">('.htmlspecialchars($row['lang_from']).')</span><br />
			<input type="hidden" name="lang_from" value="'.htmlspecialchars($row['lang_from']).'" />
			<textarea name="lang_to" style="height:30px;width:500px; background-color: transparent;" class="trans_text">'.htmlspecialchars($row['lang_to']).'</textarea>
			<span class="tstatus">not saved(Press "Ctrl+Enter" to Save)</span>
			</form>');
		}
		section_close('Untranslated');
		
		$res = sql_query("select * from `".tb()."langs` where lang='{$client['lang']}' "." and lang_to!='' order by lang_from limit 1000");
		while ($row = sql_fetch_array($res)) {
			if(!strlen($row['lang_to'])) {
				$row['lang_to'] = $row['lang_from'];
			}

			section_content('<form class="translate_form">
			<span class="sub">('.htmlspecialchars($row['lang_from']).')</span><br />
			<input type="hidden" name="lang_from" value="'.htmlspecialchars($row['lang_from']).'" />
			<textarea name="lang_to" style="height:30px;width:500px; background-color: transparent;" class="trans_text">'.htmlspecialchars($row['lang_to']).'</textarea>
			<span class="tstatus">(Press "Ctrl+Enter" to Save change)</span>
			</form>');
		}
		section_close('Translated');
		
		/*
		$res = sql_query("select count(*) as total from `".tb()."langs` where lang='{$client['lang']}' ");
		$row = sql_fetch_array($res);
		$total = $row['total'];
		$pb       = new PageBar($total, 50, $page);
		$pb->paras = url('admin/translate');
		$pagebar  = $pb->whole_num_bar();
		c($pagebar);
		*/
	}

	}

	function translateadd() {
		global $langs,$langs_enabled;
		$key = $_POST['new_lang'];
		if (!in_array($key,$langs_enabled)) {
			$langs_enabled[] = $key;
			$le = implode(',',$langs_enabled);
			set_gvar('langs_enabled',$le);
		}
		redirect('admin/translateadded',1);
	}

	function translateadded() {
		c('Language added. <strong>'.url('admin/translate','Back to Translate'));
	}
	

	
	


	
}


function admin_check_status($user, $val) {
	if ($user['disabled'] == $val) {
		return 'checked';
	}
}

function get_style_list($dirname) {
	if ($handle = opendir($dirname)) {
		while (false !== ($file = readdir($handle))) {
			if (is_dir($dirname . '/' .$file) && $file != '.' && $file != '..' && $file != '.svn' ) {
				$dirs[] = $file;
			}
		}
		closedir($handle);
		
		if (is_array($dirs)) {
			asort($dirs);
			return $dirs;
		}
		else {
			return 0;
		}
	}
}



function remove_remarks($sql)
{
	$lines = explode("\n", $sql);
	
	// try to keep mem. use down
	$sql = "";
	
	$linecount = count($lines);
	$output = "";

	for ($i = 0; $i < $linecount; $i++)
	{
		if (($i != ($linecount - 1)) || (strlen($lines[$i]) > 0))
		{
			if ($lines[$i][0] != "#")
			{
				$output .= $lines[$i] . "\n";
			}
			else
			{
				$output .= "\n";
			}
			// Trading a bit of speed for lower mem. use here.
			$lines[$i] = "";
		}
	}
	
	return $output;
	
}

//

//
function split_sql_file($sql, $delimiter)
{
	$tokens = explode($delimiter, $sql);

	$sql = "";
	$output = array();
	
	$matches = array();
	
	$token_count = count($tokens);
	for ($i = 0; $i < $token_count; $i++)
	{
		if (($i != ($token_count - 1)) || (strlen($tokens[$i] > 0)))
		{
			$total_quotes = preg_match_all("/'/", $tokens[$i], $matches);
			// Counts single quotes that are preceded by an odd number of backslashes, 
			// which means they're escaped quotes.
			$escaped_quotes = preg_match_all("/(?<!\\\\)(\\\\\\\\)*\\\\'/", $tokens[$i], $matches);
			
			$unescaped_quotes = $total_quotes - $escaped_quotes;
			
			if (($unescaped_quotes % 2) == 0)
			{
				// It's a complete sql statement.
				$output[] = $tokens[$i];
				// save memory.
				$tokens[$i] = "";
			}
			else
			{
				// incomplete sql statement. keep adding tokens until we have a complete one.
				// $temp will hold what we have so far.
				$temp = $tokens[$i] . $delimiter;
				// save memory..
				$tokens[$i] = "";
				
				// Do we have a complete statement yet? 
				$complete_stmt = false;
				
				for ($j = $i + 1; (!$complete_stmt && ($j < $token_count)); $j++)
				{
					// This is the total number of single quotes in the token.
					$total_quotes = preg_match_all("/'/", $tokens[$j], $matches);
					// Counts single quotes that are preceded by an odd number of backslashes, 
					// which means they're escaped quotes.
					$escaped_quotes = preg_match_all("/(?<!\\\\)(\\\\\\\\)*\\\\'/", $tokens[$j], $matches);
			
					$unescaped_quotes = $total_quotes - $escaped_quotes;
					
					if (($unescaped_quotes % 2) == 1)
					{
						$output[] = $temp . $tokens[$j];

						$tokens[$j] = "";
						$temp = "";
						
						$complete_stmt = true;
						$i = $j;
					}
					else
					{
						$temp .= $tokens[$j] . $delimiter;
						$tokens[$j] = "";
					}
					
				}
			} 
		}
	}

	return $output;
}