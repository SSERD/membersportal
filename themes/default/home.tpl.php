<?php

echo '
<style>
body {
	background:white;
}
	#appmain {
		width: 645px;
		float: right;
	}
	#appside {
		width: 310px;
		float: left;
	}
	.jcow_story_video {
		width:130px;
		height:100px;
		}
	#appside .block_title {
		margin: 5px;
		}
	#appside .block_content {
		padding: 2px;
		}
	.home_blog_entries {
		border-bottom: #eeeeee 1px solid;
		padding:3px;
		margin:2px;
		}
	.home_blog_entries img {
		vertical-align:middle;
		}
	.home_status {
		margin:3px;
		float:left;
		width:130px;
		}
	.hot_topics  {
		margin:3px;
		background:#eeeeee;
		float:left;
		width:130px;
		}
	.sf_field {
		margin-bottom:10px;
		padding-left:10px;
	}
	#wallpaper {
		background:white;
	}

</style>
<div id="jcow_app_container" >
<div style="font-size:2.0em;padding:10px 0 5px 0;text-align:center;color:#999999">Welcome to '.h(get_gvar('site_name')).'</div>';
if ($slogan = get_gvar('site_slogan')) {
	echo '<div style="font-size:1.2em;padding-bottom:5px;text-align:center;color:#999999">'.h($slogan).'</div>';
}
echo '<div style="width:530px;float:left" >


<div style="text-align:right">';
if ($landing = get_gvar('custom_landing')) {
	echo '<img src="'.$landing.'" style="max-width:530px;height:auto" />';
}
else {
	echo '<img src="'.uhome().'/themes/default/landing.jpg" style="max-width:530px;height:auto" />';
}
echo '</div>



</div>';// end appmain


// ##################### sidebar ########################
echo '<div style="width:400px;float:left;padding-left:10px">';
echo '
<div style="font-size:30px;padding:10px">'.t('Sign Up').'</div>
<form action="'.url('member/signup').'" method="post" class="form-inline">
<div class="sf_field"><input type="text"  name="fullname" style="width:362px" class="form-control" placeholder="'.t('Full Name').'" /></div>
<div class="sf_field"><input type="email"  class="form-control" name="email"  style="width:362px" placeholder="'.t('Email Address').'" /></div>

<div class="sf_field">
<input type="text"  class="form-control" style="width:180px" name="username"  placeholder="'.t('Username').' (0-9a-z)" /> 
<input type="password" name="password"  class="form-control" style="width:180px"   placeholder="'.t('Password').'" />
</div>


<div class="sf_field">'.t('Birth').'<br />
					<select name="birthmonth" class="fpost">';
					echo '<option value="0">'.t('Month').':</option>';
					for ($i=1;$i<13;$i++) {
						if ($i<10)$j='0'.$i;else $j=$i;$iss='';
						if ($_REQUEST['birthmonth'] == $j) $iss='selected';
						echo '<option value="'.$j.'" '.$iss.' >'.$j.'</option>';
					}
					echo '</select>
					<select name="birthday" class="fpost">
					<option value="0">'.t('Day').':</option>';
					for ($i=1;$i<=31;$i++) {
						if ($i<10)$j='0'.$i;else $j=$i;$iss='';
						if ($_REQUEST['birthday'] == $j) $iss='selected';
						echo '<option value="'.$j.'" '.$iss.'>'.$j.'</option>';
					}
					echo '</select>
					<select name="birthyear" class="fpost">
					';
					$year_from = gmdate("Y",time()) - 8;
					$year_to = gmdate("Y",time()) - 100;
					if ($_REQUEST['birthyear'])
						$yearkey = $_REQUEST['birthyear'];
					echo '<option value="0">'.t('Year').':</option>';
					for ($i=$year_from;$i>$year_to;$i--) {
						$selected = '';
						if ($yearkey == $i)
							$selected = 'selected';
						echo '<option value="'.$i.'" '.$selected.'>'.$i.'</option>';
					}
					if ($row['hide_age']) $hide_age = 'checked';
					echo'
					</select>
					 </div>
					';
					if ($_POST['gender'] == 0) {
						$gender0 = 'checked';
					}
					else {
						$gender1 = 'checked';
					}
echo '<div class="sf_field">
					<label for="gender_1"><input id="gender_1" type="radio" name="gender" value="1" '.$gender1.' /> '.t('Male').'</label> 
					<label for="gender_0"><input id="gender_0" type="radio" name="gender" value="0" '.$gender0.' /> '.t('Female').'</label></div>
<div class="sf_field"><button type="submit" class="btn btn-default btn-lg" ><span style="padding:0 30px">'.t("Sign Up").'</span></button></div>
</form>
<div style="margin-top:10px;padding:10px 0 0 10px;border-top:#cccccc 2px solid"></div>';
					
if (get_gvar('fb_id')) {
	echo '<a href="'.url('fblogin').'" class="btn btn-primary"><i class="fa fa-facebook fa-lg"></i> '.t('Connect Facebook').'</a> ';
}
if (get_gvar('oauth_google_id') && get_gvar('oauth_google_secret')) {
	echo '<a href="'.url('oauth/google').'" class="btn btn-danger"><i class="fa fa-google fa-lg"></i> '.t('Connect Google').'</a> ';
}

echo '</div>';// sidebar
echo '<div style="clear:both;padding-top:5px;text-align:center">';
$res = sql_query("select * from jcow_accounts where avatar!='' order by id DESC limit 16");
while ($user = sql_fetch_array($res)) {
	echo ' '.avatar($user).' ';
}
echo '</div>';
echo '</div>';//jcow_app_container