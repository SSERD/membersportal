<?php
/* ############################################################ *\
 ----------------------------------------------------------------
Jcow Software (http://www.jcow.net)
IS NOT FREE SOFTWARE
http://www.jcow.net/commercial_license
Copyright (C) 2009 - 2010 jcow.net.  All Rights Reserved.
 ----------------------------------------------------------------
\* ############################################################ */

class friends{
	function __construct() {
		global $tab_menu, $client, $menuon;
		hide_ad();
		need_login();
		/*
		if (get_gvar('disable_friendship')) {
			redirect('u/'.$client['username'].'/following');
		}
		*/
		$menuon = 'friends';
		if (!$client['id']) {
			redirect('member/login/1');
		}
		set_title('Friends');
		$tab_menu = array();
		$tab_menu[] = array('path'=>'friends', 'name'=>'Friends');
		$tab_menu[] = array('path'=>'friends/requests', 'name'=>'Friend requests');
	}
	function ajax_add() {
		global $client;
		die('sorry, friendship is not enabled in CE');
	}
	function my() {
		global $client;
		if ($client['id']) {
			redirect('friends/listing/'.$client['id']);
		}
		else {
			redirect('home');
		}
	}
	
	function index() {
		global $db, $client, $offset, $num_per_page, $page, $ubase, $nav, $current_sub_menu;
		$uid = $client['id'];
		if (!check_license()) {
			c('Not available in Jcow '.jedition);
			stop_here();
		}
		// requests
		$res = sql_query("select u.*,r.created as timeline,r.msg from `".tb()."friend_reqs` as r 
									left join `".tb()."accounts` as u on u.id=r.uid
									where r.fid={$client['id']} limit 5");
		if (sql_counts($res)) {
			while($row = sql_fetch_array($res)) {
				if (strlen($row['username'])) {
					c('
					<div class="post">
				<div class="post_author">'.avatar($row).'<br />
				'.url('u/'.$row['username'],$row['username']).'</div>
				<a href="'.url('friends/approve/'.$row['id']).'" class="btn btn-default btn-sm jcow_btn">'.t('Approve').'</a>
					<a href="'.url('friends/deletes/'.$row['id']).'" class="btn btn-default btn-sm jcow_btn">'.t('Reject').'</a>
				</div>');
				}
			}
			section_close(t('Requests'));
		}
		else {
			//c('Currently no friend requests');
		}
		c('<div><a href="'.url('browse').'"><i class="fa fa-user-plus"></i> '.t('Find new friends').'</a></div>');
		section_close('');

		//ass(friends_box());
		$current_sub_menu['href'] = 'friends';
		$nav[] = t('My friends');

		$res = sql_query("SELECT u.* FROM `".tb()."friends` as f left join `".tb()."accounts` as u on u.id=f.fid where f.uid={$uid} and u.id>0 ORDER BY f.created DESC LIMIT 50");
		c('<ul class="small_avatars">');
		while ($row = sql_fetch_array($res)) {
			c('<li>');
			c('<span>'.url('u/'.$row['username'], $row['username']).'</span><br />'.avatar($row,'m'));
			c('<br /><a href="'.url('friends/delete/'.$row['id']).'" class="jcow_btn">'.t('Remove').'</a>');
			c('</li>');
		}
		c('</ul>');

	}
	
	function delete($fid) {
		global $client, $ubase;
		sql_query("delete from `".tb()."friends` where uid={$client['id']} and fid='$fid' ");
		sql_query("delete from `".tb()."friends` where uid='$fid' and fid={$client['id']} ");
		die('removed');
	}
	
	function add($uid) {
		global $db, $client, $offset, $num_per_page, $page, $ubase, $captcha;
		nav('Add a friend');
		if (get_gvar('disable_friendship')) {
			c('Friendship disabled');
			stop_here();
		}
		if(!$user = valid_user($uid)) {
			sys_back('wrong uid');
		}
		if ($user['id'] == $client['id']) {
			sys_back('you can not add yourself');
		}
		if ($_POST['step'] == 'post') {
			$error = '';
			if ($user['id'] == $client['id']) {
				$error = 'you cannot add yourself';
			}
			if ($this->load_recaptcha($user['id'])) {
				if (!recaptcha_valid()) {
						$error = 'Incorrect reCaptcha';
				}
			}
			$res = sql_query("select * from `".tb()."friends` where uid={$client['id']} and fid={$user['id']} ");
			if (sql_counts($res)) {
				$error = 'This user is already in your friend listings';
			}
			if (!strlen($error)) {
				// do add
				$res = sql_query("select * from `".tb()."friend_reqs` where uid={$client['id']} and fid={$user['id']} ");
				if (!sql_counts($res)) {
					sql_query("insert into `".tb()."friend_reqs` (uid,fid,created,msg) values ({$client['id']},{$user['id']},".time().",'{$_POST['msg']}')");
				}
				else {
					sql_query("update `".tb()."friend_reqs` set created=".time().",msg='{$_POST['msg']}' where uid={$client['id']} and fid={$user['id']} ");
				}
				jcow_social_mail($uid,
					t('{1} wants to be your friend',$client['fullname']),
					t('{1} wants to be your friend',full_url('u/'.$client['username'],h($client['fullname']))).' '.full_url('friends/requests',t('View'))
				);
				redirect(url('friends/requests'),t('Your request has been sent successfully'));
			}
			else {
				sys_notice(h($error));
			}
		}
		$res = sql_query("select * from `".tb()."friends` where uid={$client['id']} and fid={$user['id']} ");
		if (sql_counts($res)) {
			sys_back('This user is already in your friend listings');
		}
		c('<p>'.t('Adding {1} {2} as friend',url('u/'.$user['username'],$user['username'])).'</p>');
		$res = sql_query("select * from `".tb()."blacks` where bid={$client['id']} and uid={$user['id']} ");
		if (sql_counts($res)) {
			c(t('This user has blocked you'));
		}
		else {
			c('
					<form method="post" name="form1" action="'.url('friends/add/'.$uid).'"  enctype="multipart/form-data">
					<p>
					'.label(t('Request message')).'
					<input type="text" name="msg" size="78" />
					</p>');
			if ($this->load_recaptcha($uid)) {
				c('<p>'. recaptcha_get_html($captcha['publickey'],$captchaerror).'</p>');
			}
			c('
					<p>
					<input type="hidden" name="step" value="post" />
					<input type="hidden" name="uid" value="'.$uid.'" />
					<input class="button" type="submit" value="'.t('Send request').'" />
					</p>
					</form>
					');
		}
	}

	function load_recaptcha($uid=0) {
		global $client,$captcha;
		if ($captcha['disabled']) return false;
			$res = sql_query("select * from ".tb()."messages_sent where from_id='$uid' and to_id='{$client['id']}'");
			if (sql_counts($res)) {
				return false;
			}
			return true;

	}
	
	
	function requests() {
		global $client, $content, $title, $current_sub_menu;
		if (!check_license()) {
			c('Not available in Jcow '.jedition);
			stop_here();
		}
		//ass(friends_box());
		$current_sub_menu['href'] = 'friends/requests';
		nav('Requests');
		$title = 'Requests';
		$res = sql_query("select count(*) as num from ".tb()."friend_reqs where uid='{$client['id']}' ");
		$row = sql_fetch_array($res);
		c(t('You have {1} pending requests','<strong>'.$row['num'].'</strong>'));
		section_close(t('To others'));


		$res = sql_query("select u.*,r.created as timeline,r.msg from `".tb()."friend_reqs` as r 
									left join `".tb()."accounts` as u on u.id=r.uid
									where r.fid={$client['id']} limit 100");
		if (sql_counts($res)) {
			while($row = sql_fetch_array($res)) {
				if (strlen($row['username'])) {
					c('
					<div class="post">
				<div class="post_author">'.avatar($row).'<br />
				'.url('u/'.$row['username'],$row['username']).'</div>
				<a href="'.url('friends/approve/'.$row['id']).'" class="btn btn-default btn-sm jcow_btn">'.t('Approve').'</a>
					<a href="'.url('friends/deletes/'.$row['id']).'" class="btn btn-default btn-sm jcow_btn">'.t('Reject').'</a>
				</div>');
				}
			}
		}
		else {
			c('Currently no friend requests');
		}
		section_close(t('To you'));
	}

	
	function approve($uid) {
		global $client, $ubase;
		if (!$user = valid_user($uid)) die('wrong uid');
		// 
		$res = sql_query("select * from `".tb()."friend_reqs` where uid='$uid' and fid={$client['id']} ");
		if (sql_counts($res)) {
			$res = sql_query("select * from `".tb()."friends` where uid='$uid' and fid={$client['id']} ");
			// 
			if (!sql_counts($res)) {
				sql_query("insert into `".tb()."friends` (uid,fid,created) values ($uid,{$client['id']},".time().")");
				sql_query("insert into `".tb()."friends` (uid,fid,created) values ({$client['id']},$uid,".time().")");
				
				//follow
				$res = sql_query("select * from ".tb()."followers where uid='{$client['id']}' and fid='$uid' limit 1");
				if (!sql_counts($res)) {
					$follow['uid'] = $client['id'];
					$follow['fid'] = $uid;
					sql_insert($follow, tb().'followers');
					sql_query("update ".tb()."accounts set followers=followers+1 where id='$uid'");
				}
				$res = sql_query("select * from ".tb()."followers where fid='{$client['id']}' and uid='$uid' limit 1");
				if (!sql_counts($res)) {
					$follow['fid'] = $client['id'];
					$follow['uid'] = $uid;
					sql_insert($follow, tb().'followers');
					sql_query("update ".tb()."accounts set followers=followers+1 where id='{$client['id']}'");
				}
				
			}
			// 
			sql_query("delete from `".tb()."friend_reqs` where uid=$uid and fid={$client['id']} ");
			sql_query("delete from `".tb()."friend_reqs` where uid={$client['id']} and fid=$uid ");
			// 
			stream_publish(
				t(
					'became a friend with {1}',
					url('u/'.$user['username'],$user['username'],'','',1)
				)
			);
			mail_notice('dismail_friend_request_c',$user['username'],t('{1} confirmed your friend request',$client['fullname']),t('{1} confirmed your friend request',$client['fullname']) );
			die(t('Approved'));
		}
	}
	
	function deletes($uid) {
		global $client, $ubase;
		// 确保有这个请求
		$res = sql_query("select * from `".tb()."friend_reqs` where uid='$uid' and fid={$client['id']} ");
		if (sql_counts($res)) {
			// 删除请求
			sql_query("delete from `".tb()."friend_reqs` where uid=$uid and fid={$client['id']} ");
			sql_query("delete from `".tb()."friend_reqs` where uid={$client['id']} and fid=$uid ");
		}
		die(t('Rejected'));
	}


	function invite() {
		global $content, $db, $nav, $client, $uhome, $locations, $current_sub_menu;
		$current_sub_menu['href'] = 'invite';
		if ($_POST['import'] == 'msn') {
			$gm = new msnlistgrab($_POST['msn_user'],$_POST['msn_pass']);
			$gm->GetRecords();
			if (is_array($gm->res)) {
				foreach ($gm->res as $val)
				{
				    $contacts .= $contacts ? ', '.$val : $val;
				}
			}
			else {
				sys_back(t('Sorry, we failed to import contacts, please check you input info'));
			}
		}

		$encoded_url = urlencode(uhome());


		c('

		<form method="post" action="'.url('friends/invitepost').'" >
		<!--
		<p>
		<img src="'.uhome().'/files/icons/msn.gif" />'.url('invite/import_contacts',t('Click here to import your msn contacts')).'
		</p>
		-->
					<p>
					'.label(t('To').' (Email address)').'
					<input name="emails" size="80" value="'.$contacts.'" class="form-control" /><br />
					<span>'.t('Multiple email addresses should be Separated with commas').'(,)</span>
					</p>
					
					<p>
					'.label(t('Message').' (Optional)').'
					<textarea name="message" rows="5" class="form-control"></textarea></p>
	
		<p>
					<input type="submit" class="button" value="'.t('Send invitations').'" />
					</p>
					</form>
					');
		section_close(t('Send invitation'));

		$res = sql_query("select * from ".tb()."invites where uid='{$client['id']}' "." order by id DESC LIMIT 100");
		c('<table class="stories">
		<tr class="table_line1"><td>Email address</td><td>Time</td><td>Status</td></tr>');
		while ($invite = sql_fetch_array($res) ) {
			$invite['status'] = $invite['status'] ? '<font color="green">Joined</font>' : 'Sent';
			c('<tr><td class="row1">'.$invite['email'].'</td><td>'.get_date($invite['created']).'</td><td>'.$invite['status'].'</td></tr>');
		}
		c('</table>');
		section_close(t('Histories'));
		
	}
	


	function invitepost() {
		global $db, $client, $uhome, $ubase, $config;
		if (!$_POST['emails']) {
			sys_back(t('Please fill all the required blanks'));
		}
		
		$emails = explode(',',$_POST['emails']);
		$i = 0;
		$sents = $ignores = 0;
		foreach ($emails as $email) {
			//check
			$res = sql_query("select * from `".tb()."invites` where email='$email' ");
			$res2 = sql_query("select * from `".tb()."accounts` where email='$email' ");
			if (!sql_counts($res) && !sql_counts($res2)) {
				$sents++;
				$rand = get_rand(15);
				sql_query("insert into `".tb()."invites` (uid,email,created,code) values('{$client['id']}','$email',".time().",'$rand')");
				$iid = insert_id();
				$url = full_url('member/signup','','',array('iid'=>$iid,'code'=>$rand));
				$message = get_gvar('site_name').'<br />
				'.$_POST['message'].'<br />
				'.t('Click the following link to Join').'<br />
			<a href="'.$url.'">'.$url.'</a><br />';
				$email = trim($email);
				$email = str_replace("\r\n","",$email);
				if ($i > 50) {
					redirect(url('invite'),t('Invitations have been sent!'));
				}
				$message .= '<br /><br />invited by '.$client['fullname'];
				@jcow_mail($email,t('you are invited to join {1}',get_gvar('site_name')),$message,$client['email']);
				$i++;
			}
			else {
				$ignores++;
			}
		}
		$total = $ignores+$sents;
		redirect(url('friends/invite'),t('{1} submitted',$total).' ('.$sents.' '.t('sents').', '.$ignores.' '.t('ignores').')');
	}


}

	function friends_box() {
		global $client;
		$res = sql_query("select count(*) as num from `".tb()."friends` where uid='{$client['id']}' ");
		$row = sql_fetch_array($res);
		$content = t('You have {1} friends','<strong>'.url('friends',$row['num']).'</strong>');
		$content .= '<table border="0">
				<tr><td><img src="'.uhome().'/files/icons/invite.gif" /></td><td>'.url('invite',t('Invite friends to join our community')).'</td></tr>
				<tr><td><img src="'.uhome().'/files/icons/browse.gif" /></td><td>'.url('members/listing',t('Browse people')).' </td></tr>
				</table>';
		return array('title'=>t('Friends').' '.url('friends',t('View')), 'content' => $content);
	}