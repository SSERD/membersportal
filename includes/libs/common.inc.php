<?php
/* ############################################################ *\
 ----------------------------------------------------------------
Jcow Software (http://www.jcow.net)
IS NOT FREE SOFTWARE
http://www.jcow.net/commercial_license
Copyright (C) 2009 - 2010 jcow.net.  All Rights Reserved.
 ----------------------------------------------------------------
\* ############################################################ */
define('jedition','NE');
$version = '12.0';
date_default_timezone_set("Europe/London");
$gvars = array();
$langs = array(
	'am' => 'Amharic',
	'ar' => 'Arabic',
	'be' => 'Belarusian',
	'bg' => 'Bulgarian',
	'br' => 'Breton',
	'ca' => 'Catalan',
	'ch' => 'Chamorro',
	'zh' => 'Chinese',
	'cs' => 'Czech',
	'cy' => 'Welsh',
	'da' => 'Danish',
	'de' => 'German',
	'el' => 'Greek',
	'en' => 'English',
	'eo' => 'Esperanto',
	'es' => 'Spanish',
	'et' => 'Estonian',
	'eu' => 'Basque',
	'fa' => 'Farsi',
	'fi' => 'Finnish',
	'fr' => 'French',
	'ga' => 'Irish',
	'gl' => 'Galician',
	'gu' => 'Gujarati',
	'he' => 'Hebrew',
	'hi' => 'Hindi',
	'hr' => 'Croatian',
	'hu' => 'Hungarian',
	'ia' => 'Interlingua',
	'id' => 'Indonesian',
	'it' => 'Italian',
	'ja' => 'Japanese',
	'ka' => 'Georgian',
	'ko' => 'Korean',
	'kw' => 'Cornish',
	'la' => 'Latin',
	'lt' => 'Lithuanian',
	'mg' => 'Malagasy',
	'ne' => 'Nepali',
	'nl' => 'Dutch',
	'no' => 'Norwegian',
	'ps' => 'Pashto',
	'pl' => 'Polish',
	'pt' => 'Portuguese',
	'ro' => 'Romanian',
	'ru' => 'Russian',
	'sa' => 'Sanskrit',
	'sk' => 'Slovak',
	'sl' => 'Slovenian',
	'so' => 'Somali',
	'sq' => 'Albanian',
	'sr' => 'Serbian',
	'sv' => 'Swedish',
	'ta' => 'Tamil',
	'tr' => 'Turkish',
	'uk' => 'Ukrainian',
	'vi' => 'Vietnamese',
	'wa' => 'Walloon'
	);
$countries = array
(
	'AF' => 'Afghanistan',
	'AX' => 'Aland Islands',
	'AL' => 'Albania',
	'DZ' => 'Algeria',
	'AS' => 'American Samoa',
	'AD' => 'Andorra',
	'AO' => 'Angola',
	'AI' => 'Anguilla',
	'AQ' => 'Antarctica',
	'AG' => 'Antigua And Barbuda',
	'AR' => 'Argentina',
	'AM' => 'Armenia',
	'AW' => 'Aruba',
	'AU' => 'Australia',
	'AT' => 'Austria',
	'AZ' => 'Azerbaijan',
	'BS' => 'Bahamas',
	'BH' => 'Bahrain',
	'BD' => 'Bangladesh',
	'BB' => 'Barbados',
	'BY' => 'Belarus',
	'BE' => 'Belgium',
	'BZ' => 'Belize',
	'BJ' => 'Benin',
	'BM' => 'Bermuda',
	'BT' => 'Bhutan',
	'BO' => 'Bolivia',
	'BA' => 'Bosnia And Herzegovina',
	'BW' => 'Botswana',
	'BV' => 'Bouvet Island',
	'BR' => 'Brazil',
	'IO' => 'British Indian Ocean Territory',
	'BN' => 'Brunei Darussalam',
	'BG' => 'Bulgaria',
	'BF' => 'Burkina Faso',
	'BI' => 'Burundi',
	'KH' => 'Cambodia',
	'CM' => 'Cameroon',
	'CA' => 'Canada',
	'CV' => 'Cape Verde',
	'KY' => 'Cayman Islands',
	'CF' => 'Central African Republic',
	'TD' => 'Chad',
	'CL' => 'Chile',
	'CN' => 'China',
	'CX' => 'Christmas Island',
	'CC' => 'Cocos (Keeling) Islands',
	'CO' => 'Colombia',
	'KM' => 'Comoros',
	'CG' => 'Congo',
	'CD' => 'Congo, Democratic Republic',
	'CK' => 'Cook Islands',
	'CR' => 'Costa Rica',
	'CI' => 'Cote D\'Ivoire',
	'HR' => 'Croatia',
	'CU' => 'Cuba',
	'CY' => 'Cyprus',
	'CZ' => 'Czech Republic',
	'DK' => 'Denmark',
	'DJ' => 'Djibouti',
	'DM' => 'Dominica',
	'DO' => 'Dominican Republic',
	'EC' => 'Ecuador',
	'EG' => 'Egypt',
	'SV' => 'El Salvador',
	'GQ' => 'Equatorial Guinea',
	'ER' => 'Eritrea',
	'EE' => 'Estonia',
	'ET' => 'Ethiopia',
	'FK' => 'Falkland Islands (Malvinas)',
	'FO' => 'Faroe Islands',
	'FJ' => 'Fiji',
	'FI' => 'Finland',
	'FR' => 'France',
	'GF' => 'French Guiana',
	'PF' => 'French Polynesia',
	'TF' => 'French Southern Territories',
	'GA' => 'Gabon',
	'GM' => 'Gambia',
	'GE' => 'Georgia',
	'DE' => 'Germany',
	'GH' => 'Ghana',
	'GI' => 'Gibraltar',
	'GR' => 'Greece',
	'GL' => 'Greenland',
	'GD' => 'Grenada',
	'GP' => 'Guadeloupe',
	'GU' => 'Guam',
	'GT' => 'Guatemala',
	'GG' => 'Guernsey',
	'GN' => 'Guinea',
	'GW' => 'Guinea-Bissau',
	'GY' => 'Guyana',
	'HT' => 'Haiti',
	'HM' => 'Heard Island & Mcdonald Islands',
	'VA' => 'Holy See (Vatican City State)',
	'HN' => 'Honduras',
	'HK' => 'Hong Kong',
	'HU' => 'Hungary',
	'IS' => 'Iceland',
	'IN' => 'India',
	'ID' => 'Indonesia',
	'IR' => 'Iran, Islamic Republic Of',
	'IQ' => 'Iraq',
	'IE' => 'Ireland',
	'IM' => 'Isle Of Man',
	'IL' => 'Israel',
	'IT' => 'Italy',
	'JM' => 'Jamaica',
	'JP' => 'Japan',
	'JE' => 'Jersey',
	'JO' => 'Jordan',
	'KZ' => 'Kazakhstan',
	'KE' => 'Kenya',
	'KI' => 'Kiribati',
	'KR' => 'Korea',
	'KW' => 'Kuwait',
	'KG' => 'Kyrgyzstan',
	'LA' => 'Lao People\'s Democratic Republic',
	'LV' => 'Latvia',
	'LB' => 'Lebanon',
	'LS' => 'Lesotho',
	'LR' => 'Liberia',
	'LY' => 'Libyan Arab Jamahiriya',
	'LI' => 'Liechtenstein',
	'LT' => 'Lithuania',
	'LU' => 'Luxembourg',
	'MO' => 'Macao',
	'MK' => 'Macedonia',
	'MG' => 'Madagascar',
	'MW' => 'Malawi',
	'MY' => 'Malaysia',
	'MV' => 'Maldives',
	'ML' => 'Mali',
	'MT' => 'Malta',
	'MH' => 'Marshall Islands',
	'MQ' => 'Martinique',
	'MR' => 'Mauritania',
	'MU' => 'Mauritius',
	'YT' => 'Mayotte',
	'MX' => 'Mexico',
	'FM' => 'Micronesia, Federated States Of',
	'MD' => 'Moldova',
	'MC' => 'Monaco',
	'MN' => 'Mongolia',
	'ME' => 'Montenegro',
	'MS' => 'Montserrat',
	'MA' => 'Morocco',
	'MZ' => 'Mozambique',
	'MM' => 'Myanmar',
	'NA' => 'Namibia',
	'NR' => 'Nauru',
	'NP' => 'Nepal',
	'NL' => 'Netherlands',
	'AN' => 'Netherlands Antilles',
	'NC' => 'New Caledonia',
	'NZ' => 'New Zealand',
	'NI' => 'Nicaragua',
	'NE' => 'Niger',
	'NG' => 'Nigeria',
	'NU' => 'Niue',
	'NF' => 'Norfolk Island',
	'MP' => 'Northern Mariana Islands',
	'NO' => 'Norway',
	'OM' => 'Oman',
	'PK' => 'Pakistan',
	'PW' => 'Palau',
	'PS' => 'Palestinian Territory, Occupied',
	'PA' => 'Panama',
	'PG' => 'Papua New Guinea',
	'PY' => 'Paraguay',
	'PE' => 'Peru',
	'PH' => 'Philippines',
	'PN' => 'Pitcairn',
	'PL' => 'Poland',
	'PT' => 'Portugal',
	'PR' => 'Puerto Rico',
	'QA' => 'Qatar',
	'RE' => 'Reunion',
	'RO' => 'Romania',
	'RU' => 'Russian Federation',
	'RW' => 'Rwanda',
	'BL' => 'Saint Barthelemy',
	'SH' => 'Saint Helena',
	'KN' => 'Saint Kitts And Nevis',
	'LC' => 'Saint Lucia',
	'MF' => 'Saint Martin',
	'PM' => 'Saint Pierre And Miquelon',
	'VC' => 'Saint Vincent And Grenadines',
	'WS' => 'Samoa',
	'SM' => 'San Marino',
	'ST' => 'Sao Tome And Principe',
	'SA' => 'Saudi Arabia',
	'SN' => 'Senegal',
	'RS' => 'Serbia',
	'SC' => 'Seychelles',
	'SL' => 'Sierra Leone',
	'SG' => 'Singapore',
	'SK' => 'Slovakia',
	'SI' => 'Slovenia',
	'SB' => 'Solomon Islands',
	'SO' => 'Somalia',
	'ZA' => 'South Africa',
	'GS' => 'South Georgia And Sandwich Isl.',
	'ES' => 'Spain',
	'LK' => 'Sri Lanka',
	'SD' => 'Sudan',
	'SR' => 'Suriname',
	'SJ' => 'Svalbard And Jan Mayen',
	'SZ' => 'Swaziland',
	'SE' => 'Sweden',
	'CH' => 'Switzerland',
	'SY' => 'Syrian Arab Republic',
	'TW' => 'Taiwan',
	'TJ' => 'Tajikistan',
	'TZ' => 'Tanzania',
	'TH' => 'Thailand',
	'TL' => 'Timor-Leste',
	'TG' => 'Togo',
	'TK' => 'Tokelau',
	'TO' => 'Tonga',
	'TT' => 'Trinidad And Tobago',
	'TN' => 'Tunisia',
	'TR' => 'Turkey',
	'TM' => 'Turkmenistan',
	'TC' => 'Turks And Caicos Islands',
	'TV' => 'Tuvalu',
	'UG' => 'Uganda',
	'UA' => 'Ukraine',
	'AE' => 'United Arab Emirates',
	'GB' => 'United Kingdom',
	'US' => 'United States',
	'UM' => 'United States Outlying Islands',
	'UY' => 'Uruguay',
	'UZ' => 'Uzbekistan',
	'VU' => 'Vanuatu',
	'VE' => 'Venezuela',
	'VN' => 'Viet Nam',
	'VG' => 'Virgin Islands, British',
	'VI' => 'Virgin Islands, U.S.',
	'WF' => 'Wallis And Futuna',
	'EH' => 'Western Sahara',
	'YE' => 'Yemen',
	'ZM' => 'Zambia',
	'ZW' => 'Zimbabwe',
);
function get_country($code = '') {
	global $countries;
	if (preg_match("/^[a-z]+$/i",$code)) {
		return $countries[$code];
	}
}
if (!is_array($config['ontopic_apps'])) {
	$config['ontopic_apps'] = array('blogs','videos','polls');
}
if (is_array($lang_options)) {
	foreach ($lang_options as $key=>$val) {
		$langs[$key] = $val;
	}
}
function get_r($arr) {
    foreach ($arr as $val) {
		GLOBAL $$val;
		if (isset( $_REQUEST[$val])) {
			if (!is_array($_REQUEST[$val])) {
				if (get_magic_quotes_gpc())
					$$val = trim($_REQUEST[$val]);
				else
					$$val = addslashes(trim($_REQUEST[$val]));
			}
			else
				$$val = $_REQUEST[$val];
		}
	}
}
function utf8_substr($string, $max = 255){
   if(mb_strlen($string, 'utf-8') >= $max){
       $string = mb_substr($string, 0, $max, 'utf-8');
   } return $string;
}
// get table name
function table($name) {
	GLOBAL $db_info;
	return $db_info.$name;
}

function sys_break($msg) {
	echo $msg;
	die();
}

function sys_back($msg) {
	global $alerts, $app;
	c('<div class="notice">'.$msg.'<br /><br />
	&gt;&gt; <a href="javascript:history.go(-1);void(0);">'.t('Click here to go back').'</a>
	</div>');
	set_title('Redirecting');
	nav('Redirecting');
	clear_as();
	stop_here();
}

function set_title($val) {
	global $title;
	$title  = $val;
}
function set_page_title($val) {
	global $page_title;
	$page_title  = $val;
}


function clear_as() {
	global $clear_as;
	$clear_as = 1;
}

function jcow_access($role) {
	GLOBAL $client;
	if ($role == 2) {
		if ($client['id'] != 1) {
			return false;
		}
	}
}

function do_auth($roleids, $force_uid = 0, $msg = '') {
	global $client,$parr;
	if (!allow_access($roleids, $force_uid)) {
		if ($force_uid) {
			sys_back('Sorry, you have no permission to do this.');
		}
		if ($roleids == 2 || jcow_in_array(2,$roleids)) {
			if ($parr[0] != 'member') {
				set_return($_REQUEST['p']);
			}
			redirect('member/login/1');
		}
		if (!strlen($msg)) {
			$msg = t('Sorry, you have no permission to do this.');
		}
		if (!is_array($roleids)) {
			if (!$roleids) {
				$need_roles = '<br />'.t('The following roles have the permission').':<ul>
				<li>'.t('Super admin').'</li></ul>';
			}
		}
		else {
			if (!$roleids[0]) {
				$need_roles = '<br />'.t('The following roles have the permission').':<ul>
				<li>'.t('Super admin').'</li></ul>';
			}
			else {
				if (is_array($roleids)) {
					$where = ' where id in ('.implode(',',$roleids).')';
				}
				else {
					$where = ' where id='.$roleids;
				}
				$res = sql_query("select name from ".tb()."roles $where order by id");
				$need_roles = '<br />'.t('The following roles have the permission').':<ul>';
				while ($role = sql_fetch_array($res)) {
					$need_roles .= '<li>'.h($role['name']).'</li>';
				}
				$need_roles .= '</ul>';
			}
		}
		$res = sql_query("select name from ".tb()."roles where id in(".implode(',',$client['roles']).") order by id");
		$your_roles = '<br /><br />'.t('Your current roles').':<ul>';
		while ($role = sql_fetch_array($res)) {
			$your_roles .= '<li>'.h($role['name']).'</li>';
		}
		$your_roles .= '</ul>';
		sys_back($msg.'<br /><br />'.$need_roles.$your_roles);
	}
}

function full_url($link,$name = '',$target='',$gets=array()) {
	GLOBAL $client,$ubase, $uhome,$client,$parr,$bdarray;
	if ($client['id']) {
		if (in_array($link,$bdarray)) {
			$gets = array('bdid'=>$client['id']);
		}
	}
	if ('member/logout' == $link) {
		$gets['pin'] = substr($client['password'],-5,5);
	}
	if (preg_match("/^http/i",$link)) {
		$url = $link;
	}
	else {
		if (preg_match("/^account/i",$link) || preg_match("/^admin/i",$link) || preg_match("/^login/i",$link) || $link == 'logout') {
			$url = $uhome.'/index.php?p='.$link;
		}
		else {
			if (preg_match("/^u\//i",$link)) {
				$nofollow = ' rel="nofollow" ';
			}
			$url = $ubase.$link;
		}
	}
	if (is_array($gets) && count($gets) > 0) {
		foreach ($gets as $key=>$val) {
			$pars[] = $key.'='.urlencode($val);
		}
		$getss = implode('&',$pars);
		if (preg_match("/\?/",$url)) {
			$url = $url.'&'.$getss;
		}
		else {
			$url = $url.'?'.$getss;
		}
	}
	if ($name == 'ohno' || $name == '') {
		return $url;
	}
	if (preg_match("/delete/i",$link)) {
		$cfm = cfm();
	}
	if ($target)
		$target = ' target="'.$target.'"';
	return '<a href="'.$url.'"'.$target.$cfm.' '.$nofollow.' >'.$name.'</a>';
}

function is_mobile() {
	if ($_GET['agent'] == 'jcow_mobile' || $_POST['agent'] == 'jcow_mobile') {
		return true;
	}
	else return false;
}

function url($link,$name = '',$target='',$gets=array()) {
	GLOBAL $client,$ubase, $uhome,$client,$parr,$bdarray;
	if (is_mobile()) {
		$ubase = str_replace($uhome.'/','',$ubase);
	}
	if ($client['id']) {
		if (in_array($link,$bdarray)) {
			$gets = array('bdid'=>$client['id']);
		}
	}
	if ('member/logout' == $link) {
		$gets['pin'] = substr($client['password'],-5,5);
	}
	if (!preg_match("/index.php/",$link)) {
		if (preg_match("/^http/i",$link)) {
			$url = $link;
		}
		else {
			/*
			if (preg_match("/^account/i",$link) || preg_match("/^admin/i",$link) || preg_match("/^login/i",$link) || $link == 'logout') {
				$url = $ubase.'/index.php?p='.$link;
			}
			else {
				if (preg_match("/^u\//i",$link)) {
					$nofollow = ' rel="nofollow" ';
				}
				$url = $ubase.$link;
			}
			*/
			if (preg_match("/^u\//i",$link)) {
				$nofollow = ' rel="nofollow" ';
			}
			$url = $ubase.$link;
			/*
			if (preg_match("/^account/i",$link) || preg_match("/^admin/i",$link) || preg_match("/^login/i",$link) || $link == 'logout') {
				$url = 'index.php?p='.$link;
			}
			else {
				if (preg_match("/^u\//i",$link)) {
					$nofollow = ' rel="nofollow" ';
				}
				$url = $link;
			}*/
		}
		if (is_array($gets) && count($gets) > 0) {
			foreach ($gets as $key=>$val) {
				$pars[] = $key.'='.urlencode($val);
			}
			$getss = implode('&',$pars);
			if (preg_match("/\?/",$url)) {
				$url = $url.'&'.$getss;
			}
			else {
				$url = $url.'?'.$getss;
			}
		}
	}
	else {
		$url = $link;
	}
	if ($name == 'ohno' || $name == '') {
		return $url;
	}
	if (preg_match("/delete/i",$link)) {
		$cfm = cfm();
	}
	if ($target)
		$target = ' target="'.$target.'"';
	return '<a href="'.$url.'"'.$target.$cfm.' '.$nofollow.' >'.$name.'</a>';
}

function need_login_to_see_this() {
	global $client;
	if (!$client['id']) {
		return t('You need {1} to see this',url('member/login',t('login')));
	}
}
function name2profile($name) {
	return url('u/'.$name,$name);
}
function gurl($link,$name='',$igroup = '') {
	if (!$igroup) {
		global $group;
	}
	else {
		$group = $igroup;
	}
	if ($name == '') $name = 'ohno';
	return url('group/'.$group['uri'].'/'.$link,$name);
}

function button($link, $name, $cfm = '') {
	global $ubase, $buttons;
	if (preg_match("/delete/i",$link) || $cfm) {
		$cfm = cfm($cfm);
	}
	$buttons[] = '<a class="btn btn-primary btn-sm" href="'.$ubase.$link.'" '.$cfm.'>'.$name.'</a>';
}

function get_date($timeo, $type = 'time') {
	GLOBAL $settings, $client;
	$timeline = $timeo + $client['timezone']*3600;
	$current = time() + $client['timezone']*3600;
	$it_s = intval($current - $timeline);
	$it_m = intval($it_s/60);
	$it_h = intval($it_m/60);
	$it_d = intval($it_h/24);
	$it_y = intval($it_d/365);
	//9.3
	$timec = time() - $timeo;
	if ($timec < 3600 && $timec>=0) {
		return t('{1} mins ago',ceil($timec/60));
	}
	elseif ($timec < 12*3600 && $timec>=0) {
		return t('{1} hrs ago', ceil($timec/3600));
	}
	else {
		if ($type == 'time'){
			return gmdate($settings['date_format'].', '.$settings['time_format'],$timeline);
		}
		else {
			return gmdate($settings['date_format'],$timeline);
		}
	}


	if ($type == 'date'){
		return gmdate($settings['date_format'],$timeline);
	}
	else {
		if(gmdate("j",$timeline) == gmdate("j",$current)) {
			return $settings['date_today'].', '.gmdate($settings['time_format'],$timeline);
		}
		elseif(gmdate("j",$timeline) == gmdate("j",($current-3600*24) ) ) {
			return $settings['date_yesterday'].', '.gmdate($settings['time_format'],$timeline);
		}
		return gmdate($settings['date_format'].', '.$settings['time_format'],$timeline);
	}
}

function redirect($url, $message = 0) {
	global $ubase;
	$url = url($url);
	if (is_mobile()) {
		form_go($url);
	}
	clear_as();
	if (!preg_match("/\/\//i",$url)) {
		//$url = $ubase.$url;
	}
	if (!$message) {
		header("Location:$url");
		exit;
	}
	elseif ($message == 1) {
		if (preg_match("/index\.php/i",$url)) {
			$url = $url.'&succ=1';
		}
		else {
			$url = $url.'?succ=1';
		}
		header("Location:$url");
		exit;
		redirecting($url, t('Operation success'),'auto');
	}
	else {
		redirecting($url, $message);
	}
}

function autocompletetagjs() {// removed from 9.7
	return '';
	global $client;
	$topics = array();
	$res = sql_query("select * from ".tb()."topics where featured=1 limit 50");
	while ($topic = sql_fetch_array($res)) {
		$flag = '"'.addslashes($topic['name']).'"';
		$topics[] = $flag;
	}
	if ($client['id']) {
		$res = sql_query("select t.* from ".tb()."topics_followed as f left join ".tb()."topics as t on t.id=f.tid where f.uid='{$client['id']}' limit 50");
		while ($topic = sql_fetch_array($res)) {
			$flag = '"'.addslashes($topic['name']).'"';
			if (!in_array($flag,$topics)) {
				$topics[] = $flag;
			}
		}
		$topics = implode(',',$topics);
	}
	return '
	var availableTags = [
					'.$topics.'];
				function split(val) {
					return val.split(/,\s*/);
				}
				function extractLast(term) {
					return split(term).pop();
				}
				$("#tags")
					.bind("keydown",function(event){
						if (event.keyCode === $.ui.keyCode.TAB && $(this).data("ui-autocomplete").menu.active){
							event.preventDefault();
						}
					})
					.autocomplete({
						minLength:0,
						source: function(request,response) {
							response($.ui.autocomplete.filter(
								availableTags,extractLast( request.term)));
						},
						select:function(event,ui) {
							var terms = split(this.value);
							terms.pop();
							terms.push(ui.item.value);
							terms.push("");
							this.value = terms.join(", ");
							return false;
						}
					});
';
}

function redirecting($url, $message, $option = '') {
	global $alerts, $uhome, $auto_redirect, $config;
	$config['hide_ad'] = 1;
	if ($option == 'auto') {
		$auto_redirect = '<meta http-equiv="Refresh" content="1; url='.url($url).'" />';
		c('<div class="message"><p>'.$message.'</p>
		<i class="fa fa-2x fa-spinner fa-spin"></i> Now redirecting to: '.url($url,$url).
			'</div>');
	}
	else {
		c('<div class="message"><p>'.$message.'</p>
		&gt;&gt; '.url($url,t('Click here to go on')).
			'</div>');
	}
	stop_here();
}

function gen_nav() {
	global $nav;
	return implode(' <span class="sub">&gt;</span> ', $nav);
}

function valid_user($val, $type = 'id') {
	global $db;
	if ($type == 'id') {
		$res = sql_query("select * from `".tb()."accounts` where id='$val' ".dbhold() );
	}
	else {
		$res = sql_query("select * from `".tb()."accounts` where username='$val' ".dbhold() );
	}
	if (sql_counts($res)) {
		return sql_fetch_array($res);
	}
	else {
		return false;
	}
}



function ip() { 
	global $config;
	return $_SERVER["REMOTE_ADDR"];
} 


function markdown($text) {
	return Parsedown::instance()->setBreaksEnabled(true)->setMarkupEscaped(true)->text($text);
}
function get_rand($length, $possible = "0123456789abcdefghijklmnopqrstuvwxyz") { 
	srand((double)microtime()*1000000);
    $str = ""; 
    while(strlen($str) < $length) { 
        $str .= substr($possible, rand(0,50), 1); 
        } 
    return($str); 
} 


function t($str, $att1 = '', $att2 = '', $att3 = '',$godb = 0) {
	global $client, $lang_options, $config;
	if (!$config['disable_language']) {
		$dbstr = addslashes($str);
		$res = sql_query("select * from `".tb()."langs` where lang_from='$dbstr' and lang='{$client['lang']}' LIMIT 1");
		if (!sql_counts($res)) {
			sql_query("insert into `".tb()."langs` (lang_from,lang_to,lang) values('$dbstr','','{$client['lang']}')");
		}
		else {
			$row = sql_fetch_array($res);
			if (strlen($row['lang_to'])) {
				$str = $row['lang_to'];
			}
			else {
				$str = $row['lang_from'];
			}
		}
	}
	if ($godb) {
		$str = addslashes($str);
	}
	return str_replace(array('{1}','{2}','{3}'),array($att1,$att2,$att3), $str);
}


// 
function save_img($src, $ext='jpg', $target = '') {
	$dir = date("Ym",time());
	$s_folder = uploads.'/userfiles/'.$dir;
	if (!is_dir($s_folder))
		mkdir($s_folder, 0777);

	// check photo
	if ($target) {
		$photo_name = $target;
	}
	else {
		$photo_name = $s_folder.'/'.date("H_i",time()).'_'.get_rand(5).'.'.$ext;
	}
	if (preg_match("/^http/i",$src)) {
		if ($get_file = @file_get_contents($src)) {
			$fp = @fopen($photo_name,"w");
			@fwrite($fp,$get_file);
			@fclose($fp);
			return $photo_name;
		}
		else {
			return false;
		}
	}
	else {
		if (copy($src, $photo_name)) {
			return $photo_name;
		}
		else {
			return false;
		}
	}
}

function module_actived($module_name='') {
	global $current_modules;
	if ($current_modules[$module_name]['actived'])
		return true;
	else 
		return false;
}

// 上传文件
function save_file($file, $allowed_ext = array('jpg','png','gif')) {
	$ext = substr($file['name'],-3,3);
	$ext = strtolower($ext);
	if (!in_array($ext, $allowed_ext)) {
		return false;
	}
	$fname = date("H_i",time()).'_'.get_rand(5);
	$dir = date("Ym",time());
	$folder = uploads.'/userfiles/'.$dir;
	$uri = $folder.'/'.$fname.'.'.$ext;
	if (!is_dir($folder))
		mkdir($folder, 0777);
	if (copy($file['tmp_name'],$uri))
		return $uri;
	else {
		return false;
	}
}

function save_thumbnail($file, $width = 100, $height = 100,$c=0) {
	include_once('includes/libs/resizeimage.inc.php');
	$ext = substr($file['name'],-3,3);
	$ext = strtolower($ext);
	$allowed_ext = array('jpg','png','gif','png');
	if (!in_array($ext, $allowed_ext)) {
		die('Sorry, the pic type is uncorrect:'.$ext);
	}
	$fname = date("H_i",time()).'_'.get_rand(5);
	$dir = date("Ym",time());
	$folder = uploads.'/userfiles/'.$dir;
	$uri = $folder.'/'.$fname.'.'.$ext;
	if (!is_dir($folder))
		mkdir($folder, 0777);
	if ($width == '100') {
		$fill = 'white';
	}
	$resizeimage = new resizeimage($file['tmp_name'], $ext, $folder.'/'.$fname, $width, $height, $c, 80,$fill);
	return $folder.'/'.$fname.".".$resizeimage->type;
}

function jcow_cache_alert($keys = array(),$buffer = 0,$cache = '') {
	if (!$buffer) $buffer = get_gvar('jcow_cache_buffer');
	if (!$buffer) $buffer = 60;
	$timeline = time() + $buffer;
	if (!count($keys)) return false;
	$keys = implode("','",$keys);
	sql_query("update ".tb()."cache set expired='$timeline' where ckey in ('$keys') and expired>$timeline");
}



function cfm($msg = '') {
	if (!$msg)
		$msg = t('Are you sure to delete?');
	return ' onclick="return confirm(\''.$msg.'\');" ';
}

class PageBar
{
        public $total;        
        public $onepage;
        public $num;                        
        public $pagecount;
        public $total_page;
        public $offset;        
        public $linkhead;
		public $type_id;
		public $first = '';
		public $paras = "";
		public $prefix = "322_";

		public $next_page = 'Next';
		public $last_page = 'Prev';
		public $first_page = 'First';
		public $end_page = 'End';
    
        function __construct($total, $onepage, $pagecount)
        {
                $this->total      = $total;
                $this->onepage    = $onepage;
                $this->total_page = ceil($total/$onepage);
                if (empty($pagecount))
                {
                        $this->pagecount = 1;
                        $this->offset         = 0;        
                }
                else
                {
                        $this->pagecount = $pagecount;
                        $this->offset    = ($pagecount-1)*$onepage;
                }

                $linkarr = explode("pagecount=", $_SERVER['QUERY_STRING']);
                $linkft  = $linkarr[0];

                if (empty($linkft))
                {
                        $this->linkhead = $_SERVER['PHP_SELF']."?".$formlink;
                }
                else
                {
                        $linkft    = (substr($linkft, -1)=="&")?$linkft:$linkft."&";
                        $this->linkhead = $_SERVER['PHP_SELF']."?".$linkft.$formlink;
                }

        }
        function offset()
        {
                return $this->offset;
        }

        function pre_page($char='')
        {
                $linkhead  = $this->linkhead;
                $pagecount = $this->pagecount;
                if (empty($char))
                {
                        $char = $this->last_page;
                }

                if ($pagecount>1)
                {
                        $pre_page = $pagecount - 1;
						if ($pre_page == 1) {
							return " <a href=\"".$this->paras."\">$char</a> ";
						}
						else {
							return " <a href=\"".$this->paras."page=".$pre_page."\">$char</a> ";
						}
                }
                else
                {
                        return '';
                }

        }

        function next_page($char='')
        {
                $linkhead   = $this->linkhead;
                $total_page = $this->total_page;
                $pagecount  = $this->pagecount;
                if (empty($char))
                {
                        $char = $this->next_page;
                }
                if ($pagecount<$total_page)
                {
                        $next_page = $pagecount + 1;
                        return " <a href=\"".$this->paras."page=".$next_page."\">$char</a> ";
                }
                else
                {
                        return '';
                }
        }

        function num_bar($num='', $color='', $left='', $right='')
        {
                $num       = (empty($num))?10:$num;
                $this->num = $num;
                $mid       = floor($num/2);
                $last      = $num - 1; 
                $pagecount = $this->pagecount;
                $totalpage = $this->total_page;
                $linkhead  = $this->linkhead;
                $color     = (empty($color))?"#ff0000":$color;
                $minpage   = (($pagecount-$mid)<1)?1:($pagecount-$mid);
                $maxpage   = $minpage + $last;
                if ($maxpage>$totalpage)
                {
                        $maxpage = $totalpage;
                        $minpage = $maxpage - $last;
                        $minpage = ($minpage<1)?1:$minpage;
                }

                for ($i=$minpage; $i<=$maxpage; $i++)
                {
                        $char = $left.$i.$right;
                        if ($i==$pagecount)
                        {
                        $linkchar = "<strong>$char</strong>";
                        }
						elseif ($i == 1) {
							$linkchar = " <a href=\"".$this->paras."\">".$char."</a> ";
						}
						else{
                        //$linkchar = " <a href='".$this->prefix.$i.".htm'>".$char."</a> ";
                        $linkchar = " <a href=\"".$this->paras."page=$i\">".$char."</a> ";
						}
                        $linkbar  = $linkbar.$linkchar;
                }

                return $linkbar;
        }

        function pre_group($char='')
        {
                $pagecount   = $this->pagecount;
				if ($pagecount > 2)
					if ($this->first) {
						$content = " <a href=\"".$this->first."\">".$this->first_page."</a> ";
					}
					else {
						$content = " <a href=\"".$this->paras."\">".$this->first_page."</a> ";
					}
				else
					$content = "";
                return "";
        }

        function next_group($char='')
        {
                $pagecount = $this->pagecount;
                $linkhead  = $this->linkhead;
                $totalpage = $this->total_page;
				if ($pagecount < ($totalpage - 1))
					$content = " <a href=\"".$this->paras."page=".$totalpage."\">".$this->end_page."</a> ";
				else
					$content = "";
                return "";
        }

    function whole_num_bar($num='', $color='')
    {
		if ($this->total <= $this->onepage) {
			return '';
		}
		if (preg_match("/\?/i",$this->paras)) {
			$this->paras = $this->paras.'&';
		}
		else {
			$this->paras = $this->paras.'?';
		}

        $num_bar    = $this->num_bar($num, $color);
        $pre_group  = $this->pre_group();
        $pre_page   = $this->pre_page();
        $next_page  = $this->next_page();
        $next_group = $this->next_group();

            $pageb =  $pre_group.$pre_page.$num_bar.$next_page.$next_group;
			if ($pageb == '<strong>1</strong>')
				return "";
			else
				return '<div id="pager">'.$pageb.'</div>';
    }

}

function pager($offset,$got_next,$paras) {
	global $page;
	if ($offset>0) {
		if ($offset == 1) {
			$page_prev = '<a href="'.$paras.'">'.t('Prev').'</a>';
		}
		elseif (preg_match("/\?/",$paras)) {
			$page_prev = '<a href="'.$paras.'&page='.($page-1).'"><i class="fa fa-angle-left"></i> '.t('Prev').'</a>';
		}
		else {
			$page_prev = '<a href="'.$paras.'?page='.($page-1).'"><i class="fa fa-angle-left"></i> '.t('Prev').'</a>';
		}
	}
	if ($got_next) {
		if (preg_match("/\?/",$paras)) {
			$page_next = '<a href="'.$paras.'&page='.($page+1).'">'.t('Next').' <i class="fa fa-angle-right"></i></a>';
		}
		else {
			$page_next = '<a href="'.$paras.'?page='.($page+1).'">'.t('Next').' <i class="fa fa-angle-right"></i></a>';
		}
	}
	if ($offset>0 || $got_next) {
		return '<div style="width:100%;clear:both"></div>
		<div class="jcow_pager">'.$page_prev.' '.$page_next.'</div>';
	}
}
function hide_menubar() {
	global $config;
	$config['hide_menubar'] = 1;
}

function nav($foo) {
	global $nav;
	$nav[] = $foo;
}

function frd_request() {
	global $client;
	if (!$client['id']) {
		return false;
	}
	else {
		$res = sql_query("select count(*) as num from `".tb()."friend_reqs` where fid='{$client['id']}'");
		$row = sql_fetch_array($res);
		$frd_new = $row['num'] ? $row['num'] : '';
		if ($row['num']) {
			$link = url('friends/requests');
		}
		else {
			$link = url('friends');
		}
		return '<a href="'.$link.'" id="jcow_frd_link">'.t('Friends').' <span id="jcow_frd_new" class="jcow_note_num">'.$frd_new.'</span></a>';
	}
}
function  frd_unread() {
	global $client;
	if (!$client['id']) {
		return '';
	}
	else {
		$res = sql_query("select count(*) as num from `".tb()."friend_reqs` where fid='{$client['id']}'");
		$row = sql_fetch_array($res);
		if ($row['num']) {
			return '<span id="jcow_frd_new" class="jcow_note_num">'.$row['num'].'</span>';
		}
		else {
			return '';
		}
	}
}

function alert_verify() {
	global $client;
	if (!$client['id'] || !$client['disabled']) return '';
	return '<div style="padding:10px;">'.t('Your account is pending approval').'
		</div>';
	$res = sql_query("select * from ".tb()."requests where uid='{$client['id']}' limit 1");
	if (sql_counts($res)) {
		return '<div style="padding:10px;">'.t('Your account is pending approval').'
		</div>';
	}
	else {
		return '
		<a href="'.url('account/requestverify').'" style="display:block;padding:10px;font-size:1.2em;background:#FFF8DC;color:blue;">'.t('Please click Here to complete Verification').'</a>';
	}
}


function msg_unread() {
	global $client;
	if ($client['id']) {
		$res = sql_query("select count(*) as num from `".tb()."im` where to_id='{$client['id']}' and !hasread");
		$row = sql_fetch_array($res);
		$num = $row['num'];
		if (!$num) {
			return '<span id="jcow_msg_new" class="jcow_note_num" style="display:none"></span>';
		}
		else {
			return '<span id="jcow_msg_new" class="jcow_note_num">'.$num.'</span>';
		}
	}
	else {
		return '';
	}
}

function note_unread() {
	global $client;
	if ($client['id']) {
		$res = sql_query("select count(*) as num from `".tb()."notes` where uid='{$client['id']}' and hasread=0 order by created desc limit 100");
		$row = sql_fetch_array($res);
		$num = $row['num'];
		if (!$num) {
			return '<span id="jcow_note_new" class="jcow_note_num" style="display:none"></span>';
		}
		else {
			return '<span id="jcow_note_new" class="jcow_note_num">'.$num.'</span>';
		}
	}
	else {
		return '';
	}
}

function send_note($uid, $msg, $from_uid=0) {
	if ($GLOBALS['client']['id'] == $uid)
		return false;
	if (!is_numeric($from_uid)) $from_uid = 0;
	$note['from_uid'] = $from_uid;
	$note['message'] = safe($msg);
	$note['created'] = time();
	$note['uid'] = $uid;
	$res = sql_query("select id from jcow_notes where uid='$uid' order by created desc limit 100,1");
	$row = sql_fetch_array($res);
	if ($row['id']) {
		$note['id'] = $row['id'];
		sql_update($note,'jcow_notes');
	}
	else {
		sql_insert($note,tb().'notes');
	}
}

function set_text($key, $value) {
	$res = sql_query("select * from `".tb()."texts` where tkey='$key'");
	if (sql_counts($res)) {
		sql_query("update `".tb()."texts` SET tvalue='$value' WHERE tkey='$key'");
	}
	else {
		sql_query("insert into `".tb()."texts` (tkey,tvalue) values('$key','$value')");
	}
}

function get_text($key) {
	$res = sql_query("select * from `".tb()."texts` where tkey='$key' ");
	$row = sql_fetch_array($res);
	return $row['tvalue'];
}

function delete_text($key) {
	sql_query("delete from `".tb()."texts` where tkey='$key' ");
}

function avatar($row, $type = 'normal',$arr = array()) {
	global $uhome, $ubase;
	if (is_numeric($type) && $type == 25) {
		$type = 'small';
	}
	if ($row['uid']) {
		$row['id'] = $row['uid'];
	}
	if ($arr['nofollow']) {
		$nofollow = 'rel="nofollow"';
	}
	if (!$row['avatar']) {
		$cn = strlen($row['fullname'])%7;
		$arr['img'] = '<span class="">'.strtoupper(utf8_substr($row['fullname'],1)).'</span>';
		if ($type == 'small') {
			$sz = ' gi_sm ';
		}
		if ($type == 'large') {
			$sz = ' gi_lg';
		}
		if ($arr['nolink']) {
			return '<div class="gallery_img gbox_'.$cn.$sz.'">'.$arr['img'].'</div>';
		}
		else {
			return '<a  href="'.url('u/'.$row['username']).'" '.$nofollow.'><div class="gallery_img gbox_'.$cn.$sz.'">'.$arr['img'].'</div></a>';
		}
	}
	else {
		
		if (is_numeric($type) || $type != 'large') {
			$row['avatar'] = 's_'.$row['avatar'];
		}
		if (is_numeric($type)) {
			$csize = ' width="'.$type.'" height=auto ';
		}
		if ($type == 'small') {
			$csize = ' width="25" height=auto ';
		}
		if ($type == 'large' && !is_numeric($type)) {
			$csize = ' width="160" height=auto ';// add from 7.0
		}
		if ($arr['nolink']) {
			return '<img '.$csize.' src="'.$uhome.'/'.uploads.'/avatars/'.$row['avatar'].'" class="avatar"  />';
		}
		else {
			return '<a href="'.url('u/'.$row['username']).'"><img '.$csize.' src="'.$uhome.'/'.uploads.'/avatars/'.$row['avatar'].'" class="avatar"  '.$nofollow.' /></a>';
		}
	}
}
function gallery_box($arr = array()) {
	if (!$arr['img']) {
		$cn = strlen($arr['name'])%7;
		$arr['img'] = '<span class="">'.strtoupper(utf8_substr($arr['name'],1)).'</span>';
	}
	else  {
		$arr['img'] = '<img src="'.$arr['img'].'" style="min-height:100%;min-width:100%" />';
	}
	if ($arr['sm']) {
		$sm = ' gi_sm ';
	}
	if (!$arr['hide_name']) {
		return '<a class="gallery_box" href="'.$arr['link'].'"><div class="gallery_img gbox_'.$cn.$sm.'">'.$arr['img'].'</div>
	<div class="gallery_text">'.$arr['name'].'</div></a>';
	}
	else {
		return '<a  href="'.$arr['link'].'"><div class="gallery_img gbox_'.$cn.$sm.'">'.$arr['img'].'</div></a>';
	}
}
function listing_box($arr = array()) {
	if (!$arr['img']) {
		$cn = strlen($arr['name'])%7;
		$arr['img'] = '<span class="">'.strtoupper(utf8_substr($arr['name'],1)).'</span>';
	}
	else  {
		$arr['img'] = '<img src="'.$arr['img'].'" style="min-height:100%;min-width:100%" />';
	}
	return '<div class="listing_box"><a href="'.$arr['link'].'"><div class="listing_img gbox_'.$cn.'">'.$arr['img'].'</div></a>
	<div class="listing_title"><a href="'.$arr['link'].'">'.$arr['name'].'</a> '.$arr['opt'].'</div>
	<div class="listing_des">'.$arr['des'].'</div>
	<div class="listing_opt">'.$arr['opt'].'</div>
	</div>';
}
function page_logo($page, $type = 'small', $arr = array()) {
	global $uhome, $ubase;
	if ($page['logo'] == 'logo.jpg') {
		$page['logo'] = '';
	}
	if (!$page['logo']) {
		$cn = strlen($page['name'])%7;
		$arr['img'] = '<span class="">'.strtoupper(utf8_substr($page['name'],1)).'</span>';
		if (is_numeric($type)) $type = 'small';
		if ($type == 'small') {
			$sz = ' gi_sm ';
		}
		if ($type == 'large') {
			$sz = ' gi_lg';
		}
		if ($arr['link']) {
			return '<a  href="'.$arr['link'].'"><div class="gallery_img gbox_'.$cn.$sz.'">'.$arr['img'].'</div></a>';
		}
		else {
			if ($arr['nolink']) {
				return '<div class="gallery_img gbox_'.$cn.$sz.'">'.$arr['img'].'</div>';
			}
			else {
				return '<a  href="'.$ubase.$page['type'].'/'.$page['uri'].'"><div class="gallery_img gbox_'.$cn.$sz.'">'.$arr['img'].'</div></a>';
			}
		}
	}
	else {
		if ($type == 'small') {
			$page['logo'] = 's_'.$page['logo'];
		}
		
		if (is_numeric($type)) {
			$page['logo'] = 's_'.$page['logo'];
			$csize = ' width="'.$type.'" height="'.$type.'" ';
		}
		if ($type != 'small' && !is_numeric($type)) {
			$csize = ' width="160" height="160" ';
		}
		if ($arr['nolink']) {
			return '<img '.$csize.' src="'.$uhome.'/'.uploads.'/avatars/'.$page['logo'].'" class="avatar" />';
		}
		else {
			return '<a href="'.$ubase.$page['type'].'/'.$page['uri'].'"><img '.$csize.' src="'.$uhome.'/'.uploads.'/avatars/'.$page['logo'].'" class="avatar" /></a>';
		}
	}
}


function ubase() {
	global $ubase;
	return $ubase;
}
function uhome() {
	global $uhome;
	return $uhome;
}
function theme_folder() {
	global $theme_folder;
	return $theme_folder;
}

function get_friends($uid = 0) {
	global $client;
	if (!$uid)
		$uid = $client['id'];
	if (!$uid)
		return false;
	$res = sql_query("select fid from `".tb()."friends` where uid='$uid' LIMIT 10");
	while ($row = sql_fetch_array($res)) {
		$ids[] = $row['fid'];
	}
	return $ids;
}

function get_members() {
	$num = get_gvar('members_num');
	if (!is_numeric($num)) {
		$res = sql_query("select count(*) as num from jcow_accounts");
		$row = sql_fetch_array($res);
		set_gvar('members_num',$row['num']);
		return $row['num'];
	}
	else {
		return $num;
	}
}
// active sidebars
function ass($arr, $status = '') {
	block($arr, $status);
}

function enable_jcow_inline_ad() {
	global $config;
	$config['hide_ad'] = false;
}

function block($arr, $status = '') {
	global $blocks;
	if ($arr['box']) {
		$arr['content'] = '<div class="block_box">'.$arr['box'].'</div>'.$arr['content'];
	}
	if ($status == 'highlight') {
		$arr['highlight'] = 1;
	}
	$blocks[] = $arr;
}

function section($arr) {
	global $sections;
	$sections[] = $arr;
}


function section_content($content = '') {
	global $section_content;
	if (!strlen($content)) {
		return $section_content;
	}
	else {
		$section_content .= $content;
	}
}

function section_close($title = '',$arr = array()) {
	global $section_content;
	if (strlen($section_content)) {
		section(array('title'=>$title,'content'=>$section_content,'pure'=>$arr['pure']));
		$section_content = '';
	}
}

function sys_notice($notice) {
	global $notices;
	$notices[] = $notice;
}

function user_page_id($user) {
	if (!$user['id']) return false;
	if ($user['page_id']) return $user['page_id'];
	else {
		$res = sql_query("select id from ".tb()."pages where uid='{$user['id']}' and type='u'");
		$row = sql_fetch_array($res);
		return $row['id'];
	}
}

function client($key='') {
	global $client;
	if (!$key)
		return $client;
	else
		return $client[$key];
}

function jcow_mail($to,$subject,$message,$reply = '') {
	if (preg_match("/jcow\.sample/",$to)) return false;
	global $network,$uri,$config;
	if (!$reply)
		$reply = get_gvar('site_name').'<noreply@'.$_SERVER['HTTP_HOST'].'>';
	$headers = "From: $reply\r\n" .
			"MIME-Version: 1.0\r\n" .
			"Content-Type: text/html; charset=utf-8 \r\n" .
			"Content-Transfer-Encoding: 8bit\r\n\r\n";
	// Send
	if ($config['smtp_enabled']) {
		$smtp_enabled = 1;
		$smtp_host = $config['smtp_host'];
		$smtp_port = $config['smtp_port'];
		$smtp_username = $config['smtp_username'];
		$smtp_password = $config['smtp_password'];
		$domain = $config['mail_domain'];
		if (function_exists('jcow_send_mail')) {
			if (!jc_send_mail())
				return false;
		}
	}
	elseif (get_gvar('smtp_enabled')) {
		$smtp_enabled = 1;
		$smtp_host = get_gvar('smtp_host');
		$smtp_port = get_gvar('smtp_port');
		$smtp_username = get_gvar('smtp_username');
		$smtp_password = get_gvar('smtp_password');
		$domain = $_SERVER['HTTP_HOST'];
	}
	if ($smtp_enabled && strlen($smtp_host) && strlen($smtp_username) && strlen($smtp_port) && strlen($smtp_password)) {
		$mail = new PHPMailer();
		$mail->IsSMTP();
		$mail->CharSet = 'UTF-8';
		$mail->Host       = $smtp_host; // SMTP server example
		$mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
		$mail->SMTPAuth   = true;                  // enable SMTP authentication
		$mail->Port       = $smtp_port;                    // set the SMTP port for the GMAIL server
		$mail->Username   = $smtp_username; // SMTP account username example
		$mail->Password   = $smtp_password;
		$mail->setFrom('noreply@'.$domain, get_gvar('site_name'));
		$mail->addAddress($to, '');     // Add a recipient
		$mail->isHTML(true);                                  // Set email format to HTML

		$mail->Subject = $subject;
		$mail->Body    = '<div style="font-size:15px;padding:15px;margin:20px auto;background:white;color:#333">'.
		$message.'</div>
		<div style="font-size:12px;border-top:#eee 2px solid;padding:10px">
		'.t('This message was sent from {1}, if you don\'t want to receive these emails, please go to your account settings and unsubscribe.','<a href="'.uhome().'">'.h(get_gvar('site_name')).'</a>').'</div>';
		$mail->Timeout       =   10;
		if (!$mail->send()) {
			//echo "Mailer Error: " . $mail->ErrorInfo;exit;
		}
	}
	else {
		mail($to,$subject, $message,$headers);
		return false;
	}
}


function save_u_settings($arr) {
	global $client;
	if (!$client['id']) return false;
	foreach ($arr as $key=>$value) {
		$client['settings'][$key] = $value;
	}
	sql_query("update ".tb()."accounts set settings='".addslashes(serialize($client['settings']))."' where id='{$client['id']}'");
	return true;
}

function get_gvar($key) {
	global $gvars;
	return $gvars[$key];
}

function get_config($key) {
	global $config;
	return $config[$key];
}


function record_this_posting($message) {
	return true;
	// this function was disabled from 5.2.1
	/*
	if (get_gvar('autoban')) {
		global $client;
		if (!$autoban_acts = get_gvar('autoban_acts')) {
			$autoban_acts = 3;
		}
		if (!$autoban_trusted = get_gvar('autoban_trusted')) {
			$autoban_trusted = 30;
		}
		if (allow_access(3)) {
			return true;
		}
		if ((time() - $client['created']) > 3600*24*$autoban_trusted) {
			return true;
		}
		$hash = substr(md5(trim($message)),0,5);
		sql_query("insert into ".tb()."user_crafts (uid,hash,created) values('{$client['id']}','$hash',".time().")");
		$timeline = time() - 3600;
		$res = sql_query("select count(*) as num from ".tb()."user_crafts where uid='{$client['id']}' and hash='$hash' and created>$timeline");
		$row = sql_fetch_array($res);
		if ($row['num'] > $autoban_acts) {
			jcow_ban($client['ip'],$client['username'],time()+3600*24);
			sql_query("delete from ".tb()."user_crafts where uid='{$client['id']}'");
		}
	}
	*/
}

function jcow_ban($ip,$username,$expired,$operator='') {
	$ips = explode('.',$ip);
	sql_query("insert into ".tb()."banned (ip1,ip2,ip3,ip4,username,expired,created,operator)
	values('{$ips[0]}','{$ips[1]}','{$ips[2]}','{$ips[3]}','$username',$expired,".time().",'$operator')");
}

function set_gvar($key, $value = '') {
	global $gvars;
	if ($value == '') {
		sql_query("delete from `".tb()."gvars` WHERE gkey='$key'");
	}
	else {
		if (!isset($gvars[$key])) {
			sql_query("insert into `".tb()."gvars` (gkey,gvalue) values ('$key', '$value')");
		}
		else {
			sql_query("update `".tb()."gvars` set gvalue='$value' where gkey='$key'");
		}
	}
}

function set_tmp($key, $value = 'deleteit', $live = 1) {
	if ($value == 'deleteit') {
		sql_query("delete from `".tb()."tmp` where tkey='$key'");
	}
	else {
		$res = sql_query("select tkey from ".tb()."tmp where tkey='$key'  limit 1");
		if (sql_counts($res)) {
			sql_query("update ".tb()."tmp set tcontent='$value' where tkey='$key'");
		}
		else {
			sql_query("insert into `".tb()."tmp` (tkey,tcontent) values('$key','$value')");
		}
	}
}
function get_tmp($key, $opt = '') {
	$res = sql_query("select * from `".tb()."tmp` where tkey='$key'");
	$row = sql_fetch_array($res);
	if ($opt == 'delete') {
		sql_query("delete from `".tb()."tmp` where tkey='$key'");
	}
	return $row['tcontent'];
}


function set_cache($key, $value = '', $live = 0) {
	if (!$live) {
		$live = 1;
	}
	$value .= '<!-- jcow cache |'.$key.'| timestamp:'.time().' -->';
	$expired = time() + 3600*$live;
	$res = sql_query("select content from `".tb()."cache` where ckey='$key'");
	if (!sql_counts($res)) {
		sql_query("insert into ".tb()."cache (ckey,content,expired) values('$key','".addslashes($value)."',$expired)");
	}
	else {
		sql_query("update ".tb()."cache set content='".addslashes($value)."',expired=$expired where ckey='$key'");
	}
}
function get_cache($key) {
	$res = sql_query("select content from `".tb()."cache` where ckey='$key' and expired>".time());
	if (!sql_counts($res))
		return false;
	else {
		$row = sql_fetch_array($res);
		return $row['content'];
	}
}


function convert_blocks($content) {
	global $client;
	if ($client['id']) {
		$content = preg_replace("({guest}(.+){\/guest})",'',$content);
		$content = str_replace('{member}','',$content);
		$content = str_replace('{/member}','',$content);
	}
	else {
		$content = preg_replace("({member}(.+){\/member})",'',$content);
		$content = str_replace('{guest}','',$content);
		$content = str_replace('{/guest}','',$content);
	}
	return $content;
}

function user_post($row, $convert = 1, $simple = 0, $hide_avatar = 0) {
	// $convert: convert HTML or not
	global $user_post_i;
	$i = $user_post_i%2 + 1;
	$user_post_i++;
	if ($row['vote'] != 0) {
		if ($row['vote'] > 0) $row['vote'] = '+'.$row['vote'];
		$row['vote_msg'] = '('.$row['vote'].')';
	}
	if ($convert) {
		$row['content'] = nl2br(decode_bb(htmlspecialchars($row['content'])));
	}
	/*
	if ($row['sid'] && $row['app']) {
		$row['content'] = url($row['app'].'/viewstory/'.$row['sid'],'#'.h($row['stitle'])).'<br />'.$row['content'];
	}
	*/
	if (!$hide_avatar) $avatar = '<td class="user_post_left" width="42" valign="top">'.avatar($row,25).'</td>';
	if ($simple) {
		return '
		<div class="user_post_'.$i.'">
			<table>
			<tr>
			'.$avatar.'
			<td class="user_post_right" valign="top">'.url('u/'.$row['username'],$row['username']).
			' '.$row['vote_msg'].' <span class="sub">'.get_date($row['created']).'</span><br />'.$row['content'].' </td>
			</tr>
			</table>
		</div>
			';
	}
	else {
		if ($row['num']) {
			$row_num = '<span class="sub">#'.$row['num'].'</span> ';
		}
		return '
		<div class="user_post_'.$i.'">
			<table width="100%">
			<tr>
			<td class="user_post_left" width="60" valign="top">'.avatar($row).'</td>
			<td class="user_post_right" valign="top"><div class="user_post_head">'.$row_num.url('u/'.$row['username'],$row['username']).
			' '.$row['vote_msg'].' <span class="sub">'.get_date($row['created']).'</span></div>'.$row['content'].'</td>
			</tr>
			</table>
		</div>
			';
	}
}


function group_post($row, $type = 'summary') {
	// $convert: convert HTML or not
	global $client, $group;
	if (!$row['uri']) $row['uri'] = $group['uri'];
	$row['rname_label'] = $row['rname'];
	$row['username_label'] = $row['username'];
	$row['message'] = nl2br(decode_bb(htmlspecialchars($row['message'])));
	if ($type == 'summary') {
		$row['message'] .= '..<br />'.url('group/'.$row['uri'].'/viewpost/'.$row['id'],t('See more'));
		$row['message'] .= ' | '.url('group/'.$row['uri'].'/viewpost/'.$row['id'],t('Reply({1})',$row['replies']));
	}
	else {
		$row['message'] .= '<br />'.url('group/'.$row['uri'].'/viewpost/'.$row['id'],'Reply('.$row['replies'].')');
	}
	if ($row['tid']) {
		$topic = '<br />'.url('group/'.$row['uri'].'/viewtopic/'.$row['tid'],'#'.h($row['topic']) );
	}
	if ($row['rtid']) {
		$reply = url('group/'.$row['uri'].'/viewpost/'.$row['rtid'],'@'.h($row['rname_label']) );
	}
	if (!$hide_avatar) $avatar = '<td class="user_post_left" width="42" valign="top">'.avatar($row,50).'</td>';

		if ($row['num']) {
			$row_num = '<span class="sub">#'.$row['num'].'</span> ';
		}
		return '
		<div class="user_post_1">
			<table width="100%">
			<tr>
			<td class="user_post_left" width="62" valign="top">'.avatar($row).'</td>
			<td class="user_post_right" valign="top"><div class="user_post_head">'.$row_num.url('u/'.$row['username'],$row['username']).
			' '.$reply.' <span class="sub">'.get_date($row['created']).$topic.'</span></div>'
			.$row['message'].'
			</td>
			</tr>
			</table>
		</div>
			';

}

function convert_html($content) {
	global $config;
	return preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $content);
}

function tweet($status) {
	$username = get_gvar('twitter_username');
	$password = get_gvar('twitter_password');
	if (!strlen($username) || !strlen($password)) return false;
	if ($status) {
		$tweetUrl = 'http://www.twitter.com/statuses/update.xml';
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, "$tweetUrl");
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 2);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, "status=$status");
		curl_setopt($curl, CURLOPT_USERPWD, "$username:$password");
		$result = curl_exec($curl);
		$resultArray = curl_getinfo($curl);
		if ($resultArray['http_code'] == 200)
			return true;
		else
			return false;
		curl_close($curl);
	}
}


function var_cache($key, $value = 'none') {
	global $var_cache_live;
	$timeline = time() - $var_cache_live;
	if ($value == 'none') { // get
		$res = sql_query("select content from `".tb()."var_cache` where created>$timeline and name='$key' ORDER BY created DESC LIMIT 1");
		if (!sql_counts($res)) {
			return false;
		}
		else {
			$row = sql_fetch_array($res);
			return $row['content'];
		}
	}
	else { // set
		sql_query("insert into `".tb()."var_cache` (name,content,created) values ('$key', '$value','".time()."')");
	}
}

function h($str) {
	return htmlspecialchars($str,ENT_NOQUOTES);
}

function tb() {
	global $table_prefix;
	return $table_prefix;
}

function p2l($p) {
	if($p >= get_gvar('user_lv8'))
		return 8;
	if($p >= get_gvar('user_lv7'))
		return 7;
	if($p >= get_gvar('user_lv6'))
		return 6;
	if($p >= get_gvar('user_lv5'))
		return 5;
	if($p >= get_gvar('user_lv4'))
		return 4;
	if($p >= get_gvar('user_lv3'))
		return 3;
	if($p >= get_gvar('user_lv2'))
		return 2;
	else
		return 1;
}

function parseurl($msg) {
	return preg_replace(array("#http://([\S]+?)#Uis","#https://([\S]+?)#Uis"), array('<a rel="nofollow" href="http://\\1">\\1</a>','<a rel="nofollow" href="https://\\1">\\1</a>'), $msg);
        $search_array = array(
            "/([^\]]+)(http:\/\/.+)([\r\n\s]+)/isU"
		);
		$replace_array = array(
            "\\1[url]\\2[/url]\\3"
		);
		return preg_replace($search_array, $replace_array, $msg.' ');
}


function set_subtitle($title) {
	global $sub_title;
	$sub_title = $title;
}

function get_filesize($size) {
	if ($size < 1000) {
		return $size.' bytes';
	}
	elseif ($size < 1024*1024) {
		return ceil($size/1024).' kb';
	}
	elseif ($size < 1024*1024*1024) {
		return number_format($size/(1024*1024),2).' mb';
	}
	else {
		return number_format($size/(1024*1024*1024),2).' gb';
	}
}

function need_login() {
	global $client,$parr;
	if (!$client['id']) {
		if ($parr[0] != 'member') {
			set_return($_REQUEST['p']);
		}
		//include_once('modules/member/member.php');
		//member::login(1);
		//load_tpl();
		redirect('member/login/1');
	}
}

function jcow_in_array($str, $arr) {
	if (!is_array($arr))
		return false;
	if (in_array($str, $arr))
		return true;
	else
		return false;
}

function show_ad($key) {
	if ($croles = get_gvar('hide_ad_roles')) {
		$croles = explode('|',$croles);
		global $client;
		foreach ($client['roles'] as $role) {
			if (in_array($role, $croles)) {
				return '';
			}
		}
	}
	return get_gvar($key);
}

function nid() {
	global $network;
	return $network['id'];
}
function network($key) {
	global $network;
	return $network[$key];
}

function label($val) {
	return '<span class="form_label">'.$val.'</span>';
}

function gender($val) {
	if ($val == 1) {
		return t('Male');
	}
	elseif ($val == 0) {
		return t('Female');
	}
	else {
		return '<i>'.t('Hidden').'</i>';
	}
}

function get_age($birthyear, $hidden = 0,$birthmonth=1,$birthday=1) {
	if ($hidden)
		return '<i>'.t('Hidden').'</i>';
	$age = date("Y",time()) - $birthyear;
	if ($birthmonth > date("m",time()) || 
				($birthmonth == date("m",time())&& $birthday>date("d",time()) )
			){
				$age = $age-1;
			}
	return $age;
}


function check_menu_on($menu_item) {
	global $current_menu_path,$menu_items,$parr;
	$row = explode('/',$menu_item);
	$top_menu_item = $row[0];
	if (strlen($top_menu_item) && ( $menu_item == $current_menu_path || $top_menu_item.'/index' == $current_menu_path || $menu_item == $menu_items[$current_menu_path]['parent'])) {
		return ' class="menuon" ';
	}
	elseif (strlen($parr[1])) {
		$key = $parr[0].'/'.$parr[1];
		if (!$menu_items[$key] && $parr[0] == $menu_item) {
			return ' class="menuon" ';
		}
		else {
			return ' class="menugen" ';
		}
	}
	else {
		return ' class="menugen" ';
	}
}

function check_tabmenu_on($menu_item) {
	global $current_menu_path,$real_path,$parr,$defined_current_tab;
	if ($defined_current_tab) {
		$tab = $defined_current_tab;
	}
	elseif (strlen($real_path)) {
		$tab = $real_path;
	}
	else {
		$tab = $current_menu_path;
	}
	if (strlen($menu_item) && $menu_item == $tab) {
		return ' class="tm_on" ';
	}
	else {
		return ' class="tm_ge" ';
	}
}
function tabmenu_begin() {
	global $menu_items,$top_menu_path;
	if (strlen($menu_items[$top_menu_path]['tab_name'])) {
			return '<li '.check_tabmenu_on($menu_items[$top_menu_path]['path']).'>'.url($menu_items[$top_menu_path]['path'],t($menu_items[$top_menu_path]['tab_name'])).'</li>';
		}
	else {
		return '';
	}
}
function set_menu_path($path) {
	global $menuon;
	$menuon = $path;
}
function set_return($path) {
	setcookie('j_return_url', $path, time()+3600*24*365,"/");
}

function add_links($menu) {
	if ($name = get_gvar('cmi1_name')) {
		$menu[] = array('link'=>'<a href="'.get_gvar('cmi1_link').'"><div style="padding:3px 0 3px 23px;background:url('.uhome().'/files/appicons/links.png) 0 1px no-repeat">'.h($name).'</div></a>','name'=>h($name),'href'=>get_gvar('cmi1_link'));
	}
	if ($name = get_gvar('cmi2_name')) {
		$menu[] = array('link'=>'<a href="'.get_gvar('cmi2_link').'"><div style="padding:3px 0 3px 23px;background:url('.uhome().'/files/appicons/links.png) 0 1px no-repeat">'.h($name).'</div></a>','name'=>h($name),'href'=>get_gvar('cmi2_link'));
	}
	if ($name = get_gvar('cmi3_name')) {
		$menu[] = array('link'=>'<a href="'.get_gvar('cmi3_link').'"><div style="padding:3px 0 3px 23px;background:url('.uhome().'/files/appicons/links.png) 0 1px no-repeat">'.h($name).'</div></a>','name'=>h($name),'href'=>get_gvar('cmi3_link'));
	}
	if ($name = get_gvar('cmi4_name')) {
		$menu[] = array('link'=>'<a href="'.get_gvar('cmi4_link').'"><div style="padding:3px 0 3px 23px;background:url('.uhome().'/files/appicons/links.png) 0 1px no-repeat">'.h($name).'</div></a>','name'=>h($name),'href'=>get_gvar('cmi4_link'));
	}
	if ($name = get_gvar('cmi5_name')) {
		$menu[] = array('link'=>'<a href="'.get_gvar('cmi5_link').'"><div style="padding:3px 0 3px 23px;background:url('.uhome().'/files/appicons/links.png) 0 1px no-repeat">'.h($name).'</div></a>','name'=>h($name),'href'=>get_gvar('cmi5_link'));
	}
	return $menu;
}


function jversion() {
	global $version;
	return $version;
}

function show_rss($rss) {
	foreach ($rss['items'] as $item) {
		$items .= '
			<item>
			<title>'.$item['title'].'</title>
			<link>'.$item['link'].'</link>
			<pubDate>'.get_date($item['created']).'</pubDate>
			</item>
			';
	}

	return '<?xml version="1.0" encoding="UTF-8"?>
	<rss version="2.0"
		xmlns:content="http://purl.org/rss/1.0/modules/content/"
		xmlns:wfw="http://wellformedweb.org/CommentAPI/"
		xmlns:dc="http://purl.org/dc/elements/1.1/"
		xmlns:atom="http://www.w3.org/2005/Atom"
		xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
		xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
		xmlns:georss="http://www.georss.org/georss" xmlns:geo="http://www.w3.org/2003/01/geo/wgs84_pos#" xmlns:media="http://search.yahoo.com/mrss/"
		>

	<channel>
		<title>'.$rss['title'].'</title>
		<atom:link href="'.$rss['link'].'/feed" rel="self" type="application/rss+xml" />
		<link>'.$rss['link'].'</link>
		'.$items.'
	</channel>
	</rss>
	';
}

function limit_posting($onform = 0,$ajax = 0, $act='') {
	global $client;
	if ($client['disabled']) {
		if ($ajax) {
			echo '<span style="color:red;font-size:10px">'.t('Your account is not verified yet.').'</span>';
			exit;
		}
		else {
			c(alert_verify());
			stop_here();
		}
	}
	if (allow_access(3)) {
		return true;
	}
	if ($act == 'community_post') {
		if ($client['forum_posts'] == 30) {
			$timeline = time()-3600*24;
			$res = sql_query("select created from ".tb()."limit_posting where uid='{$client['id']}' and created>$timeline and act='community_post'");
			$row = sql_fetch_array($res);
			if ($row['created']) {
				$w_r = $row['created'] - $timeline;
				if ($w_r>3600) {
					$w_h = ceil($w_r/3600).' hour(s)';
				}
				else {
					$w_h = ceil($w_r/60).' minutes';
				}
				$error = t('Please wait').' '.$w_h;
			}
		}
	}
	if (!check_limit_posting()) {
		$error = t('You have exceeded the posting volume limit, please wait for a few hours, thanks');
	}
	elseif(!$onform){
		sql_query("insert into ".tb()."limit_posting(uid,created,act) values('{$client['id']}',".time().",'$act') ");
	}
	if (strlen($error)) {
		if ($ajax) {
			die($error);
		}
		else {
			sys_back($error);
		}
	}
}

function check_limit_posting() {
	global $client;
	if (!$client['limit_posting_exceed']) {
		return true;
	}
	else {
		return false;
	}
}


function clear_report() {
	global $config;
	$config['clear_report'] = 1;
}

function enreport() {
	global $config;
	$config['enreport'] = 1;
}

function is_redirecting() {
	global $auto_redirect;
	if (strlen($auto_redirect)) {
		return true;
	}
	else {
		return false;
	}
}

function timeselector($default = 0) {
	GLOBAL $settings, $client;
	if (!$default) {
		$default = time();
	}
	$default = $default + $client['timezone']*3600;
	$default = gmdate('g:i a',$default);
	$arr = explode(':',$default);
	$arr2 = explode(' ',$arr[1]);
	$hour = $arr[0];
	$ap =$arr2[1];
	$default = $hour.':30 '.$ap;
		
	$timearr = array('12:00 am','12:30 am','1:00 am','1:30 am','2:00 am','2:30 am','3:00 am','3:30 am','4:00 am','4:30 am','5:00 am','5:30 am','6:00 am','6:30 am','7:00 am','7:30 am','8:00 am','8:30 am','9:00 am','9:30 am','10:00 am','10:30 am','11:00 am','11:30 am','12:00 pm','12:30 pm','1:00 pm','1:30 pm','2:00 pm','2:30 pm','3:00 pm','3:30 pm','4:00 pm','4:30 pm','5:00 pm','5:30 pm','6:00 pm','6:30 pm','7:00 pm','7:30 pm','8:00 pm','8:30 pm','9:00 pm','9:30 pm','10:00 pm','10:30 pm','11:00 pm','11:30 pm');
	$output = '<select name="time">';
	foreach ($timearr as $val) {
		if ($default == $val) {
			$output .= '<option value="'.$val.'" selected>'.$val.'</option>';
		}
		else {
			$output .= '<option value="'.$val.'">'.$val.'</option>';
		}
	}
	$output .= '</select>';
	return $output;
}

function app_header($content) {
	global $app_header;
	$app_header .= $content;
}


function stream_form($preset='',$page = array(),$preset_tag = '',$arr=array()) {
	global $config ,$client,$miniblog_maximum;
	$share_apps = array('images','videos','events');
	if (!$client['id']) return false;
	if (!count($page)) {
		$page = array('id'=>$client['page']['id'],'type'=>'u','uid'=>$client['id']);
	}
	$photo_style = $video_style = 'style="display:none"';
	$status_att_button_on = ' att_button_on';
	if ($page['uid'] != $client['id']) {
		$opc = '('.t('Only page owner can see').')';
	}
	if ($page['uid'] == $client['id'] && ($page['type'] == 'u' || $page['type'] == 'page')) {
		$default_msg = t("What's on your mind?");
	}
	elseif ($page['type'] == 'group') {
		$default_msg = t('Write something..');
	}
	elseif ($page['type'] == 'u' || $page['type'] == 'page') {
		$default_msg = t('Write something..'.$opc);
	}
	else {
		return '';
	}
	if (!$_SESSION['mobile_style']) {
		$disable_fs = 'disabled';
	}

	$quick_apps = check_hooks('quick_share');
	$miniposttag = t('Status');
	if ($page['type'] == 'group') {
		$res = sql_query("select pid from ".tb()."page_users where pid='{$page['id']}' and uid='{$client['id']}'");
		if(sql_counts($res)) {
			$connected = 1;
		}
	}
	if ($client['avatar'] == 'undefined.jpg' || !$client['avatar']) {
		$client['avatar'] = '';
		$avatar = gallery_box(array('img'=>'','link'=>url('u/'.$client['username']),'name'=>h($client['fullname']),'hide_name'=>1));
	}
	else {
		$avatar = avatar($client);
	}
	if ($page['type'] == 'u' && $page['uid'] != $client['id']) {
		$res = sql_query("select username from ".tb()."accounts where id='{$page['uid']}'");
		$owner = sql_fetch_array($res);
		$target_page_id = $page['id'];
		$oncomment = 1;
		$miniposttag = t('Post');
	}
	elseif ($page['type'] == 'page') {
		if (!$page['logo']) {
			$avatar = gallery_box(array('img'=>'','link'=>url('page/'.$page['uri']),'name'=>h($page['name']),'hide_name'=>1));
		}
		else {
			$avatar = gallery_box(array('img'=>uploads.'/avatars/'.$page['logo'],'link'=>url('page/'.$page['uri']),'name'=>h($page['name']),'hide_name'=>1));
		}
		$oncomment = 0;
		$commentat = '';
		$target_page_id = $page['id'];
	}
	elseif ($page['type'] == 'group') {
		$miniposttag = t('Write post');
		$target_page_id = $page['id'];
	}
	else {
		$oncomment = 0;
		$commentat = '';
		$target_page_id = $page['id'];
	}

		
		
		$status_form = '<textarea name="message"  maxlength="'.$miniblog_maximum.'" class="form_message form-control" placeholder="'.$default_msg.'" ></textarea>';
		if (!is_numeric($preset)) {
			$hide_stream_box = 'style="display:none"';
		}
		else {
$minipost_field = '<a href="javascript:void();" class="att_button'.$status_att_button_on.'" id="stream_att_status_radio"><img src="'.uhome().'/files/appicons/status.png" align="absmiddle" /><span>'.$miniposttag.'</span></a>';
		}

		if ($page['type'] == 'u') {
			$res = sql_query("select p.* from jcow_page_users as pu left join jcow_pages as p on p.id=pu.pid where pu.uid='{$client['id']}' and p.type='group'");
			while ($page =  sql_fetch_array($res)) {
				$my_groups .= '<option value="'.$page['id'].'">'.h($page['name']).'</option>';
			}
			if ($my_groups) {
				$group_options .= '<optgroup label="'.t('My Groups').'">'.$my_groups.'</optgroup>';
			}
			$page_options = '<select name="home_page_id"   class="jcow_select show-menu-arrow fa home_page_id" data-style="btn-sm" data-width="fit" >
			<option  value="0"  selected>&#xf0ac; Public</option>
			<option value="friends" >&#xf21b; Friends only</option>
			'.$group_options.'
			</select>';
		}
		if (module_actived('videos')) {
			$video_btn = '<a href="#" class="jcow_sf_video_btn att_btn"><i class="fa fa-video-camera fa-lg"></i></a> ';
		}
		if (check_license()) {
			$checkin_btn = '<a href="#" class="jcow_sf_checkin_btn att_btn"><i class="fa fa-map-marker fa-lg"></i></a>';
		}
		$output = '
		<div class="jcow_share_box">
		<div class="jcow_share_box_content">
		<div id="error_box"></div>
			<div class="jcow_share_box_avatar" >'.$avatar.'</div>
			<div id="stream_box">
				<input type="hidden" class="page_id" value="'.$target_page_id.'" />
				<input type="hidden" name="oncomment" value="'.$oncomment.'" />
				<input type="hidden" name="page_type" value="'.$page['type'].'" />
				'.$status_form.'
				<div id="charsRemaining"></div>
				<input type="hidden" id="link_title" name="link_title" value="" /><input type="hidden" id="link_url" name="link_url" value="" /><input type="hidden" id="link_status" value="" /><input type="hidden" id="link_image" name="link_image" value="" /><input type="hidden" id="link_des" name="link_des" value="" />
				<div id="link_loading" ></div>
				<div id="link_remove" style="display:none;text-align:right"><a href="#" id="link_remove_btn"><i class="fa fa-remove"></i> '.t('Remove').'</a></div>
				<div id="link_preview" ></div>
				<div id="apps_box"></div>

				<div class="img_preview"></div>
				<div class="video_preview" style="display:none;padding-bottom:3px;"><input type="text" class="video_input form-control" placeholder="'.h('Youtube or Vimeo URL').'" /></div>
				<div class="checkin_preview" style="display:none">'.google_places(array(),'').'</div>
				<div style="height:3px"></div>
				<table border="0" width="99%">
				<tr>
				<td><a href="#" class="jcow_sf_img_btn att_btn"><i class="fa fa-camera fa-lg"></i></a> 
				'.$video_btn.'
				'.$checkin_btn.'</td>
				<td align="right">
				'.$page_options.'
				<input type="submit" value=" '.t('Post').' " class="btn btn-sm btn-primary form_submit" />
				</td>
				</tr>
				</table>
				<div style="display:none"><input type="file" class="jcow_sf_img" accept="image/*" multiple  /></div>
			</div>
		</div>
		<div class="spanstatus"></div>
		</div>';
		return $output;
		}

function hide_ad() {
	global $config;
	$config['hide_ad'] = 1;
}

function reply_form($stream_id, $replyto = '') {
	global $config, $client;
	if (!$replyto) return '';
	if (!check_limit_posting()) {
		return '';
	}
	if (!$client['id']) {
		return '';
	}
	$likeflag = t('Like');
	$res = sql_query("select s.id,u.username,s.uid,p.type as page_type,p.uid as page_uid from ".tb()."streams as s left join ".tb()."accounts as u on u.id=s.uid left join ".tb()."pages as p on p.id=s.wall_id where s.id='$stream_id'");
	$stream = sql_fetch_array($res);
	if (!$stream['id']) return '';
	$flag = t('Reply');
	$likeit = '<span> <a href="#" class="dolike" rel="nofollow">'.$likeflag.'</a></span>';
	if ($stream['page_type'] == 'page' && $stream['page_uid'] == $stream['uid']) {
		$stream['username'] = '';
		}
	
	if ($flag == 'none') {
		$comment_link = '<span></span>';
	}
	else {
		if ($no_like) {
			$comment_link = '<a href="#" class="quick_comment" uname="'.$stream['username'].'">+'.$flag.'</a>';
		}
		else {
			$comment_link = '';
			//$comment_link = '<a href="#" class="quick_comment" uname="'.$stream['username'].'">+'.$flag.'</a>';
		}
	}

		return '
		<div>
		'.$comment_link.' <span></span>
			<div class="quick_comment_form" style="display:none;">
				<table border="0"><tr><td valign="top">
				<img src="'.uhome().'/'.uploads.'/avatars/s_'.$client['avatar'].'" width="25" height="25" />
				</td><td>
				<input name="message" rows="2" maxlength="140" class="fpost commentmessage"  style="width:400px;" value="" />
				</td></tr>
				</table>
			</div>
			<input type="hidden" name="wall_id" value="'.$stream_id.'" />
			<div></div>
		</div>';
}

function comment_form($stream_id, $flag = '', $likeflag = '',$arr=array()) {
	global $config, $client,$parr;

	if (!$client['id']) {
		$comment_link = url('member/login','<i class="fa fa-user-plus"></i> '.t('Login to comment'));
	}
	$res = sql_query("select s.*,u.username,p.type as page_type,p.logo as page_logo,p.uid as page_uid,p.uri as page_uri,p.name as page_name from ".tb()."streams as s left join ".tb()."accounts as u on u.id=s.uid left join ".tb()."pages as p on p.id=s.wall_id where s.id='$stream_id'");
		$stream = sql_fetch_array($res);
	if (!$stream['id']) return '';

	if (in_array($stream['app'],array('texts','blogs','videos','images','events'))) {
		if (!get_gvar('private_network') && $stream['page_type'] != 'group') {
			$out_share = ' <a href="#" class="doshare" onclick="jQuery.facebox({ ajax: \''.url('jquery/out_share/'.$stream['aid']).'\' });return false;" ><i class="fa fa-share-alt"></i>  '.
				t('Share').
				'</a> &nbsp; ';
		}
	}

	if ($client['id']) {
		if ($stream['page_type'] == 'group') {
			$res = sql_query("select * from jcow_page_users where pid='{$stream['wall_id']}' and uid='{$client['id']}'");
			if (!sql_counts($res)) {
				return '<div class="jcow_comment_box">
				'.$out_share.'</div>';
			}
		}
		if (!check_limit_posting()) {
			return '';
		}
		if (!$flag) $flag = t('Comment');
		if (!$arr['disable_like']) {
			if (strlen($likeflag)<2) {
				$likeflag = t('Like');
			}
			$likeit = ' <span> <a href="#" class="dolike" rel="nofollow"><i class="fa fa-thumbs-o-up"></i> '.$likeflag.'</a> &nbsp; </span>';
			$res = sql_query("select * from ".tb()."liked where stream_id='$stream_id' and uid='{$client['id']}' limit 1");
			if (sql_counts($res)) {
				$likeit = ' <span><a href="#" class="dolike" rel="nofollow"><i class="fa fa-thumbs-o-down"></i> '.t('Undo-like').'</a></span>';
			}
		}
		if ($client['avatar'] == 'undefined.jpg' || !$client['avatar']) {
			$avatar = gallery_box(array('img'=>'','link'=>url('u/'.$client['username']),'name'=>h($client['fullname']),'hide_name'=>1,'sm'=>1));
		}
		else {
			$avatar = gallery_box(array('img'=>uploads.'/avatars/s_'.$client['avatar'],'link'=>url('u/'.$client['username']),'name'=>h($client['fullname']),'hide_name'=>1,'sm'=>1));
		}
		if ($stream['page_type'] == 'page' && $stream['page_uid'] == $client['id']) {
			if (!strlen($stream['page_logo'])) {
				$avatar = gallery_box(array('img'=>'','link'=>url('page/'.$stream['page_uri']),'name'=>h($stream['page_name']),'hide_name'=>1,'sm'=>1));
			}
			else {
				$avatar = gallery_box(array('img'=>uploads.'/avatars/s_'.$stream['page_logo'],'link'=>url('page/'.$stream['page_uri']),'name'=>h($stream['page_name']),'hide_name'=>1,'sm'=>1));
			}
			$tag_user = '';
		}
		else {
			$tag_user = $stream['username'];
		}
		if ($flag == 'none') {
			$comment_link = '<span></span>';
		}
		else {
			$comment_link = '<a href="#" class="quick_comment" uname="'.$tag_user.'"><i class="fa fa-pencil-square-o"></i> '.$flag.'</a> &nbsp; ';
		}
		$commentpic_btn = '<a tabindex="0" href="#" class="commentpic_btn" ><i class="fa fa-camera"></i></a><div style="display:none"><input type="file" class="cmt_img" /></div><div class="cmt_img_preview"></div>';
		$comment_form = '<div class="quick_comment_form">
				<table border="0" width="100%"><tr><td valign="top" width="30">
				'.$avatar.'
				</td><td>
				<input name="message"  maxlength="140" class="commentmessage"  style="line-height:1.7;width:100%" placeholder="'.t('Write a comment').'" uname="'.$tag_user.'" data-role="none" />'.$commentpic_btn.'


				</td></tr>
				</table>
			</div>';
		if (module_actived('ads')) {
			if ($client['id'] == $stream['uid'] && !$stream['type'] && $stream['page_type'] != 'group' && $parr[0] != 'ads' && !$arr['boosted'] && !is_mobile()) {
				$boost_btn = '<a href="'.url('ads/boostpost/'.$stream_id).'" class="boost_btn"><i class="fa fa-bolt"></i> '.t('Boost post').'</a> ';
			}
		}
	}
		return '
		<div class="jcow_comment_box">
		'.$comment_form.'
			<input type="hidden" name="wall_id" value="'.$stream_id.'" />
			<div></div>
		</div>';
}

function story_opt($stream_id, $flag = '', $likeflag = '',$arr=array()) {
	if (is_mobile()) {
		return story_opt_app($stream_id, $flag, $likeflag,$arr );
	}
	else {
		return story_opt_web($stream_id, $flag, $likeflag,$arr );
	}
}
function story_opt_app($stream_id, $flag = '', $likeflag = '',$arr=array()) {
	global $config, $client,$parr;

	$res = sql_query("select s.*,u.username,p.type as page_type,p.logo as page_logo,p.uid as page_uid,p.uri as page_uri,p.name as page_name from ".tb()."streams as s left join ".tb()."accounts as u on u.id=s.uid left join ".tb()."pages as p on p.id=s.wall_id where s.id='$stream_id'");
		$stream = sql_fetch_array($res);
	if (!$stream['id']) return '';

	// comments number
	$res = sql_query("select count(*) as num from jcow_streams where stuff_id='{$stream['id']}'");
	$row = sql_fetch_array($res);
	if ($row['num']>3) {
		$stream_comments = '<div style="float:right;padding:3px"><a href="/feed/view/'.$stream['id'].'" >'.t('{1} comments',$row['num']).'</a></div>';
	}
	if ($row['num'] > 0) {
		$comment_num = ' '.$row['num'];
	}
	// likes number
	$res = sql_query("select count(*) as num from ".tb()."liked where stream_id='$stream_id' ");
	$row = sql_fetch_array($res);
	if ($row['num'] > 0) {
		$like_num = ' '.$row['num'];
	}

	if (in_array($stream['app'],array('texts','blogs','videos','images','events'))) {
		if (!get_gvar('private_network') && $stream['page_type'] != 'group') {
			$storylink = uhome().'/index.php?p='.$stream['app'].'/viewstory/'.$stream['aid'];
			$out_share = ' <a href="javascript:void(0)" class="appdoshare" style="display:block;flex-grow:1;text-align:center;padding:5px;border:#eee 1px solid" storylink="'.$storylink.'" fblink="https://www.facebook.com/sharer/sharer.php?u='.urlencode($storylink).'" aid="'.$stream['aid'].'" ><i class="fa fa-share-alt fa-lg"></i></a>';
		}
	}

	if ($client['id']) {
		if ($stream['page_type'] == 'group') {
			$res = sql_query("select * from jcow_page_users where pid='{$stream['wall_id']}' and uid='{$client['id']}'");
			if (!sql_counts($res)) {
				return '<div class="jcow_comment_box">
				'.$out_share.'</div>';
			}
		}
		if (!check_limit_posting()) {
			return '';
		}
		if (!$flag) $flag = t('Comment');
		if (!$arr['disable_like']) {
			if (strlen($likeflag)<2) {
				$likeflag = t('Like');
			}
			$likeit = ' <span style="display:block;flex-grow:1;text-align:center;padding:5px;border:#eee 1px solid"><a href="#" style="display:block" class="dolike" sid="'.$stream['id'].'" rel="nofollow"><i class="fa fa-thumbs-o-up fa-lg"></i>'.$like_num.'</a></span>';
			$res = sql_query("select * from ".tb()."liked where stream_id='$stream_id' and uid='{$client['id']}' limit 1");
			if (sql_counts($res)) {
				$likeit = ' <span style="display:block;flex-grow:1;text-align:center;padding:5px;border:#eee 1px solid"><a href="#" style="display:block" class="dolike" sid="'.$stream['id'].'" rel="nofollow"><i class="fa fa-thumbs-up fa-lg"></i>'.$like_num.'</a></span>';
			}
		}
		if ($client['avatar'] == 'undefined.jpg' || !$client['avatar']) {
			$avatar = gallery_box(array('img'=>'','link'=>url('u/'.$client['username']),'name'=>h($client['fullname']),'hide_name'=>1,'sm'=>1));
		}
		else {
			$avatar = gallery_box(array('img'=>uploads.'/avatars/s_'.$client['avatar'],'link'=>url('u/'.$client['username']),'name'=>h($client['fullname']),'hide_name'=>1,'sm'=>1));
		}
		if ($stream['page_type'] == 'page' && $stream['page_uid'] == $client['id']) {
			if (!strlen($stream['page_logo'])) {
				$avatar = gallery_box(array('img'=>'','link'=>url('page/'.$stream['page_uri']),'name'=>h($stream['page_name']),'hide_name'=>1,'sm'=>1));
			}
			else {
				$avatar = gallery_box(array('img'=>uploads.'/avatars/s_'.$stream['page_logo'],'link'=>url('page/'.$stream['page_uri']),'name'=>h($stream['page_name']),'hide_name'=>1,'sm'=>1));
			}
			$tag_user = '';
		}
		else {
			$tag_user = $stream['username'];
		}
		if ($flag == 'none') {
			$comment_link = '<span></span>';
		}
		else {
			$comment_link = '<a style="display:block;flex-grow:1;text-align:center;padding:5px;border:#eee 1px solid" href="#" class="quick_comment" uname="'.$tag_user.'"><i class="fa fa-comment-o fa-lg"></i>'.$comment_num.'</a>';
		}
		$commentpic_btn = '<a tabindex="0" href="#" class="commentpic_btn" ><i class="fa fa-camera"></i></a><div style="display:none"><input type="file" class="cmt_img" /></div><div class="cmt_img_preview"></div>';
		$comment_form = '<div class="quick_comment_form">
				<table border="0" width="100%"><tr><td valign="top" width="30">
				'.$avatar.'
				</td><td>
				<input name="message"  maxlength="140" class="commentmessage"  style="line-height:1.7;width:100%;" placeholder="'.t('Write a comment').'" uname="'.$tag_user.'" data-role="none" />'.$commentpic_btn.'
				</td></tr>
				</table>
			</div>';
		if (module_actived('ads')) {
			if ($client['id'] == $stream['uid'] && !$stream['type'] && $stream['page_type'] != 'group' && $parr[0] != 'ads' && !$arr['boosted'] && !is_mobile()) {
				$boost_btn = '<a href="'.url('ads/boostpost/'.$stream_id).'" class="boost_btn"><i class="fa fa-bolt"></i> '.t('Boost post').'</a> ';
			}
		}
	}
	
	return '<div style="overflow:hidden">'.$boost_btn.$stream_comments.'</div>
	<div style="margin:3px 0;display:flex;flex-direction:row;justify-content:space-evenly;font-size:18px;background:white;">'.$likeit.' '.$comment_link.' '.$out_share.'</div>';
}

function story_opt_web($stream_id, $flag = '', $likeflag = '',$arr=array()) {
	global $config, $client,$parr;
	$res = sql_query("select s.*,u.username,p.type as page_type,p.logo as page_logo,p.uid as page_uid,p.uri as page_uri,p.name as page_name from ".tb()."streams as s left join ".tb()."accounts as u on u.id=s.uid left join ".tb()."pages as p on p.id=s.wall_id where s.id='$stream_id'");
		$stream = sql_fetch_array($res);
	if (!$stream['id']) return '';

	if (in_array($stream['app'],array('texts','blogs','videos','images','events'))) {
		if (!get_gvar('private_network') && $stream['page_type'] != 'group') {
			$out_share = ' <a href="#" class="doshare" onclick="jQuery.facebox({ ajax: \''.url('jquery/out_share/'.$stream['aid']).'\' });return false;" ><i class="fa fa-share-alt"></i></a> &nbsp; ';
		}
	}

	if ($client['id']) {
		if ($stream['page_type'] == 'group') {
			$res = sql_query("select * from jcow_page_users where pid='{$stream['wall_id']}' and uid='{$client['id']}'");
			if (!sql_counts($res)) {
				return '<div class="jcow_comment_box">
				'.$out_share.'</div>';
			}
		}
		if (!check_limit_posting()) {
			return '';
		}
		if (!$flag) $flag = t('Comment');
		if (!$arr['disable_like']) {
			if (strlen($likeflag)<2) {
				$likeflag = t('Like');
			}
			$likeit = ' <span> <a href="#" class="dolike" sid="'.$stream['id'].'" rel="nofollow"><i class="fa fa-lg fa-thumbs-o-up"></i> '.t('Like').'</a> &nbsp; </span>';
			$res = sql_query("select * from ".tb()."liked where stream_id='$stream_id' and uid='{$client['id']}' limit 1");
			if (sql_counts($res)) {
				$likeit = ' <span><a href="#" class="dolike" sid="'.$stream['id'].'" rel="nofollow"><i class="fa fa-lg fa-thumbs-up"></i></a></span>';
			}
		}
		if ($client['avatar'] == 'undefined.jpg' || !$client['avatar']) {
			$avatar = gallery_box(array('img'=>'','link'=>url('u/'.$client['username']),'name'=>h($client['fullname']),'hide_name'=>1,'sm'=>1));
		}
		else {
			$avatar = gallery_box(array('img'=>uploads.'/avatars/s_'.$client['avatar'],'link'=>url('u/'.$client['username']),'name'=>h($client['fullname']),'hide_name'=>1,'sm'=>1));
		}
		if ($stream['page_type'] == 'page' && $stream['page_uid'] == $client['id']) {
			if (!strlen($stream['page_logo'])) {
				$avatar = gallery_box(array('img'=>'','link'=>url('page/'.$stream['page_uri']),'name'=>h($stream['page_name']),'hide_name'=>1,'sm'=>1));
			}
			else {
				$avatar = gallery_box(array('img'=>uploads.'/avatars/s_'.$stream['page_logo'],'link'=>url('page/'.$stream['page_uri']),'name'=>h($stream['page_name']),'hide_name'=>1,'sm'=>1));
			}
			$tag_user = '';
		}
		else {
			$tag_user = $stream['username'];
		}
		if ($flag == 'none') {
			$comment_link = '<span></span>';
		}
		else {
			$comment_link = '<a href="#" class="quick_comment" uname="'.$tag_user.'"><i class="fa fa-lg fa-pencil-square-o"></i> '.t('Comment').'</a>';
		}
		$commentpic_btn = '<a tabindex="0" href="#" class="commentpic_btn" ><i class="fa fa-camera"></i></a><div style="display:none"><input type="file" class="cmt_img" /></div><div class="cmt_img_preview"></div>';
		$comment_form = '<div class="quick_comment_form">
				<table border="0" width="100%"><tr><td valign="top" width="30">
				'.$avatar.'
				</td><td>
				<input name="message"  maxlength="140" class="commentmessage"  style="line-height:1.7;width:100%" placeholder="'.t('Write a comment').'" uname="'.$tag_user.'" data-role="none" />'.$commentpic_btn.'


				</td></tr>
				</table>
			</div>';
		if (module_actived('ads')) {
			if ($client['id'] == $stream['uid'] && !$stream['type'] && $stream['page_type'] != 'group' && $parr[0] != 'ads' && !$arr['boosted'] && !is_mobile()) {
				$boost_btn = '<a href="'.url('ads/boostpost/'.$stream_id).'" class="boost_btn"><i class="fa fa-bolt"></i> '.t('Boost post').'</a> ';
			}
		}
	}
	// comments number
	$res = sql_query("select count(*) as num from jcow_streams where stuff_id='{$stream['id']}'");
	$row = sql_fetch_array($res);
	if ($row['num']>3) {
		$stream_comments = '<div style="float:right;padding:3px"><a href="/feed/view/'.$stream['id'].'" >'.t('{1} comments',$row['num']).'</a></div>';
	}
	$url = urlencode(url('feed/view/'.$stream['id']));
	if ($client['id'] && $stream['page_type'] != 'group') {
		$share_btn = '<div class="dropdown">
		<a href="#" class="share_btn dropdown-toggle" data-toggle="dropdown"><i class="fa fa-lg fa-share"></i> '.t('Share').'</a>
		<ul class="dropdown-menu dropdown-menu-right">
		<li><a href="#" class="repost_profile" sid="'.$stream['id'].'"><i class="fa fa-share"></i> '.t('Share').'..</a></li>
		<li><a href="https://pinterest.com/pin/create/button/?url='.$url.'" target="_blank"><i class="fa fa-pinterest"></i> Pinterest</a></li>
		<li><a href="https://twitter.com/intent/tweet?text='.$url.'" target="_blank"><i class="fa fa-twitter"></i> Twitter</a></li>
		<li><a href="https://www.facebook.com/sharer/sharer.php?u='.$url.'" target="_blank"><i class="fa fa-facebook"></i> Facebook</a></li>
		</ul>
		</div>';
	}
	return '<div style="overflow:hidden">'.$boost_btn.$stream_comments.'</div>
	<div style="font-size:15px;display:flex;flex-direction:row;justify-content:space-around;margin-top:3px;padding:5px;border:#eee 1px solid;border-width:1px 0 1px 0">'.$likeit.$comment_link.$share_btn.'</div>';
}

function story_like_form($stream_id,$arr=array(),$story=array()) {
	global $config, $client,$parr;
	if (!$client['id']) return '';
	if ($parr[0] == 'streampublish') {
		return '';
		}
	if (!check_limit_posting()) {
		return '';
	}
	if (!$client['id']) {
		return '';
	}
	$res = sql_query("select s.*,u.username,p.type as page_type,p.logo as page_logo,p.uid as page_uid from ".tb()."streams as s left join ".tb()."accounts as u on u.id=s.uid left join ".tb()."pages as p on p.id=s.wall_id where s.id='$stream_id'");
	$stream = sql_fetch_array($res);
	if (!$stream['id']) return '';
	$likes_num = $stream['likes'] ? '('.$stream['likes'].')':'';
	$dislikes_num = $stream['dislikes'] ? '('.$stream['dislikes'].')':'';
	$buttons = '<span>
	<a href="#" class="dolike" sid="'.$stream_id.'" rel="nofollow"><i class="fa fa-thumbs-o-up"></i>  '.t('Like').$likes_num.'</a>
	 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	 <a href="#" class="dodislike"><i class="fa fa-thumbs-o-down"></i> '.t('Dislike').$dislikes_num.'</a></span>';

	$res = sql_query("select * from ".tb()."liked where stream_id='$stream_id' and uid='{$client['id']}' limit 1");
	if (sql_counts($res)) {
		$buttons = '<span><a href="#" class="dolike" sid="'.$stream_id.'" rel="nofollow">'.t('Unlike').'</a></span>';
		$like_status = t('Likes').':'.$stream['likes'].', '.t('Dislikes').':'.$stream['dislikes'];
	}
	$res = sql_query("select * from ".tb()."disliked where stream_id='$stream_id' and uid='{$client['id']}' limit 1");
	if (sql_counts($res)) {
		$buttons = '<span><a href="#" class="dodislike">'.t('Undislike').'</a></span>';
		$like_status = t('Likes').':'.$stream['likes'].', '.t('Dislikes').':'.$stream['dislikes'];
	}
	if ($arr['reply_link']) {
		$reply_b = '&nbsp;&nbsp&nbsp&nbsp <a href="#" username="'.$stream['username'].'" class="reply_comment">Reply</a>';
	}
	if ($arr['comment_link']) {
		$reply_b = '&nbsp;&nbsp&nbsp&nbsp <a href="'.url($stream['app'].'/viewstory/'.$stream['aid']).'">'.t('Comments').' <span class="sub">('.$story['comments'].')</span></a>';
	}
		return '
		<div class="jcow_comment_box">
		'.$buttons.'
		<span>'.$like_status.$reply_b.'</span><input type="hidden" name="wall_id" value="'.$stream_id.'" />
			<div></div>
		</div>';
	}

function profile_comment_form($target_id) {
	global $config ,$client;
	if (!$client['id']) return false;
	return 
		'
		<script>
		$(document).ready( function(){
				var coptions = {beforeSubmit:showcRequest,success:showComment};
				$("#pc_form").ajaxForm(coptions);
				
				function showcRequest() {
					if ($("#pc_message")[0].value == "") {
						alert("please input something..");
						return false;
					}
					$("span#pc_status").html("<i class=\"fa fa-2x fa-spinner fa-spin\"></i> Submitting");
					$("#pc_form_box").toggle("slow");
				}
				function showComment(responseText, statusText)  { 
					$("#pc_head").after(responseText);
					$("span#pc_status").html("");
					$("#pc_message").attr("value","");
					$("#pc_submit").attr("disabled",true);
					$("#pc_form_box").toggle("slow");
				}
				$("#pc_message").click(function() {
					if ($("#pc_message").attr("rows") == 2) {
							$("#pc_message").attr("value","");
							$("#whatsbb").css("display","block");
							$("#pc_message").attr("rows",7);
						}
				});
				$("#pc_message").bind("change keyup",function() {
					$("#pc_submit").removeAttr("disabled");
					});
		});
		</script>

		<span id="pc_status"></span>
		<div id="pc_form_box">
		<form id="pc_form" action="'.url('jquery/profile_comment_publish').'" method="post">
		
		<textarea name="message" rows="2" id="pc_message" class="fpost"  style="width:95%" >'.t('Write Comment..').'</textarea>
		
		<input type="hidden" name="target_id" value="'.$target_id.'" />
		<input type="submit" value=" '.t('Comment').' " class="fbutton" id="pc_submit" disabled />
		
		</form>
		</div>
		
		<div id="pc_head"></div>';
		}

function my_jcow_home() {
	global $client;
	if (is_mobile()) {
		return url('feed');
	}
	if ($client['id']) {
		return url('feed');
	}
	else {
		return uhome();
	}
}

function mail_notice($type,$username,$title,$message) {
	global $client;
	$key = 'dismail_'.$type;
	$res = sql_query("select id,email,fullname,settings from ".tb()."accounts where username='$username'");
	$user = sql_fetch_array($res);
	if (!$user['id']) return false;
	if ($client['id'] == $user['id']) return false;
	$user_settings = unserialize($user['settings']);
	if (!$user_settings[$key]) {
		@jcow_mail(
			$user['email'],
			$title,
			"Dear ".$user['fullname'].",\r\n ".$message."\r\n".url('home',get_gvar('site_name'))
			);
	}
}

function check_hooks($act) {
	global $current_modules;
	foreach ($current_modules as $module) {
		$func = $module['name'].'_'.$act;
		if ($module['actived'] && function_exists($func)) {
			$hooks[] = $module['name'];
		}
	}
	if (is_array($hooks))
		return $hooks;
	else
		return false;
}

function member_only() {
	global $client;
}


function array_sort($array, $on, $order=SORT_ASC)
{
    $new_array = array();
    $sortable_array = array();

    if (count($array) > 0) {
        foreach ($array as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $k2 => $v2) {
                    if ($k2 == $on) {
                        $sortable_array[$k] = $v2;
                    }
                }
            } else {
                $sortable_array[$k] = $v;
            }
        }

        switch ($order) {
            case SORT_ASC:
                asort($sortable_array);
            break;
            case SORT_DESC:
                arsort($sortable_array);
            break;
        }

        foreach ($sortable_array as $k => $v) {
            $new_array[$k] = $array[$k];
        }
    }

    return $new_array;
}
function jcow_deleteuser($uid=0) {
	if (!allow_access(3)) return false;
	$res = sql_query("select * from ".tb()."accounts where id='$uid'");
	$user = sql_fetch_array($res);
	if (!$user['id']) {
		return false;
	}
	$res = sql_query("select p.* from ".tb()."streams as s left join ".tb()."status_photos as p on p.sid=s.id where s.uid='$uid' and p.id>0");
	while ($photo = sql_fetch_array($res)) {
		@unlink($photo['uri']);
		@unlink($photo['thumb']);
		sql_query("delete from ".tb()."status_photos where id='{$photo['id']}'");
	}
	sql_query("delete from ".tb()."streams where uid='$uid'");
	// stories
	$res2 = sql_query("select * from ".tb()."stories where uid='$uid'");
	while ($row2 = sql_fetch_array($res2)) {
		$res3 = sql_query("select * from ".tb()."story_photos where sid='{$row2['id']}'");
		while($photo = sql_fetch_array($res3)) {
			@unlink($photo['uri']);
			@unlink($photo['thumb']);
			sql_query("delete from ".tb()."story_photos where id='{$photo['id']}'");
		}
		sql_query("delete from ".tb()."stories where id='{$row2['id']}' and uid='$uid'");
		sql_query("delete from ".tb()."topic_ids where sid='{$row2['id']}'");
	}

	sql_query("delete from ".tb()."comments where uid='{$uid}'");
	sql_query("delete from ".tb()."messages where from_id='{$uid}'");
	sql_query("delete from ".tb()."pages where uid='{$uid}'");
	sql_query("delete from ".tb()."accounts where id='$uid'");
	sql_query("delete from ".tb()."requests where uid='$uid'");
}

function get_footer_pages() {
	$footer_pages = array();
	$res = sql_query("select id,link_name from `".tb()."footer_pages` order by weight");
	while ($row = sql_fetch_array($res)) {
		$footer_pages[] = url('footer_page/'.$row['id'],h($row['link_name']));
	}
	return $footer_pages;
}

function pending_review($post_id='',$content='',$uri='',$stream_id=0) {
	return 'verified';
	global $client,$settings,$parr;
	if (!$safe_community_posts = get_gvar('safe_community_posts')) {
		$safe_community_posts = 1;
	}
	if (allow_access(3) || $client['forum_posts'] >= $safe_community_posts) {
		return 'verified';
	}
	if (!$client['id']) return false;
	if (strlen($post_id)) {
		$arr = array(
			'uid'=>$client['id'],
			'post_id'=>$post_id,
			'content'=>$content,
			'stream_id'=>$stream_id,
			'uri'=>$uri,
			'created'=>time()
			);
		sql_insert($arr, tb().'pending_review');
	}
}

function check_pin() {
	global $client;
	if (!$client['id']) {
		die('need login');
	}
	$pin = substr($client['password'],-5,5);
	if ($_POST['pin'] != $pin && $_GET['pin'] != $pin) {
		die('wrong PIN');
	}
}
function pin_field() {
	global $client;
	return '<input type="hidden" name="pin" value="'.pincode().'" />';
}

function pincode() {
	global $client;
	if ($client['id']) {
		return substr($client['password'],-5,5);
	}
	else {
		return '';
	}
}
function page_tab_menu($current_path='') {
	global $tab_menu,$current_sub_menu,$menu_items,$top_menu_path,$conn;
	if (!strlen($current_path)) {
		$current_path = $current_sub_menu['href'];
	}
	if (is_array($tab_menu) && count($tab_menu)) {
		$output = '';
		if (strlen($top_menu_path)) {
			if (strlen($menu_items[$top_menu_path]['tab_name'])) {
				$output .= '<a href="'.url($menu_items[$top_menu_path]['path']).'" '.check_tabmenu_on($menu_items[$top_menu_path]['path']).'>'
				.t($menu_items[$top_menu_path]['tab_name']).'</a>';
			}
		}
		foreach ($tab_menu as $item) {
			if (strlen($item['name_replace'])) {
				$item['name'] = $item['name_replace'];
			}
			if ($item['path'] == $current_path) {
				$co = ' class="tm_on"';
			}
			else {
				$co = ' class="tm_ge"';
			}
			$output .= '<a href="'.url($item['path']).'" '.check_tabmenu_on($item['path']).'>'.t(mysqli_real_escape_string($conn,$item['name'])).'</a>';
		}
		//exit;
		$output .= '';
	}
	else {
		$output = '';
	}
	return $output;
}

function add_friend($uid,$fid) {
	$res = sql_query("select * from `".tb()."friends` where uid='$uid' and fid={$fid} ");
	if (!sql_counts($res)) {
		sql_query("insert into `".tb()."friends` (uid,fid,created) values ($fid,$uid,".time().")");
		sql_query("insert into `".tb()."friends` (uid,fid,created) values ({$uid},$fid,".time().")");
		
		//follow
		$follow = array();
		$res = sql_query("select * from ".tb()."followers where uid='{$uid}' and fid='$fid' limit 1");
		if (!sql_counts($res)) {
			$follow['uid'] = $uid;
			$follow['fid'] = $fid;
			sql_insert($follow, tb().'followers');
			sql_query("update ".tb()."accounts set followers=followers+1 where id='$fid'");
		}
		$res = sql_query("select * from ".tb()."followers where fid='{$uid}' and uid='$fid' limit 1");
		if (!sql_counts($res)) {
			$follow['fid'] = $uid;
			$follow['uid'] = $fid;
			sql_insert($follow, tb().'followers');
			sql_query("update ".tb()."accounts set followers=followers+1 where id='{$uid}'");
		}	
	}
	// 
	sql_query("delete from `".tb()."friend_reqs` where uid=$fid and fid={$uid} ");
	sql_query("delete from `".tb()."friend_reqs` where uid={$uid} and fid=$fid ");
}

function new_tab_menu($current_path='') {
	global $tab_menu,$new_tab_menu,$current_sub_menu,$menu_items,$top_menu_path,$conn;
	if (!strlen($current_path)) {
		$current_path = $current_sub_menu['href'];
	}
	if (is_array($new_tab_menu) && count($new_tab_menu)) {
		$output = '<div id="tabmenu">';
		if (strlen($top_menu_path)) {
			if (strlen($menu_items[$top_menu_path]['tab_name'])) {
				$output .= '<a href="'.url($menu_items[$top_menu_path]['path']).'" '.check_tabmenu_on($menu_items[$top_menu_path]['path']).'>'
				.t($menu_items[$top_menu_path]['tab_name']).'</a>';
			}
		}
		foreach ($new_tab_menu as $item) {
			if (strlen($item['name_replace'])) {
				$item['name'] = $item['name_replace'];
			}
			if ($item['path'] == $current_path) {
				$co = ' class="tm_on"';
			}
			else {
				$co = ' class="tm_ge"';
			}
			$output .= '<a href="'.url($item['path']).'" '.check_tabmenu_on($item['path']).'>'.t(mysqli_real_escape_string($conn,$item['name'])).'</a>';
		}
		//exit;
		$output .= '</div>';
	}
	else {
		$output = '';
	}
	$new_tab_menu = $tab_menu = array();
	return $output;
}

function jcow_social_mail($uid,$title='',$message='') {
	if (debug == 1) {
		return false;
	}
	if (!is_numeric($uid)) {
		return 'no uid';
	}
	$act_timeline = time() - 600; // if user online, don't send
	$sent_timeline = time() - 3600*6; // if sent an email within 6 hours, don't send another
	$res = sql_query("select id,fullname,lastlogin,email from ".tb()."accounts where id='$uid'");
	$user = sql_fetch_array($res);
	if (!$user['id']) return 'user not found';
	$settings = unserialize($user['settings']);
	if ($settings['disable_email_note']) return 'unsubscribed';
	$body = $message;
	if ($user['lastlogin'] < $act_timeline) {
		$skey = 'lastsent'.$uid;
		if (!$lastsent = get_tmp($skey)) {
			$lastsent = 0;
		}
		if ($lastsent < $sent_timeline) {
			jcow_mail($user['email'],$title,$body);
			set_tmp($skey,time());
			return 'sent';
		}
		else {
			return 'not send another within 6 hours';
		}
	}
	else {
		return 'not send to online user';
	}
}
function safe($val) {
	global $conn;
	return mysqli_real_escape_string($conn,$val);
}

function add_friend_btn($fid=0,$size='sm') {
	global $client;
	if (!is_numeric($fid)) return '';
	if ($fid == $client['id'] && $client['id'] > 0) return '';
	if ($size == 'sm') {
		$sz = 'btn-xs';
	}
	else {
		$sz = '';
	}
	if (!$client['id']) return '<a href="'.url('member/login/1').'" userid="'.$fid.'" class="btn '.$sz.' btn-default"><i class="fa fa-user-plus"></i> '.t('Add Friend').'</a>';
	$res = sql_query("select * from jcow_friends where uid='{$client['id']}' and fid='$fid'");
	if (sql_counts($res)) return '<button class="btn '.$sz.'" disabled><i class="fa fa-check"></i> '.t('Friend').'</button>';
	$res = sql_query("select * from jcow_friend_reqs where uid='{$client['id']}' and fid='$fid'");
	if (sql_counts($res)) {
		return '<button class="btn btn-xs" disabled><i class="fa fa-check"></i> '.t('Request sent').'</button>';
	}
	else {
		return '<span><a href="#" userid="'.$fid.'" class="btn '.$sz.' btn-default doadd"><i class="fa fa-user-plus"></i> '.t('Add Friend').'</a></span>';
	}
}

function google_places($arr = array(),$types='"(cities)"') {
	global $config;
	$config['loadgmap'] = 1;
	$key = 'ac_'.get_rand(5);
	return '
	<div id="'.$key.'" class="places_box">
    <div class="input-group">
      <div class="input-group-addon"><i class="fa fa-map-marker"></i></div>
      <input type="text" value="'.h($arr['placename']).'"class="form-control autocompleteinput" />
    </div>
    <input class="placeid"  type="hidden" name="placeid" value="'.$arr['placeid'].'" />
    <input class="locality"  type="hidden" name="locality" value="'.$arr['locality'].'" />
    <input class="placename"  type="hidden" name="location" value="'.$arr['placename'].'" />
	<input class="placecountry"  type="hidden" name="country" value="'.$arr['country'].'" />
	</div>
    <script>
    $(function() {
    	var ac_box = $("#'.$key.'");
	    var input = ac_box.find(".autocompleteinput")[0];
	    var autocomplete = new google.maps.places.Autocomplete(input,{types: ['.$types.']});
	    google.maps.event.addListener(autocomplete, "place_changed", function(){
	       var place = autocomplete.getPlace();
	       ac_box.find(".placename").val(place.name);
	       ac_box.find(".placeid").val(place.place_id);
	       place.address_components.forEach(function(addr) {
	       if (addr.types[0] == "country") {
	         	ac_box.find(".placecountry").val(addr.short_name);
	         }
	         if (addr.types[0] == "locality") {
	         	ac_box.find(".locality").val(addr.long_name);
	         }
	        });
	    });
	});
    </script>';
}
function google_map_show($placeid='') {
	global $config;
	$config['loadgmap'] = 1;
	$key = 'acp_'.get_rand(5);
	return "<script>
$(function() {
  var map = new google.maps.Map(document.getElementById('".$key."'), {
    center: {lat: -33.866, lng: 151.196},
    zoom: 15
  });


  var infowindow = new google.maps.InfoWindow();
  var service = new google.maps.places.PlacesService(map);

  service.getDetails({
    placeId: '{$placeid}'
  }, function(place, status) {
    if (status === google.maps.places.PlacesServiceStatus.OK) {
    map.setCenter(place.geometry.location);
      var marker = new google.maps.Marker({
        map: map,
        position: place.geometry.location
      });
      google.maps.event.addListener(marker, 'click', function() {
        infowindow.setContent('<div><strong>' + place.name + '</strong><br>' +
          'Place ID: ' + place.place_id + '<br>' +
          place.formatted_address + '</div>');
        infowindow.open(map, this);
      });
    }
  });
});

</script>
<div id='".$key."' style='height:300px;'></div>";
	}
function stream_allow_access($stream,$page='') {
	global $client;
	if ($stream['type'] == 1) {
		if (!$client['id']) return false;
		if ($stream['uid'] == $client['id']) return true;
		if (in_array($stream['uid'],$client['friends'])) {
			return true;
		}
		else {
			return false;
		}
	}
	if (!is_array($page)) {
		$res = sql_query("select id,type,var3 from jcow_pages where id='{$stream['wall_id']}'");
		$page = sql_fetch_array($res);
		if (!$page['id']) return false;
	}
	if ($page['type'] == 'group' && $page['var3']%2) {
		if (in_array($page['id'],$client['pageids'])) {
			return true;
		}
		else {
			return false;
		}
	}
	return true;
}
function jm_need_login() {
	echo json_encode(array('act'=>'login'));
	exit;
	}
function jm_return($val) {
	echo json_encode(array('output'=>$val));
	exit;
	}
function form_array($arr = array()) {
	echo json_encode($arr);
	exit;
}
function form_stop($msg) {
	echo json_encode(array('msg'=>$msg,'act'=>'msg'));
	exit;
}
function form_go($url) {
	echo json_encode(array('url'=>$url,'act'=>'redirect'));
	exit;
	}

