<?php
/* ############################################################ *\
 ----------------------------------------------------------------
Jcow Software (http://www.jcow.net)
IS NOT FREE SOFTWARE
http://www.jcow.net/commercial_license
Copyright (C) 2009 - 2010 jcow.net.  All Rights Reserved.
 ----------------------------------------------------------------
\* ############################################################ */

if ($step == 'post') {
	if (is_array($_POST['ids'])) {
		foreach ($_POST['ids'] as $id) {
			if ($_POST['opt'] == 1) {
				sql_query("update ".tb()."accounts set disabled=0 where id='{$id}'");
				sql_query("delete from ".tb()."pending_review where uid='$id' and ignored!=1");
			}
			elseif ($_POST['opt'] == 2) {
				sql_query("update ".tb()."accounts set disabled=2 where id='{$id}'");
			}
		}
	}
	redirect('admin/memberqueue',1);

}
elseif ($step == 'deleteall') {
	$res = sql_query("select r.id as r_id,r.question,r.answer,r.created as r_created,u.* from ".tb()."requests as r left join ".tb()."accounts as u on u.id=r.uid order by r.id DESC limit 50");
	$i=0;
	while ($user = sql_fetch_array($res)) {
		jcow_deleteuser($user['id']);
		sql_query("insert ignore into ".tb()."spammers (userkey,type,created) values('{$user['ipaddress']}','ip',".time().")");
		sql_query("insert ignore into ".tb()."spammers (userkey,type,created) values('{$user['email']}','email',".time().")");
	}
	redirect('admin/memberqueue',1);
}

elseif ($step == 'approveuser') {
	$res = sql_query("select * from ".tb()."requests where id='$id'");
	$reqs = sql_fetch_array($res);
	$res = sql_query("select * from ".tb()."accounts where id='{$reqs['uid']}'");
	$user = sql_fetch_array($res);
	if ($user['id'] && $user['disabled']) {
		sql_query("update ".tb()."accounts set disabled=0 where id='{$user['id']}'");
		@jcow_mail($user['email'], 'Account approved', 
			t('Congratulations, Your account on {1} has been approved!',url(uhome(),get_gvar('site_name')))
			);
	}
	sql_query("delete from ".tb()."requests where id='$id'");
	echo '
	<script>
			$(".userbox_'.$user['id'].'").css("background","green");
			$(".userbox_'.$user['id'].'").slideUp();
			</script>';
	exit;
}
elseif ($step == 'rejectuser') {
	$res = sql_query("select * from ".tb()."requests where id='$id'");
	$user = sql_fetch_array($res);
	sql_query("delete from ".tb()."requests where id='$id'");
	echo '
	<script>
			$(".userbox_'.$user['uid'].'").css("background","red");
			$(".userbox_'.$user['uid'].'").slideUp();
			</script>';
	exit;
}
else {
	c('
		<script>
		$(document).ready( function(){
			$(".approveuser").click(function() {
				$(this).parent().html("sending..");
				rid = $(this).attr("rid");
				$.post( "'.url('admin/memberqueue/approveuser/').'"+rid, function( data ) {
					$( "#mqres" ).html( data );
				});
				return false;
			});
			$(".rejectuser").click(function() {
				$(this).parent().html("sending..");
				rid = $(this).attr("rid");
				$.post( "'.url('admin/memberqueue/rejectuser/').'"+rid, function( data ) {
					$( "#mqres" ).html( data );
				});
				return false;
			});
		});
		</script>
		<style>
		.mq_button {
			display:block;
		}
		</style>
		<div id="mqres"></div>
		<strong>Approve</strong>: Verify the user.<br />
		<strong>Reject</strong>: Reject the request. The user can send request again.
		');
	$res = sql_query("select r.id as r_id,r.question,r.answer,r.created as r_created,u.* from ".tb()."requests as r left join ".tb()."accounts as u on u.id=r.uid order by r.id DESC limit 50");
	$i=0;
	while ($user = sql_fetch_array($res)) {
		$i++;
		c('
			<div class="userbox_'.$user['id'].'">
			<table width="100%" border="0"><tr>
			<td width="43%">
			<div style="font-size:0.8em;height:15px;overflow:hidden;background:#eeeeee">'.h($user['question']).'</div>
			<div>'.h($user['answer']).' ('.get_date($user['r_created']).')</div>
			</td>
			<td width="43%">
			<div>'.url('u/'.$user['username'],h($user['fullname'])).' ('.$user['email'].')</div>
			<div>'.h($user['about_me']).'</div>
			</td>
			<td>
			<div>
			<a href="#" class="approveuser mq_button" rid="'.$user['r_id'].'">Approve</a>
			<a href="#" class="rejectuser mq_button" rid="'.$user['r_id'].'">Reject</a>
			<a href="#" class="deleteuser mq_button" uid="'.$user['id'].'">Delete user</a>
			</div>
			</td>
			</tr></table>
			</div>
			');
	}
	if ($i>2) {
		c(' '.url('admin/memberqueue/deleteall','Delete all('.$i.') request and mark SPAM').' ');
	}

}