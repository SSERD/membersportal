<?php
/* ############################################################ *\
 ----------------------------------------------------------------
Jcow Software (http://www.jcow.net)
IS NOT FREE SOFTWARE
http://www.jcow.net/commercial_license
Copyright (C) 2009 - 2010 jcow.net.  All Rights Reserved.
 ----------------------------------------------------------------
\* ############################################################ */

$rps_str = get_text('recommend_pages');
$rps = explode(',',$rps_str);

$page_u_ids = explode(',',get_text('suggest_u'));
$page_p_ids = explode(',',get_text('suggest_p'));
$page_g_ids = explode(',',get_text('suggest_g'));

if ($_POST['step'] == 'post') {
	if (!strlen($_POST['page_url'])) {
		sys_back('Empty URL');
	}
	preg_match("/([a-z]+)\/([0-9a-z_]+)$/i",$_POST['page_url'],$matches);
	if (!strlen($matches[1]) || !strlen($matches[2])) {
		sys_back("Not a valid page");
	}
	/*
	if ($matches[1] == 'u') {
		$res = sql_query("select id from ".tb()."accounts where username='{$matches[2]}'");
		$user = sql_fetch_array($res);
		if (!$user['id']) sys_back('wrong URL');
		$res = sql_query("select id from ".tb()."pages where uid='{$user['id']}' and type='u'");
		$page = sql_fetch_array($res);
		$page_id = $page['id'];
		if (!$page_id) sys_back('wrong URL');
		$page_u_ids[] = $page_id;
		$new_page_u_ids = implode(',',array_filter($page_u_ids));
		set_text('suggest_u',$new_page_u_ids);
	}
	*/
	if ($matches[1] == 'page') {
		$res = sql_query("select id from ".tb()."pages where uri='{$matches[2]}' and type='page'");
		$page = sql_fetch_array($res);	
		if (!$page['id']) sys_back('Page not found');
		$page_id = $page['id'];
		if (!in_array($page_id,$page_p_ids)) {
			$page_p_ids[] = $page_id;
			$new_page_p_ids = implode(',',array_filter($page_p_ids));
			set_text('suggest_p',$new_page_p_ids);
		}
	}
	elseif ($matches[1] == 'group') {
		$res = sql_query("select id from ".tb()."pages where uri='{$matches[2]}' and type='group'");
		$page = sql_fetch_array($res);	
		if (!$page['id']) sys_back('Page not found');
		$page_id = $page['id'];
		if (!in_array($page_id,$page_g_ids)) {
			$page_g_ids[] = $page_id;
			$new_page_g_ids = implode(',',array_filter($page_g_ids));
			set_text('suggest_g',$new_page_g_ids);
		}
	}
	else {
		sys_back("Not a valid page");
	}
	redirect('admin/featurepages',1);
}
elseif ($step == 'delete_u') {
	if(is_numeric($id)) {
		if(($key = array_search($id, $page_u_ids)) !== false) {
		    unset($page_u_ids[$key]);
		}
		set_text('suggest_u',implode(',',$page_u_ids));
	}
	redirect('admin/featurepages',1);
}
elseif ($step == 'delete_p') {
	if(is_numeric($id)) {
		if(($key = array_search($id, $page_p_ids)) !== false) {
		    unset($page_p_ids[$key]);
		}
		set_text('suggest_p',implode(',',$page_p_ids));
	}
	redirect('admin/featurepages',1);
}
elseif ($step == 'delete_g') {
	if(is_numeric($id)) {
		if(($key = array_search($id, $page_g_ids)) !== false) {
		    unset($page_g_ids[$key]);
		}
		set_text('suggest_g',implode(',',$page_g_ids));
	}
	redirect('admin/featurepages',1);
}

else {
	c('<p>Here you can add existing Groups or Fan Pages that will be suggested to newcomers.</p>');
/*
	c('<h3>Suggested Users</h3>');
	foreach($page_u_ids as $page_id) {
		if ($page_id > 0) {
			$res = sql_query("select * from ".tb()."pages where id='$page_id'");
			$page = sql_fetch_array($res);
			$res = sql_query("select * from ".tb()."accounts where id='{$page['uid']}'");
			$user = sql_fetch_array($res);
			c('<div style="margin:2px;">'.url('u/'.$user['username'],h($user['fullname'])).' <a href="'.url('admin/featurepages/delete_u/'.$page_id).'" class="btn btn-sm btn-default">Remove from list</a></div>');
		}
	}
	*/
	c('<h3>Suggested Fan Pages</h3>');
	foreach($page_p_ids as $page_id) {
		if ($page_id > 0) {
			$res = sql_query("select * from ".tb()."pages where id='$page_id'");
			$page = sql_fetch_array($res);
			c('<div style="margin:2px;">'.url('page/'.$page['uri'],h($page['name'])).' <a href="'.url('admin/featurepages/delete_p/'.$page_id).'" class="btn btn-sm btn-default">Remove from list</a></div>');
		}
	}
	c('<h3>Suggested Groups</h3>');
	foreach($page_g_ids as $page_id) {
		if ($page_id > 0) {
			$res = sql_query("select * from ".tb()."pages where id='$page_id'");
			$page = sql_fetch_array($res);
			c('<div style="margin:2px;">'.url('group/'.$page['uri'],h($page['name'])).' <a href="'.url('admin/featurepages/delete_g/'.$page_id).'" class="btn btn-sm btn-default">Remove from list</a></div>');
		}
	}

		c('
<h3>Suggest a new User/Page/Group</h3>
			<p>
		
		<form action="" method="post" class="form-inline">
		Fan Page or Group URL: <input type="text" name="page_url" size="60" class="form-control" /> <input type="submit" value=" Add " class="btn btn-primary" />
		<p class="text-muted">(Example profile: <span class="text-danger">http://yourdomain.com/u/mike</span>,  or: <span class="text-danger">http://yourdomain.com/page/apple</span> )</p>
		<input type="hidden" name="step" value="post" />
		
		</form>
		</p>');

}