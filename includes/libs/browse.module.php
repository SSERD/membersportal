<?php
/* ############################################################ *\
 ----------------------------------------------------------------
Jcow Software (http://www.jcow.net)
IS NOT FREE SOFTWARE
http://www.jcow.net/commercial_license
Copyright (C) 2009 - 2010 jcow.net.  All Rights Reserved.
 ----------------------------------------------------------------
\* ############################################################ */


class browse{
	function __construct() {
		global $sub_menu, $ss, $client, $menuon, $current_sub_menu, $tab_menu;
		$menuon = 'browse';
		nav('Browse');
		set_title('Browse');
	}
	function activities() {
		need_login();
		global $client;
		$num = 20;
		c(activity_get(0,$num,0,0,0));
	}

	function index($filter = '') {
		global $config,$offset,$page,$client, $current_sub_menu;
		if ($_POST['onpost'] && is_mobile() ) {
			unset($_POST['onpost']);
			$purl = url('browse','','',$_POST);
			form_go($purl);
		}
		if (is_array($_GET)) {
			foreach ($_GET as $key=>$val) {
				if ($key != 'jmtoken') {
					$_POST[$key] = $val;
				}
			}
		}
		/*
		if ($client['id']) {
			$uids = array();
			$res = sql_query("select fid from ".tb()."friends where uid='{$client['id']}' order by rand() limit 5");
			while ($row = sql_fetch_array($res)) {
				$uids[] = $row['fid'];
			}
			if (count($uids)>0) {
				$i=0;
				$res = sql_query("select distinct f.fid,u.* from jcow_friends as f left join jcow_accounts as u on u.id=f.fid where f.uid in (".implode(',',$uids).") order by rand() limit 30");
				while ($user = sql_fetch_array($res)) {
					if ($user['id']>0 && $i < 7 && $user['id'] != $client['id']) {
						$res2 = sql_query("select * from jcow_friends where uid='{$client['id']}' and fid='{$user['id']}' ");
						$res3 = sql_query("select * from jcow_friend_reqs where uid='{$client['id']}' and fid='{$user['id']}' ");
						if (!sql_counts($res2) && !sql_counts($res3)) {
							$res2 = sql_query("select count(*) as num from jcow_friends where uid='{$user['id']}' and fid in 
								(select fid from jcow_friends where uid='{$client['id']}')
								");
							$row = sql_fetch_array($res2);
							if ($user['avatar']) {
								$user['avatar'] = uploads.'/avatars/'.$user['avatar'];
							}
							$output .= listing_box(
								array('name'=>h($user['fullname']),'img'=>$user['avatar'],'link'=>url('u/'.$user['username']),'des'=>'<div style="white-space: nowrap;">'.t('{1} mutual friend(s)',$row['num']).'</div><span><a href="#" class="btn btn-xs btn-default doadd" userid="'.$user['id'].'" ><i class="fa fa-user-plus"></i> '.t('Add Friend').'</a></span>'
							));
							$i++;
						}
					}
				}
			}
			ass(array('title'=>t('People you may know'),'content'=>$output));
		}
		*/
		$locations = explode("\r\n",get_text('locations'));
		$current_sub_menu['href'] = 'browse/index';
		if (!preg_match("/^[0-9a-z_]+$/i",$filter)) {
			$filter = '';
		}
		$num_per_page = 21;
		$page = $_POST['page'];
		if ($page > 5) {
			stop_here();
		}
		if ($client['id'] && (!strlen($client['location']) || !$client['country'])) {
			c('<div style="text-align:center;font-size:30px">'.
				url('account','<i class="fa fa-location-arrow"></i> '.t('Please enter your City first'))
				.'</div>');
			stop_here();
		}
		if (!$page) $page = 1;
		if (is_numeric($_POST['age_from']))
			$age_from = $_POST['age_from'];
		else
			$age_from = 18;
		if (is_numeric($_POST['age_to']))
			$age_to = $_POST['age_to'];
		else
			$age_to = 60;
		if ($_POST['gender'] == 'male')
			$gender_male = 'checked';
		elseif ($_POST['gender'] == 'female')
			$gender_female = 'checked';
		else
			$gender_both = 'checked';
		if ($_POST['orderby'] == 2)
			$orderby_2 = 'checked';
		else
			$orderby_1 = 'checked';
		if (isset($_POST['nearme'])) {
			$nearme = $_POST['nearme'];
		}
		else {
			$nearme = 'all';
		}
		if ($nearme == 'city') {
			need_login();
			$location_sql = " and u.location='".safe($client['location'])."' ";
		}
		if ($nearme == 'country') {
			need_login();
			$location_sql = " and u.country='".safe($client['country'])."' ";
		}
		if ($_POST['location']) {
			$key = $_POST['location'] - 1;
			if (strlen($locations[$key])) {
				$where .= " AND u.location='{$locations[$key]}' ";
			}
		}
		if ($_POST['gender'] == 'male') {
			$where .= " AND u.gender=1 ";
		}
		elseif ($_POST['gender'] == 'female') {
			$where .= " AND u.gender=0 ";
		}
		for($i=1;$i<=7;$i++) {
			$col = 'var'.$i;
			$key = 'cf_var'.$i;
			$type = get_gvar($key);
			if ($type == 'select_box') {
				if (strlen($_POST[$col])) {
					$where .= " AND u.{$col}='{$_POST[$col]}' ";
				}
			}
		}
		if (!$_POST['page']) $_POST['page'] = 1;
		$offset = ($_POST['page']-1)*$num_per_page;
		$num_per_page++;
		$i = 0;
		$fld = array();
		if ($client['id']) {
			$res = sql_query("select * from jcow_followers where uid='{$client['id']}'");
			while ($row = sql_fetch_array($res)) {
				$fld[] = $row['fid'];
			}
		}
		$res = sql_query("select u.*,p.id as pid from `".tb()."accounts` as u left join jcow_pages as p on (p.uid=u.id and p.type='u') where u.id>
			(
			select IFNULL(
			(select id from jcow_accounts  order by id desc limit 1000,1)
			,0)
			)
			and !u.disabled $where $location_sql and !hide_me order by u.id DESC limit $offset, $num_per_page");
		/*
		$res = sql_query("select * from `".tb()."accounts` $where $location_sql and !hide_me order by id DESC limit $offset, $num_per_page");
		if (!$_POST['onpost'] && sql_counts($res) <= 1) {
			$nearme = 'country';
			$location_sql = " and country='".safe($client['country'])."' ";
			$res = sql_query("select * from `".tb()."accounts` $where $location_sql and !hide_me order by id DESC limit $offset, $num_per_page");
		}
		*/
		$result = '<div class="browse_result">';
		while ($user = sql_fetch_array($res)) {
			$res2 = sql_query("select count(*) as num from jcow_streams where uid='{$user['id']}'");
			$row = sql_fetch_array($res2);
			$i++;
			if ($i < $num_per_page) {
				if (strlen($user['avatar'])) {
					$user['avatar'] = uhome().'/'.uploads.'/avatars/s_'.$user['avatar'];
				}
				if ($client['id']) {
					if ($user['id'] != $client['id']) {
						if (!in_array($user['id'],$fld)) {
							$follow_btn = '<span><a class="dofollow btn btn-default btn-sm" href="#" page_id="'.$user['pid'].'"><i class="fa fa-rss"></i>  '.t('Follow').'</a></span>';
						}
						else {
							$follow_btn = '<span><a class="dofollow btn btn-default btn-sm" href="#" page_id="'.$user['pid'].'"> '.t('Un-follow').'</a></span>';
						}
					}
					if (!in_array($user['id'],$client['friends']) && $client['id'] != $user['id']) {
						$friend_btn = ' <span><a href="#" class="btn btn-sm btn-default doadd" userid="'.$user['id'].'" ><i class="fa fa-user-plus"></i> '.t('Add Friend').'</a></span>';
					}
					
				}
				else {
					$follow_btn = '<a class=" btn btn-default btn-sm" href="'.url('member/login/1').'" page_id="'.$user['pid'].'"><i class="fa fa-rss"></i>  '.t('Follow').'</a>';
				}
				$result .= listing_box(
							array('name'=>h($user['fullname']),'img'=>$user['avatar'],'link'=>url('u/'.$user['username']),'des'=>'<div style="white-space: nowrap;">'.
								t('{1} activities','<strong>'.$row['num'].'</strong>').
								', '.t('from {1}',h($user['location'].', '.get_country($user['country']))).
								'</div>'.$follow_btn.$friend_btn
						));
			}
		}
		if (!$i) $result .= '<div>None</div>';
		$result .= '</div>';
		if (!is_mobile() && $i == $num_per_page && $page<3) {
			$page++;
			$result .= '<form action="'.url('browse').'" method="post">
			<input type="hidden" name="age_from" value="'.h(stripslashes($_POST['age_from'])).'" />
			<input type="hidden" name="age_to" value="'.h(stripslashes($_POST['age_to'])).'" />
			<input type="hidden" name="location" value="'.h(stripslashes($_POST['location'])).'" />
			<input type="hidden" name="gender" value="'.h(stripslashes($_POST['gender'])).'" />

			<input type="hidden" name="var1" value="'.h(stripslashes($_POST['var1'])).'" />
			<input type="hidden" name="var2" value="'.h(stripslashes($_POST['var2'])).'" />
			<input type="hidden" name="var3" value="'.h(stripslashes($_POST['var3'])).'" />
			<input type="hidden" name="var4" value="'.h(stripslashes($_POST['var4'])).'" />
			<input type="hidden" name="var5" value="'.h(stripslashes($_POST['var5'])).'" />
			<input type="hidden" name="var6" value="'.h(stripslashes($_POST['var6'])).'" />
			<input type="hidden" name="var7" value="'.h(stripslashes($_POST['var7'])).'" />

			<input type="hidden" name="page" value="'.$page.'" />
			<div class="hr"></div>
			<input type="submit" value=" '.t('More..').' " />
			</form>';
		}

		$nk = 'nearme_'.$nearme;
		$$nk = 'checked';
			section_content('
			<style>
			#filter_button{
				display:block;
				text-align:center;
				font-weight:bold;
				border-radius:3px;
				background:#eeeeee;
				padding:5px;
			}
			</style>
			<script>
			$(document).ready( function(){
				$("#filter_button").click(function() {
					$(this).hide();
					$("#user_filter").slideDown();
					return false;
				});
			});
			</script>
			<div style="min-height:30px">
					<div id="user_filter" style="border:#E3E3E3 1px solid;overflow:hidden;padding:5px;margin-bottom:10px">

					<form action="'.url('browse').'" method="post" >
					<i class="fa fa-location-arrow"></i> '.t('Location').': <label><input type="radio" name="nearme" value="city" '.$nearme_city.' />  '.t('In my city').'</label> 
					<label><input type="radio" name="nearme" value="country" '.$nearme_country.' /> '.t('In my country').'</label> 
					<label><input type="radio" name="nearme" value="all" '.$nearme_all.' /> '.t('All').'</label> <br />
					'.t('Gender').': 
					<label for="gender_male"><input id="gender_male" type="radio" name="gender" value="male" '.$gender_male.' /> '.t('Male').'</label>
					<label for="gender_female"><input id="gender_female" type="radio" name="gender" value="female" '.$gender_female.' /> '.t('Female').'</label>
					<label for="gender_both"><input id="gender_both" type="radio" name="gender" value="both" '.$gender_both.' /> '.t('Both').'</label>');
					// custom fields
					for($i=1;$i<=7;$i++) {
						$col = 'var'.$i;
						$key = 'cf_var'.$i;
						$key2 = 'cf_var_value'.$i;
						$key3 = 'cf_var_des'.$i;
						$key4 = 'cf_var_label'.$i;
						$type = get_gvar($key);
						$value = get_gvar($key2);
						$des = get_gvar($key3);
						$label = get_gvar($key4);
						if ($type != 'disabled') {
							if ($type == 'select_box') {
								$tarr = explode("\r\n",$value);
								section_content('<br />
								'.$label.' 
								<select name="'.$col.'">
								<option value="">'.t('All').'</option>
								');
								foreach ($tarr as $val) {
									$selected = '';
									if (stripslashes($_POST[$col]) == $val)
										$selected = 'selected';
									section_content('<option value="'.$val.'" '.$selected.'>'.$val.'</option>');
								}
								section_content('</select>');
							}
						}
					}
					
					section_content('<br /><input type="hidden" value="1" name="update" />
					
					<input type="submit" class="btn btn-primary" value="'.t('Search').'" />
					<input type="hidden" name="onpost" value="1" />
					</form>
					</div>
					</div>
			');


		c($result);
		section_close();
		

	}
	

}