<?php
/* ############################################################ *\
 ----------------------------------------------------------------
Jcow Software (http://www.jcow.net)
IS NOT FREE SOFTWARE
http://www.jcow.net/commercial_license
Copyright (C) 2009 - 2010 jcow.net.  All Rights Reserved.
 ----------------------------------------------------------------
\* ############################################################ */

if ($step == 'post' && strlen($_POST['name'])) {
	sql_query("insert into jcow_tags (name,app) values('{$_POST['name']}','pages')");
	redirect('admin/page_categories');
}
elseif ($step == 'delete' && is_numeric($id)) {
	sql_query("delete from jcow_tags where id='$id'");
	redirect('admin/page_categories');
	}
elseif ($step == 'edit' && is_numeric($id)) {
	$res = sql_query("select * from jcow_tags where id='$id'");
	$cat = sql_fetch_array($res);
	c('
		</ul>
	Create a new category:
	<form method="post" action="'.url('admin/page_categories/editpost/'.$id).'">

	<input type="text"  name="name" value="'.h($cat['name']).'" />
	<input type="submit" value=" Save " />

	</form>
	');
	stop_here();
	}
elseif ($step == 'editpost' && is_numeric($id) && strlen($_POST['name'])) {
	sql_query("update jcow_tags set name='{$_POST['name']}' where id='$id'");
	redirect('admin/page_categories');
	}
else {
	c('<h3>These categories will be used in the "Fan Pages"(if installed)</h3>
	<ul>');
	$res = sql_query("select * from jcow_tags where app='pages' order by name DESC");
	while ($cat = sql_fetch_array($res)) {
		c('<li>'.h($cat['name']).' ['.url('admin/page_categories/edit/'.$cat['id'],'Edit').'] ['.url('admin/page_categories/delete/'.$cat['id'],'Delete').']</li>');
	}
	c('
		</ul>
	Create a new category:
	<form method="post" action="'.url('admin/page_categories/post').'">

	<input type="text"  name="name" />

	<input type="submit" value=" Add " />

	</form>
	');
}