<?php
/* ############################################################ *\
 ----------------------------------------------------------------
Jcow Software (http://www.jcow.net)
IS NOT FREE SOFTWARE
http://www.jcow.net/commercial_license
Copyright (C) 2009 - 2010 jcow.net.  All Rights Reserved.
 ----------------------------------------------------------------
\* ############################################################ */

set_title(t('Dashboard'));
global $client;

if ($act == 'post' && strlen($_POST['message'])) {
	$url_search = array(            
			"/\[url]www.([^'\"]*)\[\/url]/iU",
			"/\[url]([^'\"]*)\[\/url]/iU",
			"/\[url=www.([^'\"\s]*)](.*)\[\/url]/iU",
			"/\[url=([^'\"\s]*)](.*)\[\/url]/iU",
		);
	$url_replace = array(
			"<a href=\"http://www.\\1\" target=\"_blank\" rel=\"nofollow\">www.\\1</a>",
			"<a href=\"\\1\" target=\"_blank\" rel=\"nofollow\">\\1</a>",
			"<a href=\"http://www.\\1\" target=\"_blank\" rel=\"nofollow\">\\2</a>",
			"<a href=\"\\1\" target=\"_blank\" rel=\"nofollow\">\\2</a>"
			);
	$stream_id = stream_publish(preg_replace($url_search,$url_replace, h($_POST['message']) ),'','',$client['id']);
	sys_notice(t('Posted'));

}
elseif ($act == 'avatarpost' && strlen($_FILES['avatar']['tmp_name'])) {
	$res = sql_query("select * from `".tb()."accounts` where id='{$client['id']}' ");
	$profile = sql_fetch_array($res);

	// avatar
	if (strlen($_FILES['avatar']['tmp_name'])>0 && $_FILES['avatar']['tmp_name'] != "none") {
		include_once('includes/libs/resizeimage.inc.php');
		$dir = date("Ym",time());
		$folder = uploads.'/avatars/'.$dir;
		if (!is_dir($folder))
			mkdir($folder, 0777);
		$s_folder = uploads.'/avatars/s_'.$dir;
		if (!is_dir($s_folder))
			mkdir($s_folder, 0777);
		$name = date("H_i",time()).'_'.get_rand(5);
		//small
		$resizeimage = new resizeimage($_FILES['avatar']['tmp_name'], $_FILES['avatar']['type'], $s_folder.'/'.$name, 50,50, 0,100,'white');
		//big
		$resizeimage = new resizeimage($_FILES['avatar']['tmp_name'], $_FILES['avatar']['type'], $folder.'/'.$name, 200,200, 0, 100,'white');
		$reset_avatar = "avatar='".$dir.'/'.$client['id'].".".$resizeimage->type."' ";
		$newacc['avatar'] = $dir.'/'.$name.".".$resizeimage->type;
		$newacc['id'] = $client['id'];
		sql_update($newacc,tb().'accounts');
	}
	redirect('mobile',1);
}


if ($client['avatar'] == 'undefined.jpg' || !$client['avatar']) {
	c('<form method="post" name="form1" action="'.url('mobile/index/avatarpost').'" enctype="multipart/form-data">
		'.t('Profile picture').'<br />
					<input name="avatar" type="file" id="avatar" /><br />
					<input class="button" type="submit" value="'.t('Upload').'" />
					</form>');
	section_close(t('Avatar'));
}



c('<form action="'.url('mobile/index/post').'" method="post">
<textarea name="message" maxlegth="140" style="width:200px;height:35px"></textarea>
<input type="submit" value="'.t('Share').'" />
</form>');
section_close(t("What's on your mind"));
$uids[] = $client['id'];
$res = sql_query("select f.fid from ".tb()."friends as f left join ".tb()."accounts as u on u.id=f.fid where f.uid='{$client['id']}' order by u.lastlogin desc limit 5");
while ($row = sql_fetch_array($res)) {
	$uids[] = $row['fid'];
}
$res = sql_query("select f.fid from ".tb()."followers as f left join ".tb()."accounts as u on u.id=f.fid where f.uid='{$client['id']}' order by u.lastlogin desc limit 5");
while ($row = sql_fetch_array($res)) {
	$uids[] = $row['fid'];
}
if (is_array($uids)) {
	$mpage = $_POST['mpage'];
	if (!$mpage) $mpage = 1;
	$offset = 15*($mpage-1);
	$ids = implode(',',$uids);
	$res = sql_query("select s.*,u.username,u.fullname,u.avatar,p.uid as wall_uid from ".tb()."streams as s left join ".tb()."accounts as u on u.id=s.uid left join ".tb()."pages as p on p.id=s.wall_id where u.id in ($ids) order by s.id desc limit $offset, 15");
	$acts = '';
	$i=0;
	while($row = sql_fetch_array($res)) {
		$i++;
		c(stream_display($row,'mobile'));
	}
	if ($i == 15) {
		$next = $mpage+1;
		c('
		<form action="'.url('mobile/index').'" method="post" />
		<input type="hidden" name="filter" value="'.$filter.'" />
		<input type="hidden" name="mpage" value="'.$next.'" />
		<input type="submit" value="More.." />
		</form>
		');
	}
}

else {
	c(t('None'));
}

section_close(t('News feed'));