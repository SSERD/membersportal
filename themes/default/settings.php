<?php
$theme_blocks['lsidebar'] = array(
		'name'=>'Left sidebar',
		'description'=>'Under the personal menu bar(Recommended width:150px)',
		'default_value'=>''
		);

$theme_blocks['sidebar'] = array(
		'name'=>'200px sidebar',
		'description'=>' (Recommended width:200px)',
		'default_value' => ''
		);

$theme_blocks['adsbar'] = array(
		'name'=>'160px sidebar',
		'description'=>' (Recommended width:160px)',
		'default_value'=>'ads'
		);