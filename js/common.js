﻿$( document ).ready(function() {


	$("#chatlist").css("height",($(window).height()-200)+"px");
	function fitlayout(callback) {
		$("#searchform").width( $("#jcow_banner").width() - 600);
		if ($hidemenu) {
			$("#jcow_main").css({"max-width":"1095px"});
			$("#jcow_sec_menu").hide();
		}
		else {
			if ($(window).width() > 1100) {
				$("#jcow_main").css({"margin-left":"190px"});
				$("#jcow_app_menu").css('box-shadow', 'none');
				$("#jcow_app_menu").show();
				$("#jcow_sec_menu").hide();
				$("#jcow_primary_menu").show();
			}
			else {
				$("#jcow_main").css({"margin-left":"auto"});
				$("#jcow_app_menu").hide();
				$("#jcow_app_menu").css('box-shadow', '5px 5px 8px #ccc');
				$("#jcow_sec_menu").show();
				$("#jcow_primary_menu").hide();
			}
		}
		if ($(window).width() > 900) {
			if ($("#jcow_side").length) {
				if ($hidemenu) {
					$("#jcow_content").css({"width":"745px","float":"left"});
				}
				else {
					$("#jcow_content").css({"width":"560px","float":"left"});
				}
			}
			else {
				$("#jcow_content").css({"width":"100%"});
			}
			$("#menu").show();
		}
		else {
			$("#menu").hide();
			$("#jcow_content").css({"width":"100%"});
		}
		
		
		if (typeof callback === "function") {
			callback();
		}
	}
	$("#show_jcow_app_menu").click(function() {
		$("#jcow_app_menu").show();
		return false;
	});
	$(document).mouseup(function (e) {
	    var container = $("#jcow_app_menu");
	    if (!container.is(e.target)  && container.has(e.target).length === 0)  {
	    	if ($("#jcow_sec_menu").is(":visible")) {
	        	container.hide();
	        }
	    }
	});
	$(window).load(function(){
		$('.grid').masonry({
	  columnWidth: 215,
		  itemSelector: '.grid-item'
		});
	});
	$(document).ready(function(){
		fitlayout();
	});
	$(window).resize(function() {
	  fitlayout();
	});

	$('.jcow_select').selectpicker({
	});

	$(document).on("click",".repost_profile",function() {
		$('#shareModal').modal('show');
		$("#share_content").html("<div style='padding:50px;font-size:30px'><i class=\"fa fa-lg fa-spinner fa-spin\"></i></div>");
		var sid = $(this).attr("sid");
		$.get($baseurl+"/index.php?p=feed/share/"+sid,
		  function(data){
			$("#share_content").html(data);
			init_story_img_thumb();
			},"html"
		);
		return false;
	});
	$(document).on("click",".share_post",function() {
		var sid = $(this).attr("sid");
		var block = $(this).closest(".block_content");
		var msg = block.find(".share_msg").val();
		var pageid = block.find("option:selected").val();
		$(this).parent().html("<i class=\"fa fa-lg fa-spinner fa-spin\"></i>");
		$.post($baseurl+"/index.php?p=feed/sharepost/"+sid,
			{msg:msg,pageid:pageid},
		  function(data){
			$("#share_content").html(data);
			setTimeout(function() {$('#shareModal').modal('hide');}, 1000);
			},"html"
		);
		return false;
	});

	$(document).on("click",".chatbox_btn",function() {
		$("#dmodal_title").html($(this).attr("ctitle"));
		$("#msgsend").attr("uid",$(this).attr("uid"));
		$('#chatModal').modal('show');
		$("#chatlist").html("<i class=\"fa fa-spinner fa-spin\"></i>");
		return false;
	});
	var chatupdatehandle = 0;
	$('#chatModal').on('show.bs.modal', function (e) {
		update_chatlist();
		chatupdatehandle = window.setInterval(function(){
		  update_chatlist();
		}, 2000);
	});
	function update_chatlist() {
		$.post($baseurl+"/index.php?p=message/ajax_list_output/"+$("#msgsend").attr("uid"),
		  function(data){
		  	if (data.length != $("#chatlist").html().length) {
			  	$("#chatlist").html(data);
			  	$("#chatlist").scrollTop($("#chatlist").prop("scrollHeight"));
			  }
			},"html"
		);
	}

	$('#chatModal').on('shown.bs.modal', function (e) {
		$("#msgcontent").focus();
		$("#chatlist").scrollTop($("#chatlist").prop("scrollHeight"));
	});
	$('#chatModal').on('hide.bs.modal', function (e) {
		window.clearInterval(chatupdatehandle);
	});
	if ($("#jcow_msg_new").is(":empty")) {
		$("#jcow_msg_new").hide();
	}
	if ($("#jcow_note_new").is(":empty")) {
		$("#jcow_note_new").hide();
	}
	$("#jcow_note_new").bind("DOMSubtreeModified",function(){
		if ($(this).is(":empty")) {
			$(this).hide();
		}
		else {
			$(this).show();
		}
	});
	$("#jcow_msg_new").bind("DOMSubtreeModified",function(){
		if ($(this).is(":empty")) {
			$(this).hide();
		}
		else {
			$(this).show();
		}
	});

	$("#msgcontent").keypress(function(e) {
		if(e.which == 13) {
			$("#msgsend").click();
		}
	});

	$(document).on("click","#msgsend",function() {
		var message = $("#msgcontent");
		var uid = $(this).attr("uid");
		if (!message.val().length) {
			message.focus();
			message.fadeOut(100).fadeIn(100).fadeOut(100).fadeIn(100);
			return false;
		}
		$("#chatopt").hide();
		$("#chatloading").html("<i class=\"fa fa-spinner fa-spin\"></i>");
		$.post($baseurl+"/index.php?p=message/ajax_post",
		{uid:uid,message:message.val()},
		  function(data){
		  	message.val("");
		  	$("#chatloading").html("");
		  	$("#chatopt").show();
		  	$("#chatlist").html(data);
		  	$("#chatlist").scrollTop($("#chatlist").prop("scrollHeight"));
			},"html"
		);
		return false;
	});


	$("a[href$=notifications]").click(function() {
		var link = $(this);
	    var dialog = $("#notificationbox");
	    if (!dialog.is(":visible")) {
	    	$("#jcow_note_new").html("");
	    	dialog.html("<center><i class=\"fa fa-2x fa-spinner fa-spin\"></i></center>");
		    dialog.css("top", link.position().top + 30);
		    dialog.css("left", link.position().left - 310);
		    dialog.css("position", "fixed");
		    $.post($baseurl+"/index.php?p=notifications/ajax",
			  function(data){
				dialog.html(data);
				},"html"
			);
		    dialog.show();
		}
		return false;
	});
	$(".up_msg").click(function() {
		var link = $(this);
	    var dialog = $("#notificationbox");
	    if (!dialog.is(":visible")) {
	    	$("#jcow_msg_new").html("");
	    	dialog.html("<center><i class=\"fa fa-2x fa-spinner fa-spin\"></i></center>");
		    dialog.css("top", link.position().top + 30);
		    dialog.css("left", link.position().left - 310);
		    dialog.css("position", "fixed");
		    $.post($baseurl+"/index.php?p=message/ajax_note",
			  function(data){
				dialog.html(data);
				},"html"
			);
		    dialog.show();
		}
		return false;
	});



	$(document).on("click",".quick_comment",function() {
		var ubox = $(this).closest(".user_post_content");
		ubox.find(".quick_comment_form").css("display","block");
		var scbox = ubox.find(".commentmessage");
		if (!scbox.val().length && $(this).attr("uname").length) {
			scbox.focus().val( "@"+$(this).attr("uname")+" ");
		}
		else {
			scbox.focus();
		}
		scbox.next().show();
		return false;
			
	});
	$(document).on("click",".stream_hot_btn",function() {
		var sid = $(this).attr("sid");
		cbox = $(this).parent();
		cbox.html("<i class=\"fa fa-spinner fa-spin\"></i>");
		$.post($baseurl+"/index.php?p=jquery/stream_hot_btn",
		{sid:sid},
		  function(data){
			cbox.html(data);
			},"html"
		);
		return false;
	});
	$(document).on("click",".story_feature_btn",function() {
		var sid = $(this).attr("sid");
		cbox = $(this).parent();
		cbox.html("<i class=\"fa fa-spinner fa-spin\"></i>");
		$.post($baseurl+"/index.php?p=jquery/story_feature_btn",
		{sid:sid},
		  function(data){
			cbox.html(data);
			},"html"
		);
		return false;
	});
	$(document).on("click",".quick_reply",function() {
			var qcform = $(this).parents(".user_comment").parent().find(".quick_comment_form");
			qcform.css("display","block");
			var scbox = qcform.find(".commentmessage");
			if ($(this).attr("uname").length) {
				scbox.focus().val( "@"+$(this).attr("uname")+" ");
			}
			else {
				scbox.focus().val("");
			}
			return false;
			
	});
	$(document).on("click",".commentmessage",function() {
			if (!$(this).val().length && $(this).attr("uname").length) {
				$(this).val( "@"+$(this).attr("uname")+" ");
			}
			return false;
			
	});
	$(document).on("click",".comment_smile_btn",function() {
		var cmt = $(this).prev();
		$(document).find(".jcow_live_cmt").removeClass("jcow_live_cmt");
		cmt.addClass("jcow_live_cmt");
		if (!cmt.val().length && cmt.attr("uname").length) {
			cmt.val( "@"+cmt.attr("uname")+" ");
		}
		if (!$(this).hasClass("smile_atd")) {
			$(this).popover({ 
			    html : true,
			    placement:"left",
			    trigger:"focus",
			    content: function() {
			      return $("#jcow_smile_opt").html();
			    }
			  }).popover("show");
			$(this).addClass("smile_atd");
		}
		return false;
	});
	$(document).on("click",".jcow_emoji",function() {
		var cmt = $(document).find(".jcow_live_cmt");
		cmt.focus().val(cmt.val()+" emoji"+$(this).attr("eid")+" ");
	});

	$(document).mouseup(function (e) {
	    var container = $("#notificationbox");
	    if (!container.is(e.target) && container.has(e.target).length === 0) {
	        container.hide();
	    }
	    if (!$("#searchbar").is(e.target)) {
	        $("#searchresultbox").hide();
	    }
	});

	$(document).keypress(function(e) {
	  if(e.which == 13) {
	  	if ($(e.target).hasClass("commentmessage")) {
	   		var thiscomment = $(e.target).parents(".quick_comment_form");
			var thisform = $(e.target).parents(".jcow_comment_box");
			var mbox = thiscomment.find(".commentmessage");
			if (mbox[0].value != "") {
				thiscomment.hide();
				var cbox = thiscomment.next().next();
				var tbox = thiscomment.next();
				var img_token = thiscomment.find(".img_pre").attr("token");
				cbox.html("<i class=\"fa fa-spinner fa-lg fa-spin\"></i>");
				$.post($baseurl+"/index.php?p=jquery/comment_publish",
				{message:mbox[0].value,target_id:tbox[0].value,img_token:img_token},
				  function(data){
					cbox.html("");
					thisform.before(data);
					mbox.val("");
					},"html"
				);
				return false;
			}
	   	}
	  }
	});

	$(document).on("click",".stream_delete",function() {
		var parentdd = $(this).parents(".user_post_1");
		var sid = $(this).attr("sid");
		$(this).after("<i class=\"fa fa-spinner fa-lg fa-spin\"></i> deleting..");
		$(this).hide();
		$.get($baseurl+"/index.php?p=jquery/stream_delete/"+sid, function(data) {
			parentdd.hide("slow");
		});
		return false;
	});
	
	
	$(document).on("click",".dolike",function() {
		var cbox = $(this).parent();
		var sid = $(this).attr("sid");
		cbox.html("<i class=\"fa fa-spinner fa-lg fa-spin\"></i>");
		$.post($baseurl+"/index.php?p=jquery/dolike",
		{target_id:sid},
		  function(data){
			cbox.html("");
			cbox.html(data);
			},"html"
		);
		return false;
	});
	$(document).on("click",".dodislike",function() {
		var cbox = $(this).parent();
		var sid = $(this).attr("sid");
		$(this).parent().css("display","none");
		cbox.html("<i class=\"fa fa-spinner fa-lg fa-spin\"></i>");
		$.post($baseurl+"/index.php?p=jquery/dodislike",
		{target_id:sid},
		  function(data){
			cbox.html("");
			cbox.html(data);
			},"html"
		);
		return false;
	});

	$(".dofollow").click(function() {
		alert('Sorry, "Follow" is not enabled in Community Edition');
		return false;
	});
	$(".doadd").click(function() {
		alert('Sorry, friendship is not enabled in Community Edition');
		return false;
	});
	$(document).on("click",".events_engage",function() {
		var sid = $(this).attr("eventid");
		var engage = $(this).attr("engage");
		var tbox = $(this).parent();
		$("a[eventid="+sid+"]").hide();
		tbox.html("<i class=\"fa fa-spinner fa-lg fa-spin\"></i>");
		$.post($baseurl+"/index.php?p=events/ajax_engage",
		{sid:sid,engage:engage},
		  function(data){
			tbox.html(data);
			},"html"
		);
		return false;
	});
	$(document).on("click",".join_group",function() {
		var gid = $(this).attr("groupid");
		var tbox = $(this).parent();
		$("a[groupid="+gid+"]").hide();
		tbox.html("<i class=\"fa fa-spinner fa-lg fa-spin\"></i>");
		$.post($baseurl+"/index.php?p=groups/ajax_join",
		{gid:gid},
		  function(data){
			tbox.html(data);
			},"html"
		);
		return false;
	});

	$(".menu li.menugen").mouseover(function() {
		$(this).removeClass("menugen");
		$(this).addClass("menuhover");
	});
	$(".menu li.menugen").mouseout(function() {
		$(this).removeClass("menuhover");
		$(this).addClass("menugen");
	});
	$("a[rel*=facebox]").facebox();
	$(document).on("click",".deleteuser",function() {
		uid = $(this).attr("uid");
		$.facebox({ ajax: $baseurl+"/index.php?p=jquery/deleteuser/"+uid });
		return false;
	});
	$(document).on("click",".stream_opt",function() {
		$(this).hide();
		$(this).prev().slideDown();
	});
	$(".jcow_buttonflag").click(function() {
		$(this).prev().click();
	});
	$(".jcow_buttonflag").css("cursor","pointer");


	$(document).on("click",".att_button", function() {
		var page_type = $("#page_type").val();
		var app_name = $(this).attr("appname");
		if (!$(this).hasClass("att_button_on")) {
			clear_link_preview();
			if ($(this).attr("id") == "stream_att_status_radio") {
				$("#apps_box").html("");
				$(".att_button").removeClass("att_button_on");
				$(this).addClass("att_button_on");
				$("#attachment").attr("value","status");
				$("#form_message").show();
				$("#form_message").css("height","65px");
				$("#form_message").focus();
				$("#quick_img").show();
			}
			else {
				$("#form_submit").hide();
				$("#form_message").slideUp();
				$("span#spanstatus").html("<i class=\"fa fa-2x fa-spinner fa-spin\"></i> Loading");
				$("#apps_box").slideUp();
				$.get($baseurl+"/index.php?p="+app_name+"/ajax_form/"+page_type, function(data) {
						$("#apps_box").html(data);
					$("span#spanstatus").html("");
					$("#apps_box").slideDown(function() {
						$("#form_submit").show();
					});
					$("#form_submit").prop( "disabled", false );
				})
				.done(function() {

				})
				.fail(function() {
				alert( "error" );
				});
				$(".att_button").removeClass("att_button_on");
				$(this).addClass("att_button_on");
				$("#attachment").attr("value",app_name);
				$("#charsRemaining").html("");
				$("#status_photo").hide();
				$("#quick_img").hide();
			}
		}
	});


	$("#privacy_selector_public").click(function() {
				$("#privacy_input").val("1");
				$(this).hide();
				$(document).find(".share_tags").slideUp();
				$("#privacy_selector_friends").fadeIn("slow");
				return false;
			});
			$("#privacy_selector_friends").click(function() {
				$("#privacy_input").val("0");
				$(this).hide();
				$(document).find(".share_tags").slideDown();
				$("#privacy_selector_public").fadeIn();
				return false;
			});
			

			$("#form_message").keyup(function() {
				if ($("#attachment").val() == "status") {
					var max = parseInt($("#form_message").attr("maxlength"));
					if($(this).val().length > max){
						$(this).val($(this).val().substr(0, $(this).attr("maxlength")));
					}
					$("#charsRemaining").html("You have <strong>" + (max - $(this).val().length) + "</strong> characters remaining");
				}
			});
			$("#status_photo_button").click( function() {
				$(this).hide();
				$("#status_photo_file").attr("disabled",false);
				$("#status_photo_box").show();
				$("#status_photo_file").click();
				return false;
			});
			$("#status_photo_cancel").click( function() {
				$("#status_photo_file").attr("disabled",true);
				$("#status_photo_box").hide();
				$("#status_photo_button").show("fast");
				return false;
			});
			$("#status_photo_file").bind("change", function() {
				var extension = this.files[0].name.substr( (this.files[0].name.lastIndexOf('.') +1) );
				extension = extension.toLowerCase();
				if (extension != "jpg") {
					$("#status_photo_file").attr("disabled",true);
					$("#status_photo_box").hide();
					$("#status_photo_button").show();
					alert("Sorry, only JPG file is accepted");
					return false;
					}
			});

			$("#cancel_form").click(function() {
				$("#stream_att").show();
			});

			$(document).on("click",".youtube-btn",function() {
				var youtubeid = $(this).attr("youtubeid");
				$(this).removeClass("youtube-btn");
		        $(this).html("<iframe  src='//www.youtube.com/embed/"+youtubeid+"?autoplay=1' frameborder='0' allowfullscreen></iframe>");
			});
			$("#searchbar_btn").click(function() {
				$("#searchform").submit();
			});
			$(".top_search_box").on("shown.bs.dropdown", function(){
				$("#top_search_input").focus();
			});

			function clear_link_preview() {
				$("#link_title").val("");
				$("#link_url").val("");
				$("#link_image").val("");
				$("#link_des").val("");
				$("#link_status").val("");
				$("#link_preview").html("");
				$("#link_remove").hide();
			}

			$("#link_remove").click(function() {
					clear_link_preview();
					$(".jcow_sf_img_btn").show();
					$(".jcow_sf_video_btn").show();
					return false;
				});
	$(".form_message").on("paste",function() {

		if (!$("#quick_img_file").val() && $("#link_status").val() != "shown") {
			$("#quick_img").hide();
			setTimeout(function() {
				var match_url = /\b(https?):\/\/([\-A-Z0-9.]+)(\/[\-A-Z0-9+&@#\/%=~_|!:,.;]*)?(\?[A-Z0-9+&@#\/%=~_|!:,.;]*)?/i;
			    if (match_url.test($(".form_message").val())) {
					var url = $(".form_message").val().match(match_url)[0];
					console.log(url);
					$("#link_loading").html("<i class=\"fa fa-spinner fa-pulse fa-3x fa-fw\"></i>");
					var yql_url = "//query.yahooapis.com/v1/public/yql?" 
			          + "q=SELECT%20*%20FROM%20htmlstring%20WHERE%20url=%27" 
			          + encodeURIComponent(url) 
			          + "%27%20AND%20xpath=%27descendant-or-self::meta%27"
			          + "&format=json" + "&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
			         console.log(yql_url);
					$.getJSON(yql_url
				 	 , function(data) {
				 	 	var s = $("<html/>").append(data.query.results.result);
				 	 	var og_title = s.find('meta[property="og:title"]').attr("content");
				 	 	var og_image = s.find('meta[property="og:image"]').attr("content");
				 	 	var og_des = s.find('meta[property="og:description"]').attr("content");
						console.log("ogtitle:"+og_title);
						console.log("ogimage:"+og_image);
						console.log("og_des:"+og_des);
				 	 	$("#link_loading").html("");
				      if (og_title.length > 0) {
				      	$(".jcow_sf_img_btn").hide();
				      	$(".jcow_sf_video_btn").hide();
				      	$("#link_remove").show();
				      	$("#link_title").val(og_title);
				      	$("#link_url").val(url);
				        if (og_image.length > 0) {
				        	$("#link_preview").append("<img src=\""+og_image+"\" style=\"width:100%;height:auto\" />");
				        	$("#link_image").val(og_image);
				        }
				        $("#link_preview").append("<h3>"+og_title+"</h3");
				        if (og_des.length > 0) {
				        	$("#link_preview").append("<div class=\"sub\">"+og_des+"</div>");
				        	$("#link_des").val(og_des);
				        	$("#link_status").val("shown");
				        }
				      } else {
				      };
					});
				}
			},500);
		}
	});

	var search_xhr;
	$("#searchbar").keyup(function() {
		var search = $(this);
		if (search.val().length>0) {
			clearTimeout(search_xhr);
			search_xhr = setTimeout(function() {
			    var dialog = $("#searchresultbox");
		    	$("#sr_spin").html("<center><i class=\"fa fa-2x fa-spinner fa-spin\"></i></center>");
		    	dialog.width( search.width());
			    dialog.css("top", $("#searchform").position().top + 30);
			    dialog.css("left", $("#searchform").position().left);
			    dialog.show();
			    $.get( "index.php?p=search/ajax_search&q="+search.val(), function( data ) {
			    	$("#sr_spin").html("");
				  $("#sr_results").html( data);
				});
			},500);
		}
	});

	$(document).on("click",".form_submit",function() {
		var sharebox = $(this).closest(".jcow_share_box");
		var form_message = sharebox.find(".form_message");
		if (!$.trim(form_message.val())) {
			form_message.fadeOut(100).fadeIn(100).fadeOut(100).fadeIn(100);
			return false;
		}
		sharebox.find(".spanstatus").html("<i class=\"fa fa-spinner fa-pulse fa-3x fa-fw\"></i>");
		sharebox.find(".jcow_share_box_content").slideUp();
		var img_tokens = '';
		sharebox.find(".img_pre").each(function() {
			img_tokens = img_tokens+','+$(this).attr("token");
			$(this).remove();
		});
		$.ajax({
		       url: $baseurl+"/index.php?p=streampublish",
		       type : "POST",
		       data : {
		       		message:sharebox.find(".form_message").val(),
		       		home_page_id:sharebox.find(".home_page_id :selected").val(),
		       		img_tokens:img_tokens,
		       		page_id:sharebox.find(".page_id").val(),
		       		checkin_name:sharebox.find(".autocompleteinput").val(),
		       		video_url:sharebox.find(".video_input").val(),
		       		link_url:$("#link_url").val(),
		       		link_title:$("#link_title").val(),
		       		link_image:$("#link_image").val(),
		       		link_des:$("#link_des").val()
		   		},
		       success : function(data) {
		       		sharebox.find(".form_message").val("");
		       		sharebox.find(".checkin_name").val("");
		       		sharebox.find(".video_input").val("");
		       		sharebox.find(".spanstatus").html("");
		       		sharebox.find(".video_preview").hide();
		       		sharebox.find(".jcow_sf_video_btn").removeClass("att_btn_on");
		       		sharebox.find(".jcow_sf_checkin_btn").removeClass("att_btn_on");
		       		$(".jcow_sf_img_btn").show();
					$(".jcow_sf_video_btn").show();
		       		clear_link_preview();
		       		sharebox.closest(".block").after("<div class=\"block\"><div class=\"block_content\">"+data+"</div></div>");
		       		sharebox.find(".jcow_share_box_content").slideDown(function() {
		       			init_story_img_thumb();
		       		});
		       		//obj = $.parseJSON(data);
		       }
		});

	});
	$(document).on("focus",".form_message",function() {
		autosize($(this));
	});
	$(document).on("click", ".commentpic_btn", function() {
		alert("Sorry, comment image is not available in Community Edition");
		return false;
	});
	$(document).on("click",".cmt_img_pre_remove",function() {
		var commentbox = $(this).closest(".quick_comment_form");
		$(this).closest(".img_pre").remove();
		commentbox.find(".commentpic_btn").show();
		return false;
	});

	$(document).on("click", ".jcow_sf_img_btn", function() {
		$(this).closest(".jcow_share_box").find(".jcow_sf_img").click();
		return false;
	});
	$(document).on("change", ".jcow_sf_img", function() {
		for(var i=0; i< this.files.length; i++){
			var file = this.files[i];
			var sharebox = $(this).closest(".jcow_share_box");
			var previewbox = sharebox.find(".img_preview");
			var ext = $(this).val().split(".").pop().toLowerCase();
			if (ext != "jpg" && ext != "gif" && ext != "png" && ext != "jpeg") {
				alert("Please select jpg/gif/png file"+ext);
				return false;
			}
			if (file.size > 10000000) {
				alert("Please select file less than 10M");
				return false;
			}
			sharebox.find(".jcow_sf_video_btn").hide();
			if (previewbox.find(".img_pre").length >= 10) {
				sharebox.find(".jcow_sf_img_btn").hide();
			}
			var formData = new FormData();
			formData.append("file", file);
			previewbox.append("<div  class=\"img_pre img_loading\"><i class=\"fa fa-spinner fa-pulse fa-3x fa-fw\"></i></div>");
			$.ajax({
			       url: "index.php?p=jquery/uploadimg",
			       type : "POST",
			       data : formData,
			       processData: false,  
			       contentType: false,
			       success : function(data) {
			       		previewbox.find(".img_loading").remove();
			       		obj = $.parseJSON(data);

			           previewbox.append("<div class=\"img_pre\" token=\""+obj.token+"\"><img src=\""+obj.thumb+"\" /><a href=\"#\" class=\"img_pre_remove\"><i class=\"fa fa-remove\"></i> Remove</a></div>");
			       }
			});
		}
	});
	
	$(document).on("click",".img_pre_remove",function() {
		var sharebox = $(this).closest(".jcow_share_box");
		$(this).closest(".img_pre").remove();
		if (sharebox.find(".img_pre").length <= 3) {
			sharebox.find(".jcow_sf_img_btn").show();
		}
		if (!sharebox.find(".img_pre").length) {
			sharebox.find(".jcow_sf_video_btn").show();
		}
		return false;
	});
	$(document).on("click",".test_img",function() {
		$(this).closest(".jcow_share_box").find(".img_pre").each(function() {
			$(".img_preview").append("<br />"+$(this).attr("token"));
		});
		return false;
	});
	$(document).on("click",".jcow_sf_checkin_btn",function() {
		var checkin_input = $(this).closest(".jcow_share_box").find(".checkin_preview");
		var checkin_btn = $(this);
		checkin_input.slideToggle("fast",function() {
			if (checkin_input.is(":visible")) {
				checkin_input.find("input").focus();
				checkin_btn.addClass("att_btn_on");
			}
			else {
				checkin_btn.removeClass("att_btn_on");
				checkin_input.val("");
			}
		});
		return false;
	});
	$(document).on("click",".jcow_sf_video_btn",function() {
		var video_input = $(this).closest(".jcow_share_box").find(".video_preview");
		var video_btn = $(this);
		video_input.slideToggle("fast",function() {
			if (video_input.is(":visible")) {
				$(this).closest(".jcow_share_box").find(".jcow_sf_img_btn").hide();
				video_input.find("input").focus();
				video_btn.addClass("att_btn_on");
			}
			else {
				$(this).closest(".jcow_share_box").find(".jcow_sf_img_btn").show();
				video_btn.removeClass("att_btn_on");
				video_input.val("");
			}
		});
		return false;
	});

	$(document).on("submit",".ajaxform",function() {
		var thisform = $(this);
		var formData = new FormData();
		$.each($(this).find("input[type='file']"), function(i, tag) {
        	$.each($(tag)[0].files, function(i, file) {
	            formData.append(tag.name, file);
	        });
	    });
	    var params = $(this).serializeArray();
	    $.each(params, function (i, val) {
	        formData.append(val.name, val.value);
	    });
		var action = $(this).attr("action");
		var btn = $(this).find("[type=submit]");
	    $.ajax({
	        url: action,
	        type: 'POST',
	        data: formData,
	        async: false,
	        cache: false,
	        dataType: "json",
	        processData: false,
	        contentType: false,
	        async: true,
	        beforeSend: function() {
	        	$("#overlay").show();
	        },
	        success: function (data) {
	        	$("#overlay").hide();
	        	if (data.act == 'redirect') {
	        		window.location.href = data.url;
	        	}
	        	else {
	        		thisform.find(".alert-danger").remove();
		        	btn.before('<div class="alert alert-danger" role="alert">'+data.msg+'</div>');
		        }
	        } 
	        
	    });
	    return false;
	});

	$(document).on("click",".jcow_btn",function() {
		var url = $(this).attr("href");
		var btn = $(this);
		btn.removeAttr("href");
		btn.removeClass("jcow_btn");
		btn.html("<i class=\"fa fa-spinner fa-spin\"></i>");
		$.ajax({
			url:url,
			success:function(data) {
				btn.html(data);
			}
		});
		return false;
	});

	function init_story_img_thumb() {
		$(document).find('.story_img_gallery').each(function() {
			$(this).magnificPopup({
				delegate: 'a',
			    type: 'image',
				gallery: {
		          enabled:true
		        }
			});
		});
		$(document).find('.story_img_gallery').removeClass("story_img_gallery");
	}
	init_story_img_thumb();

});



function js_check_all(formobj)
{
	exclude = new Array();
	exclude[0] = 'keepattachments';
	exclude[1] = 'allbox';
	exclude[2] = 'removeall';
	js_toggle_all(formobj, 'checkbox', '', exclude, formobj.allbox.checked);
}

function js_toggle_all(formobj, formtype, option, exclude, setto)
{
	for (var i =0; i < formobj.elements.length; i++)
	{
		var elm = formobj.elements[i];
		if (elm.type == formtype)
		{
			switch (formtype)
			{
				case 'radio':
					if (elm.value == option) 
					{
						elm.checked = setto;
					}
				break;
				case 'select-one':
					elm.selectedIndex = setto;
				break;
				default:
					elm.checked = setto;
				break;
			}
		}
	}
}

function imposeMaxLength(Object, MaxLen)
				{
				  return (Object.value.length <= MaxLen);
				}







