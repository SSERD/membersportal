<?php
/* ############################################################ *\
 ----------------------------------------------------------------
Jcow Software (http://www.jcow.net)
IS NOT FREE SOFTWARE
http://www.jcow.net/commercial_license
Copyright (C) 2009 - 2010 jcow.net.  All Rights Reserved.
 ----------------------------------------------------------------
\* ############################################################ */

class mobile {
	function mobile() {
		global $client,$parr;
		
	}


	function setpc() {
		global $from_url;
		$_SESSION['pc_style'] = 1;
		$_SESSION['mobile_style'] = 0;
		if (!strlen($from_url) || preg_match("/mobile/i",$from_url)) {
			$from_url = url('feed');
		}
		c(t('Complete, please refresh this page'));
		stop_here();
	}
	function setmobile() {
		global $from_url;
		$_SESSION['pc_style'] = 0;
		$_SESSION['mobile_style'] = 1;
		if (!strlen($from_url) || preg_match("/mobile/i",$from_url)) {
			$from_url = url('feed');
		}
		


		c(url($from_url,t('Continue')));
		stop_here();
	}
	

	function admin() {
		if ($_POST['step'] == 'post') {
			set_text('mobile_ids',$_POST['mobile_ids']);
			redirect('mobile/admin',1);
		}
		if (!$mobile_ids = get_text('mobile_ids')) {
			$mobile_ids = 'iphone,nokia,BlackBerry,HTC,Motorola,Nokia,Samsung';
		}
		c('<form action="'.url('mobile/admin').'" method="post">
		<p>'.label('Mobile Agent IDS').'
		<textarea name="mobile_ids" rows="10">'.h($mobile_ids).'</textarea>
		</p>
		<p><input type="submit" value="Save" />
		<input type="hidden" name="step" value="post" /></p>
		</form>');
	}
}


function mobile_f_request() {
	global $client;
	if (!$client['id']) {
		return false;
	}
	$res = sql_query("select count(*) as num from `".tb()."friend_reqs` where fid='{$client['id']}'");
	$row = sql_fetch_array($res);
	if (!$row['num']) {
		return false;
	}
	return url('mobile/friends/requests',t('Friends').'<span> ('.$row['num'].')</span>');
}

function mobile_avatar($row, $type = 'small') {
	global $uhome, $ubase;

	if (!$row['avatar']) {
		$row['avatar'] = 'undefined.jpg';
	}
	$row['avatar'] = 's_'.$row['avatar'];
	if ($type == 'icon') {
		return '<a href="'.url('mobile/u/'.$row['username']).'"><img  src="'.uhome().'/'.uploads.'/avatars/'.$row['avatar'].'" class="avatar"  width="25" height="25"/></a>';
	}
	else {
		return '<a href="'.url('mobile/u/'.$row['username']).'"><img  src="'.uhome().'/'.uploads.'/avatars/'.$row['avatar'].'" class="avatar" /></a>';
	}
}