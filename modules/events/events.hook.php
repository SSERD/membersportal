<?php
/* ############################################################ *\
 ----------------------------------------------------------------
GUN GPL
 ----------------------------------------------------------------
\* ############################################################ */

function events_u_menu(&$tab_menu,$page) {
	$res = sql_query("select id from jcow_stories where page_id='{$page['id']}' and app='events' limit 1");
	if (sql_counts($res)) {
		$tab_menu[] = array(
		'name'=>'Events',
		'type'=>'tab',
		'path'=>'events/liststories/page_'.$page['id']
		);
	}
}
function events_group_menu(&$tab_menu,$page) {
	$res = sql_query("select id from jcow_stories where page_id='{$page['id']}' and app='events' limit 1");
	if (sql_counts($res)) {
		$tab_menu[] = array(
		'name'=>'Events',
		'type'=>'tab',
		'path'=>'events/liststories/page_'.$page['id']
		);
	}
}
/* owner,connected,everyone */
function events_quick_share() {
	return array(
		'u' => array('access'=>'owner','flag' => t('Events'),'weight'=>10),
		'group' => array('access'=>'connected','flag' => t('Events'),'weight'=>10)
		);
}