<?php
/* ############################################################ *\
 ----------------------------------------------------------------
Jcow Software (http://www.jcow.net)
IS NOT FREE SOFTWARE
http://www.jcow.net/commercial_license
Copyright (C) 2009 - 2010 jcow.net.  All Rights Reserved.
 ----------------------------------------------------------------
\* ############################################################ */

class report{
	
	function report() {
		global $client, $menuon;
		clear_report();
		if (!$client['id']) {
			redirect('member/login/1');
		}
		clear_as();
	}

	function ajax() {
		global $client;
		if (strlen($_GET['url'])) {
			sql_query("insert into ".tb()."reports (uid,message,url,created) values('{$client['id']}','quick report','{$_GET['url']}',".time().")");
		}
		echo t('<span style="color:red">Reported</span>');
		exit;
	}
	
	function index() {
		global $db, $nav, $client;
		set_title('Report a page to Admin');
		clear_as();
		$nav[] = t('Report');
		$report_url = getenv(HTTP_REFERER);
		if (is_numeric($_GET['uid']) && $_GET['uid'] > 0) {
			$uid = $_GET['uid'];
			$res = sql_query("select * from ".tb()."accounts where id='$uid'");
			$row = sql_fetch_array($res);
			if (!$row['id']) die('wrong uid');
			$report_url = url('u/'.$row['username']);
			$r_user = '<input type="hidden" name="r_uid" value="'.$uid.'" />';
			$u_name = '<strong>'.h($row['username']).'</strong><br />';
		}
		elseif (is_numeric($_GET['pid']) && $_GET['pid'] > 0) {
			$pid = $_GET['pid'];
			$res = sql_query("select * from ".tb()."pages where id='$pid'");
			$row = sql_fetch_array($res);
			if (!$row['id']) die('wrong pid');
			$report_url = url('page/'.$row['uri']);
			$r_user = '<input type="hidden" name="r_uid" value="'.$row['uid'].'" />';
			$u_name = '<strong>'.h($row['username']).'</strong><br />';
		}
		elseif (strlen($_GET['url'])) {
			$report_url = $_GET['url'];
		}
		if (!preg_match('/'.str_replace('/','\/',uhome()).'/',$report_url) || !$report_url) die('unable to find the url');
		c('<h1>'.t('Report SPAM or Abuse').'</h1>
		<form method="post" action="'.url('report/post').'" >
					<p>
					'.$u_name.'
					URL:<br />
					<a href="'.$report_url.'">'.$report_url.'</a>
					</p>
					<p>
					'.label(t('Message to Admin')).'
					<textarea name="message" style="width:500px" rows="3"></textarea>
					</p>
					<p>
					<input type="hidden" name="report_url" value="'.h($report_url).'" />
					<input class="button" type="submit" value="'.t('Send to Admin').'" />
					'.$r_user.'
					</p>
					</form>');
	}

	function post() {
		global $db, $client, $config;
		if (is_numeric($_POST['r_uid']) && $_POST['r_uid'] > 0) {
			$uid = $_POST['r_uid'];
			$res = sql_query("select * from ".tb()."accounts where id='$uid'");
			$row = sql_fetch_array($res);
			if (!$row['id']) die('wrong uid');
			$timeline = time()-3600*48;
			if ($row['id']>100 && $row['created']>$timeline) {
				sql_query("update ".tb()."accounts set disabled=3 where id='$uid'");
			}
			$_POST['report_url'] = url('admin/useredit/'.$row['id']);
		}
		sql_query("insert into ".tb()."reports (uid,message,url,created) values('{$client['id']}','{$_POST['message']}','{$_POST['report_url']}',".time().")");
		set_title(t('Message sent, thank you!'));
		c(t('Message sent, thank you!'));
	}

	function delete($mid) {
		global $db, $client;
		// ids
		if (is_array($_REQUEST['ids'])) {
			foreach ($_REQUEST['ids'] as $id) {
				sql_query("delete from `".tb()."messages` where id='{$id}' and to_id='{$client['id']}' ");
			}
		}
		else {
			sql_query("delete from `".tb()."messages` where id='{$mid}' and to_id='{$client['id']}' ");
		}
		redirect(url('message/inbox'),1);
	}
}