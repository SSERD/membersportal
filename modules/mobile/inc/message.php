<?php
/* ############################################################ *\
 ----------------------------------------------------------------
Jcow Software (http://www.jcow.net)
IS NOT FREE SOFTWARE
http://www.jcow.net/commercial_license
Copyright (C) 2009 - 2010 jcow.net.  All Rights Reserved.
 ----------------------------------------------------------------
\* ############################################################ */
global $client,$offset,$num_per_page, $page;
if (!$act) {
	$res = sql_query("SELECT m.*,u.username FROM `".tb()."messages` as m left join `".tb()."accounts` as u on u.id=m.from_id where m.to_id='{$client['id']}' and m.from_id>0 ORDER by m.id DESC LIMIT $offset,$num_per_page ");
	c('<ul class="common"');
	while ($row = sql_fetch_array($res)) {
		if (!strlen($row['subject'])) {
			$row['subject'] = strip_tags(utf8_substr($row['message'],40));
		}
		if (!$row['from_id']) {
			$from_user = 'System';
		}
		else {
			$from_user = url('mobile/u/'.$row['username'],htmlspecialchars($row['username']));
		}
		if (!$row['hasread']) {
			$row['mark'] = '*';
		}
		c('<li>From '.$from_user.' <span class="sub">'.get_date($row['created']).'</span><br />
		'.$row['mark'].' '.url('mobile/message/view/'.$row['id'], htmlspecialchars($row['subject'])).'</li>');
	}
	c('</ul>');

	// pager
	$res = sql_query("select count(*) as total from `".tb()."messages` where to_id='{$client['id']}' and from_id>0");
	$row = sql_fetch_array($res);
	$total = $row['total'];
	$pb       = new PageBar($total, $num_per_page, $page);
	$pb->paras = $ubase.'mobile/message';
	$pagebar  = $pb->whole_num_bar();
	c($pagebar);
	section_close(t('Messages'));
}
elseif ($act == 'delete') {
	sql_query("delete from `".tb()."messages` where id='{$id}' and to_id='{$client['id']}' ");
	redirect(url('mobile/message'),1);
}

elseif ($act == 'view') {
	$res = sql_query("SELECT m.*,u.username from `".tb()."messages` as m left join `".tb()."accounts` as u on u.id=m.from_id where m.id='$id' ");
	$row = sql_fetch_array($res);


	if ($row['id']) {
		$nav[] = htmlspecialchars($row['subject']);
		set_title(htmlspecialchars($row['subject']));
		c('<strong>'.h($row['subject']).'</strong><div>');
		if ($row['from_id']) {
			c(url('mobile/message/compose/'.$id, t('Reply')).' | ');
		}
		c(url('mobile/message/delete/'.$id, t('Delete')).'</div>' );
		
		if (strlen($row['username'])) {
			$fromu = url('u/'.$row['username'],$row['username']);
		}
		else {
			$fromu = '<strong>System</strong>';
		}
		c('<span class="sub">From: '.$fromu.', '.get_date($row['created']).'</span>');
		c('<p>'.nl2br(decode_bb(htmlspecialchars($row['message']))).'</p>');
		sql_query("UPDATE `".tb()."messages` set hasread=1 where id='$id'");
	}
	else {
		die('wrong mid');
	}
	section_close();
}

elseif ($act == 'compose') {
	if (is_numeric($id)) {
		$res = sql_query("SELECT m.subject,m.message,u.username,u.id as uid from `".tb()."messages` as m LEFT JOIN `".tb()."accounts` as u on u.id=m.from_id where m.id='$id' and m.to_id='{$client['id']}' ");
		$message = sql_fetch_array($res);
		if (!preg_match("/^Re/",$message['subject'])) {
			if (!strlen($message['subject'])) {
				$message['subject'] = strip_tags(utf8_substr($message['message'],40));
			}
			$message['subject'] = 'Re:'.htmlspecialchars($message['subject']);
		}
		$uid = $message['uid'];
	}
	elseif (preg_match("/^u/",$id)) {
		$uid = str_replace('u','',$id);
		if (is_numeric($uid)) {
			$res = sql_query("select username from `".tb()."accounts` where id=$uid");
			$message = sql_fetch_array($res);
		}
	}
	else {
		die('no act');
	}
	$res = sql_query("select * from `".tb()."blacks` where bid={$client['id']} and uid={$uid} ");
	if (sql_counts($res)) {
		c(t('This user has blocked you'));
	}
	else {
		c('<form method="post" action="'.url('mobile/message/composepost').'" >
				<p>
				'.label(t('Send to')).'
				<input type="text" value="'.htmlspecialchars($message['username']).' '.htmlspecialchars($message['lastname']).'" disabled />
				</p>
				<p>
				'.label(t('Subject').' ('.t('Optional').')').'
				<input type="text" name="subject" size="25" value="'.$message['subject'].'"/>
				</p>
				<p>
				'.label(t('Message')).'
				<textarea name="message" style="width:250px" rows="5">'.$msg.'</textarea>
				</p>
				<p>
				<input type="hidden" name="uid" value="'.$uid.'" />
				<input class="button" type="submit" value="'.t('Send').'" />
				</p>
				</form>');
	}
	section_close();
}

elseif ($act == 'composepost') {
	if (!$_POST['uid'] || !$_POST['message']) {
		die(t('Please fill all the required blank'));
	}
	if(!$user = valid_user($_POST['uid'])) {
		die(t('Invalid username'));
	}
	$timeline = time();
	if ($res = sql_query("insert into `".tb()."messages` (from_id,to_id,subject,message,created) values('{$client['id']}','{$user['id']}','".$_POST['subject']."','".$_POST['message']."',$timeline)")) {
		sql_query("insert into `".tb()."messages_sent` (from_id,to_id,subject,message,created) values('{$client['id']}','{$user['id']}','".$_POST['subject']."','".$_POST['message']."',$timeline)");
		$mid = mysql_insert_id();
		mail_notice('message',$user['username'],t('You have a new PM from {1}',$client['username']),t('You have a new PM from {1}',$client['fullname']) );
		record_this_posting($_POST['message']);
	}
	redirect('mobile/message',1);
}