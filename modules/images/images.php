<?php
/* ############################################################ *\
 ----------------------------------------------------------------
Jcow Software (http://www.jcow.net)
IS NOT FREE SOFTWARE
http://www.jcow.net/commercial_license
Copyright (C) 2009 - 2010 jcow.net.  All Rights Reserved.
 ----------------------------------------------------------------
\* ############################################################ */
/*
text1: picids
*/
class images extends story{
	function __construct() {
		global $nav,$ubase,$num_per_page,$config;
		$num_per_page = 28;
		set_title(t('Images'));
		$this->top_stories = 1;
		$this->disable_category = 0;
		$this->images = 1;
		$this->allow_vote = 0;
		$this->list_type = 'gallery';
		$this->default_thumb = uploads.'/userfiles/undefined.jpg';
		parent::story();
		$this->tags = 1;
		$this->act_write = t('Shared image');
		$this->write_story = t('Upload/Manage');
		$this->label_title = t('Image Name');
		$this->submit = t('Next step');
		$this->label_entry = t('Images');
		$this->photos = 1;
		clear_as();
	}

	function index() {
		$this->liststories('hot');
	}


	function story_form_content($row = array()) {
		global $uhome;
			return '<p>'.label(t('Description')).'
	<textarea name="form_content" rows="5" class="form-control" >'.htmlspecialchars($row['content']).'</textarea>
	<input type="hidden" name="images" value="1" />
	</p>';
	}


		// 文章表单
	function writestory($page_id=0) {
		do_auth($this->story_write);
		clear_as();
		GLOBAL $ubase,$content,$nav, $client, $title, $sub_menu, $ass,$current_app,$cat_id;
		if ($page_id) {
			$page = $this->check_page_access($page_id);
			$page_id = $page['id'];
		}
		else {
			$page_id = $client['page']['id'];
		}

		// choose album
		$res = sql_query("SELECT * FROM `".tb()."stories` WHERE uid='{$client['id']}' and app='images' and page_id='{$page_id}' order by id DESC");
		if (sql_counts($res)) {
			c('<ul>');
			while ($row = sql_fetch_array($res)) {
				c('<li>'.url('images/managephotos/'.$row['id'],htmlspecialchars($row['title'])).' ('.get_date($row['created']).')</li>');
			}
			c('</ul>');
		}
		else {
			c('<p>'.t('You have no photo album').'</p>');
		}
		section_close(t('Choose an album to continue'));


		$cat_id = $cid;
		$sub_menu = $ass = '';
		$nav[] = $this->write_story;
		$this->set_current_sub_menu($cid);
		$title = $this->write_story;
		c('<div class="form"><form action="'.$ubase.$this->name.'/'.$this->writepost.'" method="post"  enctype="multipart/form-data">');
		if (!$this->disable_category) {
			c($this->story_form_cat($cid));
		}
		c($this->writestory_form_elements($row));
		if ($this->hook['writestory']) {
			c($this->hook_writestory($row));
		}
		c('<input type="hidden" name="images" value="1" />');
		c('<p><input type="hidden" name="page_id" value="'.$page_id.'" /><input class="button" type="submit" value="'.$this->submit.'" /></p>');
		c('</form></div>');
		section_close(t('Create a New album'));
	}



	function viewstory($sid) {
		clear_as();
		GLOBAL $db,$client,$ubase,$uhome,$nav,$content, $title, $page_title, $page, $client,$cat_id, $num_per_page, $offset, $config;
		enable_jcow_inline_ad();
		$res = sql_query("select s.*,u.birthyear,u.gender,u.location,u.avatar,u.username,u.fullname,u.disabled,u.created as ucreated,u.lastlogin,u.forum_posts,u.followers from `".tb()."stories` as s left join `".tb()."accounts` as u on u.id=s.uid where s.id='$sid' ");
		$row = sql_fetch_array($res);
		if (!$row['id']) die('wrong sid');

		$res = sql_query("select * from ".tb()."pages where id='{$row['page_id']}'");
		$jcow_page = sql_fetch_array($res);
		if (!$jcow_page['type']) die('unknown page id');
		if ($jcow_page['type'] == 'group') {
			include_once('modules/group/group.php');
			group::settabmenu($row['page_id'],1);
		}


		$title = $this->title_prefix.$row['title'];
		if (!$row['id']) {
			die(':)');
		}
		if ($row['cid']) {
			$cat = valid_category($row['cid']);
			$cat_id = $row['cid'];
			$closed = $row['closed'];
			$this->set_current_sub_menu($row['id']);
		}
		$res = sql_query("select * from jcow_streams where id='{$row['stream_id']}'");
		$stream = sql_fetch_array($res);
		$this->check_private($stream['type'],$row['uid']);
			// 更新文章阅读数目
			sql_query("update `".tb()."stories` set views=views+1 where id='$sid'");
			/*
			if (!$this->disable_category) {
				$nav[] = url($this->name.'/liststories/'.$cat['id'],$cat['name']);
			}
			*/
			$nav[] = $title = $page_title = htmlspecialchars($row['title']);

			
			$story .= $this->story_content($row);
			if (strlen($row['tags']) && $row['featured']) {
				if ($client['id']) {
					$tfollowed = array();
					$res2 = sql_query("select tid from ".tb()."topics_followed where uid='{$client['id']}'");
					while ($row2 = sql_fetch_array($res2)) {
						$tfollowed[] = $row2['tid'];
					}
				}
			}
			$story .= '<div id="sp_block_content_bottom">'.$config['sp_content_bottom'].show_ad('sp_block_content_bottom').'</div>';

			
			
			$story .= $this->show_tags($row);
			
			c($story);
			if (strlen(get_gvar('jcow_inline_banner_ad')) && !$config['hide_ad']) {
				c(get_gvar('jcow_inline_banner_ad'));
				global $adbanner_displayed;
				$adbanner_displayed = 1;
			}
			c('<div class="hr"></div>'.
				likes_get($row['stream_id']).comment_get($row['stream_id'],100).comment_form($row['stream_id'])
				);
		
		
	}


	function ajax_form($page_type='',$page_id=0) {
		echo images::ajax_form_data($page_type,$page_id);
		exit;
	}

	function ajax_form_data($page_type='',$page_id=0) {
		global $client;
		if (!$client) die('login');
		if (!$page_type) $page_type = 'u';
		$output = '
	<div class="form-group"><input type="text"   name="gallery_name" id="gallery_name" value="'.h($_POST['gallery_name']).'" class="form-control" placeholder="'.t('Title').' ('.t('Title').')" /></div>
	<table border="0">
	<tr><td><input type="file" class="filestyle" name="images[]" /></td>
	<td><input type="file" class="filestyle" name="images[]" /></td>
	<td><input type="file" class="filestyle" name="images[]" /></td>
	</tr></table>
		<div class="form-group"><textarea name="form_content" id="form_content" rows="1" class="form-control" placeholder="'.t('Description').'" >'.htmlspecialchars(stripslashes($_POST['form_content'])).'</textarea></div>

		';
		$output .= '<script>
		$(document).ready( function(){
			$("#gallery_name").focus();
			$(":file").filestyle({iconName: "glyphicon-picture",input:false});
		});
	</script>';
		return $output;
	}

	function ajax_post() {
		global $client;
		$page = story::check_page_access($_POST['page_id']);
		$error = '';
		if (!$_FILES['images']['tmp_name'][0]) images::ajax_error('No photo selected');

		if (strlen($_POST['gallery_name'])) {
			$gallery_name = $_POST['gallery_name'];
		}
		else {
			$gallery_name = '';
		}

		$story = array(
			'cid' => 0,
			'page_id' => $_POST['page_id'],
			'page_type'=>$page['type'],
			'title' => $gallery_name,
			'content' => $_POST['form_content'],
			'uid' => $client['id'],
			'created' => time(),
			'photos'=>0,
			'tags'=>$_POST['tags'],
			'app' => 'images'
			);
		if ($_POST['jcow_feature_story'] && allow_access(3)) {
			$story['featured'] = 1;
		}
		sql_insert($story, tb().'stories');
		$story['id'] = insert_id();
		// write act
		$attachment = array(
			'cwall_id' => 'images'.$story['id'],
			'uri' => 'images/viewstory/'.$story['id'],
			'name' => $_POST['album_name']
			);
		$args = array(
			'message'=>t('uploaded image'),
			'link' => 'images/viewstory/'.$story['id'],
			'name' =>  stripslashes($gallery_name),
			'app' => 'images',
			);
		$stream_id = jcow_page_feed($_POST['page_id'],$args);
		$set_story['stream_id'] = $story['stream_id'] = $stream_id;
	
		$sid = $album_id = $story['id'];
		if (!$_POST['privacy']) {
			parse_save_tags($_POST['tags'],$sid,'images');
		}
		$set_story['id'] = $story['id'];
		
		$photos = $story['photos'];
		foreach ($_FILES['images']['tmp_name'] as $key=>$file_tmp_name) {
			if ($file_tmp_name && $key<5) {
				$photo = array('name'=>$_FILES['images']['name'][$key],
				'tmp_name'=>$file_tmp_name,
				'type'=>$_FILES['images']['type'][$key],
				'size'=>$_FILES['images']['size'][$key]);

				list($width, $height) = getimagesize($file_tmp_name);
				if ($width <= 1200) {
					$uri = save_file($photo);
				}
				else {
					$height = floor(1200*$height/$width);
					$uri = save_thumbnail($photo, 1200, 0);
				}
				$photos++;
				$thumb = save_thumbnail($photo, 200, 500);
				$size = $photo['size'];
				sql_query("insert into `".tb()."story_photos` (sid,uri,des,thumb,size) values( {$sid},'$uri','','$thumb','$size')");
				
			}
		}
		$set_story['thumbnail'] = $thumb;
		$set_story['photos'] = $photos ;
		sql_update($set_story,tb()."stories");

		// update stream
		$res = sql_query("select thumb from ".tb()."story_photos where sid='{$story['id']}' order by id DESC limit 2");
		while ($photo = sql_fetch_array($res)) {
			$thumbs[] = $photo['thumb'];
			$pics = ' ('.$photos.')';
		}
		$attachment = array(
				'cwall_id' => 'images'.$story['id'],
				'uri' => 'images'.'/viewstory/'.$story['id'],
				'name' => addslashes($story['title']),
				'thumb' => $thumbs
				);
		$app = array('name'=>'images','id'=>$story['id']);
		$stream_id = stream_update(addslashes(t('Uploaded image').$pics),$attachment,$app, $story['stream_id']);
		$_POST['gallery_name'] = $_POST['form_content'] = '';
		$output = '<span style="background:yellow;color:black">'.t('Upload success!').'</span> <a href="'.url('images/viewstory/'.$story['id']).'"><strong>'.t('View').'</strong></a>';
		$output = images::ajax_form_data($_POST['page_type'],$_POST['page_id']);
		$stream = array('id'=>$story['stream_id'],'username'=>$client['username'],'avatar'=>$client['avatar'],'wall_id'=>$page['id'],'wall_uid'=>$page['uid'],'app'=>'images','uid'=>$client['id'],'created'=>time(),'aid'=>$story['id'],'type'=>$_POST['privacy']);
		echo json_encode(array('formdata'=>'reset','streamdata'=>stream_display($stream,'preview')));
		exit;
	}



	function upload_form($page_type='') {

		$output = '
		<script>
		$(document).ready( function(){
		'.$show_feature_box.'
	});
				function fresh_apps_box(freshurl) {
					$("#apps_box").html("");
					$("span#spanstatus").html("<img src=\"'.uhome().'/files/loading.gif\" /> Loading");
					$("#apps_box").load(freshurl, function() {
						$("span#spanstatus").html("");
					});
				}
				$("#add_another_photo").click(function() {
					$("#add_another_photo_box").before("<tr><td><input type=\"file\" name=\"images[]\" /></td><td><input type=\"text\" size=\"35\" name=\"descriptions[]\" /></td></tr>");
				});
				'.autocompletetagjs().'
	</script><table border="0">
	<tr>
		<td>'.t('Upload').$_REQUEST['gallery_name'].'</td>
		<td>'.t('Caption').'</td>
	</tr>
	<tr>
		<td><input type="file" name="images[]" capture="camera" accept="image/*" /></td>
		<td><input type="text" size="35" name="descriptions[]" /></td>
	</tr>
		<tr id="add_another_photo_box"><td colspan="2">'.t('or').' <a href="javascript:void();" onclick="javascript:fresh_apps_box(\''.url('images/ajax_create_gallery/'.$page_type.'').'\')">'.t('Upload multiple images').'</a> or <a href="javascript:void();" onclick="javascript:fresh_apps_box(\''.url('images/ajax_share_link/'.$page_type.'').'\')">'.t('Share image link').'</a></td></tr>';
		$output .= '</table>';
		if (module_actived('topics')  && $page_type != 'group') {
			$output .= '<div ><input type="text" id="tags" name="tags" style="width:50%"  placeholder="'.t('Tags').'" /></div>';
		}

		return $output;
	}

	function ajax_error($msg) {
		echo '<div style="color:red">'.$msg.'</div>';
		echo images::ajax_form();
		exit;
	}


}