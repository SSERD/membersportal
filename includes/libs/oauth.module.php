<?php
/* ############################################################ *\
 ----------------------------------------------------------------
Jcow Software (http://www.jcow.net)
IS NOT FREE SOFTWARE
http://www.jcow.net/commercial_license
Copyright (C) 2009 - 2010 jcow.net.  All Rights Reserved.
 ----------------------------------------------------------------
\* ############################################################ */
class oauth{
	function oauth() {
		global $client;
		if ($client['id']) {
			c('already logged in');
			stop_here();
		}
		if (get_gvar('signup_closed')==2) {
			c(t('Sorry, currently we are not accepting new members'));
			stop_here();
		}
		elseif (get_gvar('signup_closed') == 1) {
			c(t('Sorry, this community is invite-only'));
			stop_here();
		}
	}
	function index() {
		die('no act');
	}
	function google() {
		include_once ('sdks/google/autoload.php');
		global $client;
		$gclient_id = get_gvar('oauth_google_id');
		$gclient_secret = get_gvar('oauth_google_secret');
		$redirect_uri = url('oauth/google');
		$gclient = new Google_Client();
		$gclient->setClientId($gclient_id);
		$gclient->setClientSecret($gclient_secret);
		$gclient->setRedirectUri($redirect_uri);
		$gclient->addScope("email");
		$service = new Google_Service_Oauth2($gclient);
		if (isset($_GET['code']) && strlen($_GET['code']) > 20) {
			$gclient->authenticate($_GET['code']);
	  		$user = $service->userinfo->get();
			if (strlen($user->email) && $user->verifiedEmail) {
				email_login($user->email);
				exit;
			}
			else {
				c('Sorry, your email is not verified by Google');
				stop_here();
			}
		}
		$authUrl = $gclient->createAuthUrl();
		echo '<html>
		<head>
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
		<meta http-equiv="refresh" content="1;url='.$authUrl.'" />
		<title>Redirecting</title>
		</head>
		<body>
		<div style="text-align:center;padding-top:150px;font-size:30px">
		<i class="fa fa-lg fa-spinner fa-pulse"></i> Redirecting..
		</div>
		</body>
		</html>';
		exit;

			// ###############################
			if (strlen($_GET['code'])) {
				$url2 = 
				'https://graph.facebook.com/oauth/access_token?client_id='.$fb_id.'&redirect_uri='.$back_url.'&client_secret='.$fb_secret.'&code='.$_GET['code'];
				$fb_token = @file_get_contents($url2);
				if (!strlen($fb_token)) die('bad fb code');
				$me = @file_get_contents('https://graph.facebook.com/me?fields=name,email,verified,gender&'.$fb_token);
				$fbuser = json_decode($me,true);
				if (!$fbuser['id']) die('bad fb token');
				$_SESSION['fb_logged'] = 1;
				$_SESSION['fb_id'] = $fbuser['id'];
				$res = sql_query("select * from ".tb()."accounts where fbid='{$fbuser['id']}'");
				$user = sql_fetch_array($res);
				if ($user['id']) {//已绑定
					if ($client['id']) { //登录状态
						if ($user['id'] != $client['id']) {
							c(t('Your facebook ID has already bind to {1}','<strong>'.$user['email'].'</strong>'));
							stop_here();
						}
						elseif (!$fbuser['verified']) {
							c(t('Your facebook account is not verified. Please verify your facebook account first and turn to this page. Here is a guide to verify your facebook account: {1}','<a href="http://www.facebook.com/help/verify" target="_blank">www.facebook.com/help/verify</a>'));
							stop_here();
						}
					}
					$_SESSION['uid'] = $user['id'];
					if ($user['disabled'] == 1) {
						if (get_gvar('acc_verify') == 1) {
							$user['disabled'] = 0;
						}
						elseif (get_gvar('acc_verify') == 2) {
							if ($fbuser['verified']) {
								$user['disabled'] = 0;
							}
						}
						sql_query("update ".tb()."accounts set disabled={$user['disabled']} where id='{$user['id']}'");
					}
					redirect(url('feed','','',array('bdid'=>$user['id'])));
				}
				else {//未绑定
					if (!strlen($fbuser['email'])) {
						die('Sorry, we can not get your facebook Email');
					}
					$res = sql_query("select * from ".tb()."accounts where email='{$fbuser['email']}'");
					$user = sql_fetch_array($res);
					if ($user['id'] && !$user['fbid']) {
						$got_email_match = 1;
					}
					if ($client['id']) { //已登录用户，自动绑定
						sql_query("update ".tb()."accounts set fbid='{$fbuser['id']}' where id='{$client['id']}'");
						if ($client['disabled'] == 1) {// verify pending user
							if (get_gvar('acc_verify') == 1) {
								$uv = 'yes';
							}
							elseif (get_gvar('acc_verify') == 2) {
								if ($fbuser['verified']) {
									$uv = 'yes';
								}
							}
							if ($uv == 'yes') {
								sql_query("update ".tb()."accounts set disabled=0 where id='{$client['id']}'");
							}
						}
						redirect('feed');
					}
					elseif($got_email_match) { //email match,自动绑定
						sql_query("update ".tb()."accounts set fbid='{$fbuser['id']}' where id='{$user['id']}'");
						if ($user['disabled'] == 1) {// verify pending user
							if (get_gvar('acc_verify') == 1) {
								$uv = 'yes';
							}
							elseif (get_gvar('acc_verify') == 2) {
								if ($fbuser['verified']) {
									$uv = 'yes';
								}
							}
							if ($uv == 'yes') {
								sql_query("update ".tb()."accounts set disabled=0 where id='{$user['id']}'");
							}
						}
						$_SESSION['uid'] = $user['id'];
						redirect(url('feed'));
					}
					else {
						$_SESSION['fb_verified'] = $fbuser['verified'];
						$_SESSION['fb_email'] = $fbuser['email'];
						$_SESSION['fb_token'] = $fb_token;
						$_SESSION['fb_fullname'] = trim($fbuser['name']);
						if ($fbuser['gender'] == 'male') $_SESSION['fb_gender'] = 1;else $_SESSION['fb_gender'] = 0;
						//pic
						$big_pic = 'https://graph.facebook.com/me/picture?type=normal&'.$_SESSION['fb_token'];
						$small_pic = 'https://graph.facebook.com/me/picture?type=square&'.$_SESSION['fb_token'];
						$dir = date("Ym",time());
						$folder = uploads.'/avatars/'.$dir;
						if (!is_dir($folder))
							mkdir($folder, 0777);
						$s_folder = uploads.'/avatars/s_'.$dir;
						if (!is_dir($s_folder))
							mkdir($s_folder, 0777);
						$avatar_hash = get_rand(7);
						if ($content = @file_get_contents($big_pic)) {
							@file_put_contents($folder.'/'.$avatar_hash.'.jpg',$content);
							$_SESSION['fb_avatar'] = $dir.'/'.$avatar_hash.'.jpg';
						}
						if ($content = @file_get_contents($small_pic)) {
							@file_put_contents($s_folder.'/'.$avatar_hash.'.jpg',$content);
						}
						redirect('member/signup');
					}
				}
			}
		}
	
}

function email_login($email='') {
	$res = sql_query("select * from ".tb()."accounts where email='".safe($email)."'");
	$user = sql_fetch_array($res);
	if ($user['id']) {
		$_SESSION['uid'] = $user['id'];
		redirect(url('feed'));
	}
	else {
		$_SESSION['fb_email'] = $email;
		redirect('member/signup');
	}
}



