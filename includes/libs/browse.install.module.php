<?php
/* ############################################################ *\
 ----------------------------------------------------------------
Jcow Software (http://www.jcow.net)
IS NOT FREE SOFTWARE
http://www.jcow.net/commercial_license
Copyright (C) 2009 - 2010 jcow.net.  All Rights Reserved.
 ----------------------------------------------------------------
\* ############################################################ */

function browse_menu() {
	$items = array();
	$items['browse'] = array(
		'tab_name'=>'Browse',
		'name'=>'Members',
		'type'=>'crossplatform',
		'protected'=>1
	);
	return $items;
}

?>