<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">	

<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta name="Generator" content="Powered by Jcow" />
<meta name="viewport" content="width=device-width, initial-scale=1">

<?php echo $auto_redirect;
if (!$icon = get_gvar('custom_icon')) {
	$icon = $uhome.'/themes/'.get_gvar('theme_tpl').'/ico.gif';
}
if (!$logo = get_gvar('custom_logo')) {
	$logo = uhome().'/themes/'.get_gvar('theme_tpl').'/logo.png';
}

?>
<link rel="shortcut icon" href="<?php echo $icon ?>" type="image/x-icon" />
<?php echo $tpl_vars['javascripts'];?>
<style type="text/css" media="all">@import "<?php echo $uhome;?>/themes/<?php echo get_gvar('theme_tpl')?>/page.css";</style>
<style type="text/css" media="all">@import "<?php echo $uhome;?>/files/common_css/style.css";</style>
<title><?php echo $title?> - <?php echo get_gvar('site_name')?></title>
<?php echo $tpl_vars['custom_profile_css']?>
<?php
if (strlen(get_gvar('site_keywords'))) {
	echo '<meta name="keywords" content="'.get_gvar('site_keywords').'" />';
}
?>
<?php echo $header?>

</head>

<body>
<?php
if ($jcow_header = get_gvar('jcow_banner')) {
	echo '
<div id="jcow_header_box">
<div id="jcow_header">
'.$jcow_header.'
</div>
</div>';
}
?>

<div id="jcow_primary_menubar_box">
<div id="jcow_primary_menubar">
<div id="jcow_top_left">
<span id="jcow_primary_logo"><a href="<?php echo my_jcow_home();?>"><img src="<?php echo $logo;?>" style="max-height:100px;width:auto" /></a></span>
<span id="jcow_sec_menu"><a href="#" id="show_jcow_app_menu"><i class="fa fa-bars"></i></a></span>
</div>

<div id="jcow_primary_menu">
<div style="position:relative;width:400px">
<form action="<?php echo url('search/listing');?>" method="post">
<input id="search_box" name="title" placeholder="<?php echo t('Search');?>" />
<button id="search_btn"><i class="fa fa-search"></i></button>
</form>
</div>
</div>



<div id="jcow_user_panel">
<?php
if (!$client['id']) {
	echo '
	'.url('member/login',t('Login')).' | '.url('member/signup',t('Sign up')).'
	';
}
else {
	echo '
	<a href="'.url('u/'.$client['uname']).'">'.avatar($client,25,array('nolink'=>1)).' '.h($client['fullname']).'</a>
	<a href="'.url('friends').'" class="top_icon"><i class="fa fa-users"></i>'.frd_unread().'</a>
	<a href="#" class="up_msg top_icon"><i class="fa fa-lg fa-comments"></i>'.msg_unread().'</a> 
	<a href="'.url('notifications').'" class="top_icon"><i class="fa fa-lg fa-globe"></i>'.note_unread().'</a>
	<span class="dropdown ">
	<a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" ><i class="fa fa-chevron-down"></i></a> 
  <ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
  <li><a href="'.url('u/'.$client['uname']).'">'.t('View Profile').'</a></li>
    <li><a href="'.url('account').'">'.t('Edit Profile').'</a></li>
    <li>'.url('member/logout',t('Logout') ).'</li>
  </ul>
</span>
	';
}
?>
</div>
</div>
</div>





<!-- #################### structure ################## -->

<div id="wallpaper" style="width:100%;height:100%;overflow:hidden">
<div id="jcow_main_box">
<div id="jcow_app_menu" style="display:none">
<?php
if ($client['id']) {
	echo '<div id="jcow_app_links">
	<a href="'.url('u/'.$client['username']).'">'.avatar($client,25,array('nolink'=>1)).' '.($client['fullname']).'</a>
	<a href="'.url('account').'"><i class="fa fa-pencil"></i> '.t('Edit profile').'</a>
	<div><a href="'.url('feed').'" '.check_menu_on('feed').'><img src="modules/feed/icon.png" /> '.t('News feed').'</a></div>
	<div><a href="'.url('message').'" '.check_menu_on('message').'><img src="modules/message/icon.png" /> '.t('Messages').'</a></div>
	<div class="jcow_app_menu_title">'.t('Explore').'</div>';
	foreach ($new_apps as $item) {
		if ($item['path'] != 'feed' && $item['type'] != 'personal' && $item['path'] != 'message/inbox') {
			$skey = $item['path'];
			$menuon_class = check_menu_on($item['path']);
			if (!in_array($item['path'], array('account','groups','pages'))) {
				echo '<div>
				<a href="'.url($item['path']).'" '.$menuon_class.'><img src="modules/'.$item['app'].'/icon.png" /> '.t($item['name']).'</a>
				</div>';
			}
		}
	}
	if (module_actived('groups')) {
		echo '<div class="jcow_app_menu_title">'.t('Groups').'</div>';
		if ($client['id']) {
			$res = sql_query("select p.* from ".tb()."page_users as u left join ".tb()."pages as p on p.id=u.pid where u.uid='{$client['id']}' and p.type='group' order by p.updated DESC limit 6");
			while ($group = sql_fetch_array($res) ) {
				if ($group['logo']) {
					$group['logo'] = '<img src="'.uploads.'/avatars/s_'.$group['logo'].'" width="25" height="25" />';
				}
				else {
					$group['logo'] = '<img src="modules/groups/icon.png" />';
				}
				echo '<a href="'.url('group/'.$group['uri']).'">'.$group['logo'].' '.h($group['name']).'</a>';
			}
		}
		echo '<a href="'.url('groups').'" ><i class="fa fa-search-plus"></i> '.t('Browse groups').'</a>';
		if ($client['id']) {
			echo '<a href="'.url('groups/create').'" ><i class="fa fa-plus"></i> '.t('Create group').'</a>';
		}
	}
	if (module_actived('pages')) {
		echo '<div class="jcow_app_menu_title">'.t('Pages').'</div>';
		if ($client['id']) {
			$res = sql_query("select * from ".tb()."pages where type='page' and uid='{$client['id']}' order by updated DESC limit 6");
			while ($page = sql_fetch_array($res) ) {
				if ($page['logo']) {
					$page['logo'] = '<img src="'.uploads.'/avatars/s_'.$page['logo'].'" width="25" height="25" />';
				}
				else {
					$page['logo'] = '<img src="modules/pages/icon.png" />';
				}
				echo '<a href="'.url('page/'.$page['uri']).'">'.$page['logo'].' '.h($page['name']).'</a>';
			}
		}
		echo '<a href="'.url('pages').'" ><i class="fa fa-search-plus"></i> '.t('Browse pages').'</a>';
		if ($client['id']) { 
			echo '<a href="'.url('pages/create').'" ><i class="fa fa-plus"></i> '.t('Create page').'</a>';
		}
	}
	echo '</div>';
}
else {
	echo '
	<div style="padding:30px 10px"><form method="post" name="form1" class="ajaxform" action="'.url('member/loginpost').'" >
								<div class="form-group">
								<input type="text" size="10" name="username" autocorrect="off" autocapitalize="off" class="form-control input-sm" value="'.h($_POST['username']).'"  placeholder="'.t('Username or Email').'"/><div class="sub">'.url('member/signup',t('Register an account')).'</div>
								</div>
								<div class="form-group">
								<input type="password" size="10" class="form-control input-sm" name="password" style="min-width:120px"  value="'.h($_POST['password']).'" placeholder="'.t('Password').'" />
								<div class="sub">'.url('member/chpass',t('Forgot password?')).'</div>
								</div>
								<div class="checkbox">
								<label><input type="checkbox" name="remember_me" id="remember_me" value="1" '.$remember_check.' /> '.t('Remember me').'</label>
								</div>
								<div class="form-group">
								<button type="submit" class="btn btn-default">'.t('Login').'</button>
								'.$bakg.'
								</div>
								</p>
								</form>

								
								<script language="javascript" >
					document.form1.username.focus();
					</script>
			<div style="height:2px;margin:10px 0;background:#ccc"></div>
			<div style="padding:5px 0">
			<a href="'.url('member/signup').'" class="btn btn-sm btn-default" style="width:100%"><i class="fa fa-user-plus"></i> '.t('Create an account').'</a>
			</div>';
			if (get_gvar('fb_id')) {
				echo '<div style="padding:5px 0"><a href="'.url('fblogin').'" style="width:100%" class="btn btn-sm btn-primary"><i class="fa fa-facebook"></i> '.t('Connect with Facebook').'</a></div>';
			}
			if (get_gvar('oauth_google_id') && get_gvar('oauth_google_secret')) {
				echo '<div style="padding:5px 0"><a href="'.url('oauth/google').'" class="btn btn-sm btn-danger" style="width:100%"><i class="fa fa-google fa-lg"></i> '.t('Connect with Google').'</a></div>';
			}
	echo '</div>';
	}
?>
</div>
<div id="jcow_main">
<div id="topbar_box">
<div id="jcow_topbar">
<?php
echo new_tab_menu();
if (is_array($buttons)) {
	foreach ($buttons as $button) {
		echo ' '.$button;
	}
}
?>
</div>
</div>

<?php

if (count($nav) > 2) {
	echo '<div id="nav">'.gen_nav($nav).'</div>';
}
if (is_array($notices)) {
	foreach ($notices as $notice) {
		echo '<div class="notice">'.$notice.'</div>';
	}
}
if ($top_title) {
	echo '<div style="padding:0 0 10px 30px;background:url('.uhome().'/modules/'.$application.'/icon.png) 9px 5px no-repeat;font-size:1.5em">'.$top_title.'</div>';
}

echo $app_header;

if ($application != 'home') {
	display_application_content();
}
else {
	include 'themes/'.get_gvar('theme_tpl').'/home.tpl.php';
}
?>


</div><!-- end jcow_application -->
</div><!-- end jcow_application_box -->
</div><!-- end wallpaper -->

<div id="footer">
<div>
<?php
// footer pages
$footer_pages = get_footer_pages();
if (count($footer_pages)) {
	echo '<div id="footer_pages">'.implode(' | ',$footer_pages).'</div>';
}
?>

<?php echo $tpl_vars['language_selection']?>
</div>
<?php echo $tpl_vars['footer']?>
<br /><br />

<!-- YOU ARE NOT ALLOWED TO REMOVE jcow_attribution() WITHOUT A BRANDING REMOVAL LICENSE -->
<?php echo jcow_attribution();?>

</div>

</body>
</html>