<?php
/* ############################################################ *\
 ----------------------------------------------------------------
Jcow Software (http://www.jcow.net)
IS NOT FREE SOFTWARE
http://www.jcow.net/commercial_license
Copyright (C) 2009 - 2010 jcow.net.  All Rights Reserved.
 ----------------------------------------------------------------
\* ############################################################ */

if (basename($_SERVER["SCRIPT_NAME"]) != 'index.php') die(basename($_SERVER["SCRIPT_NAME"]));
global $page;
$num_per_page = 20;
$offset = ($page-1)*$num_per_page;


c(topics_autocomplete_js().'<script>
		$(document).ready( function(){
			var topicbox;
			$(".tsubmit").click(function() {
				var cbox = $(this).next().next();
				var mbox = $(this).prev();
				var tbox = $(this).prev().prev();
				$(this).hide();
				cbox.html("<img src=\"'.uhome().'/files/loading.gif\" /> Submitting");
				$.post("'.uhome().'/index.php?p=admin/stories/approve",
				{topics:mbox[0].value,sid:tbox[0].value},
				  function(data){
					cbox.html(data);
					},"html"
				);
				return false;
			});
			$(".admin_unapprove").click(function() {
				var cbox = $(this).parent();
				var tbox = $(this).next();
				$(this).hide();
				cbox.html("<img src=\"'.uhome().'/files/loading.gif\" /> Submitting");
				$.post("'.uhome().'/index.php?p=admin/stories/unapprove",
				{sid:tbox[0].value},
				  function(data){
					cbox.html(data);
					},"html"
				);
				return false;
			});
		});
		</script>
		');
if ($step == 'approve') {
	$res = sql_query("select * from ".tb()."stories where id='{$_POST['sid']}'");
	$story = sql_fetch_array($res);
	if ($story['id']) {
		$topics = parse_save_topics($_POST['topics'],$story['id'],$story['app']);
		sql_query("update ".tb()."stories set tags='$topics',featured=1 where id='{$_POST['sid']}'");
		echo '<font color="green">featured</font>';
	}
	else {
		echo 'story not found';
	}
	exit;
}
elseif ($step == 'unapprove') {
	$res = sql_query("select * from ".tb()."stories where id='{$_POST['sid']}'");
	$story = sql_fetch_array($res);
	if ($story['id']) {
		$topics = parse_save_topics('',$story['id'],$story['app']);
		sql_query("update ".tb()."stories set featured=0 where id='{$_POST['sid']}'");
		echo '<font color="red">Un-featured</font>';
	}
	else {
		echo 'story not found';
	}
	exit;
}
elseif ($step == 'autoapprovalpost') {
	$ap_arr = array();
	if (is_array($_POST['aproles'])) {
		foreach($_POST['aproles'] as $roleid) {
			$ap_arr[] = $roleid;
		}
	}
	set_gvar('auto_approve_roles',implode('|',$ap_arr));
	redirect('admin/stories/autoapproval',1);
}

c('
<table class="stories">');
$nppm = $num_per_page+1;
$res = sql_query("select s.*,u.username from ".tb()."stories as s left join ".tb()."accounts as u on u.id=s.uid where s.page_type='u' order by id DESC limit $offset,$nppm");
$i = 0;
while ($story = sql_fetch_array($res)) {
	$i++;
	if ($i>$num_per_page) {
		$got_next = 1;
	}
	else {
		$res2 = sql_query("select t.name from ".tb()."topic_ids as i left join ".tb()."topics as t on t.id=i.tid where i.sid='{$story['id']}'");
		$topics = array();
		while ($row = sql_fetch_array($res2)) {
			$topics[] = h($row['name']);
		}
		c('<tr class="row1"><td>'.$story['app'].'/ '.url($story['app'].'/viewstory/'.$story['id'],h($story['title'])).' <span class="sub">'.get_date($story['created']).' by '.url('u/'.$story['username'],h($story['username'])).'</span>');
		if ($story['featured']) {
			c('<br /><span class="sub">Topics:</span> ');
			if (count($topics)) {
				c('<i>'.implode(',',$topics).'</i>');
			}
			else {
				c('none');
			}
			c('
			 <span class="sub">Status:</span> <span class="tstatus">
			<font color="green">featured</font>[<a href="#" class="admin_unapprove">Un-feature</a><input type="hidden" value="'.$story['id'].'" />]
			</span>');
		}
		else {
			if (in_array($story['app'],array('blogs','images','videos','discussions','questions'))) {
				$story['set_topics'] = '<span class="sub">Tags:</span><input type="hidden" name="sid" value="'.$story['id'].'" /><input type="text" class="topicbox" name="topics" size="30" value="'.h($story['tags']).'" />';
			}
			else {
				$story['set_topics'] = '<span class="sub"></span><input type="hidden" name="sid" value="'.$story['id'].'" /><input type="hidden" class="topicbox" name="topics" value="" />';
			}
			c('
			<form class="translate_form">
			'.$story['set_topics'].'
			 <input class="tsubmit" type="submit" value="Feature this story" /> 
			</form>');
		}
		c('<div style="background:white;font-size:11px;color:#666666;padding:3px;margin-bottom:20px;">'.h(utf8_substr($story['content'],360)).'</div></td></tr>');
	}
	
}
c('</table>');
if ($got_next) {
		c('<div style="padding:5px;font-size:15px">
		'.url('admin/stories','More..','',array('page'=>$page+1)).'</div>');
	}