<?php
/* ############################################################ *\
 ----------------------------------------------------------------
Jcow Software (http://www.jcow.net)
IS NOT FREE SOFTWARE
http://www.jcow.net/commercial_license
Copyright (C) 2009 - 2010 jcow.net.  All Rights Reserved.
 ----------------------------------------------------------------
\* ############################################################ */
if ($step == 'clear') {
	if (!$id) {
		$res = sql_query("select count(*) as num from ".tb()."pending_review where ignored=1");
		$row = sql_fetch_array($res);
		if ($row['num'] > 0) {
			c('
			<p>You have <strong>'.$row['num'].'</strong> ignored posts.</p>
			<p>
			If you "clear the ignored posts", all the ignored stories&comments will be deleted from your database.<br />
			Are you sure to clear ignored posts? '.url('admin/pending_posts/clear/1','<strong>Yes</strong>').' | '.url('admin/pending_posts','<strong>No</strong>').'
			</p>');
		}
		else {
			c('Currently no ignored posts');
		}
	}
	else {
		//delete streams
		$res = sql_query("select * from ".tb()."pending_review where ignored=1");
		$i = 0;
		while ($row = sql_fetch_array($res)) {
			$i++;
			$str = preg_replace("/[0-9]/",'',$row['post_id']);
			$sid = preg_replace("/[^0-9]/",'',$row['post_id']);
			if ($row['stream_id']>0) {
				sql_query("delete from ".tb()."streams where id='$sid'");
			}
			if ($str == 'comment') {
				sql_query("delete from ".tb()."story_comments where id='$sid'");
			}
			elseif ($str == 'story') {
				sql_query("delete from ".tb()."stories where id='$sid'");
				$res2 = sql_query("select * from ".tb()."story_photos where sid='$sid'");
				while($photo = sql_fetch_array($res2)) {
					@unlink($photo['uri']);
					sql_query("delete from ".tb()."story_photos where id='{$photo['id']}'");
				}
			}
			elseif ($str == 'thread') {
				sql_query("delete from ".tb()."forum_threads where id='$sid'");
				sql_query("delete from ".tb()."forum_posts where tid='$sid'");
			}
			elseif ($str == 'forumpost') {
				sql_query("delete from ".tb()."forum_posts where id='$sid'");
			}
			sql_query("delete from ".tb()."pending_review where id='{$row['id']}'");
			sql_query("delete from ".tb()."topic_ids where sid='$sid'");
		}
		c('<strong>'.$i.'</strong> posts deleted!<br />Cleared!');
	}
}

elseif ($step == 'post') {
	set_gvar('pending_review_actived',$_POST['pending_review_actived']);
	set_gvar('pending_review_limit',$_POST['pending_review_limit']);
	redirect('admin/pending_posts',1);
}
elseif ($step == 'approve') {
	if (is_array($_POST['ids'])) {
		foreach ($_POST['ids'] as $id) {
			if ($_POST['opt'] == 1) {
				$res = sql_query("select * from ".tb()."pending_review where id='$id'");
				$post = sql_fetch_array($res);
				$str = preg_replace("/[0-9]/",'',$post['post_id']);
				$sid = preg_replace("/[^0-9]/",'',$post['post_id']);
				if (strlen($str) && $sid>0) {
					$func = $str.'_approved';
					if (function_exists($func)) {
						$func($sid);
					}
				}
				if ($str == 'comment') {
					$res2 = sql_query("select c.sid,u.username from ".tb()."story_comments as c left join ".tb()."accounts as u on u.id=c.uid where c.id='{$sid}'");
					$row2 = sql_fetch_array($res2);
					sql_query("update `".tb()."stories` set comments=comments+1,lastreply=".$post['created'].",lastreplyuname='{$row2['username']}',lastreplyuid='{$post['uid']}' where id='".$row2['sid']."'");
				}
				elseif ($str == 'thread') {
					$res2 = sql_query("select * from ".tb()."forum_threads where id='$sid'");
					$thread = sql_fetch_array($res2);
					if ($thread['id']) {
						sql_query("update ".tb()."forums set posts=posts+1,threads=threads+1,lastpostname='{$thread['username']}',lastpostcreated='{$post['created']}',lastposttopic='".sql_string($thread['topic'])."',lastposttopicid='{$thread['id']}' where id='{$thread['fid']}'");
					}
				}
				elseif ($str == 'forumpost') {
					$res2 = sql_query("select p.*,u.username from ".tb()."forum_posts as p left join ".tb()."accounts as u on u.id=p.uid where p.id='$sid'");
					$forumpost = sql_fetch_array($res2);
					$res3 = sql_query("select * from ".tb()."forum_threads where id='{$forumpost['tid']}'");
					$thread = sql_fetch_array($res3);
					if ($thread['id']) {
						sql_query("update ".tb()."forum_threads set posts=posts+1,lastpostusername='{$forumpost['username']}',lastpostcreated='{$post['created']}' where id='{$thread['id']}'");
						sql_query("update ".tb()."forums set posts=posts+1,threads=threads+1,lastpostname='{$thread['username']}',lastpostcreated='{$post['created']}',lastposttopic='".sql_string($thread['topic'])."',lastposttopicid='{$thread['id']}' where id='{$thread['fid']}'");
					}
				}
				$return = parse_mentions($post['content'],$post['uid']);
				if ($post['stream_id']>0) {
					add_mentions($return['mentions'],$post['stream_id']);
					}
				sql_query("delete from ".tb()."pending_review where id='$id'");
				sql_query("update ".tb()."accounts set forum_posts=forum_posts+1 where id='{$post['uid']}'");
			}
			elseif ($_POST['opt'] == 2) {
				sql_query("update ".tb()."pending_review set ignored=1 where id='$id'");
			}
			elseif ($_POST['opt'] == 3) {
				$res = sql_query("select * from ".tb()."pending_review where id='$id'");
				$post = sql_fetch_array($res);
				sql_query("update ".tb()."accounts set disabled=3 where id='{$post['uid']}'");
				sql_query("update ".tb()."pending_review set ignored=1 where uid='{$post['uid']}'");
			}
		}
	}
	redirect('admin/pending_posts',1);

}

else {
	$num_per_page = 20;
	global $parr;
	if (!$parr[2]) $page=1;
	else $page = $parr[2];
	$offset = ($page-1)*$num_per_page;
	$more = $num_per_page+1;
	c('
	<script>
	$(document).ready( function(){
		$("#checkallids").click(function() {
			$(".checkids").attr("checked",true);
		});
	});
	</script>
	<form method="post" action="'.url('admin/pending_posts/approve').'">
	Pending posts from Newcomers:
<table class="stories">
	
	<tr class="table_line1">
	<td width="5"></td><td>Posts</td><td>Info</td></tr>');
	$res = sql_query("select p.*,u.username from ".tb()."pending_review as p left join ".tb()."accounts as u on u.id=p.uid where p.ignored!=1 order by id DESC limit $offset,$more");
	$i=1;
	while ($row = sql_fetch_array($res)) {
		if ($i < $more) {
			c('<tr class="row1">
			<td><input type="checkbox" name="ids[]" class="checkids" value="'.$row['id'].'" /></td>
			<td>
			<div style="font-size:0.8em;max-height:70px;overflow-y:auto;">
			'.h($row['content']).'
			</div>');
			if (strlen($row['uri'])) {
				c('<br />
				URL:<a href="'.url($row['uri']).'">'.url($row['uri']));
			}
			c('</a></td><td>
			'.get_date($row['created']).'<br />
			By '.url('u/'.$row['username'],$row['username']).'<br />
			'.url('admin/useredit/'.$row['id'],'Manage this user').'<br /></td></tr>');
		}
		$i++;
	}
	c('<tr class="row2">
	<td colspan="3">
	<input type="checkbox" id="checkallids" />Select All <br /> 
	What to do?
	<label for="opt_1"><input id="opt_1" type="radio" value=1 name="opt" checked />Approve</label> 
	<label for="opt_2"><input id="opt_2" type="radio" value=2 name="opt" />Ignore</label> 
	<label for="opt_3"><input id="opt_3" type="radio" value=3 name="opt" />Ignore & Ban user</label> 
	<input type="hidden" name="act" value="havestreams" />
	<input type="submit" value=" Submit" />
	</td></tr>
	</table>
	</form>
	');
	if ($i>$num_per_page) {
		$page = $page+1;
		c('<div style="font-size:15px;padding:5px;">'.url('admin/pending_posts/'.$page,'More&gt;').'</div>');
	}
	c('<div style="font-size:15px;padding:5px;">'.url('admin/pending_posts/clear','*** Clear ignored posts ***').'</div>');
	section_close();
}